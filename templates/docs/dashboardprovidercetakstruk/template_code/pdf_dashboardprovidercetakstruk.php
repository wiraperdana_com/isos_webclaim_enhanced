<html>
	<head>
		<style>
			*{
				font-family: "Times New Roman", Times, serif;
			}
			body{
				margin:20px;
				padding:0px;
			}
			.header{
				text-align:center;
				height:30%;
			}
			.footer{
				color:#999999;
				text-align:right;
				height:7%;
			}
			.content{
				height:60%;
			}
			table tr{
				line-height:30px;
			}
		</style>
	</head>
	<body>
		<div class="header">
			<div>
				<?php
					$logo = $this->my_uconfig->get('logo');
					$logo = BASEPATH . '/../' . $logo['src'];
					$lgty = pathinfo($logo, PATHINFO_EXTENSION);
					$bs64 = file_get_contents($logo);
					$used = 'data:image/' . $lgty . ';base64,' . base64_encode($bs64);

				?>
				<img src="<?php echo $used; ?>" style="width:80px;height:80px;margin-top:1%;">
		<?php
			if( isset( $data_cetak ) ) {
				foreach( $data_cetak as $k => $v ) {
					if ($v->servicetype == 'Rawat Inap') {
						echo '
						<h1><u>'.strtoupper($v->servicetype).'</u></h1>
							</div>
							<div class="content">
							<table style="width:100%;">
								<tr><td style="width:30%;">Tanggal</td><td style="width:5%;">:</td><td style="width:65%;">'.mdate('%d-%M-%Y', strtotime($v->date_claim)).'</td></tr>
								<tr><td style="width:30%;">Jam</td><td style="width:5%;">:</td><td style="width:65%;">'.$time.'</td></tr>
								<tr><td style="width:30%;">Pershn</td><td style="width:5%;">:</td><td style="width:65%;">'.$v->client_name.'</td></tr>
								<tr><td></td><td></td><td>&nbsp;</td></tr>
								<tr><td></td><td></td><td>&nbsp;</td></tr>
								<tr style="font:bold;"><td>Nama</td><td>:</td><td>'.$v->member_name.'</td></tr>
								<tr style="font:bold;"><td>No. Kartu</td><td>:</td><td>'.$v->member_cardNo.'</td></tr>
								<tr style="font:bold;"><td>Tanggal Lahir</td><td>:</td><td>'.mdate('%d-%M-%Y', strtotime($v->tanggal_lahir)).'</td></tr>
								<tr style="font:bold;"><td>Jenis Kelamin</td><td>:</td><td>'.$v->jenis_kelamin.'</td></tr>
								<tr style="font:bold;"><td>NIK</td><td>:</td><td>'.$v->employee_ID.'</td></tr>
								<tr style="font:bold;"><td>Hak Kelas Perawatan</td><td>:</td><td>'.$v->room_class.'</td></tr>
							</table>
							<div style="text-align:left;margin-top:20px;">
								<br/><br/><br/>
								<p>'.nl2br($v->benefit, true).'</p>
							</div>';
					} elseif ($v->servicetype == 'Rawat Jalan') {
							echo '
							<h1><u>'.strtoupper($v->servicetype).'</u></h1>
							</div>
							<div class="content">
							<table style="width:100%;">
								<tr><td style="width:30%;">Date</td><td style="width:5%;">:</td><td style="width:65%;">'.mdate('%d-%M-%Y', strtotime($v->date_claim)).'</td></tr>
								<tr><td style="width:30%;">Jam</td><td style="width:5%;">:</td><td style="width:65%;">'.$time.'</td></tr>
								<tr><td style="width:30%;">Pershn</td><td style="width:5%;">:</td><td style="width:65%;">'.$v->client_name.'</td></tr>
								<tr><td></td><td></td><td>&nbsp;</td></tr>
								<tr><td></td><td></td><td>&nbsp;</td></tr>
								<tr style="font:bold;"><td>Nama</td><td>:</td><td>'.$v->member_name.'</td></tr>
								<tr style="font:bold;"><td>No. Kartu</td><td>:</td><td>'.$v->member_cardNo.'</td></tr>
								<tr style="font:bold;"><td>Tanggal Lahir</td><td>:</td><td>'.mdate('%d-%M-%Y', strtotime($v->tanggal_lahir)).'</td></tr>
								<tr style="font:bold;"><td>Jenis Kelamin</td><td>:</td><td>'.$v->jenis_kelamin.'</td></tr>
								<tr style="font:bold;"><td>NIK</td><td>:</td><td>'.$v->employee_ID.'</td></tr>
							</table>
							<div style="text-align:left;margin-top:20px;">
								<br/><br/><br/>
								<p>'.nl2br($v->benefit, true).'</p>
							</div>
							';
					} elseif ($v->servicetype == 'Rawat Gigi') {
						echo '
							<h1><u>RAWAT GIGI</u></h1>
							</div>
							<div class="content">
							<table style="width:100%;">
								<tr><td style="width:30%;">Date</td><td style="width:5%;">:</td><td style="width:65%;">'.mdate('%d-%M-%Y', strtotime($v->date_claim)).'</td></tr>
								<tr><td style="width:30%;">Jam</td><td style="width:5%;">:</td><td style="width:65%;">'.$time.'</td></tr>
								<tr><td style="width:30%;">Pershn</td><td style="width:5%;">:</td><td style="width:65%;">'.$v->client_name.'</td></tr>
								<tr><td></td><td></td><td>&nbsp;</td></tr>
								<tr><td></td><td></td><td>&nbsp;</td></tr>
								<tr style="font:bold;"><td>Nama</td><td>:</td><td>'.$v->member_name.'</td></tr>
								<tr style="font:bold;"><td>No. Kartu</td><td>:</td><td>'.$v->member_cardNo.'</td></tr>
								<tr style="font:bold;"><td>Tanggal Lahir</td><td>:</td><td>'.mdate('%d-%M-%Y', strtotime($v->tanggal_lahir)).'</td></tr>
								<tr style="font:bold;"><td>Jenis Kelamin</td><td>:</td><td>'.$v->jenis_kelamin.'</td></tr>
								<tr style="font:bold;"><td>NIK</td><td>:</td><td>'.$v->employee_ID.'</td></tr>';
								if ($v->dental_limit == '750000.00') {
									echo '<tr style="font:bold;"><td>Manfaat Gigi**</td><td>:</td><td>Rp. '.number_format($v->dental_limit,2,',','.').'/kunjungan diluar biaya obat, LAB & Radiology</td></tr>';
								} elseif ($v->dental_limit == '850000.00') {
									echo '<tr style="font:bold;"><td>Manfaat Gigi**</td><td>:</td><td>Rp. '.number_format($v->dental_limit,2,',','.').'/kunjungan diluar biaya obat, LAB & Radiology</td></tr>';
								} else {
									echo '<tr style="font:bold;"><td>Manfaat Gigi**</td><td>:</td><td>Rp. '.number_format($v->dental_limit,2,',','.').'/kunjungan diluar biaya obat, LAB & Radiology</td></tr>';
								}
						echo '	
							</table>
							<div style="text-align:left;margin-top:20px;">
								<br/><br/><br/>
								<p>'.nl2br($v->benefit, true).'</p>
							</div>
							';
					} else {
						echo '
							<h1><u>'.strtoupper($v->servicetype).'</u></h1>
							</div>
							<div class="content">
							<table style="width:100%;">
								<tr><td style="width:30%;">Date</td><td style="width:5%;">:</td><td style="width:65%;">'.mdate('%d-%M-%Y', strtotime($v->date_claim)).'</td></tr>
								<tr><td style="width:30%;">Jam</td><td style="width:5%;">:</td><td style="width:65%;">'.$time.'</td></tr>
								<tr><td style="width:30%;">Client</td><td style="width:5%;">:</td><td style="width:65%;">'.$v->client_name.'</td></tr>
								<tr><td></td><td></td><td>&nbsp;</td></tr>
								<tr><td></td><td></td><td>&nbsp;</td></tr>
								<tr style="font:bold;"><td>Nama</td><td>:</td><td>'.$v->member_name.'</td></tr>
								<tr style="font:bold;"><td>No. Kartu</td><td>:</td><td>'.$v->member_cardNo.'</td></tr>
								<tr style="font:bold;"><td>Tanggal Lahir</td><td>:</td><td>'.mdate('%d-%M-%Y', strtotime($v->tanggal_lahir)).'</td></tr>
								<tr style="font:bold;"><td>Jenis Kelamin</td><td>:</td><td>'.$v->jenis_kelamin.'</td></tr>
								<tr style="font:bold;"><td>NIK</td><td>:</td><td>'.$v->employee_ID.'</td></tr>
							</table>
							<div style="text-align:left;margin-top:20px;">
								<br/><br/><br/>
								<p>'.nl2br($v->benefit, true).'</p>
							</div>
							';
					} 
					
				} 
			}
		?>
		</div>
	</body>
</html>