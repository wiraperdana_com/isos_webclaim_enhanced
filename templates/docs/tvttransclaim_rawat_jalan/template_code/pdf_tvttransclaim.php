<html>
	<head>
		<style>
			*{
				font-family: "Times New Roman", Times, serif;
			}
			body{
				margin:20px;
				padding:0px;
			}
			.header{
				text-align:center;
				height:30%;
			}
			.footer{
				color:#999999;
				text-align:right;
				height:7%;
			}
			.content{
				height:60%;
			}
			table tr{
				line-height:30px;
			}
		</style>
	</head>
	<body>
		<div class="header">
			<div>
				<?php
					$logo = $this->my_uconfig->get('logo');
					$logo = BASEPATH . '/../' . $logo['src'];
					$lgty = pathinfo($logo, PATHINFO_EXTENSION);
					$bs64 = file_get_contents($logo);
					$used = 'data:image/' . $lgty . ';base64,' . base64_encode($bs64);
				?>
				<img src="<?php echo $used; ?>" style="width:80px;height:80px;margin-top:1%;">
			</div>
			<h1><?php echo $this->my_uconfig->get('name'); ?></h1>
			<h3>Airway Bill</h3>
			<p style="text-align:justify;">
				Dengan ini menyatakan bahwa pemeriksaan barang untuk itu dilakukan pemeriksaan dengan data sebagai berikut :
			</p>
		</div>
		<div class="content">
		<?php
			if( isset( $data_cetak ) ) {
				foreach( $data_cetak as $k => $v ) {
					echo '
					<div style="text-align:center;padding:10px;margin-top:10px;">
						<img src="'.$this->Evolusi_Code->barcode('base64', $v->kode_AWB ).'" />
					</div>
					<table style="width:100%;">
						<tr><td style="width:30%;">No. AWB</td><td style="width:5%;">:</td><td style="width:65%;">'.$v->kode_AWB.'</td></tr>
						<tr><td style="width:30%;">No. Resi/SJ</td><td style="width:5%;">:</td><td style="width:65%;">'.$v->no_resi.'</td></tr>
						<tr><td>Tgl.AWB</td><td>:</td><td>'.$v->tgl_awb.'</td></tr>
						<tr><td>Pengirim</td><td>:</td><td>'.$v->nama_pengirim.'</td></tr>
						<tr><td>Alamat</td><td>:</td><td>'.$v->alamat_pickup.'</td></tr>
						<tr><td>Penerima</td><td>:</td><td>'.$v->nama_penerima.'</td></tr>
						<tr><td>Lokasi Penerima</td><td>:</td><td>'.$v->nama_lokasi_cust_penerima.'</td></tr>
						<tr><td>Kota Asal</td><td>:</td><td>'.$v->nama_kota_origin.'</td></tr>
						<tr><td>Lokasi Asal</td><td>:</td><td>'.$v->nama_lokasi_origin.'</td></tr>
					</table>
					<div>
						<p style="text-align:justify;">
							..............................................................................................................................................................
							..............................................................................................................................................................
						</p>
					</div>
					<div style="text-align:right;margin-top:20px;">
						Layanan
						<br/><br/><br/>
						'.$v->nama_layanan.'
					</div>
		
					';
				};
			};
		?>
		</div>
		<div class="footer">
			<p><small><?php echo $this->my_uconfig->get('name'); ?></small></p>
			<p>Waktu cetak pada <?php echo $time; ?></p>
		</div>
	</body>
</html>