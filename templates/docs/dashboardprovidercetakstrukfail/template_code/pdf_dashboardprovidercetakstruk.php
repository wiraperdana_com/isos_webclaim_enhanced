<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
$html = "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html lang=\"en-US\" xml:lang=\"en-US\" xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
 <style>
			*{
				font-family:'Times New Roman', Times, serif;
			}
			body{
				margin:20px;
				padding:0px;
			}
			.header{
				text-align:center;
				height:20%;
			}
			.footer{
				color:#999999;
				text-align:right;
				height:7%;
			}
			.content{
				height:60%;
			}
			table tr{
				line-height:30px;
			}
		</style>
</head>
<body>
";
?> 
<?php
$logo = $this->my_uconfig->get('logo');
$logo = BASEPATH . '/../' . $logo['src'];
$lgty = pathinfo($logo, PATHINFO_EXTENSION);
$bs64 = file_get_contents($logo);
$used = 'data:image/' . $lgty . ';base64,' . base64_encode($bs64);
  
  
		$html .= '<div class="header">
			<div>
 				<img src="'.$used.'" style="width:140px;height:100px;margin-top:1%;">
			</div>';
		?> 	
		 
 		<?php
        if( isset( $data_cetak ) ) {
				$v = $data_cetak[0];
						$html .= '
							<h1><u>VERIFIKASI GAGAL</u></h1>
							</div>
							<div class="content">
							<table style="width:100%;">
								<tr><td style="width:30%;">Tanggal</td><td style="width:5%;">:</td><td style="width:65%;">'.date('d-M-Y').'</td></tr>
								<tr><td style="width:30%;">Jam</td><td style="width:5%;">:</td><td style="width:65%;">'.date('H:i').'</td></tr>
								<tr><td style="width:30%;">Pershn</td><td style="width:5%;">:</td><td style="width:65%;">'.$v->client_name.'</td></tr>
							</table>
							<div style="text-align:center;margin-top:20px;color:red;">
								<br/>
								<h2>Maaf Verifikasi Gagal!<br>Segera Hubungi Intl SOS 24Jam : 021-7659163</h2>
							</div>
							';
			} 

$html .= "
</body>
</html>		
";
 
 
$pdf_path = "./assets/pdf/struk/";
$filenamez = "claim_struk";
$file_path = $pdf_path.$filenamez.".pdf";
 //echo $file_path;die;
$r_html = pdf_create(gzcompress($html,9), $pdf_path, $filenamez);
if($r_html){
	echo base_url().$r_html;
}else{
	echo 'Gagal';
}
?>