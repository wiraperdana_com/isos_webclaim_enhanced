[ penggunaan di JS ]
Contoh :
Params_T_Gaji = getPeriodeAktif() + '_' + getLokasiAktif().kode_unker;
var param_cetakx = [
	'Template_ID=gaji_pegawai_gaji_ke_bank', //nama folder di template
	'Data_ID=nik', //unique field data, umumnya NIK / NIP
	'Grid_ID=Grid_Gaji_Bulanan', //grid yg datanya akan diexport
	
	//tanda tangan param, kalau tanpa set no or hapus aja param nya
	'ttd=yes'
].join('&');

//tanpa tanda tangan
//Load_Popup('win_xls_pd', BASE_URL + 'report_excel/printdialogxls/'+Params_T_Gaji + '?' + param_cetakx, 'Export Data Gaji ke Bank');

//dengan tanda tangan
//Load_Popup('win_print_pd', BASE_URL + 'report_excel/printdialogxls/'+Params_T_Gaji + '?' + param_cetakx, 'Export Data Gaji ke Bank');