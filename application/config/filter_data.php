
<?php

/*
	Untuk custom value dengan menggunakan query, bisa gunakan variable dibawah ini untuk filter
	Contoh query yang bisa digunakan
	$session_NIP = array(
		'column'	=> 'nama_kolom_untuk_dijadikan_value_filter',
		'query'		=> 'select * from table_or_view where kolom_where = ? limit 1'
	);
	Jadi, session.NIP yang digunakan untuk filter data nanti akan ditimpah dengan hasil query diatas.
	Cukup pakai tanda tanya, nanti tanda tanya itu di replace dengan variable session.
	File ini digunakan untuk menimpah data yang dari session, seperti custom value tujuannya.
	Bila tidak ingin membuat custom value untuk filter data, cukup gunakan array() seperti dibawah.
*/
$session_ID_User 			= array(); //numeric
$session_user 				= array(); //skip we, tara di gunakeun
$session_fullname 			= array(); //string
$session_general_group_name = array(); //string
$session_group 				= array(); //numeric
$session_NIP 				= array(
	'column'	=> 'kode_unker',
	'query'		=> 'select f_kode_unker_pgw("?") as kode_unker'
); //string


//jangan dirubah kalau bisa
$config[ 'session.ID_User' ] 					= array( 'key' => 'iduser_zs_exmldashboard', 'val' => $session_ID_User );
$config[ 'session.user' ] 						= array( 'key' => 'user_zs_exmldashboard', 'val' => $session_user );
$config[ 'session.fullname' ] 					= array( 'key' => 'fullname_zs_exmldashboard', 'val' => $session_fullname );
$config[ 'session.general_group_name' ] 		= array( 'key' => 'type_zs_exmldashboard', 'val' => $session_general_group_name );
$config[ 'session.group' ] 						= array( 'key' => 'gorupid_zs_exmldashboard', 'val' => $session_group );
$config[ 'session.NIP' ] 						= array( 'key' => 'nip_zs_exmldashboard', 'val' => $session_NIP );
$config[ 'session.iduser_zs_exmldashboard' ] 	= array( 'key' => 'session.ID_User', 'val' => $session_ID_User );
$config[ 'session.user_zs_exmldashboard' ] 		= array( 'key' => 'session.user', 'val' => $session_user );
$config[ 'session.fullname_zs_exmldashboard' ] 	= array( 'key' => 'session.fullname', 'val' => $session_fullname );
$config[ 'session.type_zs_exmldashboard' ] 		= array( 'key' => 'session.general_group_name', 'val' => $session_general_group_name );
$config[ 'session.gorupid_zs_exmldashboard' ] 	= array( 'key' => 'session.group', 'val' => $session_group );

