<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model
{
	public $table;
	public function __construct()
	{
		parent::__construct();
		$this->table = get_Class($this);
		$this->load->database();
	}
	
	public function save($data,$tablename="")
	{
		if($tablename=="")
		{
			$tablename = $this->table;
		}
		$op = 'update';
		$keyExists = FALSE;
		$fields = $this->db->field_data($tablename);
		foreach ($fields as $field)
		{
			if($field->primary_key==1)
			{
				$keyExists = TRUE;
				if(isset($data[$field->name]))
				{
					$this->db->where($field->name, $data[$field->name]);
				}
				else
				{
					$op = 'insert';
				}
			}
		}
	 
		if($keyExists && $op=='update')
		{
			$this->db->set($data);
			$this->db->update($tablename);
			if($this->db->affected_rows()==1)
			{
				return $this->db->affected_rows();
			}
		}
	 
		$this->db->insert($tablename,$data);
	 
		return $this->db->affected_rows();
	 
	}
 
	function search($conditions=NULL,$tablename="",$limit=500,$offset=0, $order="desc", $orderby=null)
	{	
		if($tablename=="")
		{
			$tablename = $this->table;
		}
		$this->db->set_dbprefix('tbl_');
		$this->db->dbprefix($tablename);
		
		if($conditions != NULL)
			$this->db->where($conditions);

		if($orderby){
			$this->db->order_by($orderby, $order);
		};

		$query = $this->db->get($tablename,$limit,$offset);
		
		if($tablename == "posts"){
			if($query->num_rows == 0){
				//show_404();
			}
		}
		return $query->result();
	}
 
	function insert($data,$tablename="")
	{
		if($tablename=="")
			$tablename = $this->table;
		$this->db->insert($tablename,$data);
		return $this->db->affected_rows();
	}
 
	function update($data,$conditions,$tablename="")
	{
		if($tablename=="")
			$tablename = $this->table;
		$this->db->where($conditions);
		$this->db->update($tablename,$data);
		return $this->db->affected_rows();
	}
	 
	function delete($conditions,$tablename="")
	{
		if($tablename=="")
			$tablename = $this->table;
		$this->db->where($conditions);
		$this->db->delete($tablename);
		return $this->db->affected_rows();
	}


	/* How to use 
	============================================
	
	We have defined the Model class without any methods in it, and see below how we can use it in a controller.

	$this->load->model(�Sample�);

	$condition['id'] = �1';

	$this->sample->delete($condition); //Delete from Sample table, if same as class name then no need to pass table name

	$this->sample->delete($condition,�AnotherTable�); //If delete from some other table

	$data['fieldName'] = �field value�;

	$data['fieldName2'] = �field value 2'

	$this->sample->save($data);

	I think this helped you in speeder development of Codeigniter PHP coding, next time will come up with some other tricks.
	
	============================================
	*/
	
}