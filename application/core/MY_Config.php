<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2010, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Config Class
 *
 * This class contains functions that enable config files to be managed
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/config.html
 */
class MY_Config extends CI_Config {

	/**
	 * Site URL
	 *
	 * @access	public
	 * @param	string	the URI string
	 * @return	string
	 */
	function site_url($uri = '')
	{
		//Added by ~yk~
		global $URI;
		$lang = isset($this->config['lang_uri'][strtolower($URI->segment(1))]) 
				? strtolower($URI->segment(1)) 
				: $this->config['lang_default']
				;
		if ($this->config['lang_ignore'] && $lang == $this->config['lang_default']) $lang = '';
		//*********** ~yk~
		if (is_array($uri))
		{
			$uri = implode('/', $uri);
		}

		if (strlen($uri)==2 && isset($this->config['lang_uri'][$uri])) {
			if (strlen($URI->segment(1))==2) {
				$segment = implode('/', array_slice($URI->segments,1));
			} else {
				$segment = implode('/', $URI->segments);
			}
			if ($this->config['lang_ignore'] && $uri == $this->config['lang_default']) {
				$uri = $segment;
			} else {
				$uri = $uri.'/'.$segment;
			}
			$suffix = ($this->item('url_suffix') == FALSE) ? '' : $this->item('url_suffix');
			return $this->slash_item('base_url').$this->slash_item('index_page').trim($uri, '/').$suffix;  //changed by ~yk~
		}

		$uri_is_lang = ($uri <> '') && (strlen($uri) < 3 || $uri[2] == '/');
		if ($uri == '')
		{
			return $this->slash_item('base_url').$this->item('index_page').($uri_is_lang ? '' : ($lang ? "$lang" : '')); //changed by ~yk~
		}
		else
		{
			$suffix = ($this->item('url_suffix') == FALSE) ? '' : $this->item('url_suffix');
			return $this->slash_item('base_url').$this->slash_item('index_page').($uri_is_lang ? '' : ($lang ? "$lang/" : '')).trim($uri, '/').$suffix;  //changed by ~yk~
		}
	}
	
	function lang() {
		global $URI;
		$lang = isset($this->config['lang_uri'][strtolower($URI->segment(1))]) 
				? strtolower($URI->segment(1)) 
				: $this->config['lang_default']
				;
		return $lang;
	}
	
	function language() {
		return $this->config['lang_uri'][$this->lang()];
	}

}

/* End of file */