<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Usession extends CI_Session {

    public $logged_in = FALSE;
    public $is_pegawai = FALSE;
    public $is_requestbyess = FALSE;

    public function  __construct() {
        parent::__construct();
		
        $this->is_logged_in();
		
		if( $this->logged_in ) {
			$this->ci = &get_instance();
			$routeRequest = trim( strtolower( $this->ci->router->fetch_class()) );
			$methodRequest = trim( strtolower( $this->ci->router->fetch_method()) );
			
			$group = trim( strtolower( $this->userdata('type_zs_exmldashboard') ) );
			$niplogon = trim( strtolower( $this->userdata('nip_zs_exmldashboard') ) );
			
			if( isset( $_SERVER['HTTP_REFERER'] ) ) {
				if( strpos( strtolower( trim( $_SERVER['HTTP_REFERER'] ) ), 'employeeselfservice' ) !== false ) {
					$this->is_requestbyess = $niplogon;
				};
			};
			if( $routeRequest == 'employeeselfservice' ) {
				$this->is_requestbyess = $niplogon;
			};
				
			if( strpos( $group, 'pegawai') !== false ) {
			
				$this->is_pegawai = true;
				
				$redirectUser = true;
				$whitelistEss = $this->whitelistEss();
				
				if( count( $whitelistEss ) > 0 ) {
					if( isset( $whitelistEss[ $routeRequest ] ) ) {
						if( count( $whitelistEss[ $routeRequest ] ) > 0 ){
							if( isset( $whitelistEss[ $routeRequest ][ $methodRequest ] ) ) {
								if( $whitelistEss[ $routeRequest ][ $methodRequest ] == true ) {
									$redirectUser = false;
								}
							}
						}else{
							$redirectUser = false;
						};
					};
				}else{
					//asumsi boleh di akses semua
					if( $this->is_requestbyess != false ) {
						$redirectUser = false;
					};
				};
					
				if( $redirectUser == true ){
					redirect( base_url() . 'employeeselfservice' );
					exit;
				};
			};
		}
    }

    public function is_logged_in()
    {
        $logged = $this->userdata('iduser_zs_exmldashboard');
        $this->logged_in = ($logged) ? TRUE : FALSE;
    }
	
	private function whitelistEss(){
		return array();
		//isi array untuk spesifika method controller
		//array kosong, asumsi semua method dizinkan di akses oleh pegawai.
		return array(
			'employeeselfservice' 		=> array(),
			'notifikasi' 				=> array(),
			'dashboard' 				=> array( 'fn' => true ),
			'profil_func'				=> array( 'index' => true ),
			'arsip_digital_func' 		=> array( 'index' => true ),
			'user' 						=> array( 'set_var_access' => true, 'ext_logout' => true ),
			'fm' 						=> array( 'widget' => true ),
			'combo_ref' 				=> array( 'combo_golru' => true ),
			'profil_pns' 				=> array( 'cetak_profil' => true ),
			'pengguna_login' 			=> array( 'ubahsandi' => true, 'ext_changepass' => true ),
			'klaimkesehatan' 			=> array(
				/*'index' 				=> true,
				'ext_get_all' 			=> true,
				'ext_get_all_detail' 	=> true,
				'ext_insert' 			=> true,
				'ext_proses' 			=> true,
				'ext_get_keluarganama' 	=> true,
				'ext_get_periode' 		=> true,
				'print_dialog_sk' 		=> true,
				'cetak_sk' 				=> true,
				'print_dialog_dnp' 		=> true,
				'cetak_nominatif' 		=> true,*/
			)
		);
	}
}