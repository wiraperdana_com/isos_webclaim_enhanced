<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once APPPATH."/third_party/format.php"; 
 
class prettyprint { 
    public function code( $code ) { 
        return Formatter::formatJavascript( $code );
    } 
}