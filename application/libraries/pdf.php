<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require('fpdf.php');

class Pdf extends FPDF
{
    public $height = 5;
    public $paperWSize = 210;
    public $paperHSize = 297;
    public $lengthCell;
    public $font_size = 12;
    public $title = '';
    public $creator = '';
    public $periode = '';
    public $footerStart;
    public $organisasi;
    public $withHeader = true;
    public $withTitle = true;
    public $withPeriode = true;
    public $withPageNumber = true;
    public $withFooter = true;

    public function __construct($params)
    {
        // Call parent constructor
        $this->fontpath = 'font/'; // Specify font folder
        if ($params['orientation'] == 'P') {
            $w = $this->paperWSize;
            $h = $this->paperHSize;
        } else {
            $w = $this->paperHSize;
            $h = $this->paperWSize;
        }
        $this->lengthCell = $w - 21;
        $this->footerStart = $h - 20;

        $this->title = $params['title'];
        $this->organisasi = $params['organisasi'];
        $this->periode = $params['periode'];
        $this->creator = $params['creator'];
        $this->withHeader = $params['withHeader'];
        $this->withTitle = $params['withTitle'];
        $this->withPeriode = $params['withPeriode'];
        $this->withPageNumber = $params['withPageNumber'];
        $this->withFooter = $params['withFooter'];
        $this->SetMargins(10, 10, 10);
        $this->SetAutoPageBreak(true, 10);

        parent::__construct($params['orientation'], $params['unit'], $params['size']);
    }

    public function Header()
    {
        if ($this->withHeader) {
            $this->SetFont('Arial', 'B', 16);
            // Logo
            $this->Image($this->organisasi['logo_file'], $this->getX(), $this->getY(), 49.2125, 15.345833333);

            // Judul
            $length1 = $this->lengthCell * 3/10;
            $length2 = $this->lengthCell * 7/10;

            $this->nl();
            if ($this->withTitle){
                $this->font_size = 14;
                $this->cells($length1);
                $this->cells($length2, $this->title, 1, 0, 'R');
            }
            $this->nl();

            // Periode
            if ($this->withPeriode) {
                $this->font_size = 12;
                $this->SetFont('Arial', 'B', 10);
                $this->cells($length1);
                $this->cells($length2, 'Periode: '. $this->periode, 1, 0, 'R');
            }
            $this->Ln(4);

            // Page number
            if ($this->withPageNumber) {
                $this->font_size = 8;
                $this->cells($length1, '', 0, 'B');
                $this->cells($length2, 'Halaman '.$this->PageNo().' dari {nb}', 2, 'B', 'R');
            } else {
                $this->cells($length1 + $length2, '', 0, 'B');
            }
            // Line break
            $this->nl();
        }
    }

    public function Footer()
    {
        if ($this->withFooter) {
            $org_text = 'Human Capital Integrated Systems - '. $this->organisasi['nama_organisasi'] .' - '.date('Y');
            if ($this->title != 'Laporan Jamsostek') {
                $this->SetY($this->footerStart);
                $this->font_size = 10;
                $this->cells($this->lengthCell, $org_text, 1, 'B', 'L');
                $this->Ln(5);
                $this->font_size = 8;
                $this->cells($this->lengthCell, 'Dicetak pada hari: '.date('d - F - Y'). ' Oleh: '. $this->creator, 0, 0, 'L');
            } else {
                $this->SetY($this->footerStart);
                $this->font_size = 10;
                $this->cells($this->lengthCell, $org_text, 1, 'B', 'L');
            }
        }
    }

    public function set_bold() {
        $this->SetFont('Arial', 'B', $this->font_size);
    }

    public function un_bold() {
        $this->SetFont('Arial', '', $this->font_size);
    }

    public function set_underline() {
        $this->SetFont('', 'U', $this->font_size);
    }

    public function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=0, $link='')
    {
        //Output a cell
        $k=$this->k;
        if($this->y+$h>$this->PageBreakTrigger and !$this->InFooter and $this->AcceptPageBreak()) {
            $x=$this->x;
            $ws=$this->ws;
            if($ws>0)
            {
                $this->ws=0;
                $this->_out('0 Tw');
            }
            $this->AddPage($this->CurOrientation);
            $this->x=$x;
            if($ws>0)
            {
                $this->ws=$ws;
                $this->_out(sprintf('%.3f Tw', $ws*$k));
            }
        }
        if($w==0) $w=$this->w-$this->rMargin-$this->x;
        $s='';
    // begin change Cell function 12.08.2003
        if($fill==1 or $border>0)
        {
            if($fill==1)
                $op=($border>0) ? 'B' : 'f';
            else
                $op='S';
            if ($border>1) {
                $s=sprintf(' q %.2f w %.2f %.2f %.2f %.2f re %s Q ', $border,
                    $this->x*$k, ($this->h-$this->y)*$k, $w*$k, -$h*$k, $op);
            }
            else
                $s=sprintf('%.2f %.2f %.2f %.2f re %s ', $this->x*$k, ($this->h-$this->y)*$k, $w*$k, -$h*$k, $op);
        }
        if(is_string($border))
        {
            $x=$this->x;
            $y=$this->y;
            if(is_int(strpos($border, 'L')))
                $s.=sprintf('%.2f %.2f m %.2f %.2f l S ', $x*$k, ($this->h-$y)*$k, $x*$k, ($this->h-($y+$h))*$k);
            else if(is_int(strpos($border, 'l')))
                $s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ', $x*$k, ($this->h-$y)*$k, $x*$k, ($this->h-($y+$h))*$k);

            if(is_int(strpos($border, 'T')))
                $s.=sprintf('%.2f %.2f m %.2f %.2f l S ', $x*$k, ($this->h-$y)*$k, ($x+$w)*$k, ($this->h-$y)*$k);
            else if(is_int(strpos($border, 't')))
                $s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ', $x*$k, ($this->h-$y)*$k, ($x+$w)*$k, ($this->h-$y)*$k);

            if(is_int(strpos($border, 'R')))
                $s.=sprintf('%.2f %.2f m %.2f %.2f l S ', ($x+$w)*$k, ($this->h-$y)*$k, ($x+$w)*$k, ($this->h-($y+$h))*$k);
            else if(is_int(strpos($border, 'r')))
                $s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ', ($x+$w)*$k, ($this->h-$y)*$k, ($x+$w)*$k, ($this->h-($y+$h))*$k);

            if(is_int(strpos($border, 'B')))
                $s.=sprintf('%.2f %.2f m %.2f %.2f l S ', $x*$k, ($this->h-($y+$h))*$k, ($x+$w)*$k, ($this->h-($y+$h))*$k);
            else if(is_int(strpos($border, 'b')))
                $s.=sprintf('q 2 w %.2f %.2f m %.2f %.2f l S Q ', $x*$k, ($this->h-($y+$h))*$k, ($x+$w)*$k, ($this->h-($y+$h))*$k);
        }
        if (trim($txt)!='') {
            $cr=substr_count($txt, "\n");
            if ($cr>0) { // Multi line
                $txts = explode("\n", $txt);
                $lines = count($txts);
                //$dy=($h-2*$this->cMargin)/$lines;
                for($l=0;$l<$lines;$l++) {
                    $txt=$txts[$l];
                    $w_txt=$this->GetStringWidth($txt);
                    if($align=='R')
                        $dx=$w-$w_txt-$this->cMargin;
                    elseif($align=='C')
                        $dx=($w-$w_txt)/2;
                    else
                        $dx=$this->cMargin;

                    $txt=str_replace(')', '\\)', str_replace('(', '\\(', str_replace('\\', '\\\\', $txt)));
                    if($this->ColorFlag)
                        $s.='q '.$this->TextColor.' ';
                    $s.=sprintf('BT %.2f %.2f Td (%s) Tj ET ',
                        ($this->x+$dx)*$k,
                        ($this->h-($this->y+.5*$h+(.7+$l-$lines/2)*$this->FontSize))*$k,
                        $txt);
                    if($this->underline)
                        $s.=' '.$this->_dounderline($this->x+$dx, $this->y+.5*$h+.3*$this->FontSize, $txt);
                    if($this->ColorFlag)
                        $s.='Q ';
                    if($link)
                        $this->Link($this->x+$dx, $this->y+.5*$h-.5*$this->FontSize, $w_txt, $this->FontSize, $link);
                }
            }
            else { // Single line
                $w_txt=$this->GetStringWidth($txt);
                $Tz=100;
                if ($w_txt>$w-2*$this->cMargin) { // Need compression
                    $Tz=($w-2*$this->cMargin)/$w_txt*100;
                    $w_txt=$w-2*$this->cMargin;
                }
                if($align=='R')
                    $dx=$w-$w_txt-$this->cMargin;
                elseif($align=='C')
                    $dx=($w-$w_txt)/2;
                else
                    $dx=$this->cMargin;
                $txt=str_replace(')', '\\)', str_replace('(', '\\(', str_replace('\\', '\\\\', $txt)));
                if($this->ColorFlag)
                    $s.='q '.$this->TextColor.' ';
                $s.=sprintf('q BT %.2f %.2f Td %.2f Tz (%s) Tj ET Q ',
                    ($this->x+$dx)*$k,
                    ($this->h-($this->y+.5*$h+.3*$this->FontSize))*$k,
                    $Tz, $txt);
                if($this->underline)
                    $s.=' '.$this->_dounderline($this->x+$dx, $this->y+.5*$h+.3*$this->FontSize, $txt);
                if($this->ColorFlag)
                    $s.='Q ';
                if($link)
                    $this->Link($this->x+$dx, $this->y+.5*$h-.5*$this->FontSize, $w_txt, $this->FontSize, $link);
            }
        }
        // end change Cell function 12.08.2003
        if($s)
            $this->_out($s);
        $this->lasth=$h;
        if($ln>0)
        {
            //Go to next line
            $this->y+=$h;
            if($ln==1) $this->x=$this->lMargin;
        }
        else $this->x+=$w;
    }

    public function cells($length, $content='', $bold=0, $border = 0, $align='L', $height='', $font_size='', $line=0) {
        if (!empty($font_size)) $this->font_size = $font_size;
        if($bold == 0) {
            $this->SetFont('Arial', '', $this->font_size);
        }
        else if ($bold == 1) {
            $this->SetFont('Arial', 'B', $this->font_size);
        }
        else if ($bold == 2) {
            $this->SetFont('Arial', 'I', $this->font_size);
        }
        else if ($bold == 3){
            $this->SetFont('Arial', 'U', $this->font_size);
        }
        else {
            $this->SetFont('Arial', 'BU', $this->font_size);
        }
        if ($height == '')
            $height = $this->height;

        $this->Cell($length, $height, $content, $border, $line, $align);
    }

    public function nl()
    {
        $this->Ln(7);
    }

}