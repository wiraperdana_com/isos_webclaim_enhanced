<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Uconfig{
    public static function get( $key = null ) {
		$stackConfigs = MY_Uconfig::listOfStack();
		if( $key != null ) {
			$result = isset( $stackConfigs[$key] ) ? $stackConfigs[$key] : null;
		}else{
			$result = $stackConfigs;
		}
		
		return $result;
    }
	public static function listOfStack(){
		$current_update = null;
		$exmldashboard_ph = BASEPATH . '/../exmldashboard.json';
		if( file_exists( $exmldashboard_ph ) ) {
			$current_update = @file_get_contents( $exmldashboard_ph );
			if( $current_update ){
				$current_update = @json_decode( $current_update, true );
			};
		};
		
		return array(
			'version'	=> ( $current_update != null ? strtolower( $current_update['version'] ) : '-' ),
			'appname' 	=> 'International SOS Web Claim',
			'apppower'	=> 'Powered-By PT. ABA Sarana Lestari',
			'title' 	=> 'Development',
			'name'		=> 'International SOS Web Claim',
			'theme'	 	=> 'ext-all-slate', //ext-all, ext-all-access, ext-all-gray, ext-all-scoped, ext-all-slate
			'logo'		=>	array( 'src' => 'assets/images/exmldashboard/logo.png', 'height' => 80, 'width' => 80 ),
		);
	}
}
