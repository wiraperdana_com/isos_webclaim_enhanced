<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

Ext.define('MT_Pengaturanfilter', {
    extend: 'Ext.data.Model',
    fields: [
        'ttableview_code',
        'ttableview_name',
        'ttableview_column',
        'ttableview_value',
        'ttableview_status',
    ]
});

Ext.define('MT_Pengaturanfilterdetail', {
    extend: 'Ext.data.Model',
    fields: [
        'ttableview_detail_code',
        'ttableview_detail_group_id',
        'ttableview_detail_group',
        'ttableview_detail_column',
        'ttableview_detail_value',
        'ttableview_detail_spesial',
        'ttableview_detail_status'
    ]
});

var Reader_T_Pengaturanfilter = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Pengaturanfilter',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'ttableview_code'  	
});

var Reader_T_Pengaturanfilterdetail = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Pengaturanfilterdetail',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'ttableview_detail_group'  	
});

var Proxy_T_Pengaturanfilter = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Pengaturanfilter',
    url: BASE_URL + 'pengaturanfilter/ext_get_all',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Pengaturanfilter
});

var Proxy_T_Pengaturanfilterdetail = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Pengaturanfilterdetail',
    url: BASE_URL + 'pengaturanfilter/ext_get_all_detail',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Pengaturanfilterdetail
});

var Data_T_Pengaturanfilter = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Pengaturanfilter',
    model: 'MT_Pengaturanfilter',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Pengaturanfilter
});

var Data_T_Pengaturanfilterdetail = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Pengaturanfilterdetail',
    model: 'MT_Pengaturanfilterdetail',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Pengaturanfilterdetail
});

var cbGrid_T_Pengaturanfilter = new Ext.create('Ext.selection.RowModel');

var cbGrid_T_Pengaturanfilterdetail = new Ext.create('Ext.selection.RowModel');

function deleteWindowPengaturanFilter () {
	if( Ext.getCmp('window_WindowPengaturanFilter') ) {
		Ext.getCmp('window_WindowPengaturanFilter').destroy();
	};
};

function createWindowPengaturanFilter () {
	deleteWindowPengaturanFilter();
	
	Ext.create('Ext.window.Window', {
		title: 'Pengaturan Filter',
		modal:true,
		constrainHeader:true,
		maximizable:false,
		closable:false,
		id:'window_WindowPengaturanFilter',
		itemId:'window_WindowPengaturanFilter',
		bodyStyle:'padding:10px;',
		height: 350,
		width: 500,
		layout: 'fit',
		items: [
			{
				xtype:'form',
				id:'form_window_WindowPengaturanFilter',
				url: BASE_URL + 'pengaturanfilter/ext_insert_filter',
				width: '100%',
				height: '100%',
				fieldDefaults: {
					labelAlign: 'top', 
					msgTarget: 'side'
				},
				defaultType: 'textfield', 
				defaults: {
					style:'margin:10px;',
					anchor: '100%', 
					allowBlank: false
				},
				flex:1,
				items:[
					{
						name:'id_open',
						value:1,
						xtype:'hidden'
					},
					{
						name: 'ttableview_code', 
						xtype: 'hidden'
					},{
						fieldLabel: 'Title',
						name: 'ttableview_name',
						anchor:'100%',
						emptyText:'Table Name',
						flex:1
					},{
						fieldLabel: 'Filter Column',
						name: 'ttableview_column',
						anchor:'100%',
						emptyText:'Filter Column',
						flex:1
					},{
						xtype:'combobox',
						fieldLabel: 'Filter Data by',
						name: 'ttableview_value',
						anchor:'100%',
						emptyText:'Filter Value',
						flex:1,
						store: new Ext.data.Store({
							data: [
								{ 'key' : 'session.ID_User', 'val' : 'iduser_zs_exmldashboard' },
								{ 'key' : 'session.user', 'val' : 'user_zs_exmldashboard' },
								{ 'key' : 'session.fullname', 'val' : 'fullname_zs_exmldashboard' },
								{ 'key' : 'session.general_group_name', 'val' : 'type_zs_exmldashboard' },
								{ 'key' : 'session.group', 'val' : 'gorupid_zs_exmldashboard' },
								{ 'key' : 'session.NIP', 'val' : 'nip_zs_exmldashboard' }
							], 
							fields: ['key', 'val'],
						}),
						displayField: 'key',
						valueField: 'val'
					},{
						xtype:'checkbox',
						fieldLabel: 'Aktif?',
						name: 'ttableview_status',
						anchor:'100%',
						flex:1
					},
				]
			}
		],
		buttons: [
			{
				text:'Save',
				iconCls:'icon-save',
				handler : function(){
					var fr_parent = Ext.getCmp('form_window_WindowPengaturanFilter');
					var fr = fr_parent.getForm();
					if( fr.isValid() ) {
						
						fr.submit({
							waitMsg: 'Loading...',
							success: function(form, action) {
								Ext.Msg.alert('Success', action.result.message);
								deleteWindowPengaturanFilter();
								Data_T_Pengaturanstatus.load();
							},
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							}
						});
					}
				}
			},{
				text:'Cancel',
				handler : function(){
					deleteWindowPengaturanFilter();
				}
			}
		]
	}).show();
}

var Grid_T_Pengaturanfilter = new Ext.create('Ext.grid.Panel', {
    region : 'center',
    id: 'Grid_T_Pengaturanfilter',
    store: Data_T_Pengaturanfilter,
    border: true,
    loadMask: true,
	iconCls:'icon-menu_pelantikan_pf',
	title:'Daftar Table / View',
    noCache: false,
    style: 'margin:3px 0px 0px 0px;',
    autoHeight: true,
    columnLines: true,
    flex:1.5,
    selModel: cbGrid_T_Pengaturanfilter,
    columns: [
        {header: 'No', xtype: 'rownumberer', width: 35, height : 20,resizable: true, style: 'padding-top: .2px;'}, 
        {header: "Table Name", dataIndex: 'ttableview_name', flex:0.5}, 
        {header: "Filter Column", flex : 1, columns:[
	        {header: "Filter Column", dataIndex: 'ttableview_column', flex:1}, 
	        {header: "Filter By Value", dataIndex: 'ttableview_value', flex:1},
        ]},
        {header: "Aktif", dataIndex: 'ttableview_status', flex:0.3, renderer:function(a,b,c){
        	return ( c.data.ttableview_status > 0 ? 'Yes' : 'No' );
        }}
    ],
    tbar : ['->',{
        text:'Edit',
		id:'pengaturanfilter_filter_update',
		disabled:true,
        iconCls:'icon-edit',
        handler:function(){
			var sm = Grid_T_Pengaturanfilter.getSelectionModel(), sel = sm.getSelection();
			if(sel.length > 0){
				createWindowPengaturanFilter();
				Ext.getCmp('form_window_WindowPengaturanFilter').loadRecord(sel[0]);
			};
        }
    }],
    bbar: [
        {
            text:'Refresh',
            iconCls:'x-tbar-loading',
            handler:function(){
                Data_T_Pengaturanfilter.load();
            }
        }
    ],
    listeners: {
        selectionchange: function(model, records) {
			var statusSelected  = false;
            if (records[0]) {
				statusSelected = true;
				
				Data_T_Pengaturanfilterdetail.getProxy().extraParams = {
					ttableview_code: records[0].get('ttableview_code'), 
					id_open : 1
				};
				Data_T_Pengaturanfilterdetail.load();
            };

            Ext.getCmp('pengaturanfilter_filter_update').setDisabled(!statusSelected);
			Ext.getCmp('pengaturanfilter_detail_save').setDisabled(!statusSelected);
        } 
    }  
});

function PengaturanfiltergetIdGroupeSelected(  ){
	var idreq = undefined;
	var sm = Grid_T_Pengaturanfilter.getSelectionModel(), sel = sm.getSelection();
	if(sel.length > 0){
		idreq = sel[0].get('ttableview_code');
	}
	return idreq;
}

var dragandropPluginFilter = Ext.create('Ext.grid.plugin.DragDrop', {
	pluginId:'dragandropPluginFilter'
});
var Grid_T_Pengaturanfilterdetail = new Ext.create('Ext.grid.Panel', {
    region : 'center',
    id: 'Grid_T_Pengaturanfilterdetail',
    store: Data_T_Pengaturanfilterdetail,
    border: true,
    loadMask: true,
	title:'Daftar Group',
	iconCls:'icon-gears',
    style: 'margin:0px;',
    noCache: false,
    autoHeight: true,
    columnLines: true,
    selModel: cbGrid_T_Pengaturanfilterdetail,
    viewConfig: {
        stripeRows: false,
        getRowClass: function (record) {
            return record.get('tpestd_status') == 0 ? 'child-row' : '';
        },
		plugins: dragandropPluginFilter,
		forceFit: true
    },
	plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    columns: [
        {header: 'No', xtype: 'rownumberer', width: 35, height : 20,resizable: true, style: 'padding-top: .2px;'}, 
        {header: "Group Name", dataIndex: 'ttableview_detail_group', flex:0.5}, 
        {header: "Filter Column ( Spesial )", flex : 1, columns:[
	        {header: "Filter Column", dataIndex: 'ttableview_detail_column', flex:1, editor: { xtype: 'textfield' } }, 
	        {header: "Filter By Value", dataIndex: 'ttableview_detail_value', flex:1, editor: {
				xtype: 'combobox',
				store: new Ext.data.Store({
					data: [
						{ 'key' : 'session.ID_User', 'val' : 'iduser_zs_exmldashboard' },
						{ 'key' : 'session.user', 'val' : 'user_zs_exmldashboard' },
						{ 'key' : 'session.fullname', 'val' : 'fullname_zs_exmldashboard' },
						{ 'key' : 'session.general_group_name', 'val' : 'type_zs_exmldashboard' },
						{ 'key' : 'session.group', 'val' : 'gorupid_zs_exmldashboard' },
						{ 'key' : 'session.NIP', 'val' : 'nip_zs_exmldashboard' }
					], 
					fields: ['key', 'val'],
				}),
				displayField: 'key',
				valueField: 'val'
	        } },
        ]},
        {header: "Spesial Filter?", dataIndex: 'ttableview_detail_spesial', flex:0.3, renderer:function(a,b,c){
        	return ( c.data.ttableview_detail_spesial > 0 ? 'Yes' : 'No' );
        }, editor: {
			xtype: 'combobox',
			store: new Ext.data.Store({
				data: [
					{ 'key' : 'Yes', 'val' : 1 },
					{ 'key' : 'No', 'val' : 0 },
				], 
				fields: ['key', 'val'],
			}),
			displayField: 'key',
			valueField: 'val'
		}},
        {header: "Aktif", dataIndex: 'ttableview_detail_status', flex:0.3, renderer:function(a,b,c){
        	return ( c.data.ttableview_detail_status > 0 ? 'Yes' : 'No' );
        }, editor: {
			xtype: 'combobox',
			store: new Ext.data.Store({
				data: [
					{ 'key' : 'Yes', 'val' : 1 },
					{ 'key' : 'No', 'val' : 0 },
				], 
				fields: ['key', 'val'],
			}),
			displayField: 'key',
			valueField: 'val'
		}}
    ],
    tbar: [
        '->',{
            text:'Simpan',
			id:'pengaturanfilter_detail_save',
            iconCls:'icon-save',
			disabled:true,
            handler:function(){
				var idreq = PengaturanfiltergetIdGroupeSelected();
				if( idreq != undefined ) {
					Grid_T_Pengaturanfilterdetail.getEl().mask('Loading...');
					var postdata = Ext.encode(Ext.pluck(Data_T_Pengaturanfilterdetail.data.items, 'data'));
					Ext.Ajax.request({
						url: BASE_URL + 'pengaturanfilter/ext_insert_detail_filter', 
						method: 'POST',
						params: { 
							id_open: 1,
							ttableview_detail_code: idreq,
							postdata:postdata
						},
						success: function(response){
							Grid_T_Pengaturanfilterdetail.getEl().unmask();
							Data_T_Pengaturanfilterdetail.load();
						},
						failure: function(response){ 
							Grid_T_Pengaturanfilterdetail.getEl().unmask();
							Ext.MessageBox.show({title:'Peringatan !', msg: response.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
						}
					});
				}
            }
        }
    ]
});

var new_tabpanel = {
    id: 'newtab_pengaturanfilter', 
    title: 'Pengaturan Filter', 
    iconCls: 'icon-gears',
    border: false, 
    closable: true,  
    layout: 'border', 
    items: [
		{
			region:'north',
			border:false,
			html:'<div style="background:#59a7d2;color:white;padding:8px;"><b>PENGATURAN FILTER</b></div>'
		},
        Grid_T_Pengaturanfilter,
		{
			border: false, 
			flex:2,
			layout: 'border', 
			style: 'margin:3px 0px 0px 10px;',
			region:'east',
			items:[
				{
					region:'north',
					border:false,
					style: 'margin:0px 0px 3px 0px;',
					html:'<div style="background:#55788B;color:white;padding:8px;"><b>Daftar Group untuk Filter Data</b></div>'
				},
				Grid_T_Pengaturanfilterdetail
			]
		}
    ]
}
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>
