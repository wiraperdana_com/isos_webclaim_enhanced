<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

// PANEL UTAMA MASTER DATA  -------------------------------------------- START
var Tab_MD = Ext.createWidget('tabpanel', {
    id: 'Tab_MD',
    layout: 'fit',
    resizeTabs: true,
    enableTabScroll: false,
    deferredRender: true,
    border: false,
    defaults: {
        autoScroll: true
    },
    items: [{
        id: 'default_Tab_MD',
        bodyPadding: 10,
        closable: false
    }]
});

var Center_MD = {
    region: 'center',
    layout: 'card',
    collapsible: false,
    margins: '0 0 0 0',
    width: '100%',
	split:true,
    autoScroll: true,
    items: [Tab_MD],
    tbar: Ext.create('Ext.toolbar.Toolbar', {
        layout: {
            overflowHandler: 'Menu'
        },
        items: [
		/*{text: 'Unit-Organisasi', iconCls: 'icon-course', hidden: uk_access, handler: function(){Load_TabPage_MD('master_unit_kerja', BASE_URL + 'master_data/unit_kerja');}, tooltip: {text: 'Referensi Unit-Organisasi'}},
	  	{text: 'Jabatan', iconCls: 'icon-spam', hidden: jab_access, handler: function(){Load_TabPage_MD('master_jabatan', BASE_URL + 'master_data/jabatan');}, tooltip: {text: 'Referensi Jabatan'}},
	  	{text: 'Unit-Kerja', iconCls: 'icon-spell', hidden: unor_access, handler: function(){Load_TabPage_MD('master_unit_organisasi', BASE_URL + 'master_data/unit_organisasi');}, tooltip: {text: 'Referensi Unit-Kerja'}},
	  	{text: 'Diklat', iconCls: 'icon-book', hidden: diklat_access, handler: function(){Load_TabPage_MD('master_diklat', BASE_URL + 'master_data/diklat');}, tooltip: {text: 'Referensi Diklat'}},
		{text: 'Provider Diklat', iconCls: 'icon-book', hidden: diklatpenyelenggara_access, handler: function(){Load_TabPage_MD('master_diklat_penyelenggara', BASE_URL + 'master_data/diklatpenyelenggara');}, tooltip: {text: 'Referensi Provider Diklat'}},
	  	{text: 'Penghargaan', iconCls: 'icon-star', hidden: reward_access, handler: function(){Load_TabPage_MD('master_penghargaan', BASE_URL + 'master_data/penghargaan');}, tooltip: {text: 'Referensi Penghargaan'}},
	  	{text: 'HukDis', iconCls: 'icon-law', hidden: hukdis_access, handler: function(){Load_TabPage_MD('master_hukdis', BASE_URL + 'master_data/hukdis');}, tooltip: {text: 'Referensi Hukuman Disiplin'}},
	  	{text: 'Pendidikan', iconCls: 'icon-toga', hidden: pddk_access, handler: function(){Load_TabPage_MD('master_pendidikan', BASE_URL + 'master_data/pendidikan');}, tooltip: {text: 'Referensi Pendidikan'}},
	  	{text: 'Pekerjaan', iconCls: 'icon-gnome', hidden: pkrjn_access, handler: function(){Load_TabPage_MD('master_pekerjaan', BASE_URL + 'master_data/pekerjaan');}, tooltip: {text: 'Referensi Pekerjaan'}},
	  	{text: 'PP Gapok', iconCls: 'icon-help', hidden: g_pp_access, handler: function(){Load_TabPage_MD('master_gapok_pp', BASE_URL + 'master_data/gapok_pp');}, tooltip: {text: 'Referensi PP Gaji Pokok'}},
	  	{text: 'Gapok', iconCls: 'icon-money_add', hidden: gapok_access, handler: function(){Load_TabPage_MD('master_gapok', BASE_URL + 'master_data/gapok');}, tooltip: {text: 'Referensi Gaji Pokok'}},
	  	{text: 'COA', iconCls: 'icon-templates', hidden: coa_access, handler: function(){
			Load_TabPage_MD('master_coa', BASE_URL + 'master_data/coa');}, tooltip: {text: 'COA'}
		},
		{text: 'Golru', iconCls: 'icon-templates', hidden: golru_access, handler: function(){
			Load_TabPage_MD('master_golru', BASE_URL + 'master_data/golru');}, tooltip: {text: 'Golru'}
		},
		{text: 'Aset', iconCls: 'icon-templates', hidden: aset_access, handler: function(){
			Load_TabPage_MD('master_aset', BASE_URL + 'master_data/aset');}, tooltip: {text: 'Aset'}
		},
	  	{text: 'Jabatan Fungsional', iconCls: 'icon-templates', hidden: jafung_access, handler: function(){Load_TabPage_MD('master_jafung', BASE_URL + 'master_data/jafung');}, tooltip: {text: 'Referensi Jabatan Fungsional'}},
	  	{text: 'Fungsional', iconCls: 'icon-templates', hidden: fung_access, handler: function(){Load_TabPage_MD('master_fung', BASE_URL + 'master_data/fung');}, tooltip: {text: 'Referensi Fungsional'}},
	  	{text: 'Fungsional Tertentu', iconCls: 'icon-templates', hidden: fung_t_access, handler: function(){Load_TabPage_MD('master_fung_tertentu', BASE_URL + 'master_data/fung_tertentu');}, tooltip: {text: 'Referensi Fungsional Tertentu'}},
	  	{text: 'TTD', iconCls: 'icon-templates', hidden: ttd_access, handler: function(){Load_TabPage_MD('master_ttd', BASE_URL + 'master_data/ttd');}, tooltip: {text: 'Referensi Pejabat Penandatangan'}},
	  	{text: 'Kop Surat', iconCls: 'icon-templates', hidden: kop_access, handler: function(){Load_TabPage_MD('master_kop_surat', BASE_URL + 'master_data/kop_surat');}, tooltip: {text: 'Referensi Kop Surat OPD'}},
	  	{text: 'Provinsi', iconCls: 'icon-templates', hidden: prov_access, handler: function(){Load_TabPage_MD('master_prov', BASE_URL + 'master_data/prov');}, tooltip: {text: 'Referensi Provinsi'}},
	  	{text: 'Kabupaten', iconCls: 'icon-templates', hidden: kabkota_access, handler: function(){Load_TabPage_MD('master_kabkota', BASE_URL + 'master_data/kabkota');}, tooltip: {text: 'Referensi Kabupaten / Kota'}},
	  	{text: 'Kecamatan', iconCls: 'icon-templates', hidden: kabkota_access, handler: function(){Load_TabPage_MD('master_kec', BASE_URL + 'master_data/kec');}, tooltip: {text: 'Referensi Kecamatan'}},
		*/
		<?php echo $MenuRef; ?>
        ]
    })
};

var Container_MD = {
    xtype: 'container',
    region: 'center',
    layout: 'border',
    border: false,
    items: [Center_MD]
};

var new_tabpanel = {
    id: 'master_data',
    title: 'Referensi',
    iconCls: 'icon-gears',
    border: false,
    closable: true,
    layout: 'fit',
    items: [Container_MD]
};
// PANEL UTAMA MASTER DATA  --------------------------------------------- END

function Load_TabPage_MD(tab_id, tab_url) {
    Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
    var new_tab_id = Ext.getCmp(tab_id);
    if (new_tab_id) {
        Ext.getCmp('Tab_MD').setActiveTab(tab_id);
        Ext.getCmp('layout-body').body.unmask();
    } else {
        Ext.Ajax.timeout = Time_Out;
        Ext.Ajax.request({
            url: tab_url,
            method: 'POST',
            params: {
                id_open: 1
            },
            scripts: true,
            success: function (response) {
                var jsonData = response.responseText;
                var aHeadNode = document.getElementsByTagName('head')[0];
                var aScript = document.createElement('script');
                aScript.text = jsonData;
                aHeadNode.appendChild(aScript);
                if (new_tabpanel_MD != "GAGAL") {
                    Ext.getCmp('Tab_MD').add(new_tabpanel_MD).show();
                } else {
                    Ext.MessageBox.show({
                        title: 'Peringatan !',
                        msg: 'Anda tidak dapat mengakses ke halaman ini !',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
            },
            failure: function (response) {
                Ext.MessageBox.show({
                    title: 'Peringatan !',
                    msg: 'Gagal memuat dokumen !',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            },
            callback: function (response) {
                Ext.getCmp('layout-body').body.unmask();
            },
            scope: this
        });
    }
}
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>