<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

var Show_Popup_RefPegawai_User_Config = {
	employee_asocmodule_browseref 	: <?php echo ( is_array( $employee_asocmodule_browseref ) ? @json_encode( $employee_asocmodule_browseref ) : $employee_asocmodule_browseref ); ?>,
	employee_asocmodule_unor 	: <?php echo ( is_array( $employee_asocmodule_unor ) ? @json_encode( $employee_asocmodule_unor ) : $employee_asocmodule_unor ); ?>,
	employee_asocmodule_pegawai 	: <?php echo ( is_array( $employee_asocmodule_pegawai ) ? @json_encode( $employee_asocmodule_pegawai ) : $employee_asocmodule_pegawai ); ?>,
	employee_asocmodule_jabatan 	: <?php echo ( is_array( $employee_asocmodule_jabatan ) ? @json_encode( $employee_asocmodule_jabatan ) : $employee_asocmodule_jabatan ); ?>,
};
var photo_default = BASE_URL + 'assets/photo/anonymous.jpg';
var photo_pgw = '';
var sesi_type = '<?php echo $this->session->userdata("type_zs_exmldashboard");?>';

<?php echo $var_js_menu; ?>

var var_akses_menu = 'SUKSES';

<?php }else{ echo "var var_akses_menu = 'GAGAL';"; } ?>