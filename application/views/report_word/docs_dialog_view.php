<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Content-Type: application/x-javascript");
	
if(isset($jsscript) && $jsscript == TRUE){ ?>

var popup_title = '<?php echo $this->input->post('popup_title'); ?>';
var uri_all = '<?php echo $uri_all; ?>';
var uri_selected = '<?php echo $uri_selected; ?>';
var uri_by_rows = '<?php echo $uri_by_rows; ?>';
var Grid_ID = Ext.getCmp('<?php echo $Grid_ID; ?>');
var Params_Print = <?php echo $Params_Print; ?>;
var createpdf = "<?php echo $createpdf; ?>";
var columns_visible = <?php if(isset($columns_visible)){echo $columns_visible; }else{ echo '[]';}; ?>;


var form_docs_pd = new Ext.create('Ext.form.Panel', {
	id: 'form_docs_pd', url: null, frame: true, bodyStyle: 'padding: 5px 5px 0 0;', width: '100%', height: '100%',
  fieldDefaults: {labelAlign: 'right', msgTarget: 'side'},
  defaults: {hideEmptyLabel: false, allowBlank: false},
  items: [{
  	xtype: 'fieldset',flex: 1,title:'Pilihan Cetak',defaultType:'radio',layout:'anchor',
    items: [
    	{boxLabel: 'Semua', name: 'pilihan_cetak', id: 'semua_pd', inputValue: 'semua',
    	 listeners: {
    	 	change: function (ctl, val) {if(val == true){Ext.getCmp('dari_pd_docs').reset(); Ext.getCmp('sampai_pd').reset();}}
    	 }
    	},
      {boxLabel: 'Yang Dipilih', name: 'pilihan_cetak', id: 'terpilih_pd_docs', inputValue: 'terpilih', checked: true,
    	 listeners: {
    	 	change: function (ctl, val) {if(val == true){Ext.getCmp('dari_pd_docs').reset(); Ext.getCmp('sampai_pd').reset();}}
    	 }
      },
      {boxLabel: 'Berdasarkan Baris', name: 'pilihan_cetak', id: 'perbaris_pd_docs', inputValue: 'perbaris_pd_docs'},
      {xtype: 'fieldcontainer', layout: 'hbox', defaultType: 'textfield', combineErrors: true, defaults: {hideLabel: 'true', width: 70},
			 items: [
			 	{xtype: 'numberfield', name: 'dari', id: 'dari_pd_docs', emptyText: 'Dari', margins: '0 0 0 15', minValue: 1,
         listeners:{
         	focus: function(selectedField){Ext.getCmp('perbaris_pd_docs').setValue(true);},
          change: function(selectedField){Ext.getCmp('perbaris_pd_docs').setValue(true);}
				 }
        },
        {xtype: 'numberfield', name: 'sampai', id: 'sampai_pd', emptyText: 'Sampai', margins: '0 0 0 5', minValue: 1,
         listeners:{
         	focus: function(selectedField){Ext.getCmp('perbaris_pd_docs').setValue(true);},
          change: function(selectedField){Ext.getCmp('perbaris_pd_docs').setValue(true);}
				 }
        }
       ]
      }
    ]
	}],
  buttons: [
  	{text: 'Cetak', id:'Cetak_Docs', handler: function() {submit_export_docs_pd();}},
    {text: 'Batal', handler: function() {new_popup.close();}}
  ]
});

function submit_export_docs_pd(){
	Ext.getCmp('Cetak_Docs').setDisabled(true);
	if(Ext.getCmp('semua_pd').getValue() == true){		
		Ext.getCmp('sbwin_docs_pd').showBusy({text:'Silahkan tunggu! Sedang melakukan Cetak ...'});
		Ext.Ajax.request({
    	url: BASE_URL + uri_all, method: 'POST',
    	params: objectMerge({id_open: 1, createpdf : createpdf, idx:'<?php echo $Grid_ID; ?>', columns_visible : JSON.stringify(columns_visible) }, Params_Print),
      success: function(response){preview_export_docs_pd(response);},
      scope: this
    });
	}else if(Ext.getCmp('terpilih_pd_docs').getValue() == true){
		var sm = Grid_ID.getSelectionModel(), sel = sm.getSelection(), data = '';
  	if(sel.length > 0){
  		Ext.getCmp('sbwin_docs_pd').showBusy();
     	for (i = 0; i < sel.length; i++) {
      	data = data + sel[i].get('<?php echo $Data_ID; ?>') + '-';
			}
			Ext.Ajax.request({
    		url: BASE_URL + uri_selected, method: 'POST',
    		params: objectMerge({id_open: 1, postdata: data, createpdf : createpdf, idx:'<?php echo $Grid_ID; ?>', columns_visible : JSON.stringify(columns_visible)}, Params_Print),
      	success: function(response){preview_export_docs_pd(response);},
      	scope: this
    	});
  	}else{
			Ext.MessageBox.show({title:'Peringatan !', msg:'Silahkan pilih data yang ingin Anda export !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
			win_docs_pd.close();
		}
	}else{
		var dari = Ext.getCmp('dari_pd_docs').getValue();
		var sampai = Ext.getCmp('sampai_pd').getValue();
		if(dari == null || sampai == null){
			Ext.MessageBox.show({title:'Peringatan !', msg:'Tentukan batasan baris yang akan diexport !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
			Ext.getCmp('Cetak_Docs').setDisabled(false);
		}else if(dari > sampai){
			Ext.MessageBox.show({title:'Peringatan !', msg:'Kesalahan penentuan batasan baris !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
			Ext.getCmp('Cetak_Docs').setDisabled(false);
		}else{
			Ext.getCmp('sbwin_docs_pd').showBusy();
			Ext.Ajax.request({
    		url: BASE_URL + uri_by_rows + Ext.getCmp('dari_pd_docs').getValue() + '/' + Ext.getCmp('sampai_pd').getValue(), method: 'POST',
      	params: objectMerge({id_open: 1, createpdf : createpdf, idx:'<?php echo $Grid_ID; ?>', columns_visible : JSON.stringify(columns_visible)}, Params_Print),
      	success: function(response){preview_export_docs_pd(response);},
      	scope: this
    	});
		}
	}
}

function preview_export_docs_pd(response){
	if(isURL(response.responseText)){
		win_docs_pd.close();		
		var docprint = new Ext.create('Ext.window.Window', {
			title: popup_title, iconCls: 'icon-docs',
			constrainHeader : true, closable: true, maximizable: false,
			width: '80%', height: '80%', bodyStyle: 'padding: 5px;', modal : true,
			items: [{
				xtype:'tabpanel', activeTab : 0, width: '100%', height: '100%',
				items: [{
					title: 'DOCS', frame: false, collapsible: true, autoScroll: true, iconCls: 'icon-docs',
					items: [{
						xtype : 'miframe', frame: false, height: '100%',
						src : response.responseText
					}]
				}]
			}]
		}).show();
	}else{
		Ext.MessageBox.show({title:'Peringatan !', msg:'Proses cetak gagal !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
	}	
}

var win_docs_pd = new Ext.create('Ext.window.Window', {
	id: 'win_docs_pd', title: popup_title, iconCls: 'icon-docs', constrainHeader : true, closable: true, 
	width: 300, height: 250, bodyStyle: 'padding: 5px;', modal : true, items: [form_docs_pd],
	bbar: new Ext.ux.StatusBar({
  	text: 'Ready', id: 'sbwin_docs_pd', iconCls: 'x-status-valid'
  })
});

var new_popup = win_docs_pd;

<?php }else{ echo "var new_popup = 'GAGAL';"; } ?>


