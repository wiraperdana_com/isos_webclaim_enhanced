<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

var SemarEvolusiStack = {};
var SemarEvolusi = {
	data:{},
	evo_type:undefined,
	evo_string:undefined,
	evo_listener:undefined,
	init:function(temp){
		if( typeof(temp) == 'object' ) {
			if( temp.hasOwnProperty('listener')) {
				this.evo_listener = temp['listener'];
			};
			if( temp.hasOwnProperty('type')) {
				this.evo_type = temp['type'];
			};
			this.data = temp;
			this.create();
			console.log('init evolusi');
		}
		return this;
	},
	unique : function( sInput ){
		return 'semar_evolusi_unique_' + new String( sInput ).trim().replace(/[^a-zA-Z0-9]+/g,'').toLowerCase();
	},
	visual: function(){
		var evo_text = this.evo_string;
		if( evo_text != undefined && String( evo_text ).length > 0 ) {
			this.data['text'] 			= evo_text;
			this.data['label'] 			= evo_text;
			this.data['stretchtext'] 	= 1;
			this.data['drawlabel'] 		= 1;
			var url_evo = BASE_URL + 'evolusi/embed/' + this.evo_type + '?' + Ext.urlEncode( this.data );
			if ( Ext.getCmp( this.unique( this.evo_listener ) ) ) {
				console.log('trying to update items evo')
				Ext.getCmp( this.unique( this.evo_listener ) ).getEl().dom.src = url_evo;
			};
		}else{
			if ( Ext.getCmp( this.unique( this.evo_listener ) ) ) {
				console.log('trying to update items evo')
				Ext.getCmp( this.unique( this.evo_listener ) ).getEl().dom.src = 'javascript://';
			};
		};
	},
	listener:function(){
		var instance = this;
		console.log(Ext.getCmp(this.evo_listener))
		if( Ext.getCmp(this.evo_listener) !== undefined ) {
			Ext.getCmp(this.evo_listener).on('change', function(v,x){
				instance.evo_string = Ext.getCmp(instance.evo_listener).getValue();
				instance.visual();
			});
			Ext.getCmp(this.evo_listener).fireEvent('change', true);
		};
	},
	create:function(){
		if( this.evo_type !== undefined && this.evo_listener !== undefined ) {
			var instance = this;
			var widget = {
				xtype : "component",
				bodyStyle:'border:0px;',
				id : instance.unique( instance.evo_listener ),
				frameBorder:0,
				border:false,
				autoEl : {
					flex:1,
					width:'100%',
					height:'100%',
					tag : "iframe",
					frameBorder:0,
					bodyStyle:'border:0px;width:100%;height:100%;',
					border:false
				},
				width:'100%',
				height:'100%',
				flex:1,
				layout:'fit',
				listeners:{
					afterrender:function(){
						instance.listener();
					}
				}
			};
			this.set(widget);
			this.listener();
			console.log('create evolusi');
		}
	},
	set:function(widget){
		SemarEvolusiStack['SemarEvolusiStack_'+this.unique( this.evo_listener )] = widget;
		console.log('set evolusi');
	},
	get:function(){
		if( SemarEvolusiStack['SemarEvolusiStack_'+this.unique( this.evo_listener )] != undefined ) {
			console.log('get evolusi');
			return SemarEvolusiStack['SemarEvolusiStack_'+this.unique( this.evo_listener )];
		}
		return false;
	},
};

var SemarFMStack = {};
var SemarFM = {
	data:{},
	tfm_refid:0,
	tfm_refprefix:undefined,
	tfm_listener:undefined,
	//private
	api_check: function(){
		var refid = this.tfm_refid;
		var refprefix = this.tfm_refprefix;
		var p_btn_download_id = 'fm_download_'+refprefix;
		
		Ext.getCmp(p_btn_download_id).setIconCls('');
		Ext.getCmp(p_btn_download_id).setDisabled(true);
		
		if( refid != undefined && refid !=  0 && refprefix != null && this.get() != false ) {
			Ext.Ajax.request({
				url: BASE_URL + 'fm/check',
				method: 'POST',
				params: {
					id_open: 1,
					tfm_refid: refid,
					tfm_refprefix: refprefix
				},
				renderer: 'data',
				success: function (response) {
					if (response.responseText) {
						Ext.getCmp(p_btn_download_id).setDisabled(false);
						if (response.responseText == 'jpeg') {
							Ext.getCmp(p_btn_download_id).setIconCls('icon-jpeg');
						} else if (response.responseText == 'docx') {
							Ext.getCmp(p_btn_download_id).setIconCls('icon-doc');
						} else {
							switch (response.responseText) {
							case 'jpg':
								Ext.getCmp(p_btn_download_id).setIconCls('icon-jpeg');
								break;
							case 'pdf':
								Ext.getCmp(p_btn_download_id).setIconCls('icon-pdf');
								break;
							case 'doc':
								Ext.getCmp(p_btn_download_id).setIconCls('icon-doc');
								break;
							case 'rar':
							case 'zip':
							case '7z':
							case 'tar':
								Ext.getCmp(p_btn_download_id).setIconCls('icon-zip');
								break;
							default:
								Ext.getCmp(p_btn_download_id).setIconCls('');
							}
						}
						
						if( new String(Ext.getCmp(p_btn_download_id).iconCls).length <= 0 || Ext.getCmp(p_btn_download_id).iconCls == undefined ) {
							Ext.getCmp(p_btn_download_id).setDisabled(true);
						}
					} else {
						Ext.getCmp(p_btn_download_id).setIconCls('');
						Ext.getCmp(p_btn_download_id).setDisabled(true);
					}
				},
				failure: function (response) {
					Ext.getCmp(p_btn_download_id).setIconCls('');
					Ext.getCmp(p_btn_download_id).setDisabled(true);
				},
				scope: this
			});
			console.log('api check');
		}
	},
	//private
	set:function(widget){
		SemarFMStack['SemarFMStack_'+this.tfm_refprefix] = widget;
		console.log('set form');
	},
	get:function(){
		if( SemarFMStack['SemarFMStack_'+this.tfm_refprefix] != undefined ) {
			console.log('get form');
			return SemarFMStack['SemarFMStack_'+this.tfm_refprefix];
		}
		return false;
	},
	init:function(temp){
		if( typeof(temp) == 'object' ) {
			if( temp.hasOwnProperty('listener')) {
				this.tfm_listener = temp['listener'];
			}
			if( temp.hasOwnProperty('prefix')) {
				this.tfm_refprefix = temp['prefix'];
				this.set(undefined);
			}
			if( temp.hasOwnProperty('id')) {
				this.tfm_refid = temp['id'];
			}
			this.create();
			console.log('init form');
		}
		return this;
	},
	enable:function(){
		if( this.get() != false ) {
			semarDeactiveAll( this.get(), false );
			console.log('enable form');
		}
		return this;
	},
	disable:function(){
		if( this.get() != false ) {
			semarDeactiveAll( this.get(), true );
			console.log('disable form');
		}
		return this;
	},
	download:function(){
		console.log('download form');
		return this;
	},
	//private
	listener:function(){
		var instance = this;
		
		if( Ext.getCmp(this.tfm_listener) !== undefined ) {
			Ext.getCmp(this.tfm_listener).on('change', function(v,x){
				instance.tfm_refid = Ext.getCmp(instance.tfm_listener).getValue();
				instance.api_check();
			});
			Ext.getCmp(this.tfm_listener).fireEvent('change', true);
		}
		
		if( this.get() != false ) {
			if( Ext.getCmp('fm_download_'+this.tfm_refprefix) ) {
				var p_btn_download_id = 'fm_download_'+this.tfm_refprefix;
				Ext.getCmp(p_btn_download_id).onEnable = function() {
					if( new String(Ext.getCmp(p_btn_download_id).iconCls).length <= 0 || Ext.getCmp(p_btn_download_id).iconCls == undefined ) {
						Ext.getCmp(p_btn_download_id).setDisabled(true);
						instance.api_check();
					}
				}
				Ext.getCmp(p_btn_download_id).onDisable= function() {
					Ext.getCmp(p_btn_download_id).setIconCls('');
				}
			}
		}
	},
	create:function(){
		var prefix = this.tfm_refprefix;
		if( prefix !== undefined ) {
			var instance = this;
			var widget = new Ext.create('Ext.form.Panel', {
				id: 'fm_form_'+prefix,
				flex:1,
				url: BASE_URL + 'fm/upload',
				fileUpload: true,
				frame: false,
				width: '100%',
				border:false,
				margins: '4 4 4 4',
				defaults: {
					anchor: '100%',
					allowBlank: true,
					msgTarget: 'side',
					labelWidth: 50
				},
				items: [{
					xtype: 'hidden',
					name: 'id_open',
					value:1
				},{
					xtype: 'hidden',
					name: 'fm_NIP_'+prefix,
					id: 'fm_NIP_'+prefix
				},{
					xtype: 'hidden',
					name: 'tfm_refid'
				},{
					xtype: 'hidden',
					name: 'tfm_refprefix'
				},{
					flex:1,
					xtype: 'fieldcontainer',
					layout: 'hbox',
					defaults: {
						hideLabel: true
					},
					combineErrors: false,
					items: [{
						xtype: 'fileuploadfield',
						name: 'fm_file',
						id: 'fm_file',
						emptyText: 'Upload Files',
						buttonText: '',
						buttonConfig: {
							iconCls: 'icon-image_add'
						},
						split:1,
						flex:6,
						listeners: {
							'change': function () {
								if( instance.get() != false ){
									if ( instance.get().getForm().isValid()) {
										instance.get().getForm().setValues({
											tfm_refid:instance.tfm_refid,
											tfm_refprefix:instance.tfm_refprefix
										});
										instance.get().getForm().submit({
											waitMsg: 'Sedang meng-upload ...',
											success: function (form, action) {
												obj = Ext.decode(action.response.responseText);
												if (obj.status == false ) {
													Ext.MessageBox.show({
														title: 'Gagal Upload !',
														msg: obj.message,
														buttons: Ext.MessageBox.OK,
														icon: Ext.MessageBox.ERROR
													});
												};
												instance.api_check();
											},
											failure: function (form, action) {
												obj = Ext.decode(action.response.responseText);
												if (obj.status == false ) {
													Ext.MessageBox.show({
														title: 'Gagal Upload !',
														msg: obj.message,
														buttons: Ext.MessageBox.OK,
														icon: Ext.MessageBox.ERROR
													});
												};
												instance.api_check();
											},
											scope: this
										});
										console.log('change file');
									}
								}
							}
						}
					}, {
						split:1,
						flex:1,
						margins:'0 0 0 4',
						xtype: 'button',
						text: 'Hapus',
						id: 'fm_hapus_'+prefix,
						tooltip: {
							text: 'Hapus Arsip Digital'
						},
						handler: function () {
							var refid = instance.tfm_refid;
							var refprefix = instance.tfm_refprefix;
							Ext.Msg.show({
								title: 'Konfirmasi',
								msg: 'Apakah Anda yakin untuk menghapus ?',
								buttons: Ext.Msg.YESNO,
								icon: Ext.Msg.QUESTION,
								fn: function (btn) {
									if (btn == 'yes') {
										if( refid != undefined && refid != 0 && refprefix != null && instance.get() != false ) {
											console.log('masuk')
											Ext.Ajax.request({
												url: BASE_URL + 'fm/delete',
												method: 'POST',
												params: {
													id_open: 1,
													tfm_refid: refid,
													tfm_refprefix: refprefix
												},
												renderer: 'data',
												success: function (response) {
													obj = Ext.decode(response.responseText);
													if (obj.status == false ) {
														Ext.MessageBox.show({
															title: 'Gagal Hapus !',
															msg: obj.message,
															buttons: Ext.MessageBox.OK,
															icon: Ext.MessageBox.ERROR
														});
													};
													instance.api_check();
												},
												failure: function (response) {
													obj = Ext.decode(response.responseText);
													if (obj.status == false ) {
														Ext.MessageBox.show({
															title: 'Gagal Hapus !',
															msg: obj.message,
															buttons: Ext.MessageBox.OK,
															icon: Ext.MessageBox.ERROR
														});
													};
													instance.api_check();
												},
												scope: this
											});
											console.log('hapus file');
										}
									}
								}
							});
						},
					}, {
						split:1,
						flex:1,
						margins:'0 0 0 4',
						xtype: 'button',
						text: 'Download',
						id: 'fm_download_'+prefix,
						target: '_blank',
						tooltip: {
							text: 'Download Arsip Digital'
						},
						handler: function () {
							instance.downloadform(BASE_URL + 'fm/download', [
								['id_open', '1'],
								['tfm_refid', instance.tfm_refid],
								['tfm_refprefix', instance.tfm_refprefix]
							]);
							console.log('download file');
						},
						listeners:{
							'active':function(){
								console.log('download button actived')
							}
						}
					}]
				}],
				listeners:{
					afterrender:function(){
						instance.listener();
					}
				}
			});
			this.set(widget);
			this.listener();
			console.log('create form');
		}
	},
	reset:function(){
		if( this.get() != false ) {
			this.get().reset();
		}
		return this;
	},
	downloadform : function(url,fields){
		if (!Ext.isArray(fields))
			return;
		var body = Ext.getBody(),
			frame = body.createChild({
				tag:'iframe',
				cls:'x-hidden',
				id:'hiddenform-iframe',
				name:'iframe'
			}),
			form = body.createChild({
				tag:'form',
				cls:'x-hidden',
				id:'hiddenform-form',
				method:'POST',
				action: url,
				target:'iframe'
			});

		Ext.each(fields, function(el,i){
			if (!Ext.isArray(el))
				return false;
			form.createChild({
				tag:'input',
				type:'text',
				cls:'x-hidden',
				id: 'hiddenform-' + el[0],
				name: el[0],
				value: el[1]
			});
		});

		form.dom.submit();

		return frame;
	}
}



var SemarFM_V2 = {
	data:{},
	tfm_refid:0,
	tfm_refprefix:undefined,
	tfm_listener:undefined,
	//private
	api_check: function(){
		var refid = this.tfm_refid;
		var refprefix = this.tfm_refprefix;
		var p_btn_download_id = 'fm_download_'+refprefix;
		
		Ext.getCmp(p_btn_download_id).setIconCls('');
		Ext.getCmp(p_btn_download_id).setDisabled(true);
		
		if( refid != undefined && refid !=  0 && refprefix != null && this.get() != false ) {
			Ext.Ajax.request({
				url: BASE_URL + 'fm/check',
				method: 'POST',
				params: {
					id_open: 1,
					tfm_refid: refid,
					tfm_refprefix: refprefix
				},
				renderer: 'data',
				success: function (response) {
					if (response.responseText) {
						Ext.getCmp(p_btn_download_id).setDisabled(false);
						if (response.responseText == 'jpeg') {
							Ext.getCmp(p_btn_download_id).setIconCls('icon-jpeg');
						} else if (response.responseText == 'docx') {
							Ext.getCmp(p_btn_download_id).setIconCls('icon-doc');
						} else {
							switch (response.responseText) {
							case 'jpg':
								Ext.getCmp(p_btn_download_id).setIconCls('icon-jpeg');
								break;
							case 'pdf':
								Ext.getCmp(p_btn_download_id).setIconCls('icon-pdf');
								break;
							case 'doc':
								Ext.getCmp(p_btn_download_id).setIconCls('icon-doc');
								break;
							case 'rar':
							case 'zip':
							case '7z':
							case 'tar':
								Ext.getCmp(p_btn_download_id).setIconCls('icon-zip');
								break;
							default:
								Ext.getCmp(p_btn_download_id).setIconCls('');
							}
						}
						
						if( new String(Ext.getCmp(p_btn_download_id).iconCls).length <= 0 || Ext.getCmp(p_btn_download_id).iconCls == undefined ) {
							Ext.getCmp(p_btn_download_id).setDisabled(true);
						}
					} else {
						Ext.getCmp(p_btn_download_id).setIconCls('');
						Ext.getCmp(p_btn_download_id).setDisabled(true);
					}
				},
				failure: function (response) {
					Ext.getCmp(p_btn_download_id).setIconCls('');
					Ext.getCmp(p_btn_download_id).setDisabled(true);
				},
				scope: this
			});
			console.log('api check');
		}
	},
	//private
	set:function(widget){
		SemarFMStack['SemarFMStack_'+this.tfm_refprefix] = widget;
		console.log('set form');
	},
	get:function(){
		if( SemarFMStack['SemarFMStack_'+this.tfm_refprefix] != undefined ) {
			console.log('get form');
			return SemarFMStack['SemarFMStack_'+this.tfm_refprefix];
		}
		return false;
	},
	init:function(temp){
		if( typeof(temp) == 'object' ) {
			if( temp.hasOwnProperty('listener')) {
				this.tfm_listener = temp['listener'];
			}
			if( temp.hasOwnProperty('prefix')) {
				this.tfm_refprefix = temp['prefix'];
				this.set(undefined);
			}
			if( temp.hasOwnProperty('id')) {
				this.tfm_refid = temp['id'];
			}
			this.create();
			console.log('init form');
		}
		return this;
	},
	enable:function(){
		if( this.get() != false ) {
			semarDeactiveAll( this.get(), false );
			console.log('enable form');
		}
		return this;
	},
	disable:function(){
		if( this.get() != false ) {
			semarDeactiveAll( this.get(), true );
			console.log('disable form');
		}
		return this;
	},
	download:function(){
		console.log('download form');
		return this;
	},
	//private
	listener:function(){
		var instance = this;
		
		if( Ext.getCmp(this.tfm_listener) !== undefined ) {
			Ext.getCmp(this.tfm_listener).on('change', function(v,x){
				instance.tfm_refid = Ext.getCmp(instance.tfm_listener).getValue();
				instance.api_check();
			});
			Ext.getCmp(this.tfm_listener).fireEvent('change', true);
		}
		
		if( this.get() != false ) {
			if( Ext.getCmp('fm_download_'+this.tfm_refprefix) ) {
				var p_btn_download_id = 'fm_download_'+this.tfm_refprefix;
				Ext.getCmp(p_btn_download_id).onEnable = function() {
					if( new String(Ext.getCmp(p_btn_download_id).iconCls).length <= 0 || Ext.getCmp(p_btn_download_id).iconCls == undefined ) {
						Ext.getCmp(p_btn_download_id).setDisabled(true);
						instance.api_check();
					}
				}
				Ext.getCmp(p_btn_download_id).onDisable= function() {
					Ext.getCmp(p_btn_download_id).setIconCls('');
				}
			}
		}
	},
	create:function(){
		var prefix = this.tfm_refprefix;
		if( prefix !== undefined ) {
			var instance = this;
			var widget = new Ext.create('Ext.form.Panel', {
				id: 'fm_form_'+prefix,
				flex:1,
				url: BASE_URL + 'fm/upload',
				fileUpload: true,
				frame: false,
				width: '100%',
				border:false,
				margins: '4 4 4 4',
				defaults: {
					anchor: '100%',
					allowBlank: true,
					msgTarget: 'side',
					labelWidth: 50
				},
				items: [{
					xtype: 'hidden',
					name: 'id_open',
					value:1
				},{
					xtype: 'hidden',
					name: 'fm_NIP_'+prefix,
					id: 'fm_NIP_'+prefix
				},{
					xtype: 'hidden',
					name: 'tfm_refid'
				},{
					xtype: 'hidden',
					name: 'tfm_refprefix'
				},{
					flex:1,
					xtype: 'fieldcontainer',
					layout: 'hbox',
					defaults: {
						hideLabel: true
					},
					combineErrors: false,
					items: [{
						xtype: 'fileuploadfield',
						name: 'fm_file',
						id: 'fm_file',
						emptyText: 'Upload Files',
						buttonText: '',
						buttonConfig: {
							iconCls: 'icon-image_add'
						},
						split:1,
						flex:6,
						listeners: {
							'change': function () {
								if( instance.get() != false ){
									if ( instance.get().getForm().isValid()) {
										instance.get().getForm().setValues({
											tfm_refid:instance.tfm_refid,
											tfm_refprefix:instance.tfm_refprefix
										});
										instance.get().getForm().submit({
											waitMsg: 'Sedang meng-upload ...',
											success: function (form, action) {
												obj = Ext.decode(action.response.responseText);
												if (obj.status == false ) {
													Ext.MessageBox.show({
														title: 'Gagal Upload !',
														msg: obj.message,
														buttons: Ext.MessageBox.OK,
														icon: Ext.MessageBox.ERROR
													});
												};
												instance.api_check();
											},
											failure: function (form, action) {
												obj = Ext.decode(action.response.responseText);
												if (obj.status == false ) {
													Ext.MessageBox.show({
														title: 'Gagal Upload !',
														msg: obj.message,
														buttons: Ext.MessageBox.OK,
														icon: Ext.MessageBox.ERROR
													});
												};
												instance.api_check();
											},
											scope: this
										});
										console.log('change file');
									}
								}
							}
						}
					}, {
						split:1,
						flex:1,
						margins:'0 0 0 4',
						xtype: 'button',
						text: 'Hapus',
						id: 'fm_hapus_'+prefix,
						tooltip: {
							text: 'Hapus Arsip Digital'
						},
						handler: function () {
							var refid = instance.tfm_refid;
							var refprefix = instance.tfm_refprefix;
							Ext.Msg.show({
								title: 'Konfirmasi',
								msg: 'Apakah Anda yakin untuk menghapus ?',
								buttons: Ext.Msg.YESNO,
								icon: Ext.Msg.QUESTION,
								fn: function (btn) {
									if (btn == 'yes') {
										if( refid != undefined && refid != 0 && refprefix != null && instance.get() != false ) {
											console.log('masuk')
											Ext.Ajax.request({
												url: BASE_URL + 'fm/delete',
												method: 'POST',
												params: {
													id_open: 1,
													tfm_refid: refid,
													tfm_refprefix: refprefix
												},
												renderer: 'data',
												success: function (response) {
													obj = Ext.decode(response.responseText);
													if (obj.status == false ) {
														Ext.MessageBox.show({
															title: 'Gagal Hapus !',
															msg: obj.message,
															buttons: Ext.MessageBox.OK,
															icon: Ext.MessageBox.ERROR
														});
													};
													instance.api_check();
												},
												failure: function (response) {
													obj = Ext.decode(response.responseText);
													if (obj.status == false ) {
														Ext.MessageBox.show({
															title: 'Gagal Hapus !',
															msg: obj.message,
															buttons: Ext.MessageBox.OK,
															icon: Ext.MessageBox.ERROR
														});
													};
													instance.api_check();
												},
												scope: this
											});
											console.log('hapus file');
										}
									}
								}
							});
						},
					}, {
						split:1,
						flex:1,
						margins:'0 0 0 4',
						xtype: 'button',
						text: 'Download',
						id: 'fm_download_'+prefix,
						target: '_blank',
						tooltip: {
							text: 'Download Arsip Digital'
						},
						handler: function () {
							instance.downloadform(BASE_URL + 'fm/download', [
								['id_open', '1'],
								['tfm_refid', instance.tfm_refid],
								['tfm_refprefix', instance.tfm_refprefix]
							]);
							console.log('download file');
						},
						listeners:{
							'active':function(){
								console.log('download button actived')
							}
						}
					}]
				}],
				listeners:{
					afterrender:function(){
						instance.listener();
					}
				}
			});
			this.set(widget);
			this.listener();
			console.log('create form');
		}
	},
	reset:function(){
		if( this.get() != false ) {
			this.get().reset();
		}
		return this;
	},
	downloadform : function(url,fields){
		if (!Ext.isArray(fields))
			return;
		var body = Ext.getBody(),
			frame = body.createChild({
				tag:'iframe',
				cls:'x-hidden',
				id:'hiddenform-iframe',
				name:'iframe'
			}),
			form = body.createChild({
				tag:'form',
				cls:'x-hidden',
				id:'hiddenform-form',
				method:'POST',
				action: url,
				target:'iframe'
			});

		Ext.each(fields, function(el,i){
			if (!Ext.isArray(el))
				return false;
			form.createChild({
				tag:'input',
				type:'text',
				cls:'x-hidden',
				id: 'hiddenform-' + el[0],
				name: el[0],
				value: el[1]
			});
		});

		form.dom.submit();

		return frame;
	}
}
<?php }else{ echo "var newpanel = 'GAGAL';"; } ?>