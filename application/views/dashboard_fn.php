<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

function mergeObj(obj1, obj2) {
	for (var p in obj2) {
		try {
			if ( obj2[p].constructor==Object ) {
				obj1[p] = MergeRecursive(obj1[p], obj2[p]);
			} else {
				obj1[p] = obj2[p];
			};
		} catch(e) {
			obj1[p] = obj2[p];
		};
	};

	return obj1;
};

//dynamic model
var modelFactoryArrayMerger = function merge(set1, set2){
	for (var key in set2){
		if (set2.hasOwnProperty(key))
			set1[key] = set2[key]
		}
	return set1
};

var modelFactoryRemoveData = function(cnfg){
	var url = ( cnfg['url'] ? cnfg['url'] : '' );
	var data = ( cnfg['data'] ? cnfg['data'] : '' );
	var grid = ( cnfg['grid'] ? cnfg['grid'] : '' );
	var param = ( cnfg['param'] ? cnfg['param'] : {} );
	Ext.Msg.show({
		title: 'Konfirmasi', 
		msg: 'Apakah Anda yakin untuk menghapus ?',
		buttons: Ext.Msg.YESNO, 
		icon: Ext.Msg.QUESTION,
		fn: function(btn) {
			if (btn == 'yes') {
				param['postdata'] = data;
				Ext.Ajax.request({
					url: url, 
					method: 'POST',
					params: param,
					success: function(response){
						grid.load();
					},
					failure: function(response){ 
						Ext.MessageBox.show({title:'Peringatan !', msg: obj.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
					}
				});
			}
		}
	});
};
var modelFactoryProsesData = function(cnfg){
	var url = ( cnfg['url'] ? cnfg['url'] : '' );
	var data = ( cnfg['data'] ? cnfg['data'] : {} );
	var grid = ( cnfg['grid'] ? cnfg['grid'] : '' );
	data['id_open'] = 1;
	
	Ext.Msg.show({
		title: 'Konfirmasi', 
		msg: 'Apakah Anda yakin untuk melakukan Proses terhadap data ini ?',
		buttons: Ext.Msg.YESNO, 
		icon: Ext.Msg.QUESTION,
		fn: function(btn) {
			if (btn == 'yes') {
				Ext.Ajax.request({
					url: url, 
					method: 'POST',
					params: data,
					success: function(response){
						grid.load();
					},
					failure: function(response){ 
						Ext.MessageBox.show({title:'Peringatan !', msg: obj.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
					}
				});
			}
		}
	});
};
var modelFactoryDetailLoader = function(cnfg){
	var primary = ( cnfg['primary'] ? cnfg['primary'] : '' );
	var url = ( cnfg['url'] ? cnfg['url'] : '' );
	var params = ( cnfg['params'] ? cnfg['params'] : {} );
	var component = ( cnfg['component'] ? cnfg['component'] : {} );
	var callback = ( cnfg['callback'] ? cnfg['callback'] : undefined );
	var renderer = ( cnfg['renderer'] ? cnfg['renderer'] : false );
	
	params['id_open'] = 1;
	
	var TempGrid_Detail = component.getGrid();
	var TempGrid_Detail_Store = component.getStore();
	var TempGrid_Detail_Model = component.getModel();
	
    TempGrid_Detail.setLoading("Loading...");
    TempGrid_Detail_Store.removeAll();
    Ext.Ajax.request({
        url: url, 
        method: 'POST', 
        params: params, 
        renderer: ( renderer != false ? renderer : 'data' ),
        success: function(resp){
			TempGrid_Detail_Store.removeAll();
			var obj = Ext.decode( resp.responseText );
			if( renderer != false ) {
				obj = obj[renderer];
			}
			TempGrid_Detail.setLoading(false);
			for(var x in obj){
				if( Ext.isNumeric( x ) ) {
					TempGrid_Detail_Store.add( obj[x] );
				}
			};
			if( callback != undefined ) {
				callback( component );
			}
        },
        failure: function(resp){
            TempGrid_Detail_Store.removeAll();
            TempGrid_Detail.setLoading(false);
            Ext.MessageBox.show({title:'Peringatan !', msg: resp.responseText, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
        },
        scope : this
    });
}
var modelFieldRec = function( d ){
	var data = [];
	Ext.Array.each(d, function (name, index, resultfields) {
		Ext.Array.each( Object.keys(name), function(n){
			data.push({name:n})
		});
	});
	return JSON.stringify(data);
};

var modelFactoryDetailFindStore = function(tempName){
    var storeName = 'Store_Temp_' + tempName;
	return Ext.getStore(storeName);
}

var modelFactoryDetailFindStore2 = function(tempName){
    var storeName = 'Store_' + tempName;
	return Ext.getStore(storeName);
}

var modelFactoryDetailFindGrid = function(tempName){
    var gridName = 'Grid_Temp_' + tempName;
	return Ext.getCmp(gridName);
}

var modelFactoryDetailPluck = function(tempName){
	var result = [];
	var tempStore = modelFactoryDetailFindStore(tempName);
	if( tempStore ) {
		result = Ext.encode(Ext.pluck(tempStore.data.items, 'data'));
	}
	return result;
}

var modelFactoryDetailWithForm = function(cnfg){
	var tempSingleDependency = ( cnfg['singleDependency'] || false );
	var tempForming = ( cnfg['forming'] || false );
	var tempButton  = ( cnfg['button'] || false );
	var tempGridLS	= ( cnfg['dsgridls'] || false );
	var tempData  	= ( cnfg['data'] || {} );
	var tempName  	= ( cnfg['name'] || 'semarketir' );
	var tempField 	= ( cnfg['field'] || [] );
	var tempTitle 	= ( cnfg['title'] || '-' );
	var tempConfigs	= ( cnfg['config'] || {} );
	var tempGridInit= ( cnfg['initgrid'] || {} );
	
    var gridName  = 'Grid_Temp_' + tempName;
    var windowName  = 'Window_Temp_' + tempName;
    var formName  = 'Form_Temp_' + tempName;
    var fieldSparotName  = 'Field_Temp_' + tempName + '_';
	
    var tempFactoryReset = modelFactory( tempName, tempField, tempConfigs );
	
	var formFields = [];
    Ext.Array.each(tempField, function (name, index, resultfields) {
		var statusPush = false;
        var this_formFields = {
			name: name.name,
			flex:1,
			anchor: '100%',
			labelAlign:'top',
			id: fieldSparotName + name.name
		};
		
		if( name['default'] ) {
			this_formFields['value'] = name['default'];
		}
		
		if( name['text'] ) {
			this_formFields['allowBlank'] = ( name['blank'] ? name['blank'] : false );
			this_formFields['format']	  = ( name['format'] ? name['format'] : '' );
			
			this_formFields['fieldLabel'] = name['text'];
			
			if( name['editor'] ) {
				this_formFields = modelFactoryArrayMerger( this_formFields, name['editor'] );
			};
			
			if( this_formFields['xtype'] ) { 
				statusPush = true;
			}
		}
        
		if(statusPush){
			formFields.push(this_formFields);
		}
    });
	
	var gridListener ={
		selectionchange: function(model, records) {
			if (records[0]) {
			}
		},
		itemdblclick:function(a,b,c,d,e){
			var obj = Ext.getCmp(gridName).getSelectionModel().getSelection();
			if( obj.length > 0 ) {
				Temp_Detail_Window.show();
				Ext.getCmp(formName).getForm().reset();
				Ext.getCmp(formName).getForm().loadRecord(obj[0]);
			}
		}		  
	};
	var Temp_Detail_Grid_Config = {
		id: gridName,
		itemId: gridName,
		flex:1,
		forceFit:true,
		columnLines: true, 
		selModel: new Ext.create('Ext.selection.CheckboxModel'),
		title:tempTitle,
		region:'center',
		store: tempFactoryReset.getStore(),
		columns: tempFactoryReset.getColumn(),
		dockedItems: [
			{
				store: tempFactoryReset.getStore(),
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				flex: 1,
				displayInfo: true
			}
		]
	};
	
	Temp_Detail_Grid_Config = mergeObj( Temp_Detail_Grid_Config, tempGridInit );
	
	if( tempGridLS == false ) {
		Temp_Detail_Grid_Config['listeners'] = gridListener;
	}
	
	if(Ext.getCmp(windowName)){
		Ext.getCmp(windowName).destroy();
	}
	var Temp_Detail_Window = Ext.create('Ext.window.Window', {
		title: tempTitle,
		modal:'true',
		id: windowName,
		itemId: windowName,
		bodyStyle:'padding:10px;',
		height: Scr_Height / 2,
		width: Scr_Width / 2,
		layout: 'fit',
		closeAction:'hide',
		items: [{
			xtype:'form',
			id: formName,
			itemId: formName,
			flex:1,
			bodyPadding: 10,
			autoScroll:true,
			items: formFields,
			buttons:[
				'->',{
					text:'Simpan',
					iconCls:'icon-save',
					formBind: true,
					disabled: true,
					handler:function(){
						var saveButton = this;
						saveButton.setDisabled(true);
						var form = this.up('form').getForm();
						if (form.isValid()) {
							if(tempForming['urldata']) {
								if(tempForming['urldata']['post']){
									Ext.getBody().mask('Loading..');
									form.submit({
										url:tempForming['urldata']['post'],
										params:tempData,
										success: function(form, action) {
											Ext.getBody().unmask('Loading..')
											saveButton.setDisabled(false);
											if( Ext.isNumeric( action.result.info.reason ) ) {
												Ext.Msg.alert('Success', 'Sukses Memanipulasi Data');
											}else{
												Ext.Msg.alert('Success', action.result.info.reason);
											};
											Ext.getCmp(windowName).hide();
											tempFactoryReset.getStore().load();
										},
										failure: function(form, action) {
											Ext.getBody().unmask('Loading..')
											saveButton.setDisabled(false);
											Ext.Msg.alert('Failed', action.result.info.reason);
										}
									});
								}
							}
						}
					}
				},{
					text:'Batal',
					handler:function(){
						Ext.getCmp(windowName).hide();
					}
				},
			]
		}]
	});
	if( tempForming != false ) {
		if( tempForming['button'] && tempForming['urldata'] ) {
			Temp_Detail_Grid_Config['tbar'] = [];
			if( tempForming['button']['insert'] == true ) {
				Temp_Detail_Grid_Config['tbar'].push( {
					iconCls:'icon-add',
					text:'Tambah',
					handler:function(){
						if( tempData[tempSingleDependency] ) {
							Temp_Detail_Window.show();
							Ext.getCmp(formName).getForm().reset();
						}
					}
				});
			}
			if( tempForming['button']['update'] == true ) {
				Temp_Detail_Grid_Config['tbar'].push({
					iconCls:'icon-edit',
					text:'Update',
					handler:function(){
						if( tempData[tempSingleDependency] ) {
							Ext.getCmp( gridName ).fireEvent('itemdblclick')
						}
					}
				});
			}
			if( tempForming['button']['remove'] == true ) {
				Temp_Detail_Grid_Config['tbar'].push( '->' );
				Temp_Detail_Grid_Config['tbar'].push( {
					iconCls:'icon-delete',
					text:'Hapus',
					handler:function(){
						if( tempData[tempSingleDependency] ) {
							var obj = Ext.getCmp( gridName ).getSelectionModel().getSelection();
							if( obj.length > 0 ) {
								var data = '';
								for (i = 0; i < obj.length; i++) {
									data = data + obj[i].get( tempFactoryReset.getPrimary() ) + '-';
								};
								modelFactoryRemoveData({
									url: tempForming['urldata']['destroy'],
									data: data,
									param:tempData,
									grid: tempFactoryReset.getStore()
								});
							}
						}
						
					}
				});
			}
		}
	}
	
	var Temp_Detail_Grid = Ext.create('Ext.grid.Panel', Temp_Detail_Grid_Config );
	
	return {
		getGrid: function(){
			return Temp_Detail_Grid;
		},
		getStore: function(){
			return tempFactoryReset.getStore();
		},
		getModel: function(){
			return tempFactoryReset.getModel();
		},
		getColumn: function(){
			return tempFactoryReset.getColumn();
		},
		getData: function(){
			return [];
		}
	}
};

var modelFactoryDetail = function(cnfg){
	var tempButton  = ( cnfg['button'] || false );
	var tempData  	= ( cnfg['data'] || [] );
	var tempName  	= ( cnfg['name'] || 'semarketir' );
	var tempField 	= ( cnfg['field'] || [] );
	var tempTitle 	= ( cnfg['title'] || '-' );
	
    var modelName = 'Model_Temp_' + tempName;
    var storeName = 'Store_Temp_' + tempName;
    var gridName  = 'Grid_Temp_' + tempName;
	
    var modelFields 		= [];
    var modelFieldsDefault 	= {};
    var columnFields 		= [];
	
    Ext.Array.each(tempField, function (name, index, resultfields) {
        var this_modelField 		= {};
        var this_columnField		= {};
		
        this_modelField['mapping'] 		= name.name;
        this_modelField['name'] 		= name.name;
		
		if( name['default'] ) {
			modelFieldsDefault[ name.name ] = name['default'];
		}
		
		if( name['text'] ) {
			var blank = ( name['blank'] ? name['blank'] : false );
			var format = ( name['format'] ? name['format'] : '' );
			
			this_columnField['text'] 		= name['text'];
			this_columnField['dataIndex'] 	= name.name;
			if( name['xtype'] ) { 
				this_columnField['xtype'] 	= name['xtype'];
			};
			
			if( name['editor'] ) {
				this_columnField['editor'] = name['editor'];
			};
			
			if( name['renderer'] ) {
				this_columnField['renderer'] = function(value, metaData, record, row, col, store, gridView){
					return name['renderer'](value, metaData, record, row, col, store, gridView);
				}
			};
			
			columnFields.push(this_columnField);
		}
        
		modelFields.push(this_modelField);
    });

	Ext.define( modelName, {
		extend: 'Ext.data.Model',
		fields: modelFields
	});
	
	var Temp_Detail_Data = { data: tempData };

	var Temp_Detail_Store = Ext.create('Ext.data.Store', {
		storeId: storeName,
		autoLoad: true,
		model: modelName,
		data: Temp_Detail_Data,
		proxy: {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'data'
			}
		}
	});
	
	var Temp_Detail_Grid_Config = {
		id: gridName,
		itemId: gridName,
		flex:1,
		forceFit:true,
		title:tempTitle,
		region:'center',
		store: Temp_Detail_Store,
		columns: columnFields,
		listeners: {
		  selectionchange: function(model, records) {
			  if (records[0]) {
			  }
		  }   
		},
		plugins: [
			Ext.create('Ext.grid.plugin.CellEditing', {
				clicksToEdit: 1
			})
		]
	};
	if( tempButton ) {
		Temp_Detail_Grid_Config['tbar'] = [
			{
				iconCls:'icon-add',
				text:'Tambah',
				handler:function(){
					var r = Ext.create( modelName, modelFieldsDefault);
					Temp_Detail_Store.insert(0, r);
				}
			},'->',{
				iconCls:'icon-delete',
				text:'Hapus',
				handler:function(){
					Temp_Detail_Store.remove(Temp_Detail_Grid.getSelectionModel().getSelection());
				}
			}
		];
	}
	
	var Temp_Detail_Grid = Ext.create('Ext.grid.Panel', Temp_Detail_Grid_Config );
	
	if( cnfg['onEdit'] ) {
		Temp_Detail_Grid.on('edit', function(editor, e) {
			cnfg['onEdit'](editor, e);
			e.record.commit();
		});
	}
	
	return {
		getGrid: function(){
			return Temp_Detail_Grid;
		},
		getStore: function(){
			return Temp_Detail_Store;
		},
		getModel: function(){
			return modelName;
		},
		getColumn: function(){
			return columnFields;
		},
		getData: function(){
			return Temp_Detail_Data;
		}
	}
};

var modelFactory = function ( name, fields, configs ) {
	
	var urlService 		= ( configs['url'] ? configs['url'] : '' )
		, method 		= ( configs['method'] ? configs['method'] : 'POST' )
		, extraParams 	= ( configs['params'] ? configs['params'] : {} )
		, root 			= ( configs['root'] ? configs['root'] : 'results' )
		, total 		= ( configs['total'] ? configs['total'] : 'total' )
		, primary 		= ( configs['primary'] ? configs['primary'] : 'id' );
	
	extraParams['id_open'] = 1;

    //create dynamic columns
    var modelFields = [];
    var columnFields = [];

    Ext.Array.each(fields, function (name, index, resultfields) {
        var this_modelField = {};
        var this_columnField = {};
		
        this_modelField['mapping'] 		= name.name;
        this_modelField['name'] 		= name.name;
		
		if( name['text'] && !name['hideOnGrid']) {
			this_columnField['text'] 		= name['text'];
			this_columnField['dataIndex'] 	= name.name;
			if( name['xtype'] ) { 
				this_columnField['xtype'] 	= name['xtype'];
			};
			columnFields.push(this_columnField);
		}
		
		if( name['pk'] ) {
			primary = name.name;
		};
        
		modelFields.push(this_modelField);
    });
    //define names
    var modelName = 'Model_' + name;
    var storeName = 'Store_' + name;

    //create model
    Ext.define(modelName, {
        extend: 'Ext.data.Model',
        fields: modelFields
    });
    //create store
	
    var store = Ext.create('Ext.data.Store', {
		storeId:storeName,
        model: modelName,
        proxy: {
            type: 'ajax',
			actionMethods: {
				read: method
			},
			extraParams:extraParams,
            url: urlService,
			reader:new Ext.create('Ext.data.JsonReader', {
				root: root, 
				totalProperty: total, 
				idProperty: primary 	
			}),
        },
        autoLoad: true
    });
    return {
		getPrimary: function(){
			return primary;
		},
		getModel: function(){
			return modelName;
		},
		getStore: function(){
			return store;
		},
		getColumn: function(){
			return columnFields;
		}
	};
};
//end dynamic model

var CallBack_Inc_Fn = 0;
var CallBack_Fn_Stack = {};
function Load_Popup_Assign(popup_id, popup_url, paramsData) {
	CallBack_Inc_Fn += 1;
	Ext.getCmp('layout-body').body.mask("Loading Assign...", "x-mask-loading");
	var new_popup_id = Ext.getCmp(popup_id);
	if(new_popup_id){
		Ext.getCmp('layout-body').body.unmask(); new_popup.show(); 
	}else{
		if(Object.keys(paramsData).indexOf('onSuccess') > -1){
		    var Success_Name_Fn = 'CallBack_Inc_Fn_'+CallBack_Inc_Fn+'_onSuccess';
		    CallBack_Fn_Stack[Success_Name_Fn] = paramsData['onSuccess'];
		    paramsData['onSuccess'] = Success_Name_Fn;
		}
		if(Object.keys(paramsData).indexOf('onError') > -1){
		    var Error_Name_Fn = 'CallBack_Inc_Fn_'+CallBack_Inc_Fn+'_onError';
		    CallBack_Fn_Stack[Error_Name_Fn] = paramsData['onError'];
		    paramsData['onError'] = Error_Name_Fn;
		}
		paramsData['CallBack_Inc_Fn'] = CallBack_Inc_Fn;
		Ext.Ajax.timeout = Time_Out;
		Ext.Ajax.request({
			url: popup_url, 
			method: 'POST', 
			params: {id_open: 1, params: Ext.encode(paramsData)}, 
			scripts: true, 
			renderer: 'data',
			success: function(response){
				var jsonData = response.responseText; var aHeadNode = document.getElementsByTagName('head')[0]; 
				var aScript = document.createElement('script'); 
				aScript.text = jsonData; aHeadNode.appendChild(aScript);
				var new_popup = Ext.getCmp(popup_id);
				if(new_popup != "GAGAL" && new_popup){
					new_popup.show();
				}else{
					Ext.MessageBox.show({title:'Peringatan !', msg:'Anda tidak dapat mengakses ke halaman ini !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});    			
				}    	
			},
			failure: function(response){ Ext.MessageBox.show({title:'Peringatan !', msg:'Gagal memuat dokumen !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); }, 
			callback: function(response){ Ext.getCmp('layout-body').body.unmask(); },
			scope : this
		});
	}
}
function semarDeactiveAllX(fr, st){
	fr.items.each(function(itm){
		if( itm.xtype == 'fieldcontainer' || itm.xtype == 'fieldset' || itm.xtype == 'form' ){
			semarDeactiveAll(itm, st);
		}else if( itm.xtype == 'button' || itm.xtype == 'fileuploadfield'){
			itm.setDisabled(st)
		}else if( itm.xtype != 'label' &&  itm.xtype != 'panel' ){
			if( Ext.isFunction( itm.setReadOnly ) ){
				itm.setReadOnly(st);
			};
		}else{
			semarDeactiveAllX(itm, st);
		}
	});
};
function semarDeactiveAll(fr, st){
	fr.items.each(function(itm){
		if( itm.xtype == 'fieldcontainer' || itm.xtype == 'fieldset' || itm.xtype == 'form' ){
			semarDeactiveAll(itm, st);
		}else if( itm.xtype == 'button' || itm.xtype == 'fileuploadfield'){
			itm.setDisabled(st)
		}else if( itm.xtype != 'label'){
			if( Ext.isFunction( itm.setReadOnly ) ){
				itm.setReadOnly(st);
			};
		}
	});
};

var eXML_Widget_Stack = {};
Ext.define('eXML.widget.notifikasi', {
    alias: 'widget.exmlwidgetnotifikasi',
    extend:'Ext.panel.Panel',
	layout: 'fit',
	flex:1,
	config: {
		identifyFor : undefined,
		whenSetValue : function( val ){ }
	},
	constructor:function(config) {
		this.callParent(arguments);
		this.initConfig(config);
		return this;
	},
	showMaskWidget : function(){
		if( this.getInstance() ) {
			this.getInstance().up().body.mask('Loading...');
		}
	},
	hideMaskWidget : function(){
		if( this.getInstance() ) {
			this.getInstance().up().body.unmask();
		}
	},
	getInstance : function(){
		if( this.identifyFor != undefined ) {
			return eXML_Widget_Stack[ this.identifyFor ]
		}
		return false;
	},
	renderWidget : function () {
		if( this.identifyFor != undefined ) {
			eXML_Widget_Stack[ this.identifyFor ] = this;
		}
	}
});

function Load_As_Widget( requestId, identify ) {

	if( Ext.getCmp('widget_exml_id') ) {
		Ext.getCmp('widget_exml_id').destroy();
	};

	var Load_As_Widget_Page = function( urlx ) {
		Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
		
		Ext.Ajax.timeout = Time_Out;
		Ext.Ajax.request({
			url: BASE_URL + urlx , method: 'POST', params: {id_open: 1, request_widget : 1}, scripts: true, renderer: 'data',
			success: function(response){
				var jsonData = response.responseText; 
				var aHeadNode = document.getElementsByTagName('head')[0]; 
				var aScript = document.createElement('script'); 
				aScript.text = jsonData; aHeadNode.appendChild(aScript);
				if(new_tabpanel != "GAGAL"){
					if( eXML_Widget_Stack[identify] ) {
						if( Ext.getCmp('widget_exml_id') ) {
							Ext.getCmp('widget_exml_id').destroy();
						};
						Ext.create('Ext.window.Window', {
							title: 'Widget - Modal',
							modal:'true',
							maximizable:true,
							id:'widget_exml_id',
							itemId:'widget_exml_id',
							bodyStyle:'padding:10px;',
							height: Scr_Height / 1.5,
							width: Scr_Width / 1.5,
							layout: 'fit',
							items: eXML_Widget_Stack[identify]
						}).show();
						eXML_Widget_Stack[identify].whenSetValue(requestId);
					};
				}else{
					Ext.MessageBox.show({title:'Peringatan !', msg:'Anda tidak dapat mengakses ke widget ini !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});    			
				}
				Ext.getCmp('layout-body').body.unmask();			
			},
			failure: function(response){
				Ext.getCmp('layout-body').body.unmask();
				Ext.MessageBox.show({title:'Peringatan !', msg:'Gagal memuat widget !', 
					buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
			}, 
			callback: function(response){
				Ext.getCmp('layout-body').body.unmask();
			},
			scope : this
		});
	}
	
	Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
	
	Ext.Ajax.timeout = Time_Out;
	Ext.Ajax.request({
		url: BASE_URL + 'widget' , method: 'POST', params: {id_open: 1, identify : identify}, scripts: true, renderer: 'data',
		success: function(response){
			var jsonData = response.responseText; 
			if(jsonData != "GAGAL"){
				Load_As_Widget_Page(jsonData);
			}else{
				Ext.getCmp('layout-body').body.unmask();	
				Ext.MessageBox.show({title:'Peringatan !', msg:'Anda tidak dapat mengakses ke widget ini !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});    			
			}		
		},
		failure: function(response){
			Ext.getCmp('layout-body').body.unmask();
			Ext.MessageBox.show({title:'Peringatan !', msg:'Gagal memuat widget !', 
				buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
		}, 
		callback: function(response){
		},
		scope : this
	});
};

function Load_BrowseRef( targetId, modulecode, callbackBroseRef ) {
	Ext.getCmp( targetId ).removeAll();
	Ext.getCmp( targetId ).body.mask("Loading...", "x-mask-loading");
	console.log( targetId );
	
	var browserefParamsTemp = Ext.clone( Ext.getCmp( targetId ).browserefParams );
	var browserefParams = doManualParams( browserefParamsTemp );
	var paramsBrowseref = {
		id_open: 1
	};
	if( browserefParams != undefined ){
		paramsBrowseref['browserefParams'] = browserefParams;
	};
	Ext.Ajax.timeout = Time_Out;
	Ext.Ajax.request({
		url: BASE_URL + modulecode + '/widget_browseref',
		method: 'POST',
		params: paramsBrowseref,
		scripts: true,
		renderer: 'data',
		success: function(response) {
			var jsonData = response.responseText;
			var aHeadNode = document.getElementsByTagName('head')[0];
			var aScript = document.createElement('script');
			aScript.text = jsonData;
			aHeadNode.appendChild(aScript);
			var new_tab = Ext.getCmp( targetId );
			if ( new_tabpanel_browseref != "GAGAL") {
				new_tab.add( new_tabpanel_browseref ).show();
				Ext.getCmp( targetId ).doLayout();
				
				if( browserefParams != undefined ) {
					Ext.getCmp( targetId )
						.down('[xtype=grid]')
						.getStore()
						.getProxy()
						.extraParams['browserefParams'] = browserefParams;
				};
				
				if ( Ext.isFunction( callbackBroseRef ) ){
					callbackBroseRef();
				};
			} else {
				Ext.MessageBox.show({
					title: 'Peringatan !',
					msg: 'Terjadi Kesalaha Di Browse Ref!',
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
			}
		},
		failure: function(response) {
			Ext.MessageBox.show({
				title: 'Peringatan !',
				msg: 'Gagal memuat dokumen !',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.ERROR
			});
		},
		callback: function(response) {
			Ext.getCmp( targetId ).body.unmask();
		},
		scope: this
	});
};
function Load_BrowseRef_Bak( targetId, modulecode, callbackBroseRef ) {
	Ext.getCmp( targetId ).removeAll();
	Ext.getCmp( targetId ).body.mask("Loading...", "x-mask-loading");
	
	Ext.Ajax.timeout = Time_Out;
	Ext.Ajax.request({
		url: BASE_URL + modulecode + '/widget_browseref',
		method: 'POST',
		params: {
			id_open: 1
		},
		scripts: true,
		renderer: 'data',
		success: function(response) {
			var jsonData = response.responseText;
			var aHeadNode = document.getElementsByTagName('head')[0];
			var aScript = document.createElement('script');
			aScript.text = jsonData;
			aHeadNode.appendChild(aScript);
			var new_tab = Ext.getCmp( targetId );
			if ( new_tabpanel_browseref != "GAGAL") {
				new_tab.add( new_tabpanel_browseref ).show();
				Ext.getCmp( targetId ).doLayout();
				
				if ( Ext.isFunction( callbackBroseRef ) ){
					callbackBroseRef();
				};
			} else {
				Ext.MessageBox.show({
					title: 'Peringatan !',
					msg: 'Terjadi Kesalaha Di Browse Ref!',
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
			}
		},
		failure: function(response) {
			Ext.MessageBox.show({
				title: 'Peringatan !',
				msg: 'Gagal memuat dokumen !',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.ERROR
			});
		},
		callback: function(response) {
			Ext.getCmp( targetId ).body.unmask();
		},
		scope: this
	});
};

var Generic_Semar_Icon_Combo = function( callbackChange, cnfgX ){
	var newCombo = {
		flex:1,
		xtype: 'combobox',
		valueField: 'name',
		displayField: 'name',
		emptyText: 'Pilih Icons',
		typeAhead: true,
		value:'icon-gears',
		forceSelection: false,
		selectOnFocus: true,
		valueNotFoundText: 'Pilih Icons',
		queryMode: 'local',
		store: new Ext.data.Store({
			noCache: false,
			autoLoad: true,
			proxy: {
				type: 'ajax',
				actionMethods: {
					read: 'GET'
				},
				extraParams: {
					id_open: '1',
					combox: '1'
				},
				url: BASE_URL + 'kontrol_menu/csscombo',
				reader: {
					type: 'json',
					root: 'results',
					totalProperty: 'total',
					idProperty: 'name'
				}
			},
			fields: ['name', 'class', 'bgic', 'file']
		}),
		grow:true,
		listConfig: {
			getInnerTpl: function() {
				var tpl = '<div><img src="{file}" align="left" style="width:20px;height:20px;" />&nbsp;&nbsp;{name}</div>';
				return tpl;
			}
		},
		listeners: {
			'focus': {
				fn: function (comboField) {
					comboField.doQuery(comboField.allQuery, true);
					comboField.expand();
				},
				scope: this
			},
			'change': {
				fn: function ( comboField, newValue ) {
					callbackChange( comboField, newValue );
				},
				scope: this
			}
		}
	};
	newCombo = mergeObj( newCombo, cnfgX );
	return newCombo;
};

var doManualParams = function( tempParams ){
	var result = undefined;
	if (tempParams) {
		if (tempParams.length > 0) {
			while (!Ext.isArray(tempParams)) {
				tempParams = Ext.decode(tempParams);
			};
			if (Ext.isArray(tempParams)) {
				tempParams = tempParams;
				for (var pr in tempParams) {
					if (Ext.isNumeric(pr)) {
						if (tempParams[pr]) {
							if (tempParams[pr]['set'] == 'get') {
								if (Ext.getCmp(tempParams[pr]['value'])) {
									tempParams[pr]['value'] = Ext.getCmp(tempParams[pr]['value']).getValue();
								};
							}else if( tempParams[pr]['set'] == 'grid' ){
								var tempSetValFromGrid = tempParams[pr]['value'].split('=');
								if( tempSetValFromGrid.length == 2 ){
									tempParams[pr]['value'] = 
										doGetGridSelectionSingle( tempSetValFromGrid[0], tempSetValFromGrid[1], ',', 'string' );
								};
							}
						}
					}
				};
				result = Ext.encode(tempParams);
			};
		};
	};
	return result;
};

var doManualInvoke = function( modname, invokeComponent, callbackIndex, setTocomponent ){
	var tempParams = invokeComponent.invokeParams;
	var procParams = doManualParams( tempParams ) != undefined ? { params : doManualParams( tempParams ) } : {} ;
	
	var procName = invokeComponent.invokeCall;
	var procReturn = invokeComponent.invokeReturn;
	if (new String(procName).length > 0) {
		Ext.getBody().mask('Please wait...');
		var joinUri = 'select/function';
		if (procReturn == false) {
			joinUri = 'call/procedure';
		};
		Ext.Ajax.request({
			url: BASE_URL + modname + '/call_method/' + joinUri + '/' + procName,
			method: 'POST',
			params: procParams,
			success: function(response) {
				if (new String(response.responseText).length > 0) {
					var obj = Ext.decode(response.responseText);
					if (Ext.isObject(obj)) {
						if (obj.data != null && obj.data != undefined) {
							if ( setTocomponent != undefined ) {
								if (Ext.getCmp( setTocomponent )) {
									Ext.getCmp( setTocomponent ).setValue(obj.data[procName]);
								}
							}
						}
					};
				};

				if (Ext.isFunction(callbackIndex)) {
					callbackIndex()
				};
			},
			failure: function(response) {
				Ext.MessageBox.show({
					title: 'Warning !',
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
			},
			callback: function() {
				Ext.getBody().unmask();
			}
		});
	} else {
		if (Ext.isFunction(callbackIndex)) {
			callbackIndex()
		};
	};
};

/**
//doManualInvoke on examples
var invokeManualOrderEntryForm = function( status ){
	doManualInvoke(
		'ttransorderentry',
		{
			"invokeCall": "_proses_order_entry",
			"invokeReturn": false,
			"invokeParams": [
				{
					"position": "1",
					"set": "get",
					"type": "integer",
					"value": "ttransorderentry_module_real_general_form_22_ar_0_general_form_22_blk_i_form_hidden_52"
				},{
					"position": "2",
					"set": "none",
					"type": "integer",
					"value": status
				}
			]
		},
		function(){
			//jika beres di panggil..
		},
		//iin id set ke mana bila function
		undefined
	);
};
*/

var doGetGridSelection = function( gridId, getDateColumnFrom ){
	var result = {};
	for (i2 = 0; i2 < getDateColumnFrom.length; i2++) {
		result[ getDateColumnFrom[ i2 ] ] = [];
	};
	var checkingComponent = Ext.getCmp( gridId );
	if( checkingComponent ){
		var smTemp 	= checkingComponent.getSelectionModel(),
			selTemp = smTemp.getSelection();
		if ( selTemp.length > 0 ) {
			for (i = 0; i < selTemp.length; i++) {
				for (i2 = 0; i2 < getDateColumnFrom.length; i2++) {
					relAfterCheckComp = selTemp[i].get( getDateColumnFrom[ i2 ] );
					result[ getDateColumnFrom[ i2 ] ].push( relAfterCheckComp );
				};
			};
		};
	};
	return result;
};

var doGetGridSelectionSingle = function( gridId, getDateColumnFrom, splitBy, resultType ){
	var tempResult = undefined;
	var originalSelectedSingle = undefined;
	if( Ext.isString( getDateColumnFrom ) ){
		if( String( splitBy ).length > 0 ){
			getDateColumnFrom = getDateColumnFrom.trim();
			getDateColumnFrom = getDateColumnFrom.split( splitBy );
		}else{
			getDateColumnFrom = [ getDateColumnFrom ];
		};
	};
	if( Ext.isArray( getDateColumnFrom ) ){
		if( getDateColumnFrom.length > 0 ){
			originalSelectedSingle = getDateColumnFrom[0];
			tempResult = doGetGridSelection( gridId, [ originalSelectedSingle ] );
		};
	};
	if( originalSelectedSingle != undefined && tempResult != undefined && Ext.isObject( tempResult )){
		if( tempResult[ originalSelectedSingle ] ){
			if( Ext.isArray( tempResult[ originalSelectedSingle ] ) ){
				tempResult = tempResult[ originalSelectedSingle ];
				if( resultType != undefined ){
					if( resultType.toLowerCase() == 'string' ){
						tempResult = tempResult.join( splitBy );
					};
				};
			};
		};
	};
	return tempResult;
};

var doCompareSkipOrDefaultParams = function( valx, confg ){
	var resultVal = undefined;
	var checkType = confg[ 'type' ].toLowerCase();
	
	if( valx != undefined && String( valx ).length > 0 ){
	
		if( String( confg[ 'skipif' ] ).length > 0 && Ext.isString( confg[ 'skipif' ] ) && confg[ 'skipif' ] != undefined && valx != undefined ){
			if( checkType == 'string' ){
				if( String( confg[ 'skipif' ] ).toLowerCase() == String( valx ).toLowerCase() ){
					resultVal = 'skip';
				};
			}else if( checkType == 'integer' ){
				if( parseInt( confg[ 'skipif' ] ) == parseInt( valx ) ){
					resultVal = 'skip';
				};
			}else if( checkType == 'float' ){
				if( parseFloat( confg[ 'skipif' ] ) == parseFloat( valx ) ){
					resultVal = 'skip';
				};
			}else if( checkType == 'boolean' ){
				if( JSON.parse( confg[ 'skipif' ] ) == JSON.parse( valx ) ){
					resultVal = 'skip';
				};
			};
		};
		
		if( resultVal == undefined ){
			resultVal = valx;
		};
		
	}else{
		if( String( confg[ 'default' ] ).length > 0 && Ext.isString( confg[ 'default' ] ) && confg[ 'default' ] != undefined ){
			resultVal = confg[ 'default' ];
		};
	};
	
	return resultVal;
};

var doSemarDoc = function( docType, configTemplate, configParams ){
	if( Ext.isObject( configTemplate ) && Ext.isArray( configParams ) ){
		
		var checkingComponent = Ext.getCmp( configTemplate['Grid_ID'] );
		if( checkingComponent ){
			
			var Params_T_Extra = [];
			var Temp_P_T_Extra = {};
			for( var i = 0; i < configParams.length; i++ ){
				var position = configParams[ i ][ 'position' ];
				Temp_P_T_Extra[ position ] = undefined;
			};
			for( var i = 0; i < configParams.length; i++ ){
				var position = configParams[ i ][ 'position' ];
				if( String( configParams[ i ][ 'set' ] ).toLowerCase() == 'none' ){
					Temp_P_T_Extra[ position ] = configParams[ i ][ 'value' ];
				}else if( String( configParams[ i ][ 'set' ] ).toLowerCase() == 'get' ){
					if( Ext.getCmp( configParams[ i ][ 'value' ] ) ){
						var tempGetValueDoc = Ext.getCmp( configParams[ i ][ 'value' ] ).getValue();
						
						var tempCheckSkipOrDefault = doCompareSkipOrDefaultParams( tempGetValueDoc, configParams[ i ] );
						if( tempCheckSkipOrDefault != undefined ){
							Temp_P_T_Extra[ position ] = tempCheckSkipOrDefault;
						};
					};
				}else if( String( configParams[ i ][ 'set' ] ).toLowerCase() == 'grid' ){
						var tempSetValFromGrid = configParams[ i ][ 'value' ].split('=');
						
						if( tempSetValFromGrid.length == 2 ){
							var tempGetValueDoc = 
								doGetGridSelectionSingle( tempSetValFromGrid[0], tempSetValFromGrid[1], ',', 'string' );

							var tempCheckSkipOrDefault = doCompareSkipOrDefaultParams( tempGetValueDoc, configParams[ i ] );
							if( tempCheckSkipOrDefault != undefined ){
								Temp_P_T_Extra[ position ] = tempCheckSkipOrDefault;
							};
						};
				};
			};
			
			var Temp_P_T_Extra_Keys = Ext.Object.getKeys( Temp_P_T_Extra );
			for( var ix = 0; ix < Temp_P_T_Extra_Keys.length; ix++ ){
				Params_T_Extra[ parseInt( Temp_P_T_Extra_Keys[ ix ] ) ] = Temp_P_T_Extra[ Temp_P_T_Extra_Keys[ ix ] ];
			};

			var param_cetakx = [
				'Template_ID=' + configTemplate['Template_ID'],
				'Data_ID=' + configTemplate['Data_ID'],
				'Grid_ID=' + configTemplate['Grid_ID']
			];
			
			Params_T_Extra 	= Params_T_Extra.join('_');
			param_cetakx	= param_cetakx.join('&');
			
			if( docType.toLowerCase() == 'excel' || docType.toLowerCase() == 'xls' ){
				Load_Popup(
					'win_xls_pd',
					BASE_URL + 'report_excel/printdialogxls/'+Params_T_Extra + '?' + param_cetakx,
					configTemplate['title']
				);
			}else if( docType.toLowerCase() == 'doc' || docType.toLowerCase() == 'word'  || docType.toLowerCase() == 'pdf' ){
				if( docType.toLowerCase() == 'pdf' ) {
					param_cetakx += '&createpdf=yes';
				};
				Load_Popup(
					'win_docs_pd',
					BASE_URL + 'report_word/printdialogdocs/'+Params_T_Extra + '?' + param_cetakx,
					configTemplate['title']
				);
			};
		};
		
	};
	
};
/** 
//doSemarDoc examples
doSemarDoc('excel',{
title:'Test Semar',
Template_ID:'kenaikan_pangkat_prediksi',
Data_ID:'ID_Log',
Grid_ID:'xxx2_module_real_general_grid_20'
},[
	{
		position : 2,
		set		 : 'grid',
		type 	 : 'integer',
		value  	 : 'xxx2_module_real_general_grid_6=IDx_Log',
		skipif	 : "4",
		default  : "3"
	},
	{
		position : 1,
		set		 : 'none',
		type 	 : 'string',
		value  	 : 'skip1',
		skipif	 : undefined,
		default  : undefined
	},
	{
		position : 3,
		set		 : 'get',
		type 	 : 'integer',
		value  	 : 'xxx2_module_real_general_form_7_blk_i_form_text_x9',
		skipif	 : undefined,
		default  : 4
	}
]);
**/

/*
i.e : doManualEvalOperatorInput(" ( username + password ) % 2");
*/
var doManualEvalOperatorInput = function( objOp ){
	var temp = objOp.replace( /([a-zA-Z0-9_]+)/ig, function( ob ){
		var pr = 0;
		if( Ext.getCmp( ob ) ) {
			var vl = Ext.getCmp( ob ).getValue();
			if( Ext.isNumeric( vl ) ){
				pr = 'JSON.parse( '+vl+' )';
			};
		};
		return Ext.isNumeric( ob ) ? ob : pr;
	});
	console.log(temp)
	if( temp ){
		temp = eval( temp );
	};
	return temp;
};

Ext.define('SemarWidgetSearchToolbar',{
	extend	: 'Ext.toolbar.Toolbar',
	alias	: 'widget.semarwidgetsearchtoolbar',
	grid_id	: undefined,
	store	: undefined,
	text	: undefined,
	initComponent : function()  {
		var id_x 	= this.grid_id;
		var storex 	= this.store;
		var textx	= this.text;
        this.items = [ buildSearchBoxByIDGrid( id_x, storex, textx ) ];
        this.callParent(arguments);
    }
});

var buildSearchBoxByIDGrid = function( id_x, storex, textx ){
	var tempSearch = new Ext.create('Ext.ux.form.SearchField', {
		id 			: id_x + '_searchfield_box',
		flex		: 1,
		store		: ( Ext.isString( storex ) ? Ext.StoreMgr.get( storex ) : storex ),
		emptyText	: textx  
	});
	tempSearch.onTrigger2Click = function( evt ){
		var me = this,
			store = me.store,
			proxy = store.getProxy(),
			value = me.getValue(),
			colmn = function( id_x ){
				var baka_kol = Ext.getCmp( id_x ).columns;
				var tempa = [];
				for( var i in baka_kol ){
					if( Ext.isNumeric( i ) ){
						if( baka_kol[i].search ){
							tempa.push( baka_kol[i].dataIndex );
						};
					};
				};
				return tempa;
			};
			
		if (value.length < 1) {
			me.onTrigger1Click();
			return;
		};
		proxy.extraParams[me.paramName] = value;
		proxy.extraParams['columnSearch'] = Ext.JSON.encode( colmn( id_x ) );
		//proxy.extraParams.start = 0;
		store.loadPage(1);
		//store.load();
		me.hasSearch = true;
		me.triggerEl.item(0).setDisplayed('block');
		me.doComponentLayout();
	};
	return tempSearch;
};

var colorDataGridByRecord = function( grid_id, add ){
	//remove dulu, baru add lagi..
	if( remove_or_add == true ){
		
	};
};
var doManualTestVisualBrowseref_Fragment = function( compontId, isShow, callbackIndexX, checkBrowseref, selectedCallback ){
	var dynamcID = compontId;
	if (isShow) {
		if (Ext.getCmp(dynamcID)) {
			Ext.getCmp(dynamcID).destroy();
		};
		if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
			delete checkBrowseref.xtype;
			delete checkBrowseref.id;
			checkBrowseref['id'] = dynamcID;
			checkBrowseref['width'] = checkBrowseref['browserefWidth'];
			checkBrowseref['height'] = checkBrowseref['browserefHeight'];
			checkBrowseref['modal'] = true;
			checkBrowseref['bodyStyle'] = 'padding:5px;';
			checkBrowseref['layout'] = 'fit';
			checkBrowseref['closeAction'] = 'destroy';

			if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
				checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
			};

			Ext.create('Ext.window.Window', checkBrowseref).show();
			Ext.getCmp(dynamcID).doLayout();
			if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
				Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
					var tempEventClick = undefined;
					if (checkBrowseref.browserefClick == 1) {
						tempEventClick = 'itemclick';
					} else if (checkBrowseref.browserefClick >= 2) {
						tempEventClick = 'itemdblclick';
					};
					if (tempEventClick != undefined && new String(checkBrowseref.componentTarget).length > 0 && checkBrowseref.componentTarget != undefined) {
						Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
							var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								selectedCallback( dynamcID, checkBrowseref.componentTarget, selTemp[0] );
							};
						});
					};
					if (Ext.isFunction(callbackIndexX)) {
						callbackIndexX();
					};
				});
			} else {
				if (Ext.isFunction(callbackIndexX)) {
					callbackIndexX();
				};
			};
		};
	}else{
		if (Ext.getCmp(dynamcID)) {
			Ext.getCmp(dynamcID).destroy();
		};
		if (Ext.isFunction(callbackIndexX)) {
			callbackIndexX();
		};
	};
};
//get via pengaturan ref employee asoc user.
var Show_Popup_RefPegawai_User = function( componentTarget, modulecode, browserefWidth, browserefHeight, browserefClick ){
	doManualTestVisualBrowseref_Fragment(
		'Semar_Asoc_Show_Popup_RefPegawai_User',
		true,
		function(){
			console.log('Show_Popup_RefPegawai_User is renderer!');
		},{
			componentTarget : componentTarget,
			modulecode 		: ( modulecode || Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.modulecode ),
			browserefWidth 	: ( browserefWidth || Scr_Width / 1.8 ),
			browserefHeight : ( browserefHeight || Scr_Height / 1.8 ),
			browserefClick 	: ( browserefClick || 2 )
		},
		function( dynamcID, componentTarget, selectedx ){
			var check_componentTarget = Ext.getCmp( componentTarget );
			if( check_componentTarget ){
				if( check_componentTarget.xtype == 'form' || check_componentTarget.xtype == 'formpanel' ){
					Ext.getCmp(componentTarget).getForm().setValues({
						fullname 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.fullname ),
						email 		: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.email ),
						NIP 		: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.NIP ),
						kode_unker 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_unker ),
						nama_unker 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_unker ),
						nama_unor 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_unor ),
						kode_unor 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_unor ),
						nama_jab 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_jab ),
						kode_jab 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_jab )
					});
				}else if( check_componentTarget.xtype == 'grid'|| check_componentTarget.xtype == 'gridpanel' ){
					var tempModelName = Ext.getCmp( componentTarget ).getStore().getProxy().getModel().modelName;
					Ext.getCmp( componentTarget ).getStore().add(
						Ext.create( tempModelName, {
								fullname 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.fullname ),
								email 		: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.email ),
								NIP 		: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.NIP ),
								kode_unker 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_unker ),
								nama_unker 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_unker ),
								nama_unor 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_unor ),
								kode_unor 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_unor ),
								nama_jab 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_jab ),
								kode_jab 	: selectedx.get( Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_jab )
							}
						)
					);
				};
			};
			Ext.getCmp( dynamcID ).hide();
		}
	);
};

var doManualSemarDocByGrid = function( modulecodex, popup_title, Ori_Grid_ID, Data_ID, table_or_view ){
	var Grid_ID = Ext.getCmp( Ori_Grid_ID );
	
	if( Grid_ID == undefined ){
		return ;
	};
	
	if( Grid_ID.xtype != 'grid' && Grid_ID.xtype != 'gridpanel' ){
		return ;
	};
	
	var uri_all = 'report_excel/cetakdynxls/all?export_by_grid=yes&ttd=no';
	var uri_selected = 'report_excel/cetakdynxls/selected?export_by_grid=yes&ttd=no';
	var uri_by_rows = 'report_excel/cetakdynxls/by_rows?export_by_grid=yes&ttd=no';
	
	var popup_title = ( popup_title || 'Export via Grid' );
	var Params_Print = {};
	var columns_visible = [];
	var columns_filters = [];
	var columns_sorters = [];
	var columns_paramsx = {};
	
	var doPropertiesOfGrid = function( Grid_ID ){
		Ext.each( Grid_ID.columns, function(col, index) {
			if ( String( col.dataIndex ).length > 0 && !col.hidden ){
				columns_visible.push({
					text 		: col.text,
					width 		: col.width,
					dataIndex 	: col.dataIndex
				})
			};                 
		});
		if( Grid_ID.filters ){
			if( Ext.isFunction( Grid_ID.filters.getFilterData ) ) {
				var temp_filter_data = Grid_ID.filters.getFilterData();
				Ext.each( temp_filter_data, function( fld, index) {
					if ( String( fld.field ).length > 0 && String( fld.data.value ).length > 0 ){
						columns_filters.push( fld )
					};                 
				});
			};
		};
		
		if( Ext.isFunction( Grid_ID.getStore ) ) {
			var temp_Grid_ID_Store = Grid_ID.getStore();
			if( temp_Grid_ID_Store.sorters ){
				if( Ext.isFunction( temp_Grid_ID_Store.sorters.get ) ) {
					Ext.each( columns_visible, function( sortx, index) {
						var _temp_sort = temp_Grid_ID_Store.sorters.get( sortx.dataIndex );
						if( _temp_sort ) {
							if ( String( _temp_sort.property ).length > 0 && String( _temp_sort.direction ).length > 0 ){
								columns_sorters.push( _temp_sort )
							};  
						};               
					});
				};
			};
			if( Ext.isFunction( temp_Grid_ID_Store.getProxy ) ) {
				columns_paramsx = temp_Grid_ID_Store.getProxy().extraParams;
			};
		};
		return {
			columns_visible : columns_visible,
			columns_filters : columns_filters,
			columns_sorters : columns_sorters,
			columns_paramsx : columns_paramsx
		}
	};
	
	if( Ext.getCmp( 'semar_x_win_xls_pd' ) ){
		Ext.getCmp( 'semar_x_win_xls_pd' ).destroy();
	};
	var form_xls_pd = new Ext.create('Ext.form.Panel', {
		id: 'semar_x_form_xls_pd', 
		url: null, 
		frame: true, 
		bodyStyle: 'padding: 5px 5px 0 0;', 
		width: '100%', 
		height: '100%',
		fieldDefaults: {
			labelAlign: 'right',
			msgTarget: 'side'
		},
		defaults: {
			hideEmptyLabel: false, 
			allowBlank: false
		},
		items: [{
			xtype: 'fieldset',flex: 1,
			title:'Pilihan Cetak',
			defaultType:'radio',layout:'anchor',
			items: [
				{
					boxLabel: 'Semua', 
					name: 'pilihan_cetak', 
					id: 'semar_x_semua_pd', 
					inputValue: 'semua',
					listeners: {
						change: function (ctl, val) {
							if(val == true){
								Ext.getCmp('semar_x_dari_pd_xls').reset(); Ext.getCmp('semar_x_sampai_pd').reset();
							}
						}
					}
				},
				{
					boxLabel: 'Yang Dipilih', 
					name: 'pilihan_cetak', 
					id: 'semar_x_terpilih_pd_xls', 
					inputValue: 'terpilih',
					checked: true,
					listeners: {
						change: function (ctl, val) {
							if(val == true){
								Ext.getCmp('semar_x_dari_pd_xls').reset(); 
								Ext.getCmp('semar_x_sampai_pd').reset();
							}
						}
					}
				},
				{
					boxLabel: 'Berdasarkan Baris', 
					name: 'pilihan_cetak', 
					id: 'semar_x_perbaris_pd_xls', 
					inputValue: 'semar_x_perbaris_pd_xls'
				},
				{
					xtype: 'fieldcontainer', 
					layout: 'hbox', 
					defaultType: 'textfield', 
					combineErrors: true, 
					defaults: {
						hideLabel: 'true', 
						width: 70
					},
					items: [
						{
							xtype: 'numberfield', 
							name: 'dari', 
							id: 'semar_x_dari_pd_xls', 
							emptyText: 'Dari', 
							margins: '0 0 0 15', 
							minValue: 1,
							listeners:{
								focus: function(selectedField){
									Ext.getCmp('semar_x_perbaris_pd_xls').setValue(true);
								},
								change: function(selectedField){
									Ext.getCmp('semar_x_perbaris_pd_xls').setValue(true);
								}
							}
						},
						{
							xtype: 'numberfield', 
							name: 'sampai', 
							id: 'semar_x_sampai_pd', 
							emptyText: 'Sampai',
							margins: '0 0 0 5', 
							minValue: 1,
							listeners:{
								focus: function(selectedField){
									Ext.getCmp('semar_x_perbaris_pd_xls').setValue(true);
								},
								change: function(selectedField){
									Ext.getCmp('semar_x_perbaris_pd_xls').setValue(true);
								}
							}
						}
					]
				}
			]
		}],
		buttons: [
			{
				text: 'Export', 
				id:'semar_x_Export_Xls', 
				handler: function() {
					submit_export_xls_pd();
				}
			},
			{
				text: 'Batal', 
				handler: function() {
					Ext.getCmp( 'semar_x_win_xls_pd' ).close();
				}
			}
		]
	});

	function submit_export_xls_pd() {
		Ext.getCmp('semar_x_Export_Xls').setDisabled(true);
		if (Ext.getCmp('semar_x_semua_pd').getValue() == true) {
			Ext.getCmp('semar_x_sbwin_xls_pd').showBusy({
				text: 'Silahkan tunggu! Sedang melakukan Export ...'
			});
			Ext.Ajax.request({
				url: BASE_URL + uri_all,
				method: 'POST',
				params: objectMerge({
					id_open: 1,
					idx: Ori_Grid_ID,
					table_or_view:table_or_view,
					modulecodex:modulecodex,
					Data_ID:Data_ID,
					popup_title: popup_title,
					columns_visible: JSON.stringify(columns_visible),
					properties_grid : JSON.stringify( doPropertiesOfGrid( Grid_ID ) )
				}, Params_Print),
				success: function(response) {
					preview_export_xls_pd(response);
				},
				scope: this
			});
		} else if (Ext.getCmp('semar_x_terpilih_pd_xls').getValue() == true) {
			var sm = Grid_ID.getSelectionModel(),
				sel = sm.getSelection(),
				data = '';
			if (sel.length > 0) {
				Ext.getCmp('semar_x_sbwin_xls_pd').showBusy();
				for (i = 0; i < sel.length; i++) {
					data = data + sel[i].get( Data_ID ) + '-';
				}
				Ext.Ajax.request({
					url: BASE_URL + uri_selected,
					method: 'POST',
					params: objectMerge({
						id_open: 1,
						postdata: data,
						idx: Ori_Grid_ID,
						popup_title: popup_title,
						modulecodex:modulecodex,
						table_or_view:table_or_view,
						Data_ID:Data_ID,
						columns_visible: JSON.stringify(columns_visible),
						properties_grid : JSON.stringify( doPropertiesOfGrid( Grid_ID ) )
					}, Params_Print),
					success: function(response) {
						preview_export_xls_pd(response);
					},
					scope: this
				});
			} else {
				Ext.MessageBox.show({
					title: 'Peringatan !',
					msg: 'Silahkan pilih data yang ingin Anda export !',
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
				win_xls_pd.close();
			}
		} else {
			var dari = Ext.getCmp('semar_x_dari_pd_xls').getValue();
			var sampai = Ext.getCmp('semar_x_sampai_pd').getValue();
			if (dari == null || sampai == null) {
				Ext.MessageBox.show({
					title: 'Peringatan !',
					msg: 'Tentukan batasan baris yang akan diexport !',
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
				Ext.getCmp('semar_x_Export_Xls').setDisabled(false);
			} else if (dari > sampai) {
				Ext.MessageBox.show({
					title: 'Peringatan !',
					msg: 'Kesalahan penentuan batasan baris !',
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
				Ext.getCmp('semar_x_Export_Xls').setDisabled(false);
			} else {
				Ext.getCmp('semar_x_sbwin_xls_pd').showBusy();
				Ext.Ajax.request({
					url: BASE_URL + uri_by_rows + Ext.getCmp('semar_x_dari_pd_xls').getValue() + '/' + Ext.getCmp('semar_x_sampai_pd').getValue(),
					method: 'POST',
					params: objectMerge({
						id_open: 1,
						idx: Ori_Grid_ID,
						popup_title: popup_title,
						modulecodex:modulecodex,
						table_or_view:table_or_view,
						Data_ID:Data_ID,
						columns_visible: JSON.stringify(columns_visible),
						properties_grid : JSON.stringify( doPropertiesOfGrid( Grid_ID ) )
					}, Params_Print),
					success: function(response) {
						preview_export_xls_pd(response);
					},
					scope: this
				});
			}
		}
	}

	function preview_export_xls_pd(response) {
		if (isURL(response.responseText)) {
			semar_x_win_xls_pd.close();
			var docprint = new Ext.create('Ext.window.Window', {
				title: popup_title,
				iconCls: 'icon-xls',
				constrainHeader: true,
				closable: true,
				maximizable: false,
				width: '20%',
				height: '20%',
				bodyStyle: 'padding: 5px;',
				modal: true,
				items: [{
					xtype: 'tabpanel',
					activeTab: 0,
					width: '100%',
					height: '100%',
					items: [{
						title: 'XLS',
						frame: false,
						collapsible: true,
						autoScroll: true,
						iconCls: 'icon-xls',
						items: [{
							xtype: 'miframe',
							frame: false,
							height: '100%',
							src: response.responseText
						}]
					}]
				}]
			}).show();
		} else {
			Ext.MessageBox.show({
				title: 'Peringatan !',
				msg: 'Proses cetak gagal !',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.ERROR
			});
		}
	}

	var semar_x_win_xls_pd = new Ext.create('Ext.window.Window', {
		id: 'semar_x_win_xls_pd',
		title: popup_title,
		iconCls: 'icon-xls',
		constrainHeader: true,
		closable: true,
		width: 300,
		height: 250,
		bodyStyle: 'padding: 5px;',
		modal: true,
		items: [form_xls_pd],
		bbar: new Ext.ux.StatusBar({
			text: 'Ready',
			id: 'semar_x_sbwin_xls_pd',
			iconCls: 'x-status-valid'
		})
	});
	semar_x_win_xls_pd.show();
};
//sample
//doManualSemarDocByGrid( 'xxx2', 'title popup excel','xxx2_module_real_general_grid_6', 'ID_Log' , 'tlog' );

var doSemarDocByGrid = function( modulecodex, Ori_Grid_ID_And_Data_ID, table_or_view ){
	Ori_Grid_ID_And_Data_ID = Ori_Grid_ID_And_Data_ID.trim();
	if( String( Ori_Grid_ID_And_Data_ID ).match( '=' ) ) {
		Ori_Grid_ID_And_Data_ID = String( Ori_Grid_ID_And_Data_ID ).split('=');
		if( Ext.isArray( Ori_Grid_ID_And_Data_ID ) ) {
			if( Ori_Grid_ID_And_Data_ID.length == 2 ) {
				var Ori_Grid_ID = Ori_Grid_ID_And_Data_ID[0]; //id grid
				var Data_ID 	= Ori_Grid_ID_And_Data_ID[0]; //column grid
				
				if( Ext.getCmp( Ori_Grid_ID ) ) {
					if( Ext.getCmp( Ori_Grid_ID ).xtype == 'grid' || Ext.getCmp( Ori_Grid_ID ).xtype == 'gridpanel' ){
						var popup_title = Ext.getCmp( Ori_Grid_ID ).title;
						
						doManualSemarDocByGrid( modulecodex, popup_title, Ori_Grid_ID, Data_ID, table_or_view );
					};
				};
			};
		};
	};
};
//sample
//doSemarDocByGrid( 'xxx2', 'xxx2_module_real_general_grid_6=ID_Log', 'tlog' );

Ext.define('eXML.widget.messageBOXpanel', {
    alias: 'widget.exmlwidgetmessageboxpanel',
    extend:'Ext.panel.Panel',
	initComponent: function () {
        this.addEvents( 'msgok', 'msgyes', 'msgno', 'msgcancel' );
        this.callParent();
    }
});
var SemarBOXPanelDialog = function( objComp, newConfigs ){
	var oldConfigs = {
		buttons 			: 	Ext.Msg[ objComp.MessageBOX_Buttons ],
		closable 			: 	true,
		defaultTextHeight 	: 	75,
		fn : function( buttonId, text, opt ){
			if( buttonId == 'ok' ) {
				if ( objComp.hasListener('msgok') ) {
					objComp.fireEvent('msgok',  objComp, 'msgok');
				};
			}else if( buttonId == 'yes' ) {
				if ( objComp.hasListener('msgyes') ) {
					objComp.fireEvent('msgyes',  objComp, 'msgyes');
				};
			}else if( buttonId == 'no' ) {
				if ( objComp.hasListener('msgno') ) {
					objComp.fireEvent('msgno',  objComp, 'msgno' );
				};
			}else if( buttonId == 'cancel' ) {
				if ( objComp.hasListener('msgcancel') ) {
					objComp.fireEvent('msgcancel',  objComp, 'msgcancel');
				};
			};
		},
		icon				:	Ext.Msg[ objComp.MessageBOX_Icon ],
		maxWidth			:	600,
		minWidth			:	300,
		modal 				:	true,
		msg 				: 	objComp.MessageBOX_Body,
		multiline 			: 	objComp.MessageBOX_Multiline,
		prompt 				: 	objComp.MessageBOX_Prompt,
		title 				: 	objComp.title,
		value 				: 	objComp.MessageBOX_Value
	};
	oldConfigs = Ext.merge( oldConfigs, newConfigs )
	Ext.Msg.show( oldConfigs );
};

Ext.define('eXML.widget.evochart', {
    alias: 'widget.exmlwidgetevochart',
    extend:'Ext.panel.Panel',
	initComponent: function () {
        this.addEvents( 'visualchart' );
        this.callParent();
		this.add({
			xtype:'exmlwidgetevochartvisual',
			id: this.id + '_frame_evochart'
		});
    },
	listeners:{
		visualchart:function( paramsEvoChart ){
			var Chart_Type = undefined;
			var Chart_Proc = undefined;
			if( this.Chart_Type != undefined ) {
				if( String( this.Chart_Type ).length > 0 ) {
					Chart_Type = this.Chart_Type;
				};
			};
			if( this.Chart_Proc != undefined ) {
				if( String( this.Chart_Proc ).length > 0 ) {
					Chart_Proc = this.Chart_Proc;
				};
			};
			if( Chart_Proc != undefined && Chart_Type != undefined ) {
				var cleanURLEvoChart = BASE_URL + 'evochart?type='+Chart_Type+'&proc='+Chart_Proc;
				var Str_paramsEvoChart = doManualParams( paramsEvoChart );
				if( Str_paramsEvoChart != undefined ) {
					Str_paramsEvoChart = Ext.decode( Str_paramsEvoChart );
					var temp = [];
					for( var i in Str_paramsEvoChart ){
						temp[parseInt(Str_paramsEvoChart[i]['position'])] = Str_paramsEvoChart[i]['value'];
					};
					temp = Ext.Array.clean(temp);
					if( Ext.isArray( temp ) ) {
						if( temp.length > 0 ){
							cleanURLEvoChart = cleanURLEvoChart + ":" + temp.join(":");
						};
					};
				};
				Ext.getCmp( this.id + '_frame_evochart' ).updateChart( cleanURLEvoChart );
			};
		}
	},
	updateVisual : function(){
		this.fireEvent( 'visualchart', this.Chart_Params );
	}
});
Ext.define('eXML.widget.evochartvisual', {
    alias: 'widget.exmlwidgetevochartvisual',
    extend:'Ext.Component',
	bodyStyle:'border:0px;',
	frameBorder:0,
	border:false,
	autoEl : {
		flex:1,
		width:'100%',
		height:'100%',
		tag : "iframe",
		frameBorder:0,
		src: BASE_URL + 'evochart',
		bodyStyle:'border:0px;width:100%;height:100%;',
		border:false
	},
	width:'100%',
	height:'100%',
	flex:1,
	layout:'fit',
	listeners:{
		showchart:function( cleanURLEvoChart ){
			Ext.getCmp( this.id ).getEl().dom.src = cleanURLEvoChart;
		}
	},
	initComponent: function () {
        this.addEvents( 'showchart' );
        this.callParent();
    },
	updateChart : function( cleanURLEvoChart ){
		this.fireEvent( 'showchart', cleanURLEvoChart );
	}
});
function sortOnObjectArray( property ){
    return function(a, b){
        if(a[property] < b[property]){
            return -1;
        }else if(a[property] > b[property]){
            return 1;
        }else{
            return 0;   
        }
    }
};

<?php }else{ echo "var new_popup = 'GAGAL';"; } ?>