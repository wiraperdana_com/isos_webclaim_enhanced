<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

Ext.require([
    'Ext.tree.*',
    'Ext.data.*',
    'Ext.tip.*'
]);

//MENU
function showMenuForm(mode, value_form) {
    var formManipulasiMenu = Ext.create('Ext.form.Panel', {
        id: 'formManipulasiMenu',
        bodyPadding: 5,
        flex: 1,
        url: BASE_URL + 'kontrol_menu/ext_insert_menu',
        layout: 'anchor',
        defaults: {
            anchor: '100%'
        },
        defaultType: 'textfield',
        labelAlign: 'top',
        items: [{
            fieldLabel: 'Parent',
            name: 'parent_idmenu',
            xtype: 'combobox',
            valueField: 'idmenu',
            value: 0,
            displayField: 'general_text',
            emptyText: 'Pilih Induk Menu',
            typeAhead: true,
            forceSelection: false,
            selectOnFocus: true,
            valueNotFoundText: 'Pilih Induk Menu',
            queryMode: 'local',
            store: new Ext.data.Store({
                noCache: false,
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    actionMethods: {
                        read: 'POST'
                    },
                    extraParams: {
                        id_open: '1',
                        combox: '1'
                    },
                    url: BASE_URL + 'kontrol_menu/ext_get_all_menu',
                    reader: {
                        type: 'json',
                        root: 'results',
                        totalProperty: 'total',
                        idProperty: 'idmenu'
                    }
                },
                fields: ['idmenu', 'general_text']
            }),
            listeners: {
                'focus': {
                    fn: function (comboField) {
                        comboField.doQuery(comboField.allQuery, true);
                        comboField.expand();
                    },
                    scope: this
                },
                'change': {
                    fn: function (comboField, newValue) {},
                    scope: this
                }
            }
        }, {
            fieldLabel: 'ID Menu',
            name: 'idmenu',
            value: 0,
            readOnly: true,
            allowBlank: true
        }, {
            fieldLabel: 'Text',
            name: 'general_text',
            allowBlank: false
        }, 
		Generic_Semar_Icon_Combo(function( comboField, newValue ){
		},{
            fieldLabel: 'Icon Class',
            name: 'general_iconclass',
            allowBlank: false
		}),
		{
            fieldLabel: 'Identify Widget',
            name: 'general_identify',
            allowBlank: true
        }, {
            fieldLabel: 'ID Button',
            name: 'general_idbutton',
            allowBlank: false
        }, {
            fieldLabel: 'ID New Tab / PopUp',
            name: 'general_idnewtab_popup',
            allowBlank: true
        }, {
            fieldLabel: 'URL',
            name: 'general_url',
            allowBlank: true
        }, {
            fieldLabel: 'Type',
            xtype: 'combobox',
            value: 'TabPage',
            store: new Ext.data.SimpleStore({
                data: [
                    ['MainMenu'],
                    ['TabPage'],
                    ['Page'],
                    ['PopUp'],
                    //['Widget']
                ],
                fields: ['type']
            }),
            typeAhead: true,
            forceSelection: true,
            valueField: 'type',
            displayField: 'type',
            name: 'general_type',
            allowBlank: false,
            listeners: {
                'focus': {
                    fn: function (comboField) {
                        comboField.doQuery(comboField.allQuery, true);
                        comboField.expand();
                    },
                    scope: this
                }
            }
        }, {
            fieldLabel: 'Bottom Break',
            name: 'general_bottombreak',
            xtype: 'checkbox'
        }, {
            fieldLabel: 'Is Menu',
            name: 'general_ismenu',
            xtype: 'checkbox'
        }, {
            fieldLabel: 'Variable Prefix',
            name: 'general_variableprefix',
            allowBlank: true
        }, {
            fieldLabel: 'Status',
            xtype: 'combobox',
            value: 'Active',
            store: new Ext.data.SimpleStore({
                data: [
                    ['Active'],
                    ['Inactive']
                ],
                fields: ['status']
            }),
            typeAhead: true,
            forceSelection: true,
            valueField: 'status',
            displayField: 'status',
            name: 'general_status',
            allowBlank: false,
            listeners: {
                'focus': {
                    fn: function (comboField) {
                        comboField.doQuery(comboField.allQuery, true);
                        comboField.expand();
                    },
                    scope: this
                }
            }
        }, {
            xtype: 'fieldcontainer',
            fieldLabel: 'Proses',
            layout: 'hbox',
            defaults: {
                margins: 4,
                flex: 1,
                xtype: 'checkbox',
                labelAlign: 'top'
            },
            items: [{
                fieldLabel: 'View',
                name: 'process_view'
            }, {
                fieldLabel: 'Tambah',
                name: 'process_tambah'
            }, {
                fieldLabel: 'Ubah',
                name: 'process_ubah'
            }, {
                fieldLabel: 'Hapus',
                name: 'process_hapus'
            }, {
                fieldLabel: 'Proses',
                name: 'process_proses'
            }, {
                fieldLabel: 'Cetak',
                name: 'process_cetak'
            }, {
                fieldLabel: 'Cetak SK',
                name: 'process_cetaksk'
            }]
        }],
        buttons: [{
            text: 'Simpan',
            formBind: true,
            disabled: true,
            handler: function () {
                var form = this.up('form').getForm();
                if (form.isValid()) {
                    Ext.getCmp('windowManipulasiFormMenu').body.mask('Loading...');
                    form.submit({
                        success: function (form, action) {
                            Ext.getCmp('windowManipulasiFormMenu').body.unmask();
                            obj = Ext.decode(action.response.responseText);
                            if (obj.success == true) {
                                Ext.Msg.alert('Success', 'Berhasil Memanipulasi Menu');
                            } else {
                                Ext.Msg.alert('Failed', obj.errors.reason);
                            }
                            Ext.getCmp('windowManipulasiFormMenu').destroy();
                            DataMenu.load();
                        },
                        failure: function (form, action) {
                            Ext.getCmp('windowManipulasiFormMenu').body.unmask();
                            obj = Ext.decode(action.response.responseText);
                            Ext.Msg.alert('Failed', obj.errors.reason);
                            Ext.getCmp('windowManipulasiFormMenu').destroy();
                            DataMenu.load();
                        }
                    });
                }
            }
        }]
    });

    var windowManipulasiFormMenu = new Ext.create('Ext.window.Window', {
        id: 'windowManipulasiFormMenu',
        title: 'Manipulasi Menu',
        iconCls: 'icon-user',
        modal: true,
        constrainHeader: true,
        closable: true,
        width: 600,
        height: 530,
        bodyStyle: 'padding: 5px;',
        items: [formManipulasiMenu],
        bbar: new Ext.ux.StatusBar({
            text: 'Ready',
            id: 'sbWin_windowManipulasiFormMenu',
            iconCls: 'x-status-valid'
        })
    }).show();

    if (mode == 'Ubah') {
        formManipulasiMenu.getForm().setValues(value_form);
    }
}

Ext.define('MMenu', {
    extend: 'Ext.data.Model',
    fields: ['idmenu', 'ordermenu', 'parent_text', 'parent_idmenu', 'general_ismenu', 'general_text', 'general_iconclass', 'general_identify', 'general_idbutton', 'general_idnewtab_popup', 'general_url', 'general_type', 'general_bottombreak', 'general_variableprefix', 'general_status', 'process_view', 'process_tambah', 'process_ubah', 'process_hapus', 'process_proses', 'process_cetak', 'process_cetaksk']
});

var dataReader = new Ext.create('Ext.data.JsonReader', {
    id: 'dataReaderMenu',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'idmenu'
});

var dataProxy = new Ext.create('Ext.data.AjaxProxy', {
    id: 'dataProxyMenu',
    url: BASE_URL + 'kontrol_menu/ext_get_all_menu',
    actionMethods: {
        read: 'POST'
    },
    extraParams: {
        id_open: '1'
    },
    reader: dataReader,
    afterRequest: function (request, success) {}
});

var DataMenu = new Ext.create('Ext.data.Store', {
    groupField: 'parent_idmenu',
    id: 'DataMenu',
    model: 'MMenu',
    pageSize: 100000000000000000000,
    autoLoad: true,
    proxy: dataProxy
});

var groupingFeatureMenu = Ext.create('Ext.grid.feature.Grouping', {
	id:'groupingFeatureMenu',
    groupHeaderTpl: 'Parent Menu : <span style="color:red !important;">{[values.rows[0].parent_text_idcolomns]}</span> ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
});
var dragandropPluginMenu = Ext.create('Ext.grid.plugin.DragDrop', {
	pluginId:'dragandropPluginMenu'
});

var cbGridMenu = new Ext.create('Ext.selection.CheckboxModel');
var gridMENU = new Ext.create('Ext.grid.Panel', {
    id: 'gridMENU',
    store: DataMenu,
    title: 'DAFTAR MENU',
    frame: true,
    border: true,
    loadMask: true,
    style: 'margin:0 auto;',
    height: '100%',
    width: '100%',
    selModel: cbGridMenu,
    columnLines: true,
    invalidateScrollerOnRefresh: false,
    columns: [{
        header: 'No',
        xtype: 'rownumberer',
        width: 35,
        resizable: true,
        style: 'padding-top: .5px;'
    }, {
        header: "Parent",
        columns: [{
            header: "ID Menu",
            dataIndex: 'parent_idmenu',
            width: 80
        }, {
            header: "Text",
            dataIndex: 'parent_text',
            width: 80,
            id: 'parent_text_idcolomns'
        }]
    }, {
        header: 'General',
        columns: [{
            header: "ID Menu",
            dataIndex: 'idmenu',
            width: 80
        }, {
            header: "Text",
            dataIndex: 'general_text',
            width: 80
        }, {
            header: "Icon Class",
            dataIndex: 'general_iconclass',
            width: 80
        }, {
            header: "Identify Widget",
            dataIndex: 'general_identify',
            width: 80
        }, {
            header: "Id Button",
            dataIndex: 'general_idbutton',
            width: 80
        }, {
            header: "Id New Tab / PopUp",
            dataIndex: 'general_idnewtab_popup',
            width: 80
        }, {
            header: "URL",
            dataIndex: 'general_url',
            width: 80
        }, {
            header: "Type",
            dataIndex: 'general_type',
            width: 80
        }, {
            header: "Bottom Break",
            dataIndex: 'general_bottombreak',
            width: 80,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }, {
            header: "Status",
            dataIndex: 'general_status',
            width: 80
        }, {
            header: "Variable Prefix",
            dataIndex: 'general_variableprefix',
            width: 80
        }, {
            header: "Is Menu",
            dataIndex: 'general_ismenu',
            width: 60,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }, ]
    }, {
        header: "Process",
        columns: [{
            header: "View",
            dataIndex: 'process_view',
            width: 50,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }, {
            header: "Tambah",
            dataIndex: 'process_tambah',
            width: 50,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }, {
            header: "Ubah",
            dataIndex: 'process_ubah',
            width: 50,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }, {
            header: "Hapus",
            dataIndex: 'process_hapus',
            width: 50,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }, {
            header: "Proses",
            dataIndex: 'process_proses',
            width: 50,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }, {
            header: "Cetak",
            dataIndex: 'process_cetak',
            width: 50,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }, {
            header: "Cetak SK",
            dataIndex: 'process_cetaksk',
            width: 50,
            renderer: function (v) {
                return (v == 1 ? 'Yes' : 'No')
            }
        }]
    }, {
		header: "Order",
		dataIndex: 'ordermenu'
	} ],
    tbar: [{
        text: 'Tambah',
        id: 'tambah_menu',
        iconCls: 'icon-add',
        handler: function () {
            showMenuForm('Tambah', '');
        },
        disabled: pl_insert
    }, '-',
    {
        text: 'Ubah',
        id: 'ubah_menu',
        iconCls: 'icon-edit',
        handler: function () {
            var sm = gridMENU.getSelectionModel(),
                sel = sm.getSelection();
            if (sel.length == 1) {
                var value_form = {
                    parent_idmenu: sel[0].get('parent_idmenu'),
                    idmenu: sel[0].get('idmenu'),
                    general_text: sel[0].get('general_text'),
                    general_iconclass: sel[0].get('general_iconclass'),
                    general_identify: sel[0].get('general_identify'),
                    general_idbutton: sel[0].get('general_idbutton'),
                    general_idnewtab_popup: sel[0].get('general_idnewtab_popup'),
                    general_url: sel[0].get('general_url'),
                    general_type: sel[0].get('general_type'),
                    general_bottombreak: sel[0].get('general_bottombreak'),
                    general_status: sel[0].get('general_status'),
                    general_variableprefix: sel[0].get('general_variableprefix'),
                    general_ismenu: sel[0].get('general_ismenu'),
                    process_view: sel[0].get('process_view'),
                    process_tambah: sel[0].get('process_tambah'),
                    process_ubah: sel[0].get('process_ubah'),
                    process_hapus: sel[0].get('process_hapus'),
                    process_proses: sel[0].get('process_proses'),
                    process_cetak: sel[0].get('process_cetak'),
                    process_cetaksk: sel[0].get('process_cetaksk')
                };
                showMenuForm('Ubah', value_form);
            }
        },
        disabled: pl_update
    }, '-',
    {
        text: 'Hapus',
		id:'hapus_menu',
        iconCls: 'icon-delete',
        handler: function () {
            var sm = gridMENU.getSelectionModel(),
                sel = sm.getSelection(),
                data = '';
            if (sel.length > 0) {
                Ext.Msg.show({
                    title: 'Konfirmasi',
                    msg: 'Apakah Anda yakin untuk menghapus ?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            for (i = 0; i < sel.length; i++) {
                                data = data + sel[i].get('idmenu') + '-';
                            }
                            Ext.Ajax.request({
                                url: BASE_URL + 'kontrol_menu/ext_delete_menu',
                                method: 'POST',
                                params: {
                                    postdata: data
                                },
                                scripts: true,
                                renderer: 'data',
                                success: function (response) {
                                    gridMENU.getStore().load();
                                },
                                failure: function (response) {
                                    Ext.MessageBox.show({
                                        title: 'Peringatan !',
                                        msg: 'Gagal Menghapus Data !',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                },
                                scope: this
                            });
                        }
                    }
                });
            }
        },
        disabled: pl_delete
    },'->',{
		xtype:'button',
		enableToggle:true,
		text:'Re-Order Menu',
		id:'sync_reoder_menu_toggle',
		toggleHandler:function(btn, statex){
			Ext.getCmp('reorder_parent_idmenu').clearValue();
			DataMenu.clearFilter();
			if( statex ) {
				Ext.getCmp('tambah_menu').setDisabled( true );
				Ext.getCmp('ubah_menu').setDisabled( true );
				Ext.getCmp('hapus_menu').setDisabled( true );
				
				DataMenu.filter('parent_idmenu', -1);
				Ext.getCmp('sync_reoder_menu').setDisabled( false );
				Ext.getCmp('reorder_parent_idmenu').setDisabled( false );
				pluginGriupingHeaderGridMenu( false );
				pluginDDGridMenu( true );
			}else{
				Ext.getCmp('tambah_menu').setDisabled( false );
				Ext.getCmp('ubah_menu').setDisabled( false );
				Ext.getCmp('hapus_menu').setDisabled( false );
				
				Ext.getCmp('sync_reoder_menu').setDisabled( true );
				Ext.getCmp('reorder_parent_idmenu').setDisabled( true );
				pluginDDGridMenu( false );
				pluginGriupingHeaderGridMenu( true );
			}
		}
	},{
		disabled:true,
		name: 'reorder_parent_idmenu',
		id: 'reorder_parent_idmenu',
		xtype: 'combobox',
		valueField: 'idmenu',
		value: 0,
		displayField: 'general_text',
		emptyText: 'Pilih Induk Menu',
		typeAhead: true,
		forceSelection: false,
		selectOnFocus: true,
		valueNotFoundText: 'Pilih Induk Menu',
		queryMode: 'local',
		store: new Ext.data.Store({
			noCache: false,
			autoLoad: true,
			proxy: {
				type: 'ajax',
				actionMethods: {
					read: 'POST'
				},
				extraParams: {
					id_open: '1',
					combox: '1',
					parentonly:'1',
				},
				url: BASE_URL + 'kontrol_menu/ext_get_all_menu',
				reader: {
					type: 'json',
					root: 'results',
					totalProperty: 'total',
					idProperty: 'idmenu'
				}
			},
			fields: ['idmenu', 'general_text']
		}),
		listeners: {
			'focus': {
				fn: function (comboField) {
					comboField.doQuery(comboField.allQuery, true);
					comboField.expand();
				},
				scope: this
			},
			'change': {
				fn: function (comboField, newValue) {
					DataMenu.clearFilter();
					DataMenu.filter('parent_idmenu', newValue);
				},
				scope: this
			}
		}
	},{
		text:'Sync Re-Order Menu!',
		disabled:true,
		id:'sync_reoder_menu',
		iconCls: 'x-tbar-loading',
		handler:function(){
			var dataorder = Ext.encode(Ext.pluck(DataMenu.data.items, 'data'));
			Ext.getCmp('sync_reoder_menu').setDisabled(true);
			Ext.getBody().mask('Loading...');

			Ext.Ajax.request({
				url: BASE_URL + 'kontrol_menu/ext_sync_menuorder',
				method: 'POST',
				params: {
					id_open: 1,
					dataorder: dataorder
				},
				scripts: true,
				renderer: 'data',
				success: function (response) {
					Ext.getBody().unmask();
					DataMenu.load();
					Ext.getCmp('sync_reoder_menu').setDisabled(false);
				},
				failure: function (response) {
					Ext.getBody().unmask();
					Ext.MessageBox.show({
						title: 'Peringatan !',
						msg: 'Gagal Memanipulasi Data !',
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.ERROR
					});
					Ext.getCmp('sync_reoder_menu').setDisabled(false);
				},
				scope: this
			});
		}
	}],
	selModel: {
		mode: 'MULTI'
	},
    viewConfig: {
        stripeRows: false,
        getRowClass: function (record) {
            return record.get('general_status').toLowerCase() == 'inactive' ? 'child-row' : '';
        },
		plugins: dragandropPluginMenu,
		forceFit: true
    },
    features: [groupingFeatureMenu],
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: DataMenu,
        dock: 'bottom',
        displayInfo: true
    }],
    listeners: {
        itemdblclick: function (dataview, record, item, index, e) {
            Ext.getCmp('ubah_menu').handler.call(Ext.getCmp("ubah_menu").scope);
        },
		afterrender: function(){
			Ext.getCmp('sync_reoder_menu_toggle').toggle(false);
		}
    }
});
function pluginGriupingHeaderGridMenu( status ){
	var features_groupingFeatureMenu =gridMENU.getView().getFeature('groupingFeatureMenu');
	if( features_groupingFeatureMenu ) {
		if( status ) {
			features_groupingFeatureMenu.enable();
		}else{
			features_groupingFeatureMenu.disable();
		}
	}
}
function pluginDDGridMenu( status ){
	var plugin_dragandropPluginMenu =gridMENU.getView().getPlugin('dragandropPluginMenu');
	if( plugin_dragandropPluginMenu ) {
		if( status ) {
			plugin_dragandropPluginMenu.enable();
		}else{
			plugin_dragandropPluginMenu.disable();
		}
	}
};
//end MENU

//GROUP
function showGroupForm(mode, value_form) {
    var formManipulasiGroup = Ext.create('Ext.form.Panel', {
        id: 'formManipulasiGroup',
        bodyPadding: 5,
        flex: 1,
        url: BASE_URL + 'kontrol_menu/ext_insert_group',
        layout: 'anchor',
        defaults: {
            anchor: '100%'
        },
        defaultType: 'textfield',
        labelAlign: 'top',
        items: [{
            fieldLabel: 'ID Pemetaan Group',
            name: 'ID_Group_Menu',
            allowBlank: true,
            value: 0,
            readOnly: true
        }, {
            fieldLabel: 'Group',
            name: 'ID_Group_Menu_ID_Group',
            xtype: 'combobox',
            valueField: 'id_groupcollections',
            displayField: 'general_group_name',
            emptyText: 'Pilih Group',
            typeAhead: true,
            forceSelection: false,
            selectOnFocus: true,
            valueNotFoundText: 'Pilih Group',
            queryMode: 'local',
            store: new Ext.data.Store({
                noCache: false,
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    actionMethods: {
                        read: 'POST'
                    },
                    extraParams: {
                        id_open: '1'
                    },
                    url: BASE_URL + 'kontrol_menu/ext_get_all_groupcollections',
                    reader: {
                        type: 'json',
                        root: 'results',
                        totalProperty: 'total',
                        idProperty: 'id_groupcollections'
                    }
                },
                fields: ['id_groupcollections', 'general_group_name']
            }),
            listeners: {
                'focus': {
                    fn: function (comboField) {
                        comboField.doQuery(comboField.allQuery, true);
                        comboField.expand();
                    },
                    scope: this
                },
                'change': {
                    fn: function (comboField, newValue) {},
                    scope: this
                }
            }
        }, {
            fieldLabel: 'Menu',
            name: 'ID_Group_Menu_ID_Menu',
            xtype: 'combobox',
            valueField: 'idmenu',
            displayField: 'general_text',
            emptyText: 'Pilih Menu',
            typeAhead: true,
            forceSelection: false,
            selectOnFocus: true,
            valueNotFoundText: 'Pilih Menu',
            queryMode: 'local',
            store: new Ext.data.Store({
                noCache: false,
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    actionMethods: {
                        read: 'POST'
                    },
                    extraParams: {
                        id_open: '1'
                    },
                    url: BASE_URL + 'kontrol_menu/ext_get_all_menu',
                    reader: {
                        type: 'json',
                        root: 'results',
                        totalProperty: 'total',
                        idProperty: 'idmenu'
                    }
                },
                fields: ['idmenu', 'parent_text', 'parent_idmenu', 'general_ismenu', 'general_text', 'general_iconclass', 'general_identify', 'general_idbutton', 'general_idnewtab_popup', 'general_url', 'general_type', 'general_bottombreak', 'general_variableprefix', 'general_status', 'process_view', 'process_tambah', 'process_ubah', 'process_hapus', 'process_proses', 'process_cetak', 'process_cetaksk']
            }),
            listeners: {
                'focus': {
                    fn: function (comboField) {
                        comboField.doQuery(comboField.allQuery, true);
                        comboField.expand();
                    },
                    scope: this
                },
                'change': {
                    fn: function (comboField, newValue) {
                        var tempData = comboField.findRecordByValue(newValue).data;
                        if (tempData) {
                            var temp_formManipulasiGroup = Ext.getCmp('formManipulasiGroup').getForm();
                            temp_formManipulasiGroup.findField('u_access').setVisible(parseInt(tempData.process_view));
                            temp_formManipulasiGroup.findField('u_insert').setVisible(parseInt(tempData.process_tambah));
                            temp_formManipulasiGroup.findField('u_update').setVisible(parseInt(tempData.process_ubah));
                            temp_formManipulasiGroup.findField('u_delete').setVisible(parseInt(tempData.process_hapus));
                            temp_formManipulasiGroup.findField('u_proses').setVisible(parseInt(tempData.process_proses));
                            temp_formManipulasiGroup.findField('u_print').setVisible(parseInt(tempData.process_cetak));
                            temp_formManipulasiGroup.findField('u_print_sk').setVisible(parseInt(tempData.process_cetaksk));
                        }
                    },
                    scope: this
                }
            }
        }, {
            xtype: 'fieldcontainer',
            fieldLabel: 'Proses',
            layout: 'hbox',
            defaults: {
                margins: 4,
                flex: 1,
                xtype: 'checkbox',
                labelAlign: 'top'
            },
            items: [{
                fieldLabel: 'View',
                name: 'u_access'
            }, {
                fieldLabel: 'Tambah',
                name: 'u_insert'
            }, {
                fieldLabel: 'Ubah',
                name: 'u_update'
            }, {
                fieldLabel: 'Hapus',
                name: 'u_delete'
            }, {
                fieldLabel: 'Proses',
                name: 'u_proses'
            }, {
                fieldLabel: 'Cetak',
                name: 'u_print'
            }, {
                fieldLabel: 'Cetak SK',
                name: 'u_print_sk'
            }]
        }],
        buttons: [{
            text: 'Simpan',
            formBind: true,
            disabled: true,
            handler: function () {
                var form = this.up('form').getForm();
                if (form.isValid()) {
                    Ext.getCmp('windowManipulasiFormGroup').body.mask('Loading...');
                    form.submit({
                        success: function (form, action) {
                            Ext.getCmp('windowManipulasiFormGroup').body.unmask();
                            obj = Ext.decode(action.response.responseText);
                            if (obj.success == true) {
                                Ext.Msg.alert('Success', 'Berhasil Memanipulasi Group');
                            } else {
                                Ext.Msg.alert('Failed', obj.errors.reason);
                            }
                            Ext.getCmp('windowManipulasiFormGroup').destroy();
                            DataGroup.load();
                            Load_Variabel(BASE_URL + 'user/set_var_access');
                        },
                        failure: function (form, action) {
                            Ext.getCmp('windowManipulasiFormGroup').body.unmask();
                            obj = Ext.decode(action.response.responseText);
                            Ext.Msg.alert('Failed', obj.errors.reason);
                            Ext.getCmp('windowManipulasiFormGroup').destroy();
                            DataGroup.load();
                        }
                    });
                }
            }
        }]
    });

    var windowManipulasiFormGroup = new Ext.create('Ext.window.Window', {
        id: 'windowManipulasiFormGroup',
        title: 'Manipulasi Pementaan Group & Menu',
        iconCls: 'icon-user',
        modal: true,
        constrainHeader: true,
        closable: true,
        width: 600,
        height: 250,
        bodyStyle: 'padding: 5px;',
        items: [formManipulasiGroup],
        bbar: new Ext.ux.StatusBar({
            text: 'Ready',
            id: 'sbWin_windowManipulasiFormGroup',
            iconCls: 'x-status-valid'
        })
    }).show();

    if (mode == 'Ubah') {
        formManipulasiGroup.getForm().setValues(value_form);
    }
}

Ext.define('MGroup', {
    extend: 'Ext.data.Model',
    fields: ['ID_Group_Menu', 'general_group_name', 'general_group_active', 'u_access', 'u_insert', 'u_update', 'u_delete', 'u_proses', 'u_print', 'u_print_sk', 'ID_Group_Menu_ID_Menu', 'parent_text', 'parent_idmenu', 'ID_Group_Menu_ID_Group', 'ID_Group_Menu_ID_Group', 'general_text']
});

var dataReaderGroup = new Ext.create('Ext.data.JsonReader', {
    id: 'dataReaderGroupGroup',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'ID_Group_Menu'
});

var dataProxyGroup = new Ext.create('Ext.data.AjaxProxy', {
    id: 'dataProxyGroupGroup',
    url: BASE_URL + 'kontrol_menu/ext_get_all_group',
    actionMethods: {
        read: 'POST'
    },
    extraParams: {
        id_open: '1'
    },
    reader: dataReaderGroup,
    afterRequest: function (request, success) {}
});

var groupingFeatureGroup = Ext.create('Ext.grid.feature.Grouping', {
    groupHeaderTpl: 'Parent Menu : <span style="color:red !important;">{[values.rows[0].parent_text_idcolomns_group]}</span> ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
});

var DataGroup = new Ext.create('Ext.data.Store', {
    groupField: 'parent_idmenu',
    id: 'DataGroup',
    model: 'MGroup',
    pageSize: 100000000000000000000,
    autoLoad: true,
    proxy: dataProxyGroup
});

var cbGridGroup = new Ext.create('Ext.selection.CheckboxModel');
var gridGROUP = new Ext.create('Ext.grid.Panel', {
    id: 'gridGROUP',
    store: DataGroup,
    title: 'DAFTAR PEMETAAN GROUP & MENU',
    frame: true,
    border: true,
    loadMask: true,
    style: 'margin:0 auto;',
    height: '100%',
    width: '100%',
    selModel: cbGridGroup,
    columnLines: true,
    invalidateScrollerOnRefresh: false,
    columns: [{
        header: 'No',
        xtype: 'rownumberer',
        width: 35,
        resizable: true,
        style: 'padding-top: .5px;'
    }, {
        header: "ID Group",
        dataIndex: 'ID_Group_Menu_ID_Group',
        flex: 1
    }, {
        header: "Group Name",
        dataIndex: 'general_group_name',
        flex: 1
    }, {
        header: "ID Menu",
        dataIndex: 'ID_Group_Menu_ID_Menu',
        flex: 1
    }, {
        header: "Parent Menu Name",
        dataIndex: 'parent_text',
        flex: 1,
        id: 'parent_text_idcolomns_group'
    }, {
        header: "Menu Name",
        dataIndex: 'general_text',
        flex: 1
    }, {
        header: "View",
        dataIndex: 'u_access',
        width: 60,
        renderer: function (v) {
            return (v == 1 ? 'Yes' : 'No')
        }
    }, {
        header: "Tambah",
        dataIndex: 'u_insert',
        width: 60,
        renderer: function (v) {
            return (v == 1 ? 'Yes' : 'No')
        }
    }, {
        header: "Ubah",
        dataIndex: 'u_update',
        width: 60,
        renderer: function (v) {
            return (v == 1 ? 'Yes' : 'No')
        }
    }, {
        header: "Hapus",
        dataIndex: 'u_delete',
        width: 60,
        renderer: function (v) {
            return (v == 1 ? 'Yes' : 'No')
        }
    }, {
        header: "Proses",
        dataIndex: 'u_proses',
        width: 60,
        renderer: function (v) {
            return (v == 1 ? 'Yes' : 'No')
        }
    }, {
        header: "Cetak",
        dataIndex: 'u_print',
        width: 60,
        renderer: function (v) {
            return (v == 1 ? 'Yes' : 'No')
        }
    }, {
        header: "Cetak SK",
        dataIndex: 'u_print_sk',
        width: 60,
        renderer: function (v) {
            return (v == 1 ? 'Yes' : 'No')
        }
    }, ],
    tbar: [{
        fieldLabel: 'Group',
        hideLabel: true,
        name: 'group_select_combobox',
        xtype: 'combobox',
        id: 'group_select_combobox',
        valueField: 'id_groupcollections',
        displayField: 'general_group_name',
        emptyText: 'Pilih Group',
        value: 0,
        typeAhead: true,
        forceSelection: false,
        selectOnFocus: true,
        valueNotFoundText: 'Pilih Group',
        queryMode: 'local',
        store: new Ext.data.Store({
            noCache: false,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                extraParams: {
                    id_open: '1',
                    combox: '1'
                },
                url: BASE_URL + 'kontrol_menu/ext_get_all_groupcollections',
                reader: {
                    type: 'json',
                    root: 'results',
                    totalProperty: 'total',
                    idProperty: 'id_groupcollections'
                }
            },
            fields: ['id_groupcollections', 'general_group_name']
        }),
        listeners: {
            'focus': {
                fn: function (comboField) {
                    comboField.doQuery(comboField.allQuery, true);
                    comboField.expand();
                },
                scope: this
            },
            'change': {
                fn: function (comboField, newValue) {
                    DataGroup.getProxy().extraParams.id_groupcollections = newValue;
                    DataGroup.load();
                },
                scope: this
            }
        }
    }, '-',
    {
        text: 'Tambah',
        id: 'tambah_group',
        iconCls: 'icon-add',
        handler: function () {
            showGroupForm('Tambah', '');
        },
        disabled: pl_insert
    }, '-',
    {
        text: 'Ubah',
        id: 'ubah_group',
        iconCls: 'icon-edit',
        handler: function () {
            var sm = gridGROUP.getSelectionModel(),
                sel = sm.getSelection();
            if (sel.length == 1) {
                var value_form = {
                    ID_Group_Menu: sel[0].get('ID_Group_Menu'),
                    u_access: sel[0].get('u_access'),
                    u_insert: sel[0].get('u_insert'),
                    u_update: sel[0].get('u_update'),
                    u_delete: sel[0].get('u_delete'),
                    u_proses: sel[0].get('u_proses'),
                    u_print: sel[0].get('u_print'),
                    u_print_sk: sel[0].get('u_print_sk'),
                    ID_Group_Menu_ID_Menu: sel[0].get('ID_Group_Menu_ID_Menu'),
                    ID_Group_Menu_ID_Group: sel[0].get('ID_Group_Menu_ID_Group'),
                };
                showGroupForm('Ubah', value_form);
            }
        },
        disabled: pl_update
    }, '-',
    {
        text: 'Hapus',
        iconCls: 'icon-delete',
        handler: function () {

        },
        disabled: pl_delete
    }, '-',
    {
        text: 'Auto Sync Data',
        iconCls: 'x-tbar-loading',
        handler: function () {
            Ext.Msg.show({
                title: 'Konfirmasi',
                msg: 'Timpah Data Yang Sudah Ada?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.Msg.WARNING,
                fn: function (btn) {
                    var timpah = 0;
                    if (btn == 'yes') {
                        timpah = 1;
                    }

                    Ext.getBody().mask('Loading...');

                    Ext.Ajax.request({
                        url: BASE_URL + 'kontrol_menu/ext_sync_group',
                        method: 'POST',
                        params: {
                            id_open: 1,
                            timpah: timpah
                        },
                        scripts: true,
                        renderer: 'data',
                        success: function (response) {
                            Ext.getBody().unmask();
                            DataGroup.load();
                        },
                        failure: function (response) {
                            Ext.getBody().unmask();
                            Ext.MessageBox.show({
                                title: 'Peringatan !',
                                msg: 'Gagal Menghapus Data !',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        },
                        scope: this
                    });
                }
            });
        },
        disabled: pl_insert
    }],
    features: [groupingFeatureGroup],
    viewConfig: {
        stripeRows: false,
        getRowClass: function (record) {
            return record.get('general_group_active') == 0 ? 'child-row' : '';
        }
    },
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: DataGroup,
        dock: 'bottom',
        displayInfo: true
    }],
    listeners: {
        itemdblclick: function (dataview, record, item, index, e) {
            Ext.getCmp('ubah_group').handler.call(Ext.getCmp("ubah_group").scope);
        }
    }
});
//end GROUP

//GROUP COLLECTIONS
function showGroupCollectionsForm(mode, value_form) {
    var formManipulasiGroupCollections = Ext.create('Ext.form.Panel', {
        id: 'formManipulasiGroupCollections',
        bodyPadding: 5,
        flex: 1,
        url: BASE_URL + 'kontrol_menu/ext_insert_groupcollections',
        layout: 'anchor',
        defaults: {
            anchor: '100%'
        },
        defaultType: 'textfield',
        labelAlign: 'top',
        items: [{
            fieldLabel: 'ID Group',
            name: 'id_groupcollections',
            value: 0,
            readOnly: true,
            allowBlank: true
        }, {
            fieldLabel: 'Group Name',
            name: 'general_group_name',
            allowBlank: false
        }, {
            fieldLabel: 'Status Active',
            name: 'general_group_active',
            xtype: 'checkbox'
        }],
        buttons: [{
            text: 'Simpan',
            formBind: true,
            disabled: true,
            handler: function () {
                var form = this.up('form').getForm();
                if (form.isValid()) {
                    Ext.getCmp('windowManipulasiFormGroupCollections').body.mask('Loading...');
                    form.submit({
                        success: function (form, action) {
                            Ext.getCmp('windowManipulasiFormGroupCollections').body.unmask();
                            obj = Ext.decode(action.response.responseText);
                            if (obj.success == true) {
                                Ext.Msg.alert('Success', 'Berhasil Memanipulasi Koleksi Group');
                            } else {
                                Ext.Msg.alert('Failed', obj.errors.reason);
                            }
                            Ext.getCmp('windowManipulasiFormGroupCollections').destroy();
                            DataGroupCollections.load();
                        },
                        failure: function (form, action) {
                            Ext.getCmp('windowManipulasiFormGroupCollections').body.unmask();
                            obj = Ext.decode(action.response.responseText);
                            Ext.Msg.alert('Failed', obj.errors.reason);
                            Ext.getCmp('windowManipulasiFormGroupCollections').destroy();
                            DataGroupCollections.load();
                        }
                    });
                }
            }
        }]
    });

    var windowManipulasiFormGroupCollections = new Ext.create('Ext.window.Window', {
        id: 'windowManipulasiFormGroupCollections',
        title: 'Manipulasi GroupCollections',
        iconCls: 'icon-user',
        modal: true,
        constrainHeader: true,
        closable: true,
        width: 600,
        height: 200,
        bodyStyle: 'padding: 5px;',
        items: [formManipulasiGroupCollections],
        bbar: new Ext.ux.StatusBar({
            text: 'Ready',
            id: 'sbWin_windowManipulasiFormGroupCollections',
            iconCls: 'x-status-valid'
        })
    }).show();

    if (mode == 'Ubah') {
        formManipulasiGroupCollections.getForm().setValues(value_form);
    }
}

Ext.define('MGroupCollections', {
    extend: 'Ext.data.Model',
    fields: ['id_groupcollections', 'general_group_name', 'general_group_active']
});

var dataReaderGroupCollections = new Ext.create('Ext.data.JsonReader', {
    id: 'dataReaderGroupCollectionsGroupCollections',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'id_groupcollections'
});

var dataProxyGroupCollections = new Ext.create('Ext.data.AjaxProxy', {
    id: 'dataProxyGroupCollectionsGroupCollections',
    url: BASE_URL + 'kontrol_menu/ext_get_all_groupcollections',
    actionMethods: {
        read: 'POST'
    },
    extraParams: {
        id_open: '1'
    },
    reader: dataReaderGroupCollections,
    afterRequest: function (request, success) {}
});

var DataGroupCollections = new Ext.create('Ext.data.Store', {
    id: 'DataGroupCollections',
    model: 'MGroupCollections',
    pageSize: 100000000000000000000,
    autoLoad: true,
    proxy: dataProxyGroupCollections
});

var cbGridGroupCollections = new Ext.create('Ext.selection.CheckboxModel');
var gridGROUPCOLLECTIONS = new Ext.create('Ext.grid.Panel', {
    id: 'gridGROUPCOLLECTIONS',
    store: DataGroupCollections,
    title: 'DAFTAR KOLEKSI GROUP',
    frame: true,
    border: true,
    loadMask: true,
    style: 'margin:0 auto;',
    height: '100%',
    width: '100%',
    selModel: cbGridGroupCollections,
    columnLines: true,
    invalidateScrollerOnRefresh: false,
    columns: [{
        header: 'No',
        xtype: 'rownumberer',
        width: 35,
        resizable: true,
        style: 'padding-top: .5px;'
    }, {
        header: "ID Group",
        dataIndex: 'id_groupcollections',
        flex: 1
    }, {
        header: "Group Name",
        dataIndex: 'general_group_name',
        flex: 1
    }, {
        header: "Active",
        dataIndex: 'general_group_active',
        flex: 1,
        renderer: function (v) {
            return (v == 1 ? 'Yes' : 'No')
        }
    }],
    tbar: [{
        text: 'Tambah',
        id: 'tambah_groupcollections',
        iconCls: 'icon-add',
        handler: function () {
            showGroupCollectionsForm('Tambah', '');
        },
        disabled: pl_insert
    }, '-',
    {
        text: 'Ubah',
        id: 'ubah_groupcollections',
        iconCls: 'icon-edit',
        handler: function () {
            var sm = gridGROUPCOLLECTIONS.getSelectionModel(),
                sel = sm.getSelection();
            if (sel.length == 1) {
                var value_form = {
                    id_groupcollections: sel[0].get('id_groupcollections'),
                    general_group_name: sel[0].get('general_group_name'),
                    general_group_active: sel[0].get('general_group_active'),
                };
                showGroupCollectionsForm('Ubah', value_form);
            }
        },
        disabled: pl_update
    }, '-',
    {
        text: 'Hapus',
        iconCls: 'icon-delete',
        handler: function () {

        },
        disabled: pl_delete
    }],
    viewConfig: {
        stripeRows: false,
        getRowClass: function (record) {
            return record.get('general_group_active') == 0 ? 'child-row' : '';
        }
    },
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: DataGroupCollections,
        dock: 'bottom',
        displayInfo: true
    }],
    listeners: {
        itemdblclick: function (dataview, record, item, index, e) {
            Ext.getCmp('ubah_groupcollections').handler.call(Ext.getCmp("ubah_groupcollections").scope);
        }
    }
});
//end GROUP COLLECTIONS

// PANEL UTAMA KONTROL MENU  -------------------------------------------- START
var box_border = {
    id: 'border-panel',
    layout: 'border',
    border: false,
    bodyBorder: false,
    defaults: {
        collapsible: true,
        split: true,
        animFloat: false,
        autoHide: false,
        useSplitTips: true,
        bodyStyle: 'padding: 0px;'
    },
    items: [
		{
			region: 'center',
			xtype: 'tabpanel',
			collapsible: false,
			margins: '0 0 0 0',
			border: false,
			items: [
				gridGROUPCOLLECTIONS,
				gridMENU, 
				gridGROUP
			],
			listeners:{
				'tabchange' :{
					fn : function(){
						Ext.getCmp('sync_reoder_menu_toggle').toggle(false);
					}
				}
			}
		}
	]
};
var new_tabpanel = {
    id: 'kontrol_menu',
    title: 'KONTROL MENU',
    iconCls: 'icon-menu_log_pengguna',
	border: false, 
	closable: true,  
	layout: 'fit', 
	items: [ box_border ],
};
// PANEL UTAMA PENGGUNA LOGIN  --------------------------------------------- END
	
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>


