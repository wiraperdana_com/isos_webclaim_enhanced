<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
header("Content-Type: application/x-javascript");

if (isset($jsscript) && $jsscript == TRUE) { ?>

	var popup_title = '<?php echo $this->input->post('popup_title'); ?>';
	var uri_all = '<?php echo $uri_all; ?>';
	var uri_selected = '<?php echo $uri_selected; ?>';
	var uri_by_rows = '<?php echo $uri_by_rows; ?>';
	var Grid_ID = Ext.getCmp('<?php echo $Grid_ID; ?>');
	var Params_Print = <?php echo $Params_Print; ?> ;
	var fpdf_mode = null;
	fpdf_mode = <?php if(isset($fpdf_mode)){ echo 'true'; } else { echo 'false'; } ?>;

	// START - GRID PENANDA TANGAN
	Ext.define('MP_TTD', {
		extend: 'Ext.data.Model',
		fields: ['NIP', 'nama_lengkap', 'kode_unor', 'nama_unor', 'nama_unker', 'kode_jab', 'nama_jab', 'kode_golru', 'nama_pangkat', 'nama_golru']
	});

	var Reader_P_TTD = new Ext.create('Ext.data.JsonReader', {
		id: 'Reader_P_TTD',
		root: 'results',
		totalProperty: 'total',
		idProperty: 'NIP'
	});

	var Proxy_P_TTD = new Ext.create('Ext.data.AjaxProxy', {
		id: 'Proxy_P_TTD',
		url: BASE_URL + 'browse_ref/ext_get_all_p_ttd',
		actionMethods: {
			read: 'POST'
		},
		extraParams: {
			id_open: 1
		},
		reader: Reader_P_TTD
	});

	var Data_P_TTD = new Ext.create('Ext.data.Store', {
		id: 'Data_P_TTD',
		model: 'MP_TTD',
		pageSize: 20,
		noCache: false,
		autoLoad: true,
		proxy: Proxy_P_TTD
	});

	var cbGrid_P_TTD = new Ext.create('Ext.selection.CheckboxModel');

	var Grid_P_TTD = new Ext.create('Ext.grid.Panel', {
		id: 'Grid_P_TTD',
		store: Data_P_TTD,
		frame: true,
		border: true,
		loadMask: true,
		noCache: false,
		style: 'margin:0 auto;',
		autoHeight: true,
		columnLines: true,
		selModel: cbGrid_P_TTD,
		height: '90%',
		width: '100%',
		columns: [{
			header: "Nama Lengkap",
			dataIndex: 'nama_lengkap',
			width: 200
		}, {
			header: "Jabatan",
			dataIndex: 'nama_jab',
			width: 200
		}]
	});
	// END - GRID PENANDA TANGAN

	var form_print_pd = new Ext.create('Ext.form.Panel', {
		id: 'form_print_pd',
		url: null,
		frame: true,
		bodyStyle: 'padding: 1px 5px 0 0;',
		width: '100%',
		height: '100%',
		fieldDefaults: {
			labelAlign: 'right',
			msgTarget: 'side'
		},
		defaults: {
			hideEmptyLabel: false,
			allowBlank: false
		},
		items: [
			// START - PILIHAN EXPORT
			{
				xtype: 'fieldset',
				flex: 1,
				title: 'Pilihan Export',
				layout: 'anchor',
				height: 70,
				items: [{
					xtype: 'fieldcontainer',
					combineErrors: false,
					layout: 'hbox',
					msgTarget: 'side',
					items: [{
						xtype: 'fieldset',
						defaultType: 'radio',
						defaults: {
							anchor: '100%'
						},
						margins: '0 5px 0 0',
						style: 'padding: 0 0 0 0; border-width: 0px;',
						flex: 1,
						items: [{
							boxLabel: 'Semua',
							name: 'pilihan_export',
							id: 'semua_pd',
							inputValue: 'semua',
							listeners: {
								change: function(ctl, val) {
									if (val == true) {
										Ext.getCmp('dari_pd').reset();
										Ext.getCmp('sampai_pd').reset();
									}
								}
							}
						}, {
							boxLabel: 'Yang Dipilih',
							name: 'pilihan_export',
							id: 'terpilih_pd',
							inputValue: 'terpilih',
							checked: true,
							listeners: {
								change: function(ctl, val) {
									if (val == true) {
										Ext.getCmp('dari_pd').reset();
										Ext.getCmp('sampai_pd').reset();
									}
								}
							}
						}]
					}, {
						xtype: 'fieldset',
						defaultType: 'radio',
						defaults: {
							anchor: '100%'
						},
						margins: '0 5px 0 0',
						style: 'padding: 0 0 0 0; border-width: 0px;',
						flex: 1,
						items: [{
							boxLabel: 'Berdasarkan Baris',
							name: 'pilihan_export',
							id: 'perbaris_pd',
							inputValue: 'perbaris_pd'
						}, {
							xtype: 'fieldcontainer',
							layout: 'hbox',
							defaultType: 'textfield',
							combineErrors: true,
							defaults: {
								hideLabel: 'true',
								width: 70
							},
							items: [{
								xtype: 'numberfield',
								name: 'dari',
								id: 'dari_pd',
								emptyText: 'Dari',
								margins: '0 0 0 15',
								minValue: 1,
								listeners: {
									focus: function(selectedField) {
										Ext.getCmp('perbaris_pd').setValue(true);
									},
									change: function(selectedField) {
										Ext.getCmp('perbaris_pd').setValue(true);
									}
								}
							}, {
								xtype: 'numberfield',
								name: 'sampai',
								id: 'sampai_pd',
								emptyText: 'Sampai',
								margins: '0 0 0 5',
								minValue: 1,
								listeners: {
									focus: function(selectedField) {
										Ext.getCmp('perbaris_pd').setValue(true);
									},
									change: function(selectedField) {
										Ext.getCmp('perbaris_pd').setValue(true);
									}
								}
							}]
						}]
					}]
				}]
			},
			// END - PILIHAN EXPORT

			// START - PENDANDA TANGAN
			{
				xtype: 'fieldset',
				flex: 1,
				title: 'Penanda Tangan',
				layout: 'anchor',
				style: 'padding: 0 1px 2px 1px;',
				height: 155,
				items: [Grid_P_TTD]
			}
			// END - PENDANDA TANGAN
		],
		buttons: [{
			text: 'Export',
			id: 'Export_pd',
			handler: function() {
				if(fpdf_mode){
					submit_export_pd_2(); //untuk FExcel
				}
				else{
					submit_export_pd();
				}
			}
		}, {
			text: 'Batal',
			handler: function() {
				new_popup.close();
			}
		}]
	});

	function submit_export_pd_2() {
		var sm = Grid_P_TTD.getSelectionModel(),
			sel = sm.getSelection();
		if (sel.length == 1) {
			Ext.getCmp('Export_pd').setDisabled(true);
			var params_pttd = {};
			var pttd_NIP = sel[0].get('NIP');
			var pttd_nama = sel[0].get('nama_lengkap');
			var pttd_pangkat = sel[0].get('nama_pangkat');
			var pttd_golru = sel[0].get('nama_golru');
			var pttd_jab = sel[0].get('nama_jab');
			var pttd_unker = sel[0].get('nama_unker');
			var kode_unker = 'ALL';
			params_pttd = {
				pttd_NIP: pttd_NIP,
				pttd_nama: pttd_nama,
				pttd_pangkat: pttd_pangkat,
				pttd_golru: pttd_golru,
				pttd_jab: pttd_jab,
				pttd_unker: pttd_unker,
				kode_unker: kode_unker
			};

			if (Ext.getCmp('semua_pd').getValue() == true) {
				Ext.getCmp('sbwin_print_pd').showBusy({
					text: 'Silahkan tunggu! Sedang melakukan export ke Excel ...'
				});
				var params = serialize(objectMerge(Params_Print, params_pttd));
				export_export_pdf(BASE_URL + uri_all + params);
				// Ext.Ajax.timeout = Time_Out;
				// Ext.Ajax.request({
				// 	url: BASE_URL + uri_all,
				// 	method: 'POST',
				// 	params: objectMerge({
				// 		id_open: 1,
				// 		idx: '<?php echo $Grid_ID; ?>'
				// 	}, Params_Print, params_pttd),
				// 	success: function(response) {
				// 		export_export_pd(response);
				// 	},
				// 	scope: this
				// });
			} else if (Ext.getCmp('terpilih_pd').getValue() == true) {
				var sm = Grid_ID.getSelectionModel(),
					sel = sm.getSelection(),
					data = '';
				if (sel.length > 0) {
					Ext.getCmp('sbwin_print_pd').showBusy({
						text: 'Silahkan tunggu! Sedang melakukan export ke Excel ...'
					});
					for (i = 0; i < sel.length; i++) {
						data = data + sel[i].get('<?php echo $Data_ID; ?>') + '-';
					}
					var params = serialize(objectMerge({postdata: data},Params_Print, params_pttd));
					export_export_pdf(BASE_URL + uri_selected + params);
				} else {
					Ext.MessageBox.show({
						title: 'Peringatan !',
						msg: 'Silahkan pilih data yang ingin Anda export !',
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.ERROR
					});
					win_print_pd.close();
				}
			} else {
				var dari = Ext.getCmp('dari_pd').getValue();
				var sampai = Ext.getCmp('sampai_pd').getValue();
				if (dari == null || sampai == null) {
					Ext.MessageBox.show({
						title: 'Peringatan !',
						msg: 'Tentukan batasan baris yang akan diexport !',
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.ERROR
					});
					Ext.getCmp('Export_pd').setDisabled(false);
				} else if (dari > sampai) {
					Ext.MessageBox.show({
						title: 'Peringatan !',
						msg: 'Kesalahan penentuan batasan baris !',
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.ERROR
					});
					Ext.getCmp('Export_pd').setDisabled(false);
				} else {
					Ext.getCmp('sbwin_print_pd').showBusy({
						text: 'Silahkan tunggu! Sedang melakukan export ke Excel ...'
					});
					var params = serialize(objectMerge(Params_Print, params_pttd));
					export_export_pdf(BASE_URL + uri_by_rows + Ext.getCmp('dari_pd').getValue() + '/' + Ext.getCmp('sampai_pd').getValue() + params);
				}
			}
		} else {
			Ext.MessageBox.show({
				title: 'Peringatan !',
				msg: 'Pilih salah satu penanda tangan !',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.ERROR
			});
			Ext.getCmp('Export_pd').setDisabled(false);
		}
	}
	
	function submit_export_pd(){
			var sm = Grid_P_TTD.getSelectionModel(), sel = sm.getSelection();
		  if(sel.length <= 0 || sel.length == 1){
			Ext.getCmp('Export_pd').setDisabled(true);
			var params_pttd = {};
			if(sel.length == 1){
				var pttd_NIP = sel[0].get('NIP');
				var pttd_nama = sel[0].get('nama_lengkap');
				var pttd_pangkat = sel[0].get('nama_pangkat');
				var pttd_golru = sel[0].get('nama_golru');
				var pttd_jab = sel[0].get('nama_jab');
				var pttd_unker = sel[0].get('nama_unker');
				params_pttd = {pttd_NIP:pttd_NIP, pttd_nama:pttd_nama, pttd_pangkat:pttd_pangkat, pttd_golru:pttd_golru, pttd_jab:pttd_jab, pttd_unker:pttd_unker};
			}
				if(Ext.getCmp('semua_pd').getValue() == true){
					Ext.getCmp('sbwin_print_pd').showBusy({text:'Silahkan tunggu! Sedang melakukan export ke Excel ...'});
					Ext.Ajax.timeout = Time_Out;
					Ext.Ajax.request({
					url: BASE_URL + uri_all, method: 'POST',
				  params: objectMerge({id_open: 1, idx:'<?php echo $Grid_ID; ?>'}, Params_Print, params_pttd),
				  success: function(response){export_export_pd(response);},
				  scope: this
				});
				}else if(Ext.getCmp('terpilih_pd').getValue() == true){
					var sm = Grid_ID.getSelectionModel(), sel = sm.getSelection(), data = '';
				if(sel.length > 0){
					Ext.getCmp('sbwin_print_pd').showBusy({text:'Silahkan tunggu! Sedang melakukan export ke Excel ...'});
					for (i = 0; i < sel.length; i++) {
					data = data + sel[i].get('<?php echo $Data_ID; ?>') + '-';
						}
						Ext.Ajax.timeout = Time_Out;
						Ext.Ajax.request({
						url: BASE_URL + uri_selected, method: 'POST',
					params: objectMerge({id_open: 1, postdata: data, idx:'<?php echo $Grid_ID; ?>'}, Params_Print, params_pttd),
					success: function(response){export_export_pd(response);},
					scope: this
					});
				}else{
						Ext.MessageBox.show({title:'Peringatan !', msg:'Silahkan pilih data yang ingin Anda export !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
						win_print_pd.close();
					}
				}else{
					var dari = Ext.getCmp('dari_pd').getValue();
					var sampai = Ext.getCmp('sampai_pd').getValue();
					if(dari == null || sampai == null){
						Ext.MessageBox.show({title:'Peringatan !', msg:'Tentukan batasan baris yang akan diexport !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
						Ext.getCmp('Export_pd').setDisabled(false);
					}else if(dari > sampai){
						Ext.MessageBox.show({title:'Peringatan !', msg:'Kesalahan penentuan batasan baris !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
						Ext.getCmp('Export_pd').setDisabled(false);
					}else{
						Ext.getCmp('sbwin_print_pd').showBusy({text:'Silahkan tunggu! Sedang melakukan export ke Excel ...'});
						Ext.Ajax.timeout = Time_Out;
						Ext.Ajax.request({
						url: BASE_URL + uri_by_rows + Ext.getCmp('dari_pd').getValue() + '/' + Ext.getCmp('sampai_pd').getValue(), method: 'POST',
					params: objectMerge(Params_Print, params_pttd, {idx:'<?php echo $Grid_ID; ?>'}),
					success: function(response){export_export_pd(response);},
					scope: this
					});
					}
				}
			}else{
				Ext.MessageBox.show({title:'Peringatan !', msg:'Pilih salah satu penanda tangan !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
				Ext.getCmp('Export_pd').setDisabled(false);
			}
		}

	function export_export_pdf(url, title) {
		var docprint = new Ext.create('Ext.window.Window', {
			title: 'Export Excel',
			iconCls: 'icon-xls',
			constrainHeader: true,
			closable: true,
			maximizable: true,
			width: '100%',
			height: '100%',
			bodyStyle: 'padding: 5px;',
			modal: true,
			items: {
	            xtype: 'component',
	            autoEl: {
	                tag: 'iframe',
	                style: 'height: 100%; width: 100%; border: none',
	                src: url
	            }
	        }
		}).show();
		Ext.getCmp('Export_pd').setDisabled(false);
		win_print_pd.close();
	}

	function export_export_pd(response) {
		if (isURL(response.responseText)) {
			win_print_pd.close();
			var docprint = new Ext.create('Ext.window.Window', {
				title: popup_title,
				iconCls: 'icon-printer',
				constrainHeader: true,
				closable: true,
				maximizable: true,
				width: '20%',
				height: '20%',
				bodyStyle: 'padding: 5px;',
				modal: true,
				items: [{
					xtype: 'tabpanel',
					activeTab: 0,
					width: '100%',
					height: '100%',
					items: [{
						title: 'Export',
						frame: false,
						collapsible: true,
						autoScroll: true,
						iconCls: 'icon-xls',
						items: [{
							xtype: 'miframe',
							frame: false,
							height: '100%',
							src: response.responseText
						}]
					}]
				}]
			}).show();
		} else {
			Ext.MessageBox.show({
				title: 'Peringatan !',
				msg: 'Proses export gagal !',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.ERROR
			});
			win_print_pd.close();
		}
	}

	var win_print_pd = new Ext.create('Ext.window.Window', {
		id: 'win_print_pd',
		title: popup_title,
		iconCls: 'icon-printer',
		constrainHeader: true,
		closable: true,
		width: 500,
		height: 350,
		bodyStyle: 'padding: 5px;',
		modal: true,
		items: [form_print_pd],
		bbar: new Ext.ux.StatusBar({
			text: 'Ready',
			id: 'sbwin_print_pd',
			iconCls: 'x-status-valid'
		})
	});

	var new_popup = win_print_pd;

	function serialize( obj ) {
		return '?'+Object.keys(obj).reduce(function(a,k){a.push(k+'='+encodeURIComponent(obj[k]));return a},[]).join('&')
	}

	<?php
} else {
	echo "var new_popup = 'GAGAL';";
} ?>