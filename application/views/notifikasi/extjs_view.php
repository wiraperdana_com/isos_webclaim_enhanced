<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

Ext.define('MT_Notifikasi', {
    extend: 'Ext.data.Model',
    fields: [
        'tnf_id',
        'tnf_title',
        'tnf_description',
        'tnf_send_date',
        'tnf_send_NIP',
        'tnf_ref_id',
        'tnf_ref_table',
        'tnf_type',
        'tnf_receiver_id',
        'tnf_receiver_value',
        'tnf_receiver_date',
        'tnf_receiver_is_read',
        'tnf_receiver_tnf_id',
        'tnf_receiver_as_approval',
        'tnf_receiver_type',
        'tnf_approval_id',
        'tnf_approval_value',
        'tnf_approval_value_approve',
        'tnf_approval_value_reject',
        'tnf_approval_date',
        'tnf_approval_tnf_id',
        'tnf_approval_by_NIP',
        'tnf_approval_note_1',
        'tnf_approval_note_2'
    ]
});

var Reader_T_Notifikasi = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Notifikasi',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'tnf_id'  	
});

var Proxy_T_Notifikasi = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Notifikasi',
    url: BASE_URL + 'notifikasi/ext_get_all',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Notifikasi
});

var Data_T_Notifikasi = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Notifikasi',
    model: 'MT_Notifikasi',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Notifikasi
});

var cbGrid_T_Notifikasi = new Ext.create('Ext.selection.RowModel');

var Grid_T_Notifikasi = new Ext.create('Ext.grid.Panel', {
    region : 'center',
    id: 'Grid_T_Notifikasi',
    store: Data_T_Notifikasi,
    border: true,
    loadMask: true,
    noCache: false,
    style: 'margin:0 auto;',
    autoHeight: true,
    columnLines: true,
    flex:1.5,
    selModel: cbGrid_T_Notifikasi,
    viewConfig: { 
        stripeRows: false, 
        getRowClass: function(record) { 
            return record.get('tnf_receiver_is_read') == 0 ? 'child-bold-row' : ''; 
        } 
    },
    columns: [
        {header: 'No', xtype: 'rownumberer', width: 35, height : 20,resizable: true, style: 'padding-top: .2px;'}, 
        {header: "Title", dataIndex: 'tnf_title', flex:1}, 
        {header: "Description", dataIndex: 'tnf_description', flex:1}, 
        {header: "Date", dataIndex: 'tnf_send_date', flex:1}, 
        {header: "Send By", dataIndex: 'tnf_send_NIP', flex:1}, 
        {
            header: "Status", dataIndex: 'tnf_type', flex:1,
            renderer: function(v, d, x){
                if (x.data.tnf_type == 1){
                    var msgPr = ' by Other';
                    if (x.data.tnf_receiver_as_approval == 1 ) {
                        msgPr = ' by Me';
                    }
                    var msg = 'Waiting for Approval';
                    
                    if (x.data.tnf_approval_value == x.data.tnf_approval_value_approve ){
                        msg = 'Approved'+msgPr;
                    }else if( x.data.tnf_approval_value == x.data.tnf_approval_value_reject) {
                        msg = 'Rejected'+msgPr;
                    }else{
                        msg = 'Waiting for Approval'+msgPr;
                    }
                    return msg;
                }else{
                    return 'Informasi';
                }
            }
        },
        {header: "Type", dataIndex: 'type', flex:1, renderer:function(a,b,c){
                return ( c.data.tnf_receiver_type == 0  ? 'Personal' : 'Group' );
        }}
    ],
    tbar: [
        {
			disabled:true,
            text:'Lihat Data',
			id:'view_data_widget',
            iconCls:'icon-menu_lap_nom',
            handler:function(){
				var sm = Grid_T_Notifikasi.getSelectionModel(),
					sel = sm.getSelection();
				if (sel.length > 0) {
					Grid_T_Notifikasi.fireEvent('itemdblclick', Grid_T_Notifikasi, sel[0]);
				}
            }
        },'->',{
            text:'Sudah Dibaca',
			enableToggle:true,
			iconCls: 'icon-filter',
            toggleHandler :function(btn, stat){
				Data_T_Notifikasi.getProxy().extraParams['all'] = ( stat ? 1 : 0 );
				Data_T_Notifikasi.load();
            }
		}
    ],
    bbar: [
        {
            text:'Refresh',
            iconCls:'x-tbar-loading',
            handler:function(){
                Data_T_Notifikasi.load();
            }
        }
    ],
    listeners: {
        selectionchange: function(model, records) {
			Ext.getCmp('view_data_widget').setDisabled( records.length > 0 ? false : true );
			
            if (records[0]) {
                var statusBtn = false;
                if (records[0].data.tnf_type == 1){
                    if (records[0].data.tnf_receiver_as_approval == 1 ) {
                        if (records[0].data.tnf_approval_value != records[0].data.tnf_approval_value_approve && records[0].data.tnf_approval_value != records[0].data.tnf_approval_value_reject) {
                            statusBtn = true;
                        }
                    }
                }
                ActionEnableBtn( statusBtn );
                
                Tpl_T_Notifikasi.overwrite(Panel_T_Notifikasi.body, records[0].data);
                
                notifyIsRead();
            }
        },
        itemdblclick: function(dataview, record, item, index, e) {
            if (record) {
				if( record.data.tnf_ref_id && record.data.tnf_ref_table ) {
					Load_As_Widget( record.data.tnf_ref_id, record.data.tnf_ref_table );
				}
			}
        }    
    }  
});

var Tpl_T_Notifikasi = new Ext.XTemplate(
    '<div style="width:100%; border:0px solid silver;">',
        '<div>',
            '<div style="padding:5px;padding-bottom:0px"><b>Title</b></div>',
            '<div style="padding:5px;padding-top:0px;">{tnf_title}</div>',
        '</div>',
        '<div>',
            '<div style="padding:5px;padding-bottom:0px;"><b>Date</b></div>',
            '<div style="padding:5px;padding-top:0px;">{tnf_send_date}</div>',
        '</div>',
        '<div>',
            '<div style="padding:5px;padding-bottom:0px;"><b>Send By</b></div>',
            '<div style="padding:5px;padding-top:0px;">{tnf_send_NIP}</div>',
        '</div>',
        '<div>',
            '<div style="padding:5px;padding-bottom:0px;"><b>Description</b></div>',
            '<div style="padding:5px;padding-top:0px;">{tnf_description}</div>',
        '</div>',
        '<div>',
            '<div style="padding:5px;padding-bottom:0px;"><b>Note 1</b></div>',
            '<div style="padding:5px;padding-top:0px;">{tnf_approval_note_1}</div>',
        '</div>',
        '<div>',
            '<div style="padding:5px;padding-bottom:0px;"><b>Note 2</b></div>',
            '<div style="padding:5px;padding-top:0px;">{tnf_approval_note_2}</div>',
        '</div>',
        '<div>',
            '<div style="padding:5px;padding-bottom:0px;"><b>Approve / Reject By</b></div>',
            '<div style="padding:5px;padding-top:0px;">{tnf_approval_by_NIP}</div>',
        '</div>',
        '<div>',
            '<div style="padding:5px;padding-bottom:0px;"><b>Approve / Reject Date</b></div>',
            '<div style="padding:5px;padding-top:0px;">{tnf_approval_date}</div>',
        '</div>',
    '</div>'
);

function notifyIsRead(){
    var selectionreal = Grid_T_Notifikasi.getSelectionModel().getSelection();
    if(selectionreal.length > 0){
        var selection = selectionreal[0].data;
        if(selection.tnf_receiver_is_read == 0){
            var idnotify = selection.tnf_id;
            var idreceiver = selection.tnf_receiver_id;
            
            selectionreal[0].set('tnf_receiver_is_read', 1)
            
            Ext.Ajax.request({
                url: BASE_URL + 'notifikasi/ext_receiver_isread',
                method: 'POST',
                params: { id_open:1, idnotify: idnotify, idreceiver: idreceiver },
                success: function(response){
                },
                failure: function(response){
                }
            });
        }
    }
}

function createPromtNotifikasi(isapprove) {
    var titlePromt = ( isapprove ? "Approve" : "Reject" );
    Ext.Msg.show({
        title: titlePromt,
        buttons: Ext.Msg.OKCANCEL,
        multiline: true,
        prompt:true,
        fn:function(btn, text){
            if (btn == 'ok'){
                var selection = Grid_T_Notifikasi.getSelectionModel().getSelection();
                if(selection.length > 0){
                    selection = selection[0].data;
                    
                    var action = ( isapprove ? 1 : 0 );
                    var id = selection.tnf_id;
                    var note2 = text;
                    
                    Ext.Ajax.request({
                        url: BASE_URL + 'notifikasi/ext_approval_action',
                        method: 'POST',
                        params: { id_open:1, action: action, id: id, note2: note2 },
                        success: function(response){
                            ActionEnableBtn( false );
                            Tpl_T_Notifikasi.overwrite(Panel_T_Notifikasi.body, {});
                            var obj = Ext.decode(response.responseText);
                            Ext.MessageBox.show({
                                    title:'Informasi !',
                                    msg: obj.message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: ( obj.success ? Ext.MessageBox.INFO : Ext.MessageBox.ERROR )
                            });
                            Data_T_Notifikasi.load();
                        },
                        failure: function(response){
                            ActionEnableBtn( false );
                            Tpl_T_Notifikasi.overwrite(Panel_T_Notifikasi.body, {});
                            Ext.MessageBox.show({
                                    title:'Peringatan !',
                                    msg: 'Telah Terjadi kesalahan',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR
                            });
                            Data_T_Notifikasi.load();
                        }
                    });
                }
            }
        }
    });
}

var Panel_T_Notifikasi = new Ext.create('Ext.panel.Panel',{
    region : 'east',
    layout: 'fit',
    margins:'0px 0px 0px 4px',
    bodyStyle:'padding:10px;',
    flex:0.5,
    autoScroll:true,
    title : 'Notifikasi Detil',
    bbar:[
        {
            text:'Approve',
            id:'action_approve',
            hidden:true,
            handler:  function(){
                createPromtNotifikasi(true);
            }
        }, '->',{
            xtype:'label',
            height:25,
        },'->', {
            text:'Reject',
            hidden:true,
            id:'action_reject',
            handler:  function(){
                createPromtNotifikasi(false);
            }
        }
    ]
});

function ActionEnableBtn(st) {
    Ext.getCmp('action_approve').setVisible(st);
    Ext.getCmp('action_reject').setVisible(st);
}

var new_tabpanel = {
    id: 'newtab_notifikasi', 
    title: 'Notifikasi', 
    iconCls: 'icon-information',
    border: false, 
    closable: true,  
    layout: 'border', 
    items: [
        Grid_T_Notifikasi,
        Panel_T_Notifikasi
    ]
}
<?php
	if ( $this->input->post('request_ess') ) {
?>
var new_tabpanel_ess = {
    id: 'newtab_notifikasi', 
    title: 'To-Do ESS', 
    iconCls: 'icon-information',
    border: false, 
    layout: 'border', 
    items: [
        Grid_T_Notifikasi,
        Panel_T_Notifikasi
    ]
}
<?php
	}
?>
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>
