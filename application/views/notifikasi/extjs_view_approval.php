<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Content-Type: application/x-javascript");
	
if(isset($jsscript) && $jsscript == TRUE){ ?>

Ext.define('paramsDataApproval',{
	data : {},
	add:function(d){
		var tempD = Object.keys(d)
		for(var i in tempD){
			if (Ext.isString(tempD[i])){
				this.data[tempD[i]] = d[tempD[i]];
			}
		}
	},
	set:function(d){
		this.data = d;
	},
	get:function(){
		return this.data;
	}
});

var paramsDataApproval = Ext.create('paramsDataApproval');
paramsDataApproval.set(<?php echo $paramsData; ?>);

var paramsDataApprovalStatusNIP = (Object.keys(paramsDataApproval.get()).indexOf('NIP') == -1 ? false : true);

Ext.define('MT_Notifikasi_Approval', {
    extend: 'Ext.data.Model',
    fields: [
        'value',
        'name',
        'type',
        'asapproval'
    ]
});

var Reader_T_Notifikasi_Approval = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Notifikasi_Approval',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'value'  	
});

var Proxy_T_Notifikasi_Approval_User = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Notifikasi_Approval_User',
    url: BASE_URL + 'notifikasi/ext_popup_approval_get_user_avaiable',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Notifikasi_Approval
});
var Proxy_T_Notifikasi_Approval_Group = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Notifikasi_Approval_Group',
    url: BASE_URL + 'notifikasi/ext_popup_approval_get_group_avaiable',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Notifikasi_Approval
});

var Proxy_T_Notifikasi_Approval_Atasan_ExtraParams = {id_open: '1'};
if(paramsDataApprovalStatusNIP){
	Proxy_T_Notifikasi_Approval_Atasan_ExtraParams['NIP'] = paramsDataApproval.get().NIP;
}
var Proxy_T_Notifikasi_Approval_Atasan = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Notifikasi_Approval_Atasan',
    url: BASE_URL + 'notifikasi/ext_popup_approval_get_atasan_avaiable',
    actionMethods: {read:'POST'},
    extraParams : Proxy_T_Notifikasi_Approval_Atasan_ExtraParams,
    reader: Reader_T_Notifikasi_Approval
});

var Data_T_Notifikasi_User = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Notifikasi_User',
    model: 'MT_Notifikasi_Approval',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Notifikasi_Approval_User
});
var Data_T_Notifikasi_Group = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Notifikasi_Group',
    model: 'MT_Notifikasi_Approval',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Notifikasi_Approval_Group
});
var Data_T_Notifikasi_Atasan = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Notifikasi_Atasan',
    model: 'MT_Notifikasi_Approval',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Notifikasi_Approval_Atasan
});

var cbGrid_T_Notifikasi_Approval_User = new Ext.create('Ext.selection.CheckboxModel');
var cbGrid_T_Notifikasi_Approval_Group = new Ext.create('Ext.selection.CheckboxModel');
var cbGrid_T_Notifikasi_Approval_Atasan = new Ext.create('Ext.selection.CheckboxModel');

var Grid_T_Notifikasi_Approval_User = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_Notifikasi_Approval_User',
	store: Data_T_Notifikasi_User,
	border: true,
	loadMask: true,
	noCache: false,
	style: 'margin:0 auto;',
	autoHeight: true,
	columnLines: true,
	selModel: cbGrid_T_Notifikasi_Approval_User,
	flex:1,
	title:'User Available',
	columns: [
		{header: 'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .2px;'}, 
		{header: "NIP", dataIndex: 'value', flex:1},
		{header: "Name", dataIndex: 'name', flex:1}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			store: Data_T_Notifikasi_User,
			dock: 'bottom',
			displayInfo: true
		}
	],
	listeners: {
		selectionchange: function(model, records) {
			if (records[0]) {
			}
		},
		itemdblclick: function(dataview, record, item, index, e) {
		
		}    
	},
	tbar:[
		'->',{
			text:'Assign',
			handler:function(){
				Data_T_Notifikasi_Approval_Recevier.add(Grid_T_Notifikasi_Approval_User.getSelectionModel().getSelection());
				Grid_T_Notifikasi_Approval_User.getSelectionModel().clearSelections();
			}
		}
	]  
});
var Grid_T_Notifikasi_Approval_Group = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_Notifikasi_Approval_Group',
	store: Data_T_Notifikasi_Group,
	border: true,
	loadMask: true,
	noCache: false,
	style: 'margin:0 auto;',
	autoHeight: true,
	columnLines: true,
	selModel: cbGrid_T_Notifikasi_Approval_Group,
	flex:1,
	title:'Group Available',
	columns: [
		{header: 'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .2px;'}, 
		{header: "ID", dataIndex: 'value', flex:1},
		{header: "Name", dataIndex: 'name', flex:1}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			store: Data_T_Notifikasi_Group,
			dock: 'bottom',
			displayInfo: true
		}
	],
	listeners: {
		selectionchange: function(model, records) {
			if (records[0]) {
			}
		},
		itemdblclick: function(dataview, record, item, index, e) {
		
		}    
	},
	tbar:[
		'->',{
			text:'Assign',
			handler:function(){
				Data_T_Notifikasi_Approval_Recevier.add(Grid_T_Notifikasi_Approval_Group.getSelectionModel().getSelection());
				Grid_T_Notifikasi_Approval_Group.getSelectionModel().clearSelections();
			}
		}
	]
});
var Grid_T_Notifikasi_Approval_Atasan = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_Notifikasi_Approval_Atasan',
	store: Data_T_Notifikasi_Atasan,
	border: true,
	loadMask: true,
	noCache: false,
	style: 'margin:0 auto;',
	autoHeight: true,
	columnLines: true,
	selModel: cbGrid_T_Notifikasi_Approval_Atasan,
	flex:1,
	title:'Atasan Available',
	columns: [
		{header: 'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .2px;'}, 
		{header: "ID", dataIndex: 'value', flex:1},
		{header: "Name", dataIndex: 'name', flex:1}
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			store: Data_T_Notifikasi_Atasan,
			dock: 'bottom',
			displayInfo: true
		}
	],
	listeners: {
		selectionchange: function(model, records) {
			if (records[0]) {
			}
		},
		itemdblclick: function(dataview, record, item, index, e) {
		
		}    
	},
	tbar:[
		'->',{
			text:'Assign',
			handler:function(){
				Data_T_Notifikasi_Approval_Recevier.add(Grid_T_Notifikasi_Approval_Atasan.getSelectionModel().getSelection());
				Grid_T_Notifikasi_Approval_Atasan.getSelectionModel().clearSelections();
			}
		}
	]
});

var Data_T_Notifikasi_Approval_Recevier_Stack = <?php echo $Data_T_Notifikasi_Approval_Recevier_Stack; ?>;
var Data_T_Notifikasi_Approval_Recevier = Ext.create('Ext.data.Store', {
    storeId:'Data_T_Notifikasi_Approval_Recevier',
    autoLoad: true,
    model: 'MT_Notifikasi_Approval',
    data:Data_T_Notifikasi_Approval_Recevier_Stack,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
var cbGrid_T_Notifikasi_Approval_Recevier = new Ext.create('Ext.selection.CheckboxModel');
var Grid_T_Notifikasi_Approval_Recevier = Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_Notifikasi_Approval_Recevier',
	store: Data_T_Notifikasi_Approval_Recevier,
	border: true,
	loadMask: true,
	noCache: false,
	style: 'margin:0 auto;',
	autoHeight: true,
	selModel:cbGrid_T_Notifikasi_Approval_Recevier,
	region:'center',
	flex:1,
	margins:'0px 0px 4px 0px',
	plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
	columns: [
		{header: "Value", dataIndex: 'value', flex:1},
		{header: "Name", dataIndex: 'name', flex:1},
		{header: "Type", dataIndex: 'type', flex:1, renderer:function(a,b,c){
			return ( c.data.type == 0  ? 'Personal' : 'Group' );
		}},
		{header: "As Approval", dataIndex: 'asapproval', flex:1, editor:
		{
			xtype: 'combobox',
			store: new Ext.data.SimpleStore({
				data: [['Yes'], ['No']], fields: ['asapproval']
			}),
			displayField: 'asapproval',
			valueField: 'asapproval'
		}}
	],
	listeners: {
		selectionchange: function(model, records) {
			if (records[0]) {
			}
		}   
	},
	tbar:[
		/*{
			text:'Set as Default Receiver',
			handler:function(){
				
			}
		},*/'->',{
			text:'Remove',
			handler:function(){
				Data_T_Notifikasi_Approval_Recevier.remove(Grid_T_Notifikasi_Approval_Recevier.getSelectionModel().getSelection());
			}
		}
	]
});

var win_notifikasi_popup = new Ext.create('Ext.window.Window', {
	id: 'win_notifikasi_popup',
	title: "Assign Notifications",
	iconCls: 'icon-menu_lap_nom',
	constrainHeader : true,
	closable: true,
	width: Scr_Width / 1.5,
	height: Scr_Height / 1.3,
	bodyStyle: 'padding: 5px;',
	modal : true,
	layout:'border',
	items: [
		{
			flex:1,
			region:'west',
			xtype:'tabpanel',
			border:false,
			items:[
				Grid_T_Notifikasi_Approval_User,
				Grid_T_Notifikasi_Approval_Group,
				Grid_T_Notifikasi_Approval_Atasan
			],
			listeners:{
				tabchange:function(a,b){
					if(b.id == 'Grid_T_Notifikasi_Approval_User'){
						Data_T_Notifikasi_User.load();
					}else if(b.id == 'Grid_T_Notifikasi_Approval_Group'){
						Data_T_Notifikasi_Group.load();
					}else if(b.id == 'Grid_T_Notifikasi_Approval_Atasan'){
						Data_T_Notifikasi_Atasan.load();
					}
				}
			}
		},{
			region:'center',
			flex:1,
			bodyStyle:'padding:4px;',
			margins:'0px 0px 0px 4px',
			title:'Assign Stack',
			layout:'border',
			items:[
				Grid_T_Notifikasi_Approval_Recevier,
				{
					region:'south',
					flex:0.3,
					id:'note1',
					value:'Tidak ada Catanan',
					xtype:'textarea',
					emptyText:'Note'
				}
			],
			bbar:[
				{
					text:'Submit',
					handler:function(){
						var note1 = Ext.getCmp('note1').getValue();
						if (Data_T_Notifikasi_Approval_Recevier.data.items.length > 0 && note1.length > 0){
							Ext.Msg.show({
								title: 'Confirm',
								msg: 'Submit to Receiver Notify ?',
								buttons: Ext.Msg.YESNO, icon: Ext.Msg.QUESTION,
								fn: function(btn) {
									if (btn == 'yes') {
										var receiverNotify = Ext.pluck(Data_T_Notifikasi_Approval_Recevier.data.items, 'data');
										paramsDataApproval.add({receiver:receiverNotify, note1:note1});
										
										Ext.Ajax.request({
											url: BASE_URL + 'notifikasi/ext_popup_approval_add',
											method: 'POST',
											params: { id_open:1, postdata: Ext.encode(paramsDataApproval.get()) },
											success: function(response){
												var obj = Ext.decode(response.responseText);
												Ext.MessageBox.show({
													title:'Informasi !',
													msg: obj.message,
													buttons: Ext.MessageBox.OK,
													icon: ( obj.success ? Ext.MessageBox.INFO : Ext.MessageBox.ERROR )
												});
												
												if (Object.keys(paramsDataApproval.get()).indexOf('onSuccess') > -1){
													CallBack_Fn_Stack[paramsDataApproval.get().onSuccess](obj);
												}
												
												Ext.getCmp('win_notifikasi_popup').destroy();
											},
											failure: function(response){
												if (Object.keys(paramsDataApproval.get()).indexOf('onError') > -1){
													CallBack_Fn_Stack[paramsDataApproval.get().onError](response);
												}
												
												Ext.MessageBox.show({
													title:'Peringatan !',
													msg: 'Telah Terjadi kesalahan',
													buttons: Ext.MessageBox.OK,
													icon: Ext.MessageBox.ERROR
												});
											}
										});
									}
								}
							});
						}
					}
				}
			]
		}
	],
	listeners:{
		afterrender:function(){
			setTimeout(function(){
				if(paramsDataApprovalStatusNIP == false){
					Ext.MessageBox.show({
						title:'Peringatan !',
						msg: 'NIP tidak dimasukan, data atasan tidak akan tersedia!',
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.ERROR
					});
				}
			}, 1000);
		}
	},
	bbar: new Ext.ux.StatusBar({
		text: 'Ready',
		id: 'sbwin_notifikasi_popup',
		iconCls: 'x-status-valid'
	})
});

var new_popup = win_notifikasi_popup;

<?php }else{ echo "var new_popup = 'GAGAL';"; } ?>