<html>
	<head>
		<style>
			body{
				padding:0px;
				margin:0px;
			}
		</style>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart.min.js"></script>
		<script>
			$(document).ready(function(){
				Chart.defaults.global = {
					// Boolean - Whether to animate the chart
					animation: true,

					// Number - Number of animation steps
					animationSteps: 60,

					// String - Animation easing effect
					// Possible effects are:
					// [easeInOutQuart, linear, easeOutBounce, easeInBack, easeInOutQuad,
					//  easeOutQuart, easeOutQuad, easeInOutBounce, easeOutSine, easeInOutCubic,
					//  easeInExpo, easeInOutBack, easeInCirc, easeInOutElastic, easeOutBack,
					//  easeInQuad, easeInOutExpo, easeInQuart, easeOutQuint, easeInOutCirc,
					//  easeInSine, easeOutExpo, easeOutCirc, easeOutCubic, easeInQuint,
					//  easeInElastic, easeInOutSine, easeInOutQuint, easeInBounce,
					//  easeOutElastic, easeInCubic]
					animationEasing: "easeOutQuart",

					// Boolean - If we should show the scale at all
					showScale: true,

					// Boolean - If we want to override with a hard coded scale
					scaleOverride: false,

					// ** Required if scaleOverride is true **
					// Number - The number of steps in a hard coded scale
					scaleSteps: null,
					// Number - The value jump in the hard coded scale
					scaleStepWidth: null,
					// Number - The scale starting value
					scaleStartValue: null,

					// String - Colour of the scale line
					scaleLineColor: "rgba(0,0,0,.1)",

					// Number - Pixel width of the scale line
					scaleLineWidth: 1,

					// Boolean - Whether to show labels on the scale
					scaleShowLabels: true,

					// Interpolated JS string - can access value
					scaleLabel: "<%=value%>",

					// Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
					scaleIntegersOnly: true,

					// Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
					scaleBeginAtZero: false,

					// String - Scale label font declaration for the scale label
					scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

					// Number - Scale label font size in pixels
					scaleFontSize: 12,

					// String - Scale label font weight style
					scaleFontStyle: "normal",

					// String - Scale label font colour
					scaleFontColor: "#666",

					// Boolean - whether or not the chart should be responsive and resize when the browser does.
					responsive: false,

					// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
					maintainAspectRatio: true,

					// Boolean - Determines whether to draw tooltips on the canvas or not
					showTooltips: true,

					// Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
					customTooltips: false,

					// Array - Array of string names to attach tooltip events
					tooltipEvents: ["mousemove", "touchstart", "touchmove"],

					// String - Tooltip background colour
					tooltipFillColor: "rgba(0,0,0,0.8)",

					// String - Tooltip label font declaration for the scale label
					tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

					// Number - Tooltip label font size in pixels
					tooltipFontSize: 14,

					// String - Tooltip font weight style
					tooltipFontStyle: "normal",

					// String - Tooltip label font colour
					tooltipFontColor: "#fff",

					// String - Tooltip title font declaration for the scale label
					tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

					// Number - Tooltip title font size in pixels
					tooltipTitleFontSize: 14,

					// String - Tooltip title font weight style
					tooltipTitleFontStyle: "bold",

					// String - Tooltip title font colour
					tooltipTitleFontColor: "#fff",

					// Number - pixel width of padding around tooltip text
					tooltipYPadding: 6,

					// Number - pixel width of padding around tooltip text
					tooltipXPadding: 6,

					// Number - Size of the caret on the tooltip
					tooltipCaretSize: 8,

					// Number - Pixel radius of the tooltip border
					tooltipCornerRadius: 6,

					// Number - Pixel offset from point x to tooltip edge
					tooltipXOffset: 10,

					// String - Template string for single tooltips
					tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

					// String - Template string for multiple tooltips
					multiTooltipTemplate: "<%= value %>",

					// Function - Will fire on animation progression.
					onAnimationProgress: function(){},

					// Function - Will fire on animation completion.
					onAnimationComplete: function(){}
				};
				
				<?php if( isset( $dataChart ) && isset( $typeChart ) ){ ?>
				
				var doRenderChart = function(){
					$('body').empty().html([
						'<canvas id="evochart" width="'+$(window).width()+'" height="'+$(window).height()+'"></canvas>'
					].join(''));
					var evochartData = <?php echo @json_encode( $dataChart ); ?>;
					var evochartCanvas = $("#evochart").get(0).getContext("2d");
					var evochart = new Chart( evochartCanvas );
					var tempEvoChart = evochart.<?php echo $typeChart; ?>( evochartData );
				};
				
				$(window).resize(function(){
					console.log('Window resize...');
					/*$('#evochart').width( $(window).width() );
					$('#evochart').height( $(window).height() );
					tempEvoChart.resize();*/
					doRenderChart();
				});
				doRenderChart();
				//Type 1 tersedia : bar, line, radar
				//Type 2 tersedia : pie, doughnut, polararea
				
				<?php }; ?>
			});
		</script>
	</head>
	<body>
		<div>
			<p style="top:45%;left:45%;display:block;position:absolute;font-size:14px;font-weight:bold;color:#333;">
				eXML Chart(s)
			</p>
			<img style="opacity:.30;width:100%;height:100%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAxEAAAGDCAYAAABUVp0HAAAABGdBTUEAANkDQtZPoQAAAAlwSFlzAAALEwAACxMBAJqcGAAAACR0RVh0U29mdHdhcmUAUXVpY2tUaW1lIDcuNi42IChNYWMgT1MgWCkAoItfqAAAAAd0SU1FB9sECBQIFToyrSwAACpLSURBVHhe7d0PmJV1nTf+DwUy2hAjCvEnmExFWbZydewp19C6oC5bNRtXd/NZx9bHh1XcR0srXTB+xhU+atmqz6bGxaOJrbW4iwiubMKug6WxOba4LYxiKn8UDDIGGGGUUX7n3PPlZ8po5+Cvbu4zr5fXZ875vu/xz7lmzsz99txfTr9dJQEAAFChd6RbAACAiigRAABAVZQIAACgKkoEAABQFSUCAACoihIBAABURYkAAACqokQAAABVUSIAAICqVFEiOuLWS2bE8s6eVdfT98X5LS3RUprm5kkxY/6qUrop7praHE1NTdFy7X2lv6Os0gwAACiCfrtK0v0317U8Wo4/L1bGH8Ts1jlxVH1E5/Jb4sTzNsadiy6IQd3d0X/Q0Oha8tfx2Tkfi9Z5n4z/23RSvHTzkvjcc5dXlH3l2Ib0LwMAAPZllb0SUXdk3Ljw9jj1wIjtKYoB5Q+D48ChQ2PEiBExtL5/vLBmY0w4b0LUx9CYOGVMLH50dcXZG1155ZV7zLRp0+KZZ55JnwEAAOShwsuZ6qJhRGMMG5SWmfLi3jhrUlM0NV8bq7rKWX00Du35pMajT4hBW3dWkb3e5MmT95hhw4bF4sWL02cAAAB5qGJPRHe67VE//r9HW9vi0kl9W1x31Ny4/M4V6UiPHVu2xLZ0f7dKs7KRI0fuMfvvv386CgAA5KWKEtE/+5hdxVTSuWFddGSvPnTF5vURRx46JAYM7Iw7Fq3Mjj92z4I4dOzwijMAAKAYKtxYvSKmnnJO3L+5vBgTl905Jz70yJfirL9tyw5H05RY+HfnxojuVTHjlLNiQfnzxpwdd8+9OEZXmvV0lF61tfX8e5YsWRJDhgzJLm0CAADyUVmJeDPdXdHZFVFfX5eCsu7o6OiK+ob69NpFWaVZ7+6///7sdtmyZTF8+HAlAgAAcvT2SsTv2axZs7JbJQIAAPJTxZ4IAAAAJQIAAKhSIUrEzJkzs2lvb08JAACQF3siAACAqricCQAAqIoSAQAAVKUQJWLu3LnZrFu3LiUAAEBeClEi/uiP/iibhoaGlAAAAHkpRIk4/PDDsxk0aFBKAACAvNgTAQAAVKUQJWL9+vXZ7NixIyUAAEBeClEifvjDH2bz/PPPpwQAAMiLN5sDAACqYk8EAABQFSUCAACoSiFKxDXXXJPN448/nhIAACAvhdgT8corr2S3s2fPjn79+tkTAQAAOSrEKxHvfOc7sykXCAAAIF/2RAAAAFUpRIl46KGHsvnVr36VEgAAIC+FKBE7d+7M5tVXX00JAACQl0KUiBNPPDGbYcOGpQQAAMiLPREAAEBVlAgAAKAqhSgRM2fOzKa9vT0lAABAXgrxZnO7zZo1K7v1ZnMAAJAflzMBAABVUSIAAICqFOJypu9973vZbXlPRGNjo8uZAApuzYbn43sL74tdpb9qQb/SXxOO+aP4WGkA+oJCvBIxYcKEbIYOHZoSAIqs/OahO7u7o7v7lZqY7LG88kp6dAC1rxAlYsyYMdkccMABKQEAAPJiTwQAAFCVQpSI1atXZ/Piiy+mBAAAyEshSsTDDz+czQsvvJASAAAgL4UoEWeddVY25X0RAABAvqooER1x6yUzYnlnWibd65bE+S3Xxqqu8mpT3DW1OZqamqLl2vtKf0c1GQAAUASVlYiu5dHSNDFuevAXKUi6VsVff/byaFu5LLZ3R6yb/9W45vHTo7VtURwzd3rMeqSj4gwAACiGykpE3ZFx48Lb49QDI7anqPxqwg2nnBXjZlwWf1Ba7SzNC2s2xoTzJkR9DI2JU8bE4kdXV5y90aWXXrrHPPbYY+koAACQlwovZ6qLhhGNMWxQWkZ3LJ1xUtx7+uy4+JOHR2dsi53d5bw+Gof2fFLj0SfEoK3lalFp9npf+9rX9pjx48enowAAQF6q2BORtYSkKzZ2jIlBD98SLWd+OdbG5rhoxvx4OR0t27FlS6lavF6lWVl9ff0e079//3QUAADISxUloucEfkD2sT7O+Na8mDfnlpgz9/oYU/pr9lWnxbsGdsYdi1Zmn/HYPQvi0LHDY0CFGQAAUAz9dpWk+2+ua0VMPeWcuH9zeTEmLrtzTpwxtj47lG26Pv5bcUnrnDiq/6qYccpZsaD8eWPOjrvnXhyjuyvM3uJFhgceeCC7feihh2LYsGExefLkbA1QC7a++GJse/G1HWe14OCGwTFwv/3Sak/PPLc+br/n3rSqDSc0HR0f/3BTWgHUtspKRFW6o6OjK+ob6tNrF2WVZr37yU9+kt22trbGQQcdpEQANeWBn7bF0rafpVVtOOczJ8cho0am1Z6UCIBiq+Jypkr1j4Y9ikGlWe8++tGPZlMuEAAAQL5+ByUCAACoZYUoETt37szm1VdfTQkAAJCXQpSI66+/Ppsnn3wyJQAAQF4KUSK+/OUvZ3PEEUekBAAAyIs9EQAAQFWUCAAAoCqFKBG33nprNs8880xKAACAvBSiRHz605/OZsSIESkBAADyUogSMXz48Gzq6upSAgAA5MWeCAAAoCqFKBGrVq3KZtu2bSkBAADyUogS8Z//+Z/ZbNmyJSUAAEBeClEi/vRP/zSb9773vSkBAADyYk8EAABQFSUCAACoSiFKxMyZM7Npb29PCQAAkJdClIhp06ZlM27cuJQAAAB5cTkTAABQFSUCAACoSiFKxA9/+MNsnn/++ZQAAAB5KUSJOPjgg7MZOHBgSgAAgLwUokQcc8wx2Rx44IEpAQAA8mJPBAAAUJVClIgXX3wxm+7u7pQAAAB5KUSJmD17djbPPPNMSgAAgLwUokRcfPHF2Rx++OEpAQAA8mJPBAAAUBUlAgAAqEohSsTNN9+czVNPPZUSAAAgL4UoEX/2Z3+WzejRo1MCAADkpRAlYsiQIdnst99+KQEAAPJiTwQAAFCVQpSIn//859ls2bIlJQAAQF4KUSLKbzJXnvK7VgMAAPkqRIk49dRTsxk5cmRKAACAvFRRIjri1ktmxPLOntWGh/8+WiZNiqampmi5dn5sytJNcdfU5pTdV/o7qskAAIAiqKxEdC2PlqaJcdODv0hBxLaNL8WfX39ntP14bhw29+tx5yMdsW7+V+Oax0+P1rZFcczc6TGrigwAACiGykpE3ZFx48Lb49QDI7anaOxp58anxw8tHRsSw0r5oMH944U1G2PCeROiPobGxCljYvGjqyvO3uiv/uqv9phHH300HQUAAPJS4eVMddEwojGGDUrLpGvVXdHUNDFmx3nRPLa+lNRH49CeT2o8+oQYtHVnFdnrfec739ljjjnmmHQUAADISxV7IrrT7Wvqxp4RbW2tccUHZseNS3t2Rey2Y8uW2Jbu71ZpBgAA7LuqKBH9s48Dso8RHevWRUfWK0r59oiuUskYMLAz7li0Mjv+2D0L4tCxwyvOAACAYqhwY/WKmDrpxJi9dmVccGJz3LWqMzY+ODMmfqQpmpqOj6/HlPhfJ4yI8Z+/Ok790UXZn7p0+eqzY9rJoyvO3sqCBQuyWb9+fUoAAIC89NtVku5Xr7srOrsi6uvrUlDWHR0dXVHfUJ9euyirNOtd+d2qy/7lX/4lBg8eHJMnT87WALXggZ+2xdK2n6VVbTjnMyfHIaPe/L19nnlufdx+z71pVRtOaDo6Pv7hprQCqG1VXM7Ui/51bygQZf2jYY9iUGnWuw984APZlAsEAACQr7dXIgAAgD6nECXi17/+dTYvv/xySgAAgLwUokT8wz/8Qzbr1q1LCQAAkJdClIgLLrggm0MPPTQlAABAXuyJAAAAqqJEAAAAVSlEibjhhhuyefLJJ1MCAADkpRAl4rzzzsvmkEMOSQkAAJCXQpSId73rXdn071/JW9MBAAC/S/ZEAAAAVSlEiXj00Uez2bx5c0oAAIC8FKJE/OpXv8rmpZdeSgkAAJCXQpSIT33qU9kMHz48JQAAQF7siQAAAKqiRAAAAFUpRImYOXNmNu3t7SkBAADyUogSMW3atGzGjRuXEgAAIC8uZwIAAKqiRAAAAFUpRIn4x3/8x2yeffbZlAAAAHkpRIn44Ac/mM3gwYNTAgAA5KUQJWLs2LHZDBo0KCUAAEBe7IkAAACqUogS8fzzz2fT1dWVEgAAIC+FKBH33XdfNhs2bEgJAACQl0KUiHPPPTebQw45JCUAAEBe7IkAgN+TF3fsqKnpeunl9MiAvqbfrpJ0f583a9as7Hby5MnZLUAteOCnbbG07WdpVRvO+czJcciokWm1p2eeWx+333NvWtWGE5qOjo9/uCmtenflTT2/x2pF48gR8ZennZJWQF9SiFcivvGNb2TzxBNPpAQAAMhLIUrEF77whWwOP/zwlAAAAHkpRIkYMGBANu94hy0cAACQN2flAABAVQqxsfonP/lJdtva2hoHHXSQjdVATbGxujbYWA19zw8W3Z/u1YZhQ4bEJ/7bW/8c260QJeKBBx7Ibh966KEYNmyYEgHUFCWiNigR0Pfsfk7369cvuy2yciWo5jldiMuZPv7xj2dTLhAAALAvKZ+AF32qVUWJ6IhbL5kRyzt7Vl3rlkRLU1M0leb8G5ZET7wp7pranGUt195X+juqyQAAgCKo7HKmruXRcvx5sTL+IGa3zomj6iM6Vy2JZTs/FMePei6+MPG8OOr21viTJ78Un53zsWid98n4v00nxUs3L4nPPXd5RdlXjm1I/7IenZ2prfyGOXPmRP/+/V3ORJ+x4hdPp3u14YD9697yEpe+yuVMtcHlTL0rf6237+hKq9ow/rD3p3v0dX35EsUK90R0RceGp+LGlmti4t1z4rhSiXhNd/z9pI/Ec1ctiU8+fG7MOfyG+NanR8eKW5vjCy9Pj2+8NKOibPH5R6V/Xo9LL7003XtNV1dXfOhDH1Ii6DNcP903KBG1QYno3W3zF8aa9RvSqjZcOcV5CD368u/pCi9nqouGEY0xbFBa/obup++Nv93cFH/ygfIrCfXROLTnkxqPPiEGbd1ZRfZ611133R5TLhAAAEC+qtgT0Z1uf8OmpXHmmV+Py+78ZoyvS1myY8uW2Jbu71ZpBgAA7LuqKBH9s48Dso/ljdX3xaSTLo3P3dkaZ4ztub5pwMDOuGPRyuz+Y/csiEPHDq84AwAAiqGyEtG1IqZOOjFmr10ZF5zYHHet6ozH//kHsbl0aNaFn+35U5ZuXR7jP391nPqji7L15avPjmknj644eyt33nlnNmvXrk0JAACQl8pKRN34uGpxW7S1lWde9srDUefPydaLFy/Obuece1Tp88bG9MXLYsmS1lg27+IYXX7xotLsLRx33HHZlN+tGgAAyFcVlzNVqn80NNSni592qzTr3fve975s3vWud6UEAADIy++gRMD//17dtSt2dnfX1Lzy6qvp0QEAFEuF7xORr917IebNmxcHHHCA94nog/yZ8rXB+0T0zvtE1AbP6d55nwhqmfeJ2Mc9+OCD2WzatCklAABAXgpRIv7iL/4im8bGxpQAAAB5sScCAACoihIBAABUpRAlYubMmdm0t7enBAAAyEshSsS0adOyGTduXEoAAIC8uJwJAACoihIBAABUpRAlorW1NZuNGzemBAAAyEshSsSAAQOyecc7vHACAAB5K8RZ+R//8R9nc/DBB6cEAADIi/+1DwAAVKUQJeKVV17JZteuXSkBAADyUogS8c1vfjObJ554IiUAAEBeClEiLrvssmyOPPLIlAAAAHmxJwIAAKiKEgEAAFSlECXitttuy+aZZ55JCQAAkJdClIhPfepT2QwfPjwlAABAXgpRIkaOHJnN/vvvnxIAACAv9kQAAABVKUSJePLJJ7PZtm1bSgAA2Fe88uqrseOll2pqdnZ3p0dHb/rtKsDbQM+dOze7/fnPfx6jR4+OyZMnZ2v6jmeeWx+333NvWtWGE5qOjo9/uCmtenflTbPSvdrQOHJE/OVpp6QVuz3w07ZY2vaztKoN53zm5Dhk1Mi02pPndG2o5Dl92/yFsWb9hrSqDVdOcR7yRp7TtaGa39OFeCXizDPPzKZcIAAAgHzZEwEAAFRFiQAAAKpSiBIxc+bMbNrb21MCAADkpRAlYtq0admMGzcuJQAAQF5czgQAAFRFiQAAAKpSiBJx//33Z/P888+nBAAAyEshSsSQIUOy2W+//VICAADkpRAloqmpKZtykQAAAPJVRYnoiFsvmRHLO9My88ZsU9w1tTk74W+59r7S0WoyAACgCCorEV3Lo6VpYtz04C9SUNJLtm7+V+Oax0+P1rZFcczc6THrkY6Kszdav379HrNjx450FAAAyEtlJaLuyLhx4e1x6oER21PUW/bCmo0x4bwJUR9DY+KUMbH40dUVZ280a9asPebpp59ORwEAgLxUeDlTXTSMaIxhg9Iy01tWH41De4LGo0+IQVt3VpG93pVXXrnHjB8/Ph0FAADyUsWeiO50+5t6y3rs2LIltqX7u1WaAQAA+64qSkT/7OOA7ONur88GDOyMOxatzO4/ds+COHTs8IozAACgGCrcWL0ipk46MWavXRkXnNgcd63q7DUb//mr49QfXZT9qUuXrz47pp08uuLsrdxyyy3ZPPXUUykBAADyUuHG6vFx1eK2aGsrz7w4Y2z9m2RjY/riZbFkSWssm3dxjC6/UFFp9hbOOOOMbN773vemBAAAyEsVlzNVqn80NNSnC512qzTr3UEHHZTNwIEDUwIAAOTld1AiAACAWlaIEvFf//Vf2WzZsiUlAABAXgpRIsobqsvT2dmZEgAAIC+FKBGf+cxnshk1alRKAACAvNgTAQAAVEWJAAAAqlKIEjFz5sxs2tvbUwIAAOSlECVi2rRp2YwbNy4lAABAXlzOBAAAVEWJAAAAqlKIErFw4cJs1q9fnxIAACAvhSgRjY2N2RxwwAEpAQAA8lKIEvHBD34wm4aGhpQAAAB5sScCAACoSiFKxObNm7N5+eWXUwIAAOSlECXi+9//fjZr165NCQAAkJdClIgpU6Zkc9hhh6UEAADIiz0RAABAVZQIAACgKoUoETfeeGM2Tz75ZEoAAIC8FKJEnHvuudkccsghKQEAAPJSiBJRX1+fTf/+/VMCAADkxZ4IAACgKoUoET/72c+yKb/hHAAAkK9ClIhf/vKX2XR1daUEAADISyFKxEknnZTNiBEjUgIAAOTFnggAAKAqSgQAAFCVQpSImTNnZtPe3p4SAAAgL4UoEdOmTctm3LhxKQEAAPLiciYAAKAqSgQAAFCVQpSIf/qnf8rm2WefTQkAAJCXQpSIP/zDP8xm8ODBKQEAAPJSRYnoiFsvmRHLO9MyNsVdU5ujqakpWq69r3T07WZv7ogjjshm0KBBKQEAAPJSWYnoWh4tTRPjpgd/kYKIdfO/Gtc8fnq0ti2KY+ZOj1mPdLytDAAAKIZ+u0rS/bfQFR0bnoobW66JiXfPiePqI5bf0BxzDr8hvvXp0bHi1ub4wsvT4xsvzdjrbPH5R6V/V49Vq1ale69ZuHBh9mrE5MmTUxLxH48/ER1bt6VVbfj4h5vSPXZ75rn1cfs996ZVbTih6ejf+rW+8qZZ6V5taBw5Iv7ytFPSit0e+GlbLG37WVrVhnM+c3IcMmpkWu3Jc7o2VPKcvm3+wlizfkNa1YYrp7x2HtKbh5f/Zzz0H4+V7lVwilUA/Up//dlJn4zRw9+Tkj15TteGan5PV3g5U100jGiMYa+7mqg+Gof2BI1HnxCDtu58m9nrlQvDG2f9+vXp6GuWP74q++VbSwMAFNdLL78cL+7YUZqumpjO0mPpfuWV9OigRxV7IrrT7Z52bNkSb3wt4O1kZZdeeukeU94XAQAA5KuKEtE/+zgg+1i6HdgZdyxamd1/7J4FcejY4W8rAwAAiqHCjdUrYuqkE2P22pVxwYnNcdeqzhj/+avj1B9dlP0JS5evPjumnTz6bWUAAEAxVFYi6sbHVYvboq2tPPPijLH1pWxsTF+8LJYsaY1l8y6O0eUXKt5O9ha++c1vZvPEE0+kBAAAyEsVlzP1pn80NNSnC512eztZ7y666KJsDj/88JQAAAB5eZsl4vdjv/32y+Yd7yjEfy4AANQ0Z+UAAEBVClEili1bls0LL7yQEgAAIC+FKBHbt2/P5hVvdAIAALkrRIn4xCc+kc2wYcNSAgAA5MWeCAAAoCpKBAAAUJVClIiZM2dm097enhIAACAvhSgR06ZNy2bcuHEpAQAA8uJyJgAAoCpKBAAAUJVClIjvf//72axduzYlAABAXgpRIj7ykY9kc9BBB6UEAADISyFKxCGHHJLNu971rpQAAAB5sScCAACoSiFKxLp167LZvn17SgAAgLwUokS0trZms2nTppQAAAB5KUSJOPvss7NpbGxMCQAAkBd7IgAAgKooEQAAQFUKUSJmzpyZTXt7e0oAAIC8FKJETJs2LZtx48alBAAAyIvLmQAAgKooEQAAQFUKUSKWLl2ajfeJAACA/BWiRLzzne/Mpl+/fikBAADyUogScfzxx2dz8MEHpwQAAMiLPREAAEBVClEiXn311Wx27dqVkr7t+V+9EI89saqmpnP79vToAADY1/UrnZjv82fmV199dXa7cePGOPLII2Py5MnZuuy2+QtjzfoNaVUbrpzy2uPrzQM/bYulbT9Lq9pwzmdOjkNGjUyrPT3z3Pq4/Z5706o2nNB0dHz8w01p1bsrb5qV7tWGxpEj4i9POyWt2M1zujZ4TvfO7+na4Dndu778e7oQr0Rcfvnl2ZQLBAAAkC97IgAAgKooEQAAQFUKUSK++93vZrN69eqUAAAAeSlEiZg0aVI273nPe1ICAADkZe9LRNe6uPWS5mhqaopLblkanVm4Ke6a2pO1XHtfdFSVvblRo0Zls//++6cEAADIy16XiOXfOTe+P/xLsWzZwhg++9K46ZGOWDf/q3HN46dHa9uiOGbu9JhVRQYAABTDXpaIzlh27+Y4/bSjo3//EfG5K5pi8QNPxgtrNsaE8yZEfQyNiVPGxOJHV1ecvdGjjz66x2zevDkdBQAA8rKXJaI+xh0bMXv+j2NTx6ZY9+uIQQN78sahg7LPaDz6hBi0dWcV2espEQAAsG/a68uZTph+d1wW/xb/+9qbY85NbVF/8OB0pMeOLVtiW7q/W6VZWfldqd8473//+9NRAAAgL3u/sbpudJzxlaviW1f9z3j/gQfGKRPeHwMGdsYdi1Zmhx+7Z0EcOnZ4xRkAAFAMe18iOpfH+U1N0dR0SnRd+O04Y3T/GP/5q+PUH12U/alLl68+O6adPLriDAAAKIa9LxH1R8X1ra3x42VtMf20sT1Z3diYvnhZLFnSGsvmXRylXlF59hauuuqqbNrb21MCAADkZe9LREldfX3U7VEA+kdDQ33p42+qNOvd1KlTsxk3blxKAACAvLytEgEAAPQ9SgQAAFCVQpSIxYsXZ/PLX/4yJQAAQF4KUSIGDx6czYABA1ICAADkpRAl4sMf/nA2Q4YMSQkAAJAXeyIAAICqFKJE7NixI5tXXnklJQAAQF4KUSJuvvnmbJ566qmUAAAAeSlEibjkkkuyGTs2vTM2AACQG3siAACAqigRAABAVQpRImbNmpXN008/nRIAACAvhSgRzc3N2YwaNSolAABAXgpRIg4++OBsBg4cmBIAACAv9kQAAABVKUSJWLlyZTZbt25NCQAAkJdClIhVq1Zls23btpQAAAB5KUSJOO2007KxsRoAAPJnTwQAAFAVJQIAAKhKIUrEzJkzs2lvb08JAACQl0KUiGnTpmUzbty4lAAAAHlxORMAAFAVJQIAAKhKIUrEP//zP2ezYcOGlAAAAHkpRIl473vfm83++++fEgAAIC+FKBEf+tCHsmloaEgJAACQF3siAACAqhSiRGzZsiWbnTt3pgQAAMhLIUrE9773vWzWrFmTEgAAIC+FKBEXXnhhNocddlhKAACAvNgTAQAAVEWJAAAAqlKIEvF3f/d32fziF79ICQAAkJdClIiWlpZsGhsbUwIAAORl70tE19Nxy/nN0dTUFM2X3Bobusvhprhrak/Wcu190ZF9YqXZm3v3u9+dzYABA1ICAADkZa9LxIrv/lXMHvmlaGtriyl1N8XX7n461s3/alzz+OnR2rYojpk7PWY90lFxBgAAFMNel4idMSiiq+fkf3BdxPbS7QtrNsaE8yZEfQyNiVPGxOJHV1ecvVFra+ses3HjxnQUAADIy16XiKP+/EsR90/PLkm6YMGEuOKz7y+l9dE4tFQuShqPPiEGbS2/w3Sl2ett2LBhj+nq6kpHAQCAvOx1iXjk9m/GhCvujGU/XhhfnPBgfH3OinSkx44tW2Jbur9bpVnZ5z73uT1mzJgx6SgAAJCXvS4Rm9asjboDD4z+dSPiExMnxIZfbY8BAzvjjkUrs+OP3bMgDh07vOIMAAAohr0uEZ/44tXx7KUnRXNLc5wyfXtccfYHYvznr45Tf3RRdonT5avPjmknj644AwAAimGvS0Td6Ikxp21ZzLlpTixruyVOGFFXCsfG9MXLYsmS1lg27+IY3b/8iRVmb2HmzJnZtLe3pwQAAMjLXpeIHv2jvr6+9PE39Y+Ghr3Nejdt2rRsxo0blxIAACAvb7NEAAAAfY0SAQAAVKUQJWLevHnZPPvssykBAADyUogSUd4LUZ53v/vdKQEAAPKiRAAAAFWxJwIAAKhKIUrEpk2bsnnppZdSAgAA5KUQJWL+/PnZPPfccykBAADy0m9XSbq/z5s1a1Z2O3ny5Oy27Lb5C2PN+g1pVRuunPLa4+vNAz9ti6VtP0ur2nDOZ06OQ0aNTKs9PfPc+rj9nnvTqjac0HR0fPzDTWnVuytv6vmerxWNI0fEX552Slr1bv6/tcazv9yYVsX3jn79Ysqfn5FWvfOcrg2e073ze7o2eE73ri8+p3ezJwLYp2zeui1+tbmjZmbjrzenRwYAtUOJAAAAqlKIEnHddddl88QTT6QEAADISyFKxIUXXpjNYYcdlhIAACAvhSgRdXV12bzzne9MCQAAkBd7IgAAgKoUokT8+7//ezYvvPBCSgAAgLwUokRs27Ytm+7u7pQAAAB5KUSJmDhxYjbvec97UgIAAOTFnggAAKAqSgQAAFCVQpSIq666Kpv29vaUAAAAeSlEiZg6dWo248aNSwkAAJAXlzMBAABVUSIAAICqFKJE/OAHP8hm7dq1KQEAAPJSiBJx7LHHZjNkyJCUAAAAeSlEiTj00EOzqa+vTwkAAJAXeyIAAICqFKJEPPvss9ls3749JQAAQF4KUSL+9V//NZuNGzemBAAAyEshSsQ555yTzfve976UAAAAebEnAgAAqIoSAQAAVKUQJeKqq67Kpr29PSUAAEBeClEi/uZv/iabI488MiUAAEBe9rpErLprakxqbo6WlpbSNMeM+atK6aa4a2pzNDU1Rcu190VH9pmVZm+uX79+/98AAAD52usSMWbSF2POt78d1914XbQctjYWrNke6+Z/Na55/PRobVsUx8ydHrMe6ag4AwAAiqHfrpJ0f+90r4qWj1wYLYsWx8F3Nsecw2+Ib316dKy4tTm+8PL0+MZLMyrKFp9/VPoH9li4cGG695pHH300NmzYEMccc0xKIv7j8Sdiy7bOtKoNJx772uPrzTPPrY816zekVW340BFj48B3D0qrPW3eui0ee6L8alftaBw5Ig4ZNTKtetf6yKPpXm0YPKg+/ujII9Kqd57TtcFzunee07XBc3pPntO1obfn9OTJk9O913vbJaJcAs559kvRNv24WH5DSyw97sa4+NiG6Fx+Q7Tcf1xMH/h/KsrmfeXY9E/s0VuJKCuXiDw8/vjjccQRR/SpS6qee+65GDRoULz73e9OSe3bunVrbNu2LUaNGpWS2lf+EfDEE0/0uT1HntN9g+d039EXn9OrV6/OntMHHXRQSmqf5/Tv35uViPJ/2N7b8R+7PnvMxF0Pbe5Z/sf1Z++6/qc9i42tX9s18ZqfVpzt666++upd3d3dadU33H333btWrFiRVn1D+fGWH3dfUv6+Ln9/9zWe032D53Tf0Ref07fddtuuhx9+OK36Bs/pfcfb+tOZln/ny7H27KviuIae9YCBnXHHopXZ/cfuWRCHjh1ecQYAABTD3l/O1PlINJ/4f+JvWufEsfUp61oVM045KxZsLt0fc3bcPffiGN1dYda//A8AAKAS3/3ud7NLuD760Y+mBH5/3v7G6j10R0dHV9Q31MdrvaDSDACASigR5Ol38GZz/aNhj2JQaQYAAOzrfgevRAAAALXsd/BKBAAAUMuUCAAAoCpKBAAAUBUloiIdceslM2J5bb1r/5vqWrckWpqaoqk059+wJPrCw97w8N9Hy6RJ2WNuuXZ+bEp5X9Bd+nqf33JtrOpKQY3revq+0uNtiZbSNDdPihnzV6UjtW3T8vlx/qTy8/r8PvGzbNVdU2NSc3P2dW5pae4bX+eup+OW85uzn2PNl9waG7pTXuu61pV+R/c87ktuWdonfmf1Pb2dh9X6udnrH9++eG6mRPw2XctLX7SJcdODv0hB7eveEdFy+6L48ZLZEXdcHt9bUfs/krdtfCn+/Po7o+3Hc+OwuV+POx/pSEdqXNeq+OvPXh5tK5fF9j5ywtG9dW3p8R4WV1x3XXz723PigonvT0dqWMfSOOm8r8fHri89r1uvjiPrUl7Dxkz6Ysz59rfjuhuvi5bD1saCNdvTkdq14rt/FbNHfina2tpiSt1N8bW7n05Hatvy75wb3x/+pVi2bGEMn31p3NRXfn73Fb2dh9X6uVkvj29fPDdTIn6buiPjxoW3x6kHRtT+r6Ae9WMnxsTxQ6Ou4Q/jY6XHvbUPnF2OPe3c+HTpMUfdkBhWesyDBveFP3x4U9xwylkxbsZl8Qel1c6esPYNKH8YHAcOHRojRoyIofW1/7V+evH348Azb4zTj9i/9D3eEHV94Nu7rmH313dzzFlwYFx91lHpSO3aGYNKJx89J9CDS0Wxb/zO6oxl926O0087Ovr3HxGfu6IpFj/wZDpGTejtPKzWz816eXz74rmZEvFb1UXDiMYYVvrZ3Nd0P31v/O3mpviTDzSkpLZ1rbormkrNf3acF81jd78Ne63qjqUzTop7T58dF3/y8NKv4W2xs69c+lA+0Yp746zypT3NfeMyrh0vbI/Nc/+f+MJft8TxH5kUd63oO/+ndsWcy2PlqV+LiUNTUMOO+vMvRdw/Pbvc4YIFE+KKz/aBV9miPsYdGzF7/o9jU8emWPfr0jN8YDpUk954Cc+muGtqz6VcLdfeVzpai3o7D6v1c7M3f3z70rmZElGRPnN29ZpNS+PMM78el935zRjfBy59KKsbe0a0tbXGFR+YHTcurfVdEV2xsWNMDHr4lmg588uxNjbHRTPml9LaVz/+v5e+zotj8eK2uO6ouXH5nSvSkdq186WIs2ffHbfcMi8WXfexmPVPK9ORGte1PL5607a48aLjUlDbHrn9mzHhijtj2Y8XxhcnPBhfn1P739tlJ0y/Oy6Lf4v/fe3NMeemtqg/eHA6UmN6ucRl3fyvxjWPnx6tbYvimLnTY1bNXsrV23lYrZ+b9fL49rFzMyWiIj2v/WdXQfQBXevui0knXRqfu7M1zqj5/yPfo2PduujInq+lr/X28il2rf9wqo8zvjUv5s25JebMvT7GlP6afdVp0Rf6YueG0tc6a0tdsXl9xJGHDsnyWnbAoIg77v/P7P62jaUHPbhv/DRb/p1SQT77qjiub7yYGpvWrI26Aw+M/nUj4hMTJ8SGX/WNC5qibnSc8ZWr4ltX/c94f+nxnzKhRl+B6eUSlxfWbIwJ500o/UQfGhOnjInFj65OR2pNb+dhtX5u9vrHty+emykRv03Xipg66cSYvXZlXHBic9y1qvY3GT/+zz+IzaXbWRd+tucl0luX9xyoYRsfnBkTP1L+Uw+Oj6/HlPhfJ4xIR/qA7p2lX0ClH0i13puS9f9W+lof/xtf6z+u/a/12LOujDOXXZQ9n8+cNSSu/x/HpiM1rPORmHHHiLi5LzzW5BNfvDqevfSkaG5pjlOmb48rzv5AOlLjOpfH+aXv7aamU6Lrwm/HGaN7Tr5qT2+XuNRH49CeoPHoE2LQ1hrc3dbbeVitn5v18vj2xXOzfrtK0n3o27q7orOr9CO5vo9cv9WX9dGvdVdnZ9TV941XF/uu7ugsfXOXv861eirdm/L3dulB94E/NKAjbmm+KD44Z04cV3oqL7+hJZYed2NcfGxDbFo6I87695Ni8Vf6TnEmX16JgN361ykQfUUf/VorEH1B/9L3dt8qEGXl7+2+8KeOvfESlwEDO+OORT17nB67Z0EcOnZ4dh9+H7wSAQCwrytf4nLKOXF/+ZqWGBOX3TknzhizPmacclYsKGdjzo67514cNXs1F/scJQIAoLC6o6OjK+ob+t4rUORLiQAAAKpiTwQAAFCFiP8XsZOmrv5t2DcAAAAASUVORK5CYII=" />
		</div>
	</body>
</html>