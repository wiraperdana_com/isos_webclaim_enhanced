<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

var PengaturanModul_Daftar_Proto = function(d){
	return modelFactoryDetailWithForm({
		name:'PengaturanModul_Daftar_Proto',
		title:'Daftar Modul Tersedia!',
		data: d,
		button:false,
		dsgridls:true,
		config:{
			url	: BASE_URL + 'pengaturanmodul/ext_checking',
			params:d,
			root:'data'
		},
		initgrid : {
			viewConfig: {
				stripeRows: false,
				getRowClass: function (record) {
					return record.get('status').toLowerCase() != 'dipasang' ? 'child-row' : '';
				},
				forceFit: true
			}
		},
		field:[
			{
				name:'idmenu', text:'ID Module', default:0, editor:{
					xtype:'hiddenfield'
				}
			},{
				name:'name', text:'Name', default:'-', editor:{
					xtype:'textfield'
				}
			},{
				name:'general_text', text:'Text', default:'-', editor:{
					xtype:'textfield'
				}
			},{
				name:'description', text:'Description', default:'-', editor:{
					xtype:'textfield'
				}
			},{
				name:'status', text:'Status', default:'-', editor:{
					xtype:'textfield'
				}
			}
		]
	})
};

var PengaturanModul_Daftar = function( d ){
	var d = ( d || [] );
	var tempGrid = PengaturanModul_Daftar_Proto(d).getGrid();
	tempGrid.removeListener('selectionchange', function(){});
	tempGrid.removeListener('itemdblclick', function(){});
	tempGrid.on('selectionchange', function(model, records) {
		if (records[0]) {
		}
	});
	tempGrid.on('itemdblclick', function(a,b,c,d,e){
		console.log('222')
	});
	return tempGrid;
};

var Grid_PengaturanModul_Daftar = PengaturanModul_Daftar();

var new_tabpanel = {
    id: 'newtab_pengaturanmodul', 
    title: 'Pengaturan Modul', 
    iconCls: 'icon-gears',
    border: false, 
    closable: true,  
    layout: 'border', 
    items: [
		{
			region:'north',
			split:true,
			html:[
				'<div style="padding:10px;background:#00698C;color:#fafafa">',
					'<b>PENGATURAN MODUL</b>',
				'</div>'
			].join(''),
			bbar:[
				{
					text:'Upload Modul',
					disabled:true,
					iconCls: 'icon-gears',
					handler:function(){
					}
				},
				{
					text:'Pasang / Update',
					iconCls: 'icon-gears',
					handler:function(){
						var obj = Grid_PengaturanModul_Daftar.getSelectionModel().getSelection();
						if( obj.length > 0 ) {
							var data = obj[0].get('name');
							Ext.Msg.show({
								title: 'Konfirmasi', 
								msg: 'Apakah Anda yakin untuk memasang modul ini ?',
								buttons: Ext.Msg.YESNO, 
								icon: Ext.Msg.QUESTION,
								fn: function(btn) {
									if (btn == 'yes') {
										Ext.Ajax.request({
											url: BASE_URL + 'pengaturanmodul/ext_install', 
											method: 'POST',
											params: { postdata : data },
											success: function(response){
												Grid_PengaturanModul_Daftar.getStore().load()
											},
											failure: function(response){ 
												Ext.MessageBox.show({title:'Peringatan !', msg: obj.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
											}
										});
									}
								}
							});
						}
					}
				},'->',{
					text:'Copot',
					iconCls: 'icon-delete',
					handler:function(){
						var obj = Grid_PengaturanModul_Daftar.getSelectionModel().getSelection();
						if( obj.length > 0 ) {
							var data = obj[0].get('name');
							Ext.Msg.show({
								title: 'Konfirmasi', 
								msg: 'Apakah Anda yakin untuk mencopot modul ini ?',
								buttons: Ext.Msg.YESNO, 
								icon: Ext.Msg.QUESTION,
								fn: function(btn) {
									if (btn == 'yes') {
										Ext.Ajax.request({
											url: BASE_URL + 'pengaturanmodul/ext_uninstall', 
											method: 'POST',
											params: { postdata : data },
											success: function(response){
												Grid_PengaturanModul_Daftar.getStore().load()
											},
											failure: function(response){ 
												Ext.MessageBox.show({title:'Peringatan !', msg: obj.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
											}
										});
									}
								}
							});
						}
					}
				}
			]
		},
		Grid_PengaturanModul_Daftar
    ]
}
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>
