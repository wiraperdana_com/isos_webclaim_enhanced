<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $this->my_uconfig->get('name'); ?> - <?php echo $this->my_uconfig->get('title'); ?></title>
		<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/exmldashboard/icon.ico" type="image/x-icon" /> 
		<meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
		<meta name="Author" content="Semar Ketir" />
		<meta name="Keywords" content="<?php echo $this->my_uconfig->get('appname'); ?>" />
		<meta name="Description" content="<?php echo $this->my_uconfig->get('appname'); ?>" />

		<!--[if lt IE 9]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<!--[if lt IE 7]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
		<![endif]-->

		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/resources/css/<?php echo $this->my_uconfig->get('theme'); ?>.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/statusbar/css/statusbar.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/grid/css/GridFilters.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/grid/css/RangeMenu.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/css/CheckHeader.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/icon_css.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/allow_select_grid.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>kontrol_menu/css"/>

		<style>
			body {
				background:#fafafa;
			}
			.child-row .x-grid-cell {
				background-color: yellow !important;
			}
			.child-bold-row .x-grid-cell {
				font-weight: bold !important;
			}
		</style>

		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ext-all.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/CheckColumn.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/statusbar/StatusBar.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/form/SearchField.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/grid/FiltersFeature.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/miframe/ManagedIframe.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/class/allow_select_grid.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/functions/function_override.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/functions/jsDate1.js"></script>

		<script type="text/javascript">
			var bulanx = ['April', 'April', 'April', 'April', 'Oktober', 'Oktober', 'Oktober', 'Oktober', 'Oktober', 'Oktober', 'April', 'April'];
			var datex_bulan = bulanx[ <?php echo(date('n') - 1); ?> ].toUpperCase();
			var datex_tahun = <?php echo date('Y'); ?> ;
			if ( <?php echo(date('n') - 1); ?> >= 10) {
				datex_tahun += 1;
			}

			var sekarangx = {
				bulan: datex_bulan,
				tahun: datex_tahun
			}

			var BASE_URL = '<?php echo base_url(); ?>';
			var BASE_ICONS = BASE_URL + 'assets/images/icons/';
			var Time_Out = 2400000;
			var Tgl_Skrg = "<?php echo date('d/m/Y'); ?>";
			var Scr_Width = screen.availWidth;
			var Scr_Height = screen.availHeight;
			var type_user = '<?php echo $this->session->userdata("type_zs_exmldashboard"); ?>';
			var sesi_kode_unker = '<?php echo $this->session->userdata("kode_unker_zs_exmldashboard"); ?>';
			var Tab_Active = "";
			Ext.BLANK_IMAGE_URL = BASE_URL + 'assets/js/ext/resources/themes/images/access/tree/s.gif';
			Ext.Loader.setConfig({
				enabled: true
			});
			Ext.Loader.setPath('Ext.ux', BASE_URL + 'assets/js/ext/ux');
			Ext.require(['*']);

			Ext.form.Field.prototype.msgTarget = 'side';
			Ext.QuickTips.init();

			Ext.require(['Ext.data.*', 'Ext.grid.*']);

			Ext.namespace('SERLOKSIDASHBOARD');
			Ext.onReady(function () {
				var clock = Ext.create('Ext.toolbar.TextItem', {
					text: Ext.Date.format(new Date(), 'd M Y, g:i:s A')
				});

				var Content_Body_Tabs = Ext.createWidget('tabpanel', {
					id: 'Content_Body_Tabs',
					layout: 'fit',
					region:'center',
					resizeTabs: true,
					enableTabScroll: false,
					deferredRender: true,
					border: false,
					defaults: {
						border:false,
						closable: true,
						autoScroll: true
					},
					items: [
						{
							id: 'dashboard',
							title: 'Dashboard',
							bodyPadding: 0,
							iconCls: 'icon-gnome',
							closable: false,
							border:false,
							layout: 'fit',
							items: []
						}
					],
					listeners: {
						'tabchange': function (tabPanel, tab) {}
					}
				});

				var Layout_Body = Ext.create('Ext.panel.Panel', {
					id: 'layout-body',
					region: 'center',
					layout: 'fit',
					border: false,
					deferredRender: true,
					layout:'border',
					items: [
						{
							title:'<?php echo $this->my_uconfig->get('title'); ?>',
							collapsible:true,
							collapsed:true,
							region: 'west',
							flex:1,
							border:true,
							iconCls:'icon-information',
							layout:'border',
							split:true,
							items:[
								{
									border:false,
									region: 'center',
									flex:2.2,
									split:true,
									layout: {
										type : 'table',
										columns : 1, 
										tableAttrs : {
											style : {
												width : '100%'                              
											}
										},
									},
									autoScroll:true,
									defaults:{
										border:false,
									},
									bbar:[
										{
											text: 'Ubah Kata Sandi',
											iconCls: 'icon-key', 
											handler: function(){
												Load_Popup('winchangepass', BASE_URL + 'pengguna_login/ubahsandi');
											}
										},'->',
										{
											text: 'Logout',
											iconCls: 'icon-minus-circle',
											handler: function () {
												do_logout();
											}
										}
									],
									items:[
										{
											xtype : 'panel',
											layout : 'fit',
											bodyStyle:'padding:10px',
											html:[
												'<h1><center><br/><?php echo $this->my_uconfig->get('name'); ?><br/><br/></center></h1>'
											].join('')
										},{
											flex:1,
											region:'north',
											layout:'hbox',
											border:false,
											items:[
												{ flex:1, border:false, },
												Ext.create('Ext.Img', {
													src: BASE_URL+ "assets/photo/male.png",
													align : 'center',
													flex:2,
													height:90,
													width:90,
													margins:10,
													maxHeight:90,
													maxWidth:90,
													margins:10,
													border:false,
												}),
												{ flex:1, border:false, },
											]
										},{
											xtype : 'panel',
											layout : 'fit',
											bodyStyle:'padding:10px',
											html:[
												'<b>Nama</b>',
												'<p><?php echo $this->my_usession->userdata("fullname_zs_exmldashboard") ?></p>'
											].join('')
										},{
											xtype : 'panel',
											layout : 'fit',
											bodyStyle:'padding:10px',
											html:[
												'<b>NIP</b>',
												'<p><?php echo $this->my_usession->userdata("nip_zs_exmldashboard") ?></p>'
											].join('')
										},{
											xtype : 'panel',
											layout : 'fit',
											bodyStyle:'padding:10px',
											html:[
												'<b>Role</b>',
												'<p><?php echo $this->my_usession->userdata("type_zs_exmldashboard") ?></p>'
											].join('')
										}
									]
								},{
									border:false,
									region: 'south',
									flex:1,
									split:true,
									layout: {
										type : 'table',
										columns : 1, 
										tableAttrs : {
											style : {
												width : '100%'                              
											}
										},
									},
									autoScroll:true,
									defaults:{
										border:false,
									},
									items:[
										{
											flex:1,
											split:true,
											region:'south',
											layout:'hbox',
											border:false,
											items:[
												{ flex:1, border:false, },
												{
													flex:2,
													height:<?php echo $this->my_uconfig->get('logo')['height']; ?>,
													width:<?php echo $this->my_uconfig->get('logo')['width']; ?>,
													maxHeight:<?php echo $this->my_uconfig->get('logo')['height']; ?>,
													maxWidth:<?php echo $this->my_uconfig->get('logo')['width']; ?>,
													src: BASE_URL + '<?php echo $this->my_uconfig->get('logo')['src']; ?>',
													xtype:'image',
													margins:10,
													border:false
												},
												{ flex:1, border:false, }
											]
										},{
											html:'<div style="padding:10px;"><center><small><?php echo $this->my_uconfig->get('appname'); ?> Version <?php echo $this->my_uconfig->get('version'); ?><br/><?php echo $this->my_uconfig->get('apppower'); ?><br/>&copy; 2014-<?php echo date('Y'); ?></small></center></div>',
											border:false
										}
									]
								}
							]
						},{
							iconCls:'icon-menu_utama',
							title:'<?php echo strtoupper( $this->my_uconfig->get('name') . ' - ' . $this->my_uconfig->get('title') ); ?>',
							flex:4,
							tbar:[ <?php echo $MenuDashboard; ?>],
							region: 'center',
							id: 'items-body',
							border: true,
							layout: 'card',
							autoRender: true,
							closable: false,
							activeItem: 0,
							cls: 'big-exml-panel',
							items: [ Content_Body_Tabs ]
						}
					]
				});

				var Layout_Footer = {
					id: 'layout-footer',
					region: 'south',
					layout: 'fit',
					split: false,
					border: false,
					margins: '5 0 0 0',
					items: [{
						xtype: 'toolbar',
						dock: 'bottom',
						padding: 5,
						items: ['Pengguna : ' + '<?php echo $this->my_usession->userdata("fullname_zs_exmldashboard")." | ".$this->my_usession->userdata("type_zs_exmldashboard"); ?>', '->', clock, '-',
						{
							id: 'ChatBtn',
							hidden: true,
							iconCls: 'icon-smiley',
							handler: function () {
							Show_Chat();
							}
						}],
						listeners: {
							render: {
								fn: function () {
									Ext.fly(clock.getEl().parent()).addCls('x-status-text-panel').createChild({
										cls: 'spacer'
									});

									// Kick off the clock timer that updates the clock el every second:
									Ext.TaskManager.start({
										run: function () {
											Ext.fly(clock.getEl()).update(Ext.Date.format(new Date(), 'd M Y, g:i:s A'));
										},
										interval: 1000
									});
									<?php
										if( $WidgetDashboard != null ){
											echo $WidgetDashboard;
										};
									?>
								},
								delay: 100
							}
						}
					}]
				};

				//Print All Component     
				Ext.create('Ext.Viewport', {
					id: 'main_viewport',
					layout: {
						type: 'border',
						padding: 5,
						border:false
					},
					defaults: {
						split: true
					},
					items: [Layout_Body, Layout_Footer],
					renderTo: Ext.getBody()
				});
			});

			function Show_Chat() {
				Ext.MessageBox.show({
					title: 'Chat !',
					msg: 'Chatting !',
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
			}

			function do_logout() {
				Ext.getCmp('layout-body').body.mask("Logout ...", "x-mask-loading");
				Ext.Ajax.request({
					url: BASE_URL + 'user/ext_logout',
					method: 'POST',
					success: function (xhr) {
						window.location = BASE_URL + 'user';
					}
				});
				return false;
			}

			function Load_Page(page_id, page_url) {
				Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
				var new_page_id = Ext.getCmp(page_id);
				if (new_page_id) {
					Ext.getCmp('items-body').getLayout().setActiveItem(new_page_id);
					Ext.getCmp('items-body').doLayout();
					Ext.getCmp('layout-body').body.unmask();
				} else {
					Ext.Ajax.timeout = Time_Out;
					Ext.Ajax.request({
						url: page_url,
						method: 'POST',
						params: {
							id_open: 1
						},
						scripts: true,
						renderer: 'data',
						success: function(response) {
							// Start Register Javacript On Fly
							var jsonData = response.responseText;
							var aHeadNode = document.getElementsByTagName(
								'head')[0];
							var aScript = document.createElement('script');
							aScript.text = jsonData;
							aHeadNode.appendChild(aScript);
							// End Register Javacript On Fly
							if (newpanel != "GAGAL") {
								Ext.getCmp('items-body').add(newpanel);
								Ext.getCmp('items-body').getLayout().setActiveItem( newpanel);
								Ext.getCmp('items-body').doLayout();
							} else {
								Ext.MessageBox.show({
									title: 'Peringatan !',
									msg: 'Anda tidak dapat mengakses ke halaman ini !',
									buttons: Ext.MessageBox.OK,
									icon: Ext.MessageBox.ERROR
								});
							}
						},
						failure: function(response) {
							Ext.MessageBox.show({
								title: 'Peringatan !',
								msg: 'Gagal memuat dokumen !',
								buttons: Ext.MessageBox.OK,
								icon: Ext.MessageBox.ERROR
							});
						},
						callback: function(response) {
							Ext.getCmp('layout-body').body.unmask();
						},
						scope: this
					});
				}
			}

			function Load_TabPage(tab_id, tab_url) {
				Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
				var new_tab_id = Ext.getCmp(tab_id);
				var one_tab_items = Ext.getCmp('Content_Body_Tabs').getComponent(1);
				if (new_tab_id) {
					//add disini x
					if (one_tab_items) {
						//one_tab_items.destroy();
					}
					Ext.getCmp('items-body').getLayout().setActiveItem(0);
					Ext.getCmp('items-body').doLayout();
					Ext.getCmp('Content_Body_Tabs').setActiveTab(tab_id);
					Ext.getCmp('layout-body').body.unmask();
				} else {
					Ext.Ajax.timeout = Time_Out;
					Ext.Ajax.request({
						url: tab_url,
						method: 'POST',
						params: {
							id_open: 1
						},
						scripts: true,
						renderer: 'data',
						success: function(response) {
							var jsonData = response.responseText;
							var aHeadNode = document.getElementsByTagName(
								'head')[0];
							var aScript = document.createElement('script');
							aScript.text = jsonData;
							aHeadNode.appendChild(aScript);
							var new_tab = Ext.getCmp('Content_Body_Tabs');
							if (new_tabpanel != "GAGAL") {
								if (one_tab_items) {
									//one_tab_items.destroy();
								}
								Ext.getCmp('items-body').getLayout().setActiveItem( 0);
								Ext.getCmp('items-body').doLayout();
								new_tab.add(new_tabpanel).show();
							} else {
								Ext.MessageBox.show({
									title: 'Peringatan !',
									msg: 'Anda tidak dapat mengakses ke halaman ini !',
									buttons: Ext.MessageBox.OK,
									icon: Ext.MessageBox.ERROR
								});
							}
						},
						failure: function(response) {
							Ext.MessageBox.show({
								title: 'Peringatan !',
								msg: 'Gagal memuat dokumen !',
								buttons: Ext.MessageBox.OK,
								icon: Ext.MessageBox.ERROR
							});
						},
						callback: function(response) {
							Ext.getCmp('layout-body').body.unmask();
						},
						scope: this
					});
				}
			}

			function Load_Dashboard(tab_id, tab_url) {
				Ext.getCmp('dashboard').removeAll();
				Ext.getCmp('dashboard').body.mask("Loading...", "x-mask-loading");
				
				Ext.Ajax.timeout = Time_Out;
				Ext.Ajax.request({
					url: tab_url,
					method: 'POST',
					params: {
						id_open: 1
					},
					scripts: true,
					renderer: 'data',
					success: function(response) {
						var jsonData = response.responseText;
						var aHeadNode = document.getElementsByTagName(
							'head')[0];
						var aScript = document.createElement('script');
						aScript.text = jsonData;
						aHeadNode.appendChild(aScript);
						var new_tab = Ext.getCmp('dashboard');
						if ( new_tabpanel_dashboard != "GAGAL") {
							new_tab.add( new_tabpanel_dashboard ).show();
							Ext.getCmp('dashboard').doLayout();
						} else {
							Ext.MessageBox.show({
								title: 'Peringatan !',
								msg: 'Terjadi Kesalaha Di Dashboard!',
								buttons: Ext.MessageBox.OK,
								icon: Ext.MessageBox.ERROR
							});
						}
					},
					failure: function(response) {
						Ext.MessageBox.show({
							title: 'Peringatan !',
							msg: 'Gagal memuat dokumen !',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					},
					callback: function(response) {
						Ext.getCmp('dashboard').body.unmask();
					},
					scope: this
				});
			}

			function Load_Popup(popup_id, popup_url, popup_title, url_form, idx) {
				if (typeof idx == 'undefined') idx = null;
				console.log(idx)
				Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
				var new_popup_id = Ext.getCmp(popup_id);
				if (new_popup_id) {
					Ext.getCmp('layout-body').body.unmask();
					new_popup.show();
				} else {
					Ext.Ajax.timeout = Time_Out;
					Ext.Ajax.request({
						url: popup_url,
						method: 'POST',
						params: {
							id_open: 1,
							popup_title: popup_title,
							idx: idx
						},
						scripts: true,
						renderer: 'data',
						success: function(response) {
							var jsonData = response.responseText;
							var aHeadNode = document.getElementsByTagName(
								'head')[0];
							var aScript = document.createElement('script');
							aScript.text = jsonData;
							aHeadNode.appendChild(aScript);
							var new_popup = Ext.getCmp(popup_id);
							if (new_popup != "GAGAL" && new_popup) {
								new_popup.show();
							} else {
								Ext.MessageBox.show({
									title: 'Peringatan !',
									msg: 'Anda tidak dapat mengakses ke halaman ini !',
									buttons: Ext.MessageBox.OK,
									icon: Ext.MessageBox.ERROR
								});
							}
						},
						failure: function(response) {
							Ext.MessageBox.show({
								title: 'Peringatan !',
								msg: 'Gagal memuat dokumen !',
								buttons: Ext.MessageBox.OK,
								icon: Ext.MessageBox.ERROR
							});
						},
						callback: function(response) {
							Ext.getCmp('layout-body').body.unmask();
						},
						scope: this
					});
				}
			}

			function Load_Panel_Ref(popup_id, popup_uri, form_name, vfmode, p_key, p_key1,
				p_key2, p_key3) {
				Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
				var new_popup_id = Ext.getCmp(popup_id);
				if (new_popup_id) {
					Ext.getCmp('layout-body').body.unmask();
					var strFunc = "Funct_" + popup_id;
					var fn = window[strFunc];
					fn(form_name, vfmode);
				} else {
					Ext.Ajax.timeout = Time_Out;
					Ext.Ajax.request({
						url: BASE_URL + 'panel_ref/' + popup_uri,
						method: 'POST',
						params: {
							id_open: 1,
							VKEY: p_key,
							VKEY1: p_key1,
							VKEY2: p_key2,
							VKEY3: p_key3
						},
						scripts: true,
						renderer: 'data',
						success: function(response) {
							var jsonData = response.responseText;
							var aHeadNode = document.getElementsByTagName(
								'head')[0];
							var aScript = document.createElement('script');
							aScript.text = jsonData;
							aHeadNode.appendChild(aScript);
							var new_popup = Ext.getCmp(popup_id);
							if (new_popup != "GAGAL") {
								var strFunc = "Funct_" + popup_id;
								var fn = window[strFunc];
								fn(form_name, vfmode);
							} else {
								Ext.MessageBox.show({
									title: 'Peringatan !',
									msg: 'Anda tidak dapat mengakses ke halaman ini !',
									buttons: Ext.MessageBox.OK,
									icon: Ext.MessageBox.ERROR
								});
							}
						},
						failure: function(response) {
							Ext.MessageBox.show({
								title: 'Peringatan !',
								msg: 'Gagal memuat dokumen !',
								buttons: Ext.MessageBox.OK,
								icon: Ext.MessageBox.ERROR
							});
						},
						callback: function(response) {
							Ext.getCmp('layout-body').body.unmask();
						},
						scope: this
					});
				}
			}

			function Load_Variabel(page_url) {
				Ext.Ajax.request({
					url: page_url,
					method: 'POST',
					params: {
						id_open: 1
					},
					scripts: true,
					renderer: 'data',
					success: function(response) {
						var jsonData = response.responseText;
						var aHeadNode = document.getElementsByTagName(
							'head')[0];
						var aScript = document.createElement('script');
						aScript.text = jsonData;
						aHeadNode.appendChild(aScript);
					},
					failure: function(response) {
						Ext.MessageBox.show({
							title: 'Peringatan !',
							msg: 'Gagal memuat dokumen !',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					},
					scope: this
				});
			}

			Load_Variabel(BASE_URL + 'dashboard/fn');
			Load_Variabel(BASE_URL + 'user/set_var_access');
			Load_Variabel(BASE_URL + 'fm/widget');
		</script>
	</head>
	<body>
		<div id="script_area"></div>
		<div id="body_div"></div>
	</body>
</html>