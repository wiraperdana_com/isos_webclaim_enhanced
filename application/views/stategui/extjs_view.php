<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

var properties_stategui = Ext.create('Ext.grid.property.Grid', {
	flex:1,
	border:false,
	source: {
		"Status Title": "Nama State",
		"Status Data": 0,
		"Status Approval": 0,
		"Status Rejected": 0,
		"Status Next": 0,
		"Process": 'Yes',
		"Active": 'Yes'
	}
});

var new_tabpanel = {
    id: 'newtab_stategui', 
    title: 'State GUI', 
    iconCls: 'icon-gears',
    border: false, 
    closable: true,  
    layout: 'border',
    items: [
		{
			split:true,
			region:'north',
			border:false,
			html:''
		},
		{
			split:true,
			border: true, 
			flex:4,
			region:'center',
			layout : 'fit',
			tbar:[
				{
					iconCls:'icon-check',
					text:'Run - Test'
				},'->',{
					iconCls:'icon-save',
					text:'Save as'
				}
			], 
			items : [{
				xtype : "component",
				bodyStyle:'border:0px;',
				frameBorder:0,
				border:false,
				autoEl : {
					tag : "iframe",
					frameBorder:0,
					bodyStyle:'border:0px;',
					border:false,
					src : BASE_URL + "stategui/gui"
				}
			}]
		},{
			border:true,
			split:true,
			flex:1,
			region:'east',
			items:[
				{
					split:true,
					region:'north',
					border:false,
					html:'<div style="background:#59a7d2;color:white;padding:8px;"><b>Properties</b></div>'
				},
				properties_stategui
			]
		}
    ]
}
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>
