<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php if(isset($title)){echo $title;}else{echo "HRIS PT PLN BATAM - UBE";} ?></title>
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon" /> 
	<meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
	<meta name="Author" content="Semar Ketir" />
	<meta name="Keywords" content="" />
	<meta name="Description" content="" />

	<!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
	<!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
	<![endif]-->
	<!--[if lt IE 7]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
	<![endif]-->
  
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/resources/css/<?php echo $this->my_uconfig->get('theme'); ?>.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/statusbar/css/statusbar.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/grid/css/GridFilters.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/grid/css/RangeMenu.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/css/CheckHeader.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/icon_css.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/allow_select_grid.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>kontrol_menu/css"/>
  
	<style>
		body {background:#7F99BE;}
			.child-row .x-grid-cell {
		background-color: yellow !important;
		}
		.child-bold-row .x-grid-cell {
			font-weight: bold !important;
		}
	</style>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ext-all.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/CheckColumn.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/statusbar/StatusBar.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/form/SearchField.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/grid/FiltersFeature.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/miframe/ManagedIframe.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/class/allow_select_grid.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/functions/function_override.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/functions/jsDate1.js"></script>
	
	<script type="text/javascript">
		var bulanx = ['April', 'April', 'April', 'April', 'Oktober', 'Oktober', 'Oktober', 'Oktober', 'Oktober', 'Oktober', 'April', 'April'];
		var datex_bulan = bulanx[<?php echo (date('n') - 1); ?>].toUpperCase();
		var datex_tahun = <?php echo date('Y'); ?>;
		if(<?php echo (date('n') - 1); ?> >= 10){
			datex_tahun += 1;
		}

		var sekarangx = {
			bulan : datex_bulan,
			tahun : datex_tahun
		}

		var BASE_URL = '<?php echo base_url(); ?>';
		var BASE_ICONS = BASE_URL + 'assets/images/icons/';
		var Time_Out = 2400000; var Tgl_Skrg = "<?php echo date('d/m/Y'); ?>";
		var Scr_Width = screen.availWidth;
		var Scr_Height = screen.availHeight;
		var type_user = '<?php echo $this->session->userdata("type_zs_exmldashboard"); ?>';
		var sesi_kode_unker = '<?php echo $this->session->userdata("kode_unker_zs_exmldashboard"); ?>';
		var Tab_Active = "";
		Ext.BLANK_IMAGE_URL = BASE_URL + 'assets/js/ext/resources/themes/images/access/tree/s.gif';
		Ext.Loader.setConfig({enabled: true});
		Ext.Loader.setPath('Ext.ux', BASE_URL + 'assets/js/ext/ux');	
		Ext.require(['*']);
				
		Ext.form.Field.prototype.msgTarget = 'side';
		Ext.QuickTips.init();


		Ext.require(['Ext.data.*', 'Ext.grid.*']);

		Ext.namespace('SERLOKSIDASHBOARD');
		Ext.onReady(function() {	
			var clock = Ext.create('Ext.toolbar.TextItem', {text: Ext.Date.format(new Date(), 'd M Y, g:i:s A')});

			var Content_MainMenu = {
			id: 'content-mainmenu',
			xtype: 'toolbar',
			dock: 'bottom',
			items: [
				<?php echo $MenuDashboard; ?>'->',{
				text: 'Logout',iconCls: 'icon-minus-circle', handler: function(){do_logout();}
			  }
			]
		  };

		  var Layout_Header = {id: 'layout-header', region: 'north', hidden:true, split: false,collapsible: false,border: false, items: [Content_MainMenu]};		
			
			var Content_Body_Tabs = Ext.createWidget('tabpanel', {
				id: 'Content_Body_Tabs', layout: 'fit', resizeTabs: true, enableTabScroll: false, deferredRender: true, border: false,
				defaults: {autoScroll:true},
			  listeners: {
				'tabchange': function(tabPanel, tab){
					switch(tab.title){
						// LAYANAN
						case 'Inpassing':
							Ext.getCmp('belum_proses_IMPG').setValue(true); break;
							Ext.getCmp('semua_periode_IMPG').setValue(true); break;
						case 'KGB':
							Ext.getCmp('belum_proses_T_KGB').setValue(true); break;
							Ext.getCmp('semua_periode_T_KGB').setValue(true); break;
						case 'Kenaikan Pangkat':
							Ext.getCmp('belum_proses_KP').setValue(true); break;
							Ext.getCmp('semua_periode_KP').setValue(true); break;
						case 'Pensiun':
							Ext.getCmp('belum_proses_SK_Pensiun').setValue(true); break;
							Ext.getCmp('semua_periode_SK_Pensiun').setValue(true); break;
						case 'Izin Belajar':
							Ext.getCmp('belum_proses_T_IB').setValue(true); break;
							Ext.getCmp('semua_periode_T_IB').setValue(true); break;
						case 'Ujian Dinas':
							Ext.getCmp('belum_proses_T_UD').setValue(true); break;
							Ext.getCmp('semua_periode_T_UD').setValue(true); break;
						case 'Satya Lencana':
							Ext.getCmp('belum_proses_T_SL').setValue(true); break;
							Ext.getCmp('semua_periode_T_SL').setValue(true); break;
						case 'CUTI':
							Ext.getCmp('belum_proses_T_Cuti').setValue(true); break;
							Ext.getCmp('semua_periode_T_Cuti').setValue(true); break;
						case 'Taperum':
							Ext.getCmp('belum_proses_T_Taperum').setValue(true); break;
							Ext.getCmp('semua_periode_T_Taperum').setValue(true); break;
						case 'SK Rikes':
							Ext.getCmp('belum_proses_T_SK_RIKES').setValue(true); break;
							Ext.getCmp('semua_periode_T_SK_RIKES').setValue(true); break;
						case 'SK Meninggal':
							Ext.getCmp('belum_proses_T_DC').setValue(true); break;
							Ext.getCmp('semua_periode_T_DC').setValue(true); break;
						case 'KARPEG':
							Ext.getCmp('belum_proses_T_Karpeg').setValue(true); break;
							Ext.getCmp('semua_periode_T_Karpeg').setValue(true); break;
						case 'KARIS/KARSU':
							Ext.getCmp('belum_proses_T_Karissu').setValue(true); break;
							Ext.getCmp('semua_periode_T_Karissu').setValue(true); break;
							
						// KEPEGAWAIAN
						case 'SK Calon Pegawai':
							Ext.getCmp('belum_proses_T_SK_CPNS').setValue(true); break;
							Ext.getCmp('semua_periode_T_SK_CPNS').setValue(true); break;
						case 'SK Pegawai':
							Ext.getCmp('belum_proses_T_SK_PNS').setValue(true); break;
							Ext.getCmp('semua_periode_T_SK_PNS').setValue(true); break;
						case 'SK PMK':
							Ext.getCmp('belum_proses_PMKG').setValue(true); break;
							Ext.getCmp('semua_periode_PMKG').setValue(true); break;
						case 'SK MPP':
							Ext.getCmp('belum_proses_T_SK_MPP').setValue(true); break;
							Ext.getCmp('semua_periode_T_SK_MPP').setValue(true); break;
						case 'Mutasi Masuk':
							Ext.getCmp('belum_proses_T_MTM').setValue(true); break;
							Ext.getCmp('semua_periode_T_MTM').setValue(true); break;
						case 'Mutasi Keluar':
							Ext.getCmp('belum_proses_MTK').setValue(true); break;
							Ext.getCmp('semua_periode_MTK').setValue(true); break;
						case 'Surat Pindah Tugas':
							Ext.getCmp('belum_proses_ST').setValue(true); break;
							Ext.getCmp('semua_periode_ST').setValue(true); break;
						case 'Dispilin Pegawai':
							Ext.getCmp('belum_proses_T_HukDis').setValue(true); break;
							Ext.getCmp('semua_periode_T_HukDis').setValue(true); break;
						case 'DP3':
							Ext.getCmp('belum_proses_T_DP3').setValue(true); break;
							Ext.getCmp('semua_periode_T_DP3').setValue(true); break;

						// DIKLAT
						case 'Pra Jabatan':
							Ext.getCmp('belum_proses_T_PraJab').setValue(true); break;
							Ext.getCmp('semua_periode_T_PraJab').setValue(true); break;
						case 'Diklat PIM':
							Ext.getCmp('belum_proses_T_D_PIM').setValue(true); break;
							Ext.getCmp('semua_periode_T_D_PIM').setValue(true); break;
						case 'Diklat Teknis':
							Ext.getCmp('belum_proses_T_D_Teknis').setValue(true); break;
							Ext.getCmp('semua_periode_T_D_Teknis').setValue(true); break;
						case 'Tugas Belajar':
							Ext.getCmp('belum_proses_T_TB').setValue(true); break;
							Ext.getCmp('semua_periode_T_TB').setValue(true); break;
						case 'Riwayat Pendidikan Formal':
							Ext.getCmp('belum_proses_T_Pddk').setValue(true); break;
							Ext.getCmp('semua_periode_T_Pddk').setValue(true); break;
						default:
					}
				}
			  }    
			});

		  var Layout_Body = Ext.create('Ext.panel.Panel', {
			id: 'layout-body', region: 'center', layout: 'fit', border: false, deferredRender: true,
			items: [{
					id: 'items-body', border: false, layout: 'card',  autoRender: true, closable: false, activeItem: 0,
					items: [Content_Body_Tabs]
			}]
		  });	
			
		  var Layout_Footer = {
			id: 'layout-footer', region: 'south', layout: 'fit', split: false, border: false, margins : '5 0 0 0',
			items: [{
				xtype: 'toolbar', dock: 'bottom', items: ['Pengguna : ' + '<?php echo $this->my_usession->userdata("fullname_zs_exmldashboard")." | ".$this->my_usession->userdata("type_zs_exmldashboard")." | ".$this->my_usession->userdata("nama_unker_zs_exmldashboard"); ?>','->',clock, '-', {id: 'ChatBtn', iconCls: 'icon-smiley', handler: function(){ Show_Chat();}}],
				listeners: {
					render: {
					fn: function(){
						Ext.fly(clock.getEl().parent()).addCls('x-status-text-panel').createChild({cls:'spacer'});
							
								// Kick off the clock timer that updates the clock el every second:
								Ext.TaskManager.start({
									run: function(){
										Ext.fly(clock.getEl()).update(Ext.Date.format(new Date(), 'd M Y, g:i:s A'));								
									},
									interval: 1000
								});
							},
							delay: 100
						}
				}
			}]
		  };		
		  
		  //Print All Component     
		  Ext.create('Ext.Viewport', {
			id: 'main_viewport', layout: {type: 'border',padding: 5}, defaults: {split: true}, items: [Layout_Header,Layout_Body], renderTo: Ext.getBody()
		  });
		  
		  
		
		<?php
			if( $this->input->get('idbutton') ) {
				echo "Ext.getCmp('".$this->input->get('idbutton')."').handler()";
			};
		?>
			
		});

		function Show_Chat(){
			Ext.MessageBox.show({title:'Chat !', msg: 'Chatting !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
		}

		function do_logout() {Ext.getCmp('layout-body').body.mask("Logout ...", "x-mask-loading"); Ext.Ajax.request({ url: BASE_URL + 'user/ext_logout', method: 'POST', success: function(xhr) { window.location = BASE_URL + 'user'; } }); return false; }
		function Load_Page(page_id, page_url) {
			Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
			var new_page_id = Ext.getCmp(page_id);
			if(new_page_id){
			Ext.getCmp('items-body').getLayout().setActiveItem(new_page_id); Ext.getCmp('items-body').doLayout();	Ext.getCmp('layout-body').body.unmask();			
			}else{
				Ext.Ajax.timeout = Time_Out;
				Ext.Ajax.request({
				url: page_url, method: 'POST', params: {id_open: 1}, scripts: true, renderer: 'data',
				success: function(response){	
					// Start Register Javacript On Fly
					var jsonData = response.responseText; var aHeadNode = document.getElementsByTagName('head')[0]; var aScript = document.createElement('script'); aScript.text = jsonData; aHeadNode.appendChild(aScript);
					// End Register Javacript On Fly
					if(newpanel != "GAGAL"){
						Ext.getCmp('items-body').add(newpanel); Ext.getCmp('items-body').getLayout().setActiveItem(newpanel); Ext.getCmp('items-body').doLayout();
					}else{
						Ext.MessageBox.show({title:'Peringatan !', msg:'Anda tidak dapat mengakses ke halaman ini !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
					}
				},
				failure: function(response){Ext.MessageBox.show({title:'Peringatan !', msg:'Gagal memuat dokumen !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); },
				callback: function(response){ Ext.getCmp('layout-body').body.unmask(); },
				scope : this
				});
			}
		}

		function Load_TabPage(tab_id,tab_url) {
			Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
			var new_tab_id = Ext.getCmp(tab_id);
			var one_tab_items = Ext.getCmp('Content_Body_Tabs').getComponent(1);
			if(new_tab_id){
			  //add disini x
			  if(one_tab_items){
				one_tab_items.destroy();
			  }
			  Ext.getCmp('items-body').getLayout().setActiveItem(0); Ext.getCmp('items-body').doLayout(); Ext.getCmp('Content_Body_Tabs').setActiveTab(tab_id); Ext.getCmp('layout-body').body.unmask();
			}else{
				Ext.Ajax.timeout = Time_Out;
				Ext.Ajax.request({
				url: tab_url, method: 'POST', params: {id_open: 1}, scripts: true, renderer: 'data',
				success: function(response){    	
					var jsonData = response.responseText; var aHeadNode = document.getElementsByTagName('head')[0]; var aScript = document.createElement('script'); aScript.text = jsonData; aHeadNode.appendChild(aScript);
						var new_tab = Ext.getCmp('Content_Body_Tabs');
					if(new_tabpanel != "GAGAL"){
						if(one_tab_items){
						  one_tab_items.destroy();
						}		
						Ext.getCmp('items-body').getLayout().setActiveItem(0); Ext.getCmp('items-body').doLayout(); new_tab.add(new_tabpanel).show();
					}else{
						Ext.MessageBox.show({title:'Peringatan !', msg:'Anda tidak dapat mengakses ke halaman ini !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});    			
					}    	
				},
				failure: function(response){ Ext.MessageBox.show({title:'Peringatan !', msg:'Gagal memuat dokumen !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); }, 
				callback: function(response){ Ext.getCmp('layout-body').body.unmask(); },
				scope : this
				});
			}
		}

		function Load_Popup(popup_id, popup_url, popup_title, url_form, idx) {
			if(typeof idx == 'undefined') idx = null;
			console.log(idx)
			
			Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
			var new_popup_id = Ext.getCmp(popup_id);
			if(new_popup_id){
			Ext.getCmp('layout-body').body.unmask(); new_popup.show(); 
			}else{
				Ext.Ajax.timeout = Time_Out;
				Ext.Ajax.request({
				url: popup_url, method: 'POST', params: {id_open: 1, popup_title: popup_title, idx:idx}, scripts: true, renderer: 'data',
				success: function(response){
					var jsonData = response.responseText; var aHeadNode = document.getElementsByTagName('head')[0]; var aScript = document.createElement('script'); aScript.text = jsonData; aHeadNode.appendChild(aScript);
						var new_popup = Ext.getCmp(popup_id);
					
					if(new_popup != "GAGAL" && new_popup){
						new_popup.show();
					}else{
						Ext.MessageBox.show({title:'Peringatan !', msg:'Anda tidak dapat mengakses ke halaman ini !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});    			
					}    	
				},
				failure: function(response){ Ext.MessageBox.show({title:'Peringatan !', msg:'Gagal memuat dokumen !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); }, 
				callback: function(response){ Ext.getCmp('layout-body').body.unmask(); },
				scope : this
				});
			}
		}

		function Load_Panel_Ref(popup_id, popup_uri, form_name, vfmode, p_key, p_key1, p_key2, p_key3) {
			Ext.getCmp('layout-body').body.mask("Loading...", "x-mask-loading");
			var new_popup_id = Ext.getCmp(popup_id);
			if(new_popup_id){
			Ext.getCmp('layout-body').body.unmask(); 
			var strFunc = "Funct_" + popup_id; var fn = window[strFunc];
			fn(form_name, vfmode);
			}else{
				Ext.Ajax.timeout = Time_Out;
				Ext.Ajax.request({
				url: BASE_URL + 'panel_ref/' + popup_uri, method: 'POST', params: {id_open: 1, VKEY:p_key, VKEY1:p_key1, VKEY2:p_key2, VKEY3:p_key3}, scripts: true, renderer: 'data',
				success: function(response){
					var jsonData = response.responseText; var aHeadNode = document.getElementsByTagName('head')[0]; var aScript = document.createElement('script'); aScript.text = jsonData; aHeadNode.appendChild(aScript);
						var new_popup = Ext.getCmp(popup_id);
					if(new_popup != "GAGAL"){
						var strFunc = "Funct_" + popup_id; var fn = window[strFunc];
						fn(form_name, vfmode);
					}else{
						Ext.MessageBox.show({title:'Peringatan !', msg:'Anda tidak dapat mengakses ke halaman ini !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});    			
					}
				},
				failure: function(response){ Ext.MessageBox.show({title:'Peringatan !', msg:'Gagal memuat dokumen !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); }, 
				callback: function(response){ Ext.getCmp('layout-body').body.unmask(); },
				scope : this
				});
			}
		}

		function Load_Variabel(page_url) {
			Ext.Ajax.request({
			  url: page_url, method: 'POST', params: {id_open: 1}, scripts: true, renderer: 'data',
			success: function(response){	
				var jsonData = response.responseText; var aHeadNode = document.getElementsByTagName('head')[0]; var aScript = document.createElement('script'); aScript.text = jsonData; aHeadNode.appendChild(aScript);
			},
			failure: function(response){Ext.MessageBox.show({title:'Peringatan !', msg:'Gagal memuat dokumen !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); },
			scope : this
			});	
		}

		Load_Variabel(BASE_URL + 'dashboard/fn');
		Load_Variabel(BASE_URL + 'profil_func');
		Load_Variabel(BASE_URL + 'arsip_digital_func');
		Load_Variabel(BASE_URL + 'user/set_var_access');
		Load_Variabel(BASE_URL + 'fm/widget');
	</script>
	</head>
	<body>
		<div id="script_area"></div>
		<div id="body_div"></div>
	</body>
</html>