<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

Ext.define('MT_Pengaturannotifikasi', {
    extend: 'Ext.data.Model',
    fields: [
        'tnfst_id',
        'tnfst_title',
        'tnfst_modulecode',
    ]
});

Ext.define('MT_Pengaturannotifikasidetail', {
    extend: 'Ext.data.Model',
    fields: [
        'tnfstd_tnfst_id',
        'tnfstd_id',
        'tnfstd_nml',
        'tnfstd_NIP',
        'tnfstd_unor',
        'tnfstd_unker',
        'tnfstd_asapprv',
        'tnfstd_state',
    ]
});

Ext.define('MT_Pengaturannotifikasilevelsuperdetail', {
    extend: 'Ext.data.Model',
    fields: [
        'tnfstdlevel_id',
        'tnfstdlevel_level',
        'tnfstdlevel_asapprv',
        'tnfstdlevel_tnfst_id',
        'tnfstdlevel_state',
    ]
});

var Reader_T_Pengaturannotifikasi = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Pengaturannotifikasi',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'tnfst_id'  	
});

var Reader_T_Pengaturannotifikasidetail = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Pengaturannotifikasidetail',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'tnfstd_id'  	
});

var Reader_T_Pengaturannotifikasilevelsuperdetail = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Pengaturannotifikasilevelsuperdetail',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'tnfstdlevel_id'  	
});

var Proxy_T_Pengaturannotifikasi = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Pengaturannotifikasi',
    url: BASE_URL + 'pengaturannotifikasi/ext_get_all',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Pengaturannotifikasi
});

var Proxy_T_Pengaturannotifikasidetail = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Pengaturannotifikasidetail',
    url: BASE_URL + 'pengaturannotifikasi/ext_get_all_detail',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Pengaturannotifikasidetail
});

var Proxy_T_Pengaturannotifikasilevelsuperdetail = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Pengaturannotifikasilevelsuperdetail',
    url: BASE_URL + 'pengaturannotifikasi/ext_get_all_detaillevel',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Pengaturannotifikasilevelsuperdetail
});

var Data_T_Pengaturannotifikasi = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Pengaturannotifikasi',
    model: 'MT_Pengaturannotifikasi',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Pengaturannotifikasi
});

var Data_T_Pengaturannotifikasidetail = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Pengaturannotifikasidetail',
    model: 'MT_Pengaturannotifikasidetail',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Pengaturannotifikasidetail
});

var Data_T_Pengaturannotifikasilevelsuperdetail = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Pengaturannotifikasilevelsuperdetail',
    model: 'MT_Pengaturannotifikasilevelsuperdetail',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Pengaturannotifikasilevelsuperdetail
});

var cbGrid_T_Pengaturannotifikasi = new Ext.create('Ext.selection.RowModel');

var cbGrid_T_Pengaturannotifikasidetail = new Ext.create('Ext.selection.RowModel');

var cbGrid_T_Pengaturannotifikasilevelsuperdetail = new Ext.create('Ext.selection.RowModel');

function deleteWindowPengaturanNofitikasi () {
	if( Ext.getCmp('window_WindowPengaturanNofitikasi') ) {
		Ext.getCmp('window_WindowPengaturanNofitikasi').destroy();
	};
}

function createWindowPengaturanNofitikasi () {
	deleteWindowPengaturanNofitikasi();
	
	Ext.create('Ext.window.Window', {
		title: 'Pengaturan Notifikasi - Modul',
		modal:true,
		constrainHeader:true,
		maximizable:false,
		closable:false,
		id:'window_WindowPengaturanNofitikasi',
		itemId:'window_WindowPengaturanNofitikasi',
		bodyStyle:'padding:10px;',
		height: 250,
		width: 500,
		layout: 'fit',
		items: [
			{
				xtype:'form',
				id:'form_window_WindowPengaturanNofitikasi',
				url: BASE_URL + 'pengaturannotifikasi/ext_insert_module',
				width: '100%',
				height: '100%',
				fieldDefaults: {
					labelAlign: 'top', 
					msgTarget: 'side'
				},
				defaultType: 'textfield', 
				defaults: {
					style:'margin:10px;',
					anchor: '100%', 
					allowBlank: false
				},
				flex:1,
				items:[
					{
						name:'id_open',
						value:1,
						xtype:'hidden'
					},
					{
						name: 'tnfst_id', 
						xtype: 'hidden'
					},{
						fieldLabel: 'Title',
						name: 'tnfst_title',
						anchor:'100%',
						emptyText:'Module Title',
						flex:1
					},{
						fieldLabel: 'Module Code',
						name: 'tnfst_modulecode',
						anchor:'100%',
						emptyText:'Module Code',
						flex:1
					},
				]
			}
		],
		buttons: [
			{
				text:'Save',
				iconCls:'icon-save',
				handler : function(){
					var fr_parent = Ext.getCmp('form_window_WindowPengaturanNofitikasi');
					var fr = fr_parent.getForm();
					if( fr.isValid() ) {
						
						fr.submit({
							waitMsg: 'Loading...',
							success: function(form, action) {
								Ext.Msg.alert('Success', action.result.message);
								deleteWindowPengaturanNofitikasi();
								Data_T_Pengaturannotifikasi.load();
							},
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							}
						});
					}
				}
			},{
				text:'Cancel',
				handler : function(){
					deleteWindowPengaturanNofitikasi();
				}
			}
		]
	}).show();
}

var Grid_T_Pengaturannotifikasi = new Ext.create('Ext.grid.Panel', {
    region : 'center',
    id: 'Grid_T_Pengaturannotifikasi',
    store: Data_T_Pengaturannotifikasi,
    border: true,
    loadMask: true,
	iconCls:'icon-menu_pelantikan_pf',
	title:'Daftar Modul',
    noCache: false,
    style: 'margin:3px 0px 0px 0px;',
    autoHeight: true,
    columnLines: true,
    flex:1.5,
    selModel: cbGrid_T_Pengaturannotifikasi,
    columns: [
        {header: 'No', xtype: 'rownumberer', width: 35, height : 20,resizable: true, style: 'padding-top: .2px;'}, 
        {header: "Title", dataIndex: 'tnfst_title', flex:1}, 
        {header: "Model Name / Class", dataIndex: 'tnfst_modulecode', flex:1}
    ],
    tbar: [
        {
            text:'Tambah',
			id:'pengaturannotifikasi_module_add',
            iconCls:'icon-add',
            handler:function(){
				createWindowPengaturanNofitikasi();
            }
        },
		'->',{
            text:'Edit',
			id:'pengaturannotifikasi_module_update',
			disabled:true,
            iconCls:'icon-edit',
            handler:function(){
				var sm = Grid_T_Pengaturannotifikasi.getSelectionModel(), sel = sm.getSelection();
				if(sel.length > 0){
					createWindowPengaturanNofitikasi();
					Ext.getCmp('form_window_WindowPengaturanNofitikasi').loadRecord(sel[0]);
				}
            }
        },'-',{
            text:'Hapus',
			disabled:true,
			id:'pengaturannotifikasi_module_delete',
            iconCls:'icon-delete',
            handler:function(){
				var sm = Grid_T_Pengaturannotifikasi.getSelectionModel(), sel = sm.getSelection();
				if(sel.length > 0){
					Ext.Msg.show({
						title: 'Konfirmasi', msg: 'Apakah Anda yakin untuk menghapus ?',
						buttons: Ext.Msg.YESNO, icon: Ext.Msg.QUESTION,
						fn: function(btn) {
							if (btn == 'yes') {
								
								Ext.Ajax.request({
									url: BASE_URL + 'pengaturannotifikasi/ext_delete_module', 
									method: 'POST',
									params: { 
										id_open: 1,
										tnfst_id:sel[0].get('tnfst_id')
									},
									success: function(response){
										Data_T_Pengaturannotifikasi.load();
									},
									failure: function(response){ 
										Ext.MessageBox.show({title:'Peringatan !', msg: response.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
									}
								});
							}
						}
					});
				}
            }
        },
    ],
    bbar: [
        {
            text:'Refresh',
            iconCls:'x-tbar-loading',
            handler:function(){
                Data_T_Pengaturannotifikasi.load();
            }
        }
    ],
    listeners: {
        selectionchange: function(model, records) {
			clearAndLoadDataStatus();
			
			var statusSelected  = false;
            if (records[0]) {
				statusSelected = true;
				
				Data_T_Pengaturannotifikasidetail.getProxy().extraParams = {
					tnfst_id: records[0].get('tnfst_id'), 
					id_open : 1
				};
				Data_T_Pengaturannotifikasidetail.load();
				
				Data_T_Pengaturannotifikasilevelsuperdetail.getProxy().extraParams = {
					tnfst_id: records[0].get('tnfst_id'), 
					id_open : 1
				};
				Data_T_Pengaturannotifikasilevelsuperdetail.load();
            }
			
			Ext.getCmp('pengaturannotifikasi_module_update').setDisabled(!statusSelected);
			Ext.getCmp('pengaturannotifikasi_module_delete').setDisabled(!statusSelected);
			
			Ext.getCmp('pengaturannotifikasi_detail_add').setDisabled(!statusSelected);
			Ext.getCmp('pengaturannotifikasi_detail_save').setDisabled(!statusSelected);
			//Ext.getCmp('pengaturannotifikasi_detail_viewstate').setDisabled(!statusSelected);
        } 
    }  
});

function PengaturannotifikasigetIdModuleSelected(  ){
	var idreq = 0;
	var sm = Grid_T_Pengaturannotifikasi.getSelectionModel(), sel = sm.getSelection();
	if(sel.length > 0){
		idreq = sel[0].get('tnfst_id');
	}
	return idreq;
}

var Grid_T_Pengaturannotifikasidetail = new Ext.create('Ext.grid.Panel', {
    region : 'center',
    id: 'Grid_T_Pengaturannotifikasidetail',
    store: Data_T_Pengaturannotifikasidetail,
    border: true,
    loadMask: true,
	title:'PIC ( Personal Identification Code )',
	iconCls:'icon-user',
    style: 'margin:0px;',
    noCache: false,
    autoHeight: true,
    columnLines: true,
    selModel: cbGrid_T_Pengaturannotifikasidetail,
	plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    columns: [
        {header: 'No', xtype: 'rownumberer', width: 35, height : 20,resizable: true, style: 'padding-top: .2px;'}, 
        {header: "NIP", dataIndex: 'tnfstd_NIP', flex:1},
        {header: "Nama Lengkap", dataIndex: 'tnfstd_nml', flex:1}, 
        {header: "Unit-Organisasi", dataIndex: 'tnfstd_unor', flex:1}, 
        {header: "Unit-Kerja", dataIndex: 'tnfstd_unker', flex:1}, 
		{header: "As Approval", dataIndex: 'tnfstd_asapprv', flex:1, editor:
		{
			xtype: 'combobox',
			store: new Ext.data.SimpleStore({
				data: [['Yes'], ['No']], fields: ['asapproval']
			}),
			displayField: 'asapproval',
			valueField: 'asapproval'
		}}/*,
        {header: "State", dataIndex: 'tnfstd_state', default:0, flex:1, editor: {
			xtype: 'numberfield'
		}}*/
    ],
    listeners: {
        selectionchange: function(model, records) {
			clearAndLoadDataStatus();
			
			var statusSelected  = false;
            if (records[0]) {
				statusSelected = true;
				clearAndLoadDataStatus( records[0].get('tnfstd_id'), 'pic' );
			}
			Ext.getCmp('pengaturannotifikasi_detail_delete').setDisabled(!statusSelected);
        } 
    }  
});

var Grid_T_Pengaturannotifikasilevelsuperdetail = new Ext.create('Ext.grid.Panel', {
    region : 'center',
    id: 'Grid_T_Pengaturannotifikasilevelsuperdetail',
    store: Data_T_Pengaturannotifikasilevelsuperdetail,
    border: true,
    loadMask: true,
	iconCls:'icon-menu_sotk',
	title : 'Level of Superiors',
    style: 'margin:0px;',
    noCache: false,
    autoHeight: true,
    columnLines: true,
    selModel: cbGrid_T_Pengaturannotifikasilevelsuperdetail,
	plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    columns: [
        {header: 'No', xtype: 'rownumberer', width: 35, height : 20,resizable: true, style: 'padding-top: .2px;'}, 
        {header: "Level of Superiors", dataIndex: 'tnfstdlevel_level', flex:1, editor: {
			xtype: 'numberfield'
		}}, 
		{header: "As Approval", dataIndex: 'tnfstdlevel_asapprv', flex:1, editor:
		{
			xtype: 'combobox',
			store: new Ext.data.SimpleStore({
				data: [['Yes'], ['No']], fields: ['asapproval']
			}),
			displayField: 'asapproval',
			valueField: 'asapproval'
		}}/*,
        {header: "State", dataIndex: 'tnfstdlevel_state', default:0, flex:1, editor: {
			xtype: 'numberfield'
		}}*/
    ],
    listeners: {
        selectionchange: function(model, records) {
			clearAndLoadDataStatus();
			
			var statusSelected  = false;
            if (records[0]) {
				statusSelected = true;
				clearAndLoadDataStatus( records[0].get('tnfstdlevel_id'), 'level' );
			}
			Ext.getCmp('pengaturannotifikasi_detail_delete').setDisabled(!statusSelected);
        } 
    }  
});

//status module kombo - start
var Store_Cb_StatusModul = new Ext.data.Store({
    fields: ['tpest_id','tpest_title'], idProperty: 'tpest_id',
    proxy: new Ext.data.AjaxProxy({
		url: BASE_URL + 'pengaturanstatus/ext_get_all', 
		method: 'POST', extraParams :{id_open: 1,combo: 1},
    }), autoLoad: true
});
var Store_Cb_StatusModul_Detail = new Ext.data.Store({
    fields: ['tpestd_text','tpestd_id','tpestd_tpest_id','tpestd_state'], idProperty: 'tpestd_id',
    proxy: new Ext.data.AjaxProxy({
		url: BASE_URL + 'pengaturanstatus/ext_get_all_detail', 
		method: 'POST', extraParams :{id_open: 1,combo: 1},
    }), autoLoad: true
});
//status module kombo - end

var PengaturanNotifikasi_Factory_Daftar_Status_Proto = function(d){
	return modelFactoryDetailWithForm({
		name:'PengaturanNotifikasi_Factory_Daftar_Status',
		title:'Pengaturan Status',
		data: d,
		singleDependency:'tnfstdstate_ref_id_asal',
		forming:{
			urldata:{
				get		: BASE_URL + 'pengaturannotifikasi/ext_getall_pnft_pengaturanstatus',
				post	: BASE_URL + 'pengaturannotifikasi/ext_insert_pnft_pengaturanstatus',
				put		: BASE_URL + 'pengaturannotifikasi/ext_insert_pnft_pengaturanstatus',
				destroy	: BASE_URL + 'pengaturannotifikasi/ext_delete_pnft_pengaturanstatus',
			},
			button:{
				insert:true,
				update:false,
				remove:true,
				notviewed:true,
			},
		},
		config:{
			url	: BASE_URL + 'pengaturannotifikasi/ext_getall_pnft_pengaturanstatus',
			params:d,
			primary:'tnfstdstate_id',
			root:'data'
		},
		field:[
			{
				name:'tnfstdstate_id', text:'PK', pk:true, hideOnGrid:true, default:0, editor:{
					xtype:'hiddenfield'
				}
			},{
				spesialname:'tpest_title', name: 'tnfstdstate_ref_id_tujuan_modul',id:'cmb_status_pilih_modul', 
				text:'Pilihan Modul', default:0, 
				editor:{
					margin:5,
					fieldLabel: 'Pilih Modul',
					flex:1, 
					layout:'fit',
					anchor: '100%',
					xtype: 'combobox',
					store: Store_Cb_StatusModul,
					typeAhead: true,
					forceSelection: true,
					valueField: 'tpest_id',
					displayField: 'tpest_title',
					id: 'cmb_status_pilih_modul',
					name: 'tnfstdstate_ref_id_tujuan_modul',
					allowBlank: false,
					listeners: {
						'focus': {
							fn: function (comboField) {
								comboField.doQuery(comboField.allQuery, true);
								comboField.expand();
							}, scope: this
						},
						'change':function(){
							Store_Cb_StatusModul_Detail.getProxy().extraParams['tpest_id'] = Ext.getCmp('cmb_status_pilih_modul').getValue();
							Store_Cb_StatusModul_Detail.load();
						}
					}
				}
			},{
				spesialname:'tpestd_text', name: 'tnfstdstate_ref_id_tujuan_status',id:'cmb_status_pilih_status', 
				text:'Pilihan Status', default:0, 
				editor:{
					margin:5,
					fieldLabel: 'Pilih Status',
					flex:1, 
					layout:'fit',
					anchor: '100%',
					xtype: 'combobox',
					store: Store_Cb_StatusModul_Detail,
					typeAhead: true,
					forceSelection: true,
					valueField: 'tpestd_id',
					displayField: 'tpestd_text',
					name: 'tnfstdstate_ref_id_tujuan_status',
					id: 'cmb_status_pilih_status',
					allowBlank: false,
					listeners: {
						'focus': {
							fn: function (comboField) {
								comboField.doQuery(comboField.allQuery, true);
								comboField.expand();
							}, scope: this
						}
					}
				}
			}
		]
	})
}

var PengaturanNotifikasi_Factory_Daftar_Status = function( d ){
	var d = ( d || {} );
	return PengaturanNotifikasi_Factory_Daftar_Status_Proto(d).getGrid();
}

var clearAndLoadDataStatus = function( asal, type ){
	var tempData = {};
	if( asal && type ) {
		tempData = {
			tnfstdstate_ref_id_asal : asal,
			tnfstdstate_type : type
		};
	};
	Ext.getCmp('panel_pengaturan_status_notifikasi').removeAll();
	Ext.getCmp('panel_pengaturan_status_notifikasi').add(
		PengaturanNotifikasi_Factory_Daftar_Status( tempData )
	);
};

var new_tabpanel = {
    id: 'newtab_pengaturannotifikasi', 
    title: 'Pengaturan Notifikasi', 
    iconCls: 'icon-information',
    border: false, 
    closable: true,  
    layout: 'border', 
    items: [
		{
			region:'north',
			border:false,
			html:'<div style="background:#59a7d2;color:white;padding:8px;"><b>PENGATURAN NOTIFIKASI</b><br/><p>Default User untuk melakukan persetujuan atau penerimaan informasi terhadap data yang akan / sudah di setujui dapat dilakukan disini</p></div>'
		},
        Grid_T_Pengaturannotifikasi,
		{
			border: false, 
			flex:4,
			layout: 'border', 
			style: 'margin:3px 0px 0px 10px;',
			region:'east',
			items:[
				{
					region:'north',
					border:false,
					style: 'margin:0px 0px 3px 0px;',
					html:'<div style="background:#55788B;color:white;padding:8px;"><b>Daftar PIC ( Personal Identification Code ) dari Modul yang dipilih</b></div>'
				},
				{
					xtype:'tabpanel',
					border:true,
					split:true,
					flex:2,
					region:'center',
					layout: 'border', 
					id:'tabpanel_pengaturannotifikasidetail_comp',
					items:[
						Grid_T_Pengaturannotifikasidetail,
						Grid_T_Pengaturannotifikasilevelsuperdetail
					],
					tbar: [
						{
							text:'Tambah',
							iconCls:'icon-add',
							id:'pengaturannotifikasi_detail_add',
							disabled:true,
							handler:function(){
								if( Ext.getCmp('tabpanel_pengaturannotifikasidetail_comp').getActiveTab().id == 'Grid_T_Pengaturannotifikasidetail' ) {
									Show_Popup_RefPegawai_User( 'Grid_T_Pengaturannotifikasidetail' );
								}else{
									Data_T_Pengaturannotifikasilevelsuperdetail.add({tnfstdlevel_state:1991});
								};
							}
						},{
							text:'Simpan',
							id:'pengaturannotifikasi_detail_save',
							iconCls:'icon-save',
							disabled:true,
							handler:function(){
								if( Ext.getCmp('tabpanel_pengaturannotifikasidetail_comp').getActiveTab().id == 'Grid_T_Pengaturannotifikasidetail' ) {
									
									var idreq = PengaturannotifikasigetIdModuleSelected();
									if( idreq > 0 ) {
										Grid_T_Pengaturannotifikasidetail.getEl().mask('Loading...');
										var postdata = Ext.encode(Ext.pluck(Data_T_Pengaturannotifikasidetail.data.items, 'data'));
										Ext.Ajax.request({
											url: BASE_URL + 'pengaturannotifikasi/ext_insert_detail_module', 
											method: 'POST',
											params: { 
												id_open: 1,
												tnfstd_tnfst_id: idreq,
												postdata:postdata
											},
											success: function(response){
												Grid_T_Pengaturannotifikasidetail.getEl().unmask();
												Data_T_Pengaturannotifikasidetail.load();
											},
											failure: function(response){ 
												Grid_T_Pengaturannotifikasidetail.getEl().unmask();
												Ext.MessageBox.show({title:'Peringatan !', msg: response.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
											}
										});
									};
									
								}else{
									var idreq = PengaturannotifikasigetIdModuleSelected();
									if( idreq > 0 ) {
										Grid_T_Pengaturannotifikasilevelsuperdetail.getEl().mask('Loading...');
										var postdata = Ext.encode(Ext.pluck(Data_T_Pengaturannotifikasilevelsuperdetail.data.items, 'data'));
										Ext.Ajax.request({
											url: BASE_URL + 'pengaturannotifikasi/ext_insert_detail_level_module', 
											method: 'POST',
											params: { 
												id_open: 1,
												tnfstdlevel_tnfst_id: idreq,
												postdata:postdata
											},
											success: function(response){
												Grid_T_Pengaturannotifikasilevelsuperdetail.getEl().unmask();
												Data_T_Pengaturannotifikasilevelsuperdetail.load();
											},
											failure: function(response){ 
												Grid_T_Pengaturannotifikasilevelsuperdetail.getEl().unmask();
												Ext.MessageBox.show({title:'Peringatan !', msg: response.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
											}
										});
									};
								}
							}
						},
						'->',{
							text:'Hapus',
							disabled:true,
							id:'pengaturannotifikasi_detail_delete',
							iconCls:'icon-delete',
							handler:function(){
								if( Ext.getCmp('tabpanel_pengaturannotifikasidetail_comp').getActiveTab().id == 'Grid_T_Pengaturannotifikasidetail' ) {
									Data_T_Pengaturannotifikasidetail.remove(Grid_T_Pengaturannotifikasidetail.getSelectionModel().getSelection());
								}else{
									Data_T_Pengaturannotifikasilevelsuperdetail.remove(Grid_T_Pengaturannotifikasilevelsuperdetail.getSelectionModel().getSelection());
								}
							}
						},
					]
				},{
					iconCls:'icon-gears',
					split:true,
					region:'east',
					border:false,
					id:'panel_pengaturan_status_notifikasi',
					flex:2,
					layout:'fit',
					items:PengaturanNotifikasi_Factory_Daftar_Status()
				}
			]
		}
    ]
}
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>
