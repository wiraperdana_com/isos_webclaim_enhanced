<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $this->my_uconfig->get('name'); ?> - <?php echo $this->my_uconfig->get('title'); ?></title>
		<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/exmldashboard/icon.ico" type="image/x-icon" /> 
		<meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
		<meta name="Keywords" content="<?php echo $this->my_uconfig->get('appname'); ?>" />
		<meta name="Description" content="<?php echo $this->my_uconfig->get('appname'); ?>" />

		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/icon_css.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/resources/css/<?php echo $this->my_uconfig->get('theme'); ?>.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/ext/ux/statusbar/css/statusbar.css"/>

		<style>body {background: #7F99BE url(<?php echo base_url(); ?>assets/images/extjs/abstrackblue.jpg)  no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;}</style>

		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ext/ux/statusbar/StatusBar.js"></script>

		<script type="text/javascript">
			var BASE_URL = '<?php echo base_url(); ?>';
			var BASE_ICONS = BASE_URL + 'assets/images/icons/';
			var Time_Out = 60000;
			Ext.BLANK_IMAGE_URL = BASE_URL +
				'assets/js/ext/resources/themes/images/access/tree/s.gif';
			Ext.Loader.setConfig({
				enabled: true
			});
			Ext.Loader.setPath('Ext.ux', BASE_URL + 'assets/js/ext/ux');
			Ext.require(['*']);
			Ext.namespace('Login');
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.form.Field.prototype.msgTarget = 'side';
				var formLogin = new Ext.FormPanel({
					frame: false,
					border: false,
					buttonAlign: 'center',
					url: BASE_URL + 'user/ext_login',
					method: 'POST',
					id: 'formLogin',
					bodyStyle: 'padding:10px 10px 15px 15px;',
					labelWidth: 150,
					flex:1,
					defaults: {
						allowBlank: false,
						listeners: {
							specialkey: function(f, e) {
								if (e.getKey() == e.ENTER) {
									fnLogin();
								}
							}
						}
					},
					items: [{
						xtype: 'textfield',
						fieldLabel: 'Pengguna',
						name: 'username',
						id: 'username'
					}, {
						xtype: 'textfield',
						fieldLabel: 'Kata Sandi',
						name: 'password',
						id: 'password',
						inputType: 'password'
					}],
					keys: [{
						key: [Ext.EventObject.ENTER],
						fn: fnLogin
					}],
					buttons: [{
						text: 'Login',
						handler: fnLogin
					}, {
						text: 'Reset',
						handler: function() {
							formLogin.getForm().reset();
						}
					}],
					bbar: new Ext.ux.StatusBar({
						text: 'Ready',
						id: 'sbWin',
						iconCls: 'x-status-valid'
					})
				});

				function fnLogin() {
					// this function before action
					Ext.getCmp('formLogin').on({
						beforeaction: function() {
							Ext.getCmp('winMask').body.mask();
							Ext.getCmp('sbWin').showBusy();
						}
					});
					formLogin.getForm().submit({
						success: function() {
							var ref_to = "<?php echo isset( $_GET['ref'] ) ? $_GET['ref'] : 'dashboard'; ?>";
							window.location = BASE_URL + ref_to;
						},
						failure: function(form, action) {
							Ext.getCmp('winMask').body.unmask();
							if (action.failureType == 'server') {
								obj = Ext.decode(action.response.responseText);
								Ext.getCmp('sbWin').setStatus({
									text: obj.errors.reason,
									iconCls: 'x-status-error'
								});
							} else {
								if (typeof(action.response) == 'undefined') {
									Ext.getCmp('sbWin').setStatus({
										text: 'Pengguna dan Kata Sandi tidak boleh kosong !',
										iconCls: 'x-status-error'
									});
								} else {
									Ext.Msg.alert('Warning!',
										'Server is unreachable : ' +
										action.response.responseText
									);
								}
							}
						}
					});
				}
				var winMasking = new Ext.Window({
					title: '<?php echo $this->my_uconfig->get('name'); ?> | LOGIN',
					width: 350,
					autoHeight:true,
					flex:1,
					resizable: false,
					border:false,
					closable: false,
					items: [
						{
							flex:1,
							region:'north',
							layout:'hbox',
							border:false,
							items:[
								{ flex:1, border:false, },
								{
									flex:2,
									height:<?php echo $this->my_uconfig->get('logo')['height']; ?>,
									width:<?php echo $this->my_uconfig->get('logo')['width']; ?>,
									maxHeight:<?php echo $this->my_uconfig->get('logo')['height']; ?>,
									maxWidth:<?php echo $this->my_uconfig->get('logo')['width']; ?>,
									src: BASE_URL + '<?php echo $this->my_uconfig->get('logo')['src']; ?>',
									xtype:'image',
									border:false,
									margins:10
								},
								{ flex:1, border:false, }
							]
						},
						formLogin
					],
					draggable:true,
					id: 'winMask',
					bbar:[
						{
							xtype:'label',
							flex:1,
							html:'<div style="padding:10px;"><center><?php echo $this->my_uconfig->get('appname'); ?> Version <?php echo $this->my_uconfig->get('version'); ?><br/><?php echo $this->my_uconfig->get('apppower'); ?> &copy; 2014-<?php echo date('Y'); ?></center></div>',
						}
					]
				}).show();
				Ext.getCmp('username').focus(false, 200);
			});

		</script>
	</head>
	<body>
		<div id="script_area"></div>
		<div id="body_div"></div>
	</body>
</html>