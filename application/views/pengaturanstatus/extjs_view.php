<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

Ext.define('MT_Pengaturanstatus', {
    extend: 'Ext.data.Model',
    fields: [
        'tpest_id',
        'tpest_title',
        'tpest_modulecode',
    ]
});

Ext.define('MT_Pengaturanstatusdetail', {
    extend: 'Ext.data.Model',
    fields: [
        'tpestd_tpest_id',
        'tpestd_id',
        'tpestd_text',
        'tpestd_next',
        'tpestd_true',
        'tpestd_false',
        'tpestd_status',
        'tpestd_proses',
        'tpestd_state',
    ]
});

var Reader_T_Pengaturanstatus = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Pengaturanstatus',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'tpest_id'  	
});

var Reader_T_Pengaturanstatusdetail = new Ext.create('Ext.data.JsonReader', {
    id: 'Reader_T_Pengaturanstatusdetail',
    root: 'results',
    totalProperty: 'total',
    idProperty: 'tpestd_id'  	
});

var Proxy_T_Pengaturanstatus = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Pengaturanstatus',
    url: BASE_URL + 'pengaturanstatus/ext_get_all',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Pengaturanstatus
});

var Proxy_T_Pengaturanstatusdetail = new Ext.create('Ext.data.AjaxProxy', {
    id: 'Proxy_T_Pengaturanstatusdetail',
    url: BASE_URL + 'pengaturanstatus/ext_get_all_detail',
    actionMethods: {read:'POST'},
    extraParams : {id_open: '1'},
    reader: Reader_T_Pengaturanstatusdetail
});

var Data_T_Pengaturanstatus = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Pengaturanstatus',
    model: 'MT_Pengaturanstatus',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Pengaturanstatus
});

var Data_T_Pengaturanstatusdetail = new Ext.create('Ext.data.Store', {
    id: 'Data_T_Pengaturanstatusdetail',
    model: 'MT_Pengaturanstatusdetail',
    pageSize: 12,
    noCache: false,
    autoLoad: true,
    proxy: Proxy_T_Pengaturanstatusdetail
});

var cbGrid_T_Pengaturanstatus = new Ext.create('Ext.selection.RowModel');

var cbGrid_T_Pengaturanstatusdetail = new Ext.create('Ext.selection.RowModel');

function deleteWindowPengaturanNofitikasi () {
	if( Ext.getCmp('window_WindowPengaturanNofitikasi') ) {
		Ext.getCmp('window_WindowPengaturanNofitikasi').destroy();
	};
}

function createWindowPengaturanNofitikasi () {
	deleteWindowPengaturanNofitikasi();
	
	Ext.create('Ext.window.Window', {
		title: 'Pengaturan Status - Modul',
		modal:true,
		constrainHeader:true,
		maximizable:false,
		closable:false,
		id:'window_WindowPengaturanNofitikasi',
		itemId:'window_WindowPengaturanNofitikasi',
		bodyStyle:'padding:10px;',
		height: 250,
		width: 500,
		layout: 'fit',
		items: [
			{
				xtype:'form',
				id:'form_window_WindowPengaturanNofitikasi',
				url: BASE_URL + 'pengaturanstatus/ext_insert_module',
				width: '100%',
				height: '100%',
				fieldDefaults: {
					labelAlign: 'top', 
					msgTarget: 'side'
				},
				defaultType: 'textfield', 
				defaults: {
					style:'margin:10px;',
					anchor: '100%', 
					allowBlank: false
				},
				flex:1,
				items:[
					{
						name:'id_open',
						value:1,
						xtype:'hidden'
					},
					{
						name: 'tpest_id', 
						xtype: 'hidden'
					},{
						fieldLabel: 'Title',
						name: 'tpest_title',
						anchor:'100%',
						emptyText:'Module Title',
						flex:1
					},{
						fieldLabel: 'Module Code',
						name: 'tpest_modulecode',
						anchor:'100%',
						emptyText:'Module Code',
						flex:1
					},
				]
			}
		],
		buttons: [
			{
				text:'Save',
				iconCls:'icon-save',
				handler : function(){
					var fr_parent = Ext.getCmp('form_window_WindowPengaturanNofitikasi');
					var fr = fr_parent.getForm();
					if( fr.isValid() ) {
						
						fr.submit({
							waitMsg: 'Loading...',
							success: function(form, action) {
								Ext.Msg.alert('Success', action.result.message);
								deleteWindowPengaturanNofitikasi();
								Data_T_Pengaturanstatus.load();
							},
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							}
						});
					}
				}
			},{
				text:'Cancel',
				handler : function(){
					deleteWindowPengaturanNofitikasi();
				}
			}
		]
	}).show();
}

var Grid_T_Pengaturanstatus = new Ext.create('Ext.grid.Panel', {
    region : 'center',
    id: 'Grid_T_Pengaturanstatus',
    store: Data_T_Pengaturanstatus,
    border: true,
    loadMask: true,
	iconCls:'icon-menu_pelantikan_pf',
	title:'Daftar Modul',
    noCache: false,
    style: 'margin:3px 0px 0px 0px;',
    autoHeight: true,
    columnLines: true,
    flex:1.5,
    selModel: cbGrid_T_Pengaturanstatus,
    columns: [
        {header: 'No', xtype: 'rownumberer', width: 35, height : 20,resizable: true, style: 'padding-top: .2px;'}, 
        {header: "Title", dataIndex: 'tpest_title', flex:1}, 
        {header: "Model Name / Class", dataIndex: 'tpest_modulecode', flex:1}
    ],
    tbar: [
        {
            text:'Tambah',
			id:'pengaturanstatus_module_add',
            iconCls:'icon-add',
            handler:function(){
				createWindowPengaturanNofitikasi();
            }
        },
		'->',{
            text:'Edit',
			id:'pengaturanstatus_module_update',
			disabled:true,
            iconCls:'icon-edit',
            handler:function(){
				var sm = Grid_T_Pengaturanstatus.getSelectionModel(), sel = sm.getSelection();
				if(sel.length > 0){
					createWindowPengaturanNofitikasi();
					Ext.getCmp('form_window_WindowPengaturanNofitikasi').loadRecord(sel[0]);
				}
            }
        },'-',{
            text:'Hapus',
			disabled:true,
			id:'pengaturanstatus_module_delete',
            iconCls:'icon-delete',
            handler:function(){
				var sm = Grid_T_Pengaturanstatus.getSelectionModel(), sel = sm.getSelection();
				if(sel.length > 0){
					Ext.Msg.show({
						title: 'Konfirmasi', msg: 'Apakah Anda yakin untuk menghapus ?',
						buttons: Ext.Msg.YESNO, icon: Ext.Msg.QUESTION,
						fn: function(btn) {
							if (btn == 'yes') {
								
								Ext.Ajax.request({
									url: BASE_URL + 'pengaturanstatus/ext_delete_module', 
									method: 'POST',
									params: { 
										id_open: 1,
										tpest_id:sel[0].get('tpest_id')
									},
									success: function(response){
										Data_T_Pengaturanstatus.load();
									},
									failure: function(response){ 
										Ext.MessageBox.show({title:'Peringatan !', msg: response.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
									}
								});
							}
						}
					});
				}
            }
        },
    ],
    bbar: [
        {
            text:'Refresh',
            iconCls:'x-tbar-loading',
            handler:function(){
                Data_T_Pengaturanstatus.load();
            }
        }
    ],
    listeners: {
        selectionchange: function(model, records) {
			var statusSelected  = false;
            if (records[0]) {
				statusSelected = true;
				
				Data_T_Pengaturanstatusdetail.getProxy().extraParams = {
					tpest_id: records[0].get('tpest_id'), 
					id_open : 1
				};
				Data_T_Pengaturanstatusdetail.load();
            }
			
			Ext.getCmp('pengaturanstatus_module_update').setDisabled(!statusSelected);
			Ext.getCmp('pengaturanstatus_module_delete').setDisabled(!statusSelected);
			
			Ext.getCmp('pengaturanstatus_detail_add').setDisabled(!statusSelected);
			Ext.getCmp('pengaturanstatus_detail_save').setDisabled(!statusSelected);
        } 
    }  
});

function PengaturanstatusgetIdModuleSelected(  ){
	var idreq = 0;
	var sm = Grid_T_Pengaturanstatus.getSelectionModel(), sel = sm.getSelection();
	if(sel.length > 0){
		idreq = sel[0].get('tpest_id');
	}
	return idreq;
}

var dragandropPluginStatus = Ext.create('Ext.grid.plugin.DragDrop', {
	pluginId:'dragandropPluginStatus'
});
var Grid_T_Pengaturanstatusdetail = new Ext.create('Ext.grid.Panel', {
    region : 'center',
    id: 'Grid_T_Pengaturanstatusdetail',
    store: Data_T_Pengaturanstatusdetail,
    border: true,
    loadMask: true,
	title:'Status Proses Modul',
	iconCls:'icon-gears',
    style: 'margin:0px;',
    noCache: false,
    autoHeight: true,
    columnLines: true,
    selModel: cbGrid_T_Pengaturanstatusdetail,
    viewConfig: {
        stripeRows: false,
        getRowClass: function (record) {
            return record.get('tpestd_status') == 0 ? 'child-row' : '';
        },
		plugins: dragandropPluginStatus,
		forceFit: true
    },
	plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    columns: [
        {header: 'No', xtype: 'rownumberer', width: 35, height : 20,resizable: true, style: 'padding-top: .2px;'}, 
        {header: "Status Data", dataIndex: 'tpestd_state', flex:1, editor: {
			xtype: 'numberfield'
		}},
		{header: "Status Title", dataIndex: 'tpestd_text', flex:1, editor: {
			xtype: 'textfield'
		}},
        {header: "Status Approval", dataIndex: 'tpestd_true', flex:1, editor: {
			xtype: 'numberfield'
		}}, 
        {header: "Status Rejected", dataIndex: 'tpestd_false', flex:1, editor: {
			xtype: 'numberfield'
		}}, 
        {header: "Status Next", dataIndex: 'tpestd_next', flex:1, editor: {
			xtype: 'numberfield'
		}}, 
		{header: "Proses", dataIndex: 'tpestd_proses', flex:1, editor:
		{
			xtype: 'combobox',
			store: new Ext.data.Store({
				data: [
					{ 'key' : 'Yes', 'val' : 1 },
					{ 'key' : 'No', 'val' : 0 },
				], 
				fields: ['key', 'val'],
			}),
			displayField: 'key',
			valueField: 'val'
		},renderer:function(a,b,c,d,e,f){
			return ( c.get('tpestd_proses') == 1 ? 'Yes' : 'No' );
		}}, 
		{header: "Aktif", dataIndex: 'tpestd_status', flex:1, editor:
		{
			xtype: 'combobox',
			store: new Ext.data.Store({
				data: [
					{ 'key' : 'Yes', 'val' : 1 },
					{ 'key' : 'No', 'val' : 0 },
				], 
				fields: ['key', 'val'],
			}),
			displayField: 'key',
			valueField: 'val'
		},renderer:function(a,b,c,d,e,f){
			return ( c.get('tpestd_status') == 1 ? 'Yes' : 'No' );
		}}
    ],
    tbar: [
        {
            text:'Tambah',
            iconCls:'icon-add',
			id:'pengaturanstatus_detail_add',
			disabled:true,
            handler:function(){
				Data_T_Pengaturanstatusdetail.add(new Ext.create('MT_Pengaturanstatusdetail', {tpestd_proses:1, tpestd_status : 1}));
            }
        },{
            text:'Simpan',
			id:'pengaturanstatus_detail_save',
            iconCls:'icon-save',
			disabled:true,
            handler:function(){
				var idreq = PengaturanstatusgetIdModuleSelected();
				if( idreq > 0 ) {
					Grid_T_Pengaturanstatusdetail.getEl().mask('Loading...');
					var postdata = Ext.encode(Ext.pluck(Data_T_Pengaturanstatusdetail.data.items, 'data'));
					Ext.Ajax.request({
						url: BASE_URL + 'pengaturanstatus/ext_insert_detail_module', 
						method: 'POST',
						params: { 
							id_open: 1,
							tpestd_tpest_id: idreq,
							postdata:postdata
						},
						success: function(response){
							Grid_T_Pengaturanstatusdetail.getEl().unmask();
							Data_T_Pengaturanstatusdetail.load();
						},
						failure: function(response){ 
							Grid_T_Pengaturanstatusdetail.getEl().unmask();
							Ext.MessageBox.show({title:'Peringatan !', msg: response.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
						}
					});
				}
            }
        },
		'->',{
            text:'Hapus',
			disabled:true,
			id:'pengaturanstatus_detail_delete',
            iconCls:'icon-delete',
            handler:function(){
				Data_T_Pengaturanstatusdetail.remove(Grid_T_Pengaturanstatusdetail.getSelectionModel().getSelection());
            }
        },
    ],
    listeners: {
        selectionchange: function(model, records) {
			var statusSelected  = false;
            if (records[0]) {
				statusSelected = true;
			}
			Ext.getCmp('pengaturanstatus_detail_delete').setDisabled(!statusSelected);
        } 
    }  
});

var new_tabpanel = {
    id: 'newtab_pengaturanstatus', 
    title: 'Pengaturan Status', 
    iconCls: 'icon-gears',
    border: false, 
    closable: true,  
    layout: 'border', 
    items: [
		{
			region:'north',
			border:false,
			html:'<div style="background:#59a7d2;color:white;padding:8px;"><b>PENGATURAN STATUS</b></div>'
		},
        Grid_T_Pengaturanstatus,
		{
			border: false, 
			flex:2,
			layout: 'border', 
			style: 'margin:3px 0px 0px 10px;',
			region:'east',
			items:[
				{
					region:'north',
					border:false,
					style: 'margin:0px 0px 3px 0px;',
					html:'<div style="background:#55788B;color:white;padding:8px;"><b>Daftar Status Proses Data Di Module Yang Dipilih</b></div>'
				},
				Grid_T_Pengaturanstatusdetail
			]
		}
    ]
}
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>
