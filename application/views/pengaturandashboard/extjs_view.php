<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

var Store_Cb_Daftar_Module = new Ext.data.Store({
    fields: ['idmenu','name','general_text','description','status'], idProperty: 'name',
    proxy: new Ext.data.AjaxProxy({
		url: BASE_URL + 'pengaturanmodul/ext_checking', 
		root:'data',
		method: 'GET', extraParams :{id_open: 1,combo: 1},
    }), autoLoad: true
});

var Store_Cb_Daftar_Group = new Ext.data.Store({
    fields: ['id_groupcollections','general_group_name'], idProperty: 'id_groupcollections',
    proxy: new Ext.data.AjaxProxy({
		url: BASE_URL + 'kontrol_menu/ext_get_all_groupcollections', 
		root:'results',
		totalProperty: 'total',
		idProperty: 'id_groupcollections',
		method: 'POST', extraParams :{id_open: 1,combo: 1},
		actionMethods:  {create: "POST", read: "POST", update: "POST", destroy: "POST"},
		reader: {
			type: 'json',
			root: 'results',
			totalProperty: 'total',
			idProperty: 'id_groupcollections'
		}
    }), autoLoad: true,
	method: 'POST',
});

var PengaturanDashboard_Daftar_Proto = function(d){
	return modelFactoryDetailWithForm({
		name:'PengaturanDashboard_Daftar_Proto',
		title:'Daftar Modul Tersedia!',
		data: d,
		gridfit:false,
		singleDependency:'semar',
		forming:{
			urldata:{
				get		: BASE_URL + 'pengaturandashboard/ext_all',
				post	: BASE_URL + 'pengaturandashboard/ext_insert',
				put		: BASE_URL + 'pengaturandashboard/ext_insert',
				destroy	: BASE_URL + 'pengaturandashboard/ext_delete',
			},
			button:{
				insert:true,
				update:true,
				remove:true,
			},
		},
		config:{
			url	: BASE_URL + 'pengaturandashboard/ext_all',
			params:d,
			primary:'tdash_id',
			root:'data'
		},
		initgrid : {
			border:false,
			viewConfig: {
				stripeRows: false,
				forceFit: false
			}
		},
		field:[
			{
				name:'tdash_id', text:'ID Dash', pk:true, hideOnGrid:true, default:0, editor:{
					xtype:'hiddenfield'
				}
			},{
				text: 'No',
				width:80,
				xtype: 'rownumberer', editor:{
					width:80
				}
			},{
				spesialname:'name', name:'tdash_module', text:'Module', default:'-', editor:{
					margin:5,
					fieldLabel: 'Pilih Modul',
					flex:1, 
					layout:'fit',
					anchor: '100%',
					xtype: 'combobox',
					store: Store_Cb_Daftar_Module,
					typeAhead: true,
					forceSelection: true,
					valueField: 'name',
					displayField: 'name',
					id: 'cmb_tdash_module',
					name: 'tdash_module',
					allowBlank: false,
					listeners: {
						'focus': {
							fn: function (comboField) {
								comboField.doQuery(comboField.allQuery, true);
								comboField.expand();
							}, scope: this
						},
						'change':function(){
						}
					}
				}
			},{
				name : 'general_group_name', text : 'User Group', default:'-', editor:{
					xtype:'hiddenfield'
				}
			},{
				spesialname:'general_group_name', name:'tdash_group', hideOnGrid:true, text:'User Group', default:'-', editor:{
					margin:5,
					fieldLabel: 'Pilih Group',
					flex:1, 
					layout:'fit',
					anchor: '100%',
					xtype: 'combobox',
					store: Store_Cb_Daftar_Group,
					typeAhead: true,
					forceSelection: true,
					valueField: 'id_groupcollections',
					displayField: 'general_group_name',
					id: 'cmb_tdash_usergroup',
					name: 'tdash_group',
					allowBlank: false,
					listeners: {
						'focus': {
							fn: function (comboField) {
								comboField.doQuery(comboField.allQuery, true);
								comboField.expand();
							}, scope: this
						},
						'change':function(){
						}
					}
				}
			}
		]
	})
};

var PengaturanDashboard_Daftar = function( d ){
	var d = ( d || [] );
	var tempGrid = PengaturanDashboard_Daftar_Proto(d).getGrid();
	return tempGrid;
};

var Grid_PengaturanDashboard_Daftar = PengaturanDashboard_Daftar({semar:true});

var new_tabpanel = {
    id: 'newtab_pengaturandashboard', 
    title: 'Pengaturan Dashboard', 
    iconCls: 'icon-gears',
    border: false, 
    closable: true,  
    layout: 'border', 
	bbar:[
		{
			iconCls:'semar-stop',
			href: BASE_URL + 'update',
			text:'Click Here to : Check Update eXML Dashboard'
		}
	],
    items: [
		{
			region:'north',
			border:false,
			split:true,
			html:'<div style="background:#59a7d2;color:white;padding:8px;"><b>PENGATURAN DASHBOARD</b></div>'
		},{
			split:true,
			flex:1.5,
			xtype:'tabpanel',
			region:'west',
			title:'Assoc Browseref Employee',
			tbar:[
				'->',{
					text:'Save',
					iconCls:'icon-save',
					handler:function(){
						var Temp_Property_JSON = function( PropertiesX ){
							var storeItms  = PropertiesX.getStore().data.items,
								tempResult = {};
							for( var obj in storeItms ) {
								if( storeItms[ obj ] ) {
									if( storeItms[ obj ]['data'] ) {
										tempResult[ storeItms[ obj ]['data']['name'] ] = storeItms[ obj ]['data']['value'];
									};
								}
							};
							return tempResult;
						};
						
						var asoc_browseref_1 = Temp_Property_JSON( Ext.getCmp( 'assoc_browseref_employee_1' ) );
						var asoc_browseref_2 = Temp_Property_JSON( Ext.getCmp( 'assoc_browseref_employee_2' ) );
						var asoc_browseref_3 = Temp_Property_JSON( Ext.getCmp( 'assoc_browseref_employee_3' ) );
						var asoc_browseref_4 = Temp_Property_JSON( Ext.getCmp( 'assoc_browseref_employee_4' ) );
						
						var asoc_browseref_x = {
							employee_asocmodule_browseref 	: asoc_browseref_1,
							employee_asocmodule_unor 		: asoc_browseref_2,
							employee_asocmodule_pegawai 	: asoc_browseref_3,
							employee_asocmodule_jabatan		: asoc_browseref_4
						};
						Ext.getBody().mask('Sedang melakukan penyimpanan konfigurasi..');
						Ext.Ajax.request({
							url: BASE_URL + 'pengaturandashboard/save_cassoc_browseref_employee', 
							method: 'POST',
							params: {
								data : Ext.JSON.encode( asoc_browseref_x )
							},
							success: function(response){
								Ext.getBody().unmask('Sedang melakukan penyimpanan konfigurasi..');
								Ext.Msg.alert('Success', 'Sukses Memanipulasi Browseref Configs');
							},
							failure: function(response){ 
								Ext.getBody().unmask('Sedang melakukan penyimpanan konfigurasi..');
								Ext.MessageBox.show({title:'Peringatan !', msg: 'Gagal Memanipulasi Browseref Configs', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); 
							}
						});
					}
				}
			],
			items:[
				Ext.create('Ext.ux.grid.property.Grid', {
					title:'Browseref',
					id:'assoc_browseref_employee_1',
					groupingConfig: {
						groupHeaderTpl: '{name}',
						disabled: false
					},
					iconCls:'icon-show',
					flex:1,
					source: [
						{
							"name"		: "modulecode",
							"text"		: "modulecode",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.modulecode,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},
						
						//employee_asocmodule_browseref - start
						{
							"name"		: "fullname",
							"text"		: "fullname",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.fullname,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "email",
							"text"		: "email",
							"editor"	: "",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.email,
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "NIP",
							"text"		: "NIP",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.NIP,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_unker",
							"text"		: "kode_unker",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_unker,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "nama_unker",
							"text"		: "nama_unker",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_unker,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "nama_unor",
							"text"		: "nama_unor",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_unor,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_unor",
							"text"		: "kode_unor",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_unor,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "nama_jab",
							"text"		: "nama_jab",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.nama_jab,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_jab",
							"text"		: "kode_jab",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_browseref.kode_jab,
							"editor"	: "",
							"group"		: "employee_asocmodule_browseref",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},
						//employee_asocmodule_browseref - end
					]
				}),
				Ext.create('Ext.ux.grid.property.Grid', {
					id:'assoc_browseref_employee_2',
					title:'Unor ( Unit Organisasi )',
					groupingConfig: {
						groupHeaderTpl: '{name}',
						disabled: false
					},
					iconCls:'icon-show',
					flex:1,
					source: [
						//employee_asocmodule_unor - start
						{
							"name"		: "table",
							"text"		: "table_or_view",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_unor.table,
							"editor"	: "",
							"group"		: "employee_asocmodule_unor",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_jab",
							"text"		: "kode_jab",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_unor.kode_jab,
							"editor"	: "",
							"group"		: "employee_asocmodule_unor",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_parent",
							"text"		: "kode_parent_of_unor",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_unor.kode_parent,
							"editor"	: "",
							"group"		: "employee_asocmodule_unor",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_unor",
							"text"		: "kode_unor",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_unor.kode_unor,
							"editor"	: "",
							"group"		: "employee_asocmodule_unor",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},
						//employee_asocmodule_unor - end
					]
				}),
				Ext.create('Ext.ux.grid.property.Grid', {
					title:'Pegawai',
					id:'assoc_browseref_employee_3',
					groupingConfig: {
						groupHeaderTpl: '{name}',
						disabled: false
					},
					iconCls:'icon-show',
					flex:1,
					source: [
						//employee_asocmodule_pegawai - start
						{
							"name"		: "table",
							"text"		: "table_or_view",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.table,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "nama_lengkap",
							"text"		: "nama_lengkap",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.nama_lengkap,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},
						{
							"name"		: "fullname",
							"text"		: "fullname",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.fullname,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "email",
							"text"		: "email",
							"editor"	: "",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.email,
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "NIP",
							"text"		: "NIP",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.NIP,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_unker",
							"text"		: "kode_unker",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.kode_unker,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "nama_unker",
							"text"		: "nama_unker",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.nama_unker,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "nama_unor",
							"text"		: "nama_unor",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.nama_unor,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_unor",
							"text"		: "kode_unor",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.kode_unor,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "nama_jab",
							"text"		: "nama_jab",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.nama_jab,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_jab",
							"text"		: "kode_jab",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_pegawai.kode_jab,
							"editor"	: "",
							"group"		: "employee_asocmodule_pegawai",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},
						//employee_asocmodule_pegawai - end
					]
				}),
				Ext.create('Ext.ux.grid.property.Grid', {
					title:'Jabatan',
					id:'assoc_browseref_employee_4',
					groupingConfig: {
						groupHeaderTpl: '{name}',
						disabled: false
					},
					iconCls:'icon-show',
					flex:1,
					source: [
						
						//employee_asocmodule_jabatan - start
						{
							"name"		: "table",
							"text"		: "table_or_view",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_jabatan.table,
							"editor"	: "",
							"group"		: "employee_asocmodule_jabatan",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_jab",
							"text"		: "kode_jab",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_jabatan.kode_jab,
							"editor"	: "",
							"group"		: "employee_asocmodule_jabatan",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "kode_klp_jab",
							"text"		: "kode_klp_jab",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_jabatan.kode_klp_jab,
							"editor"	: "",
							"group"		: "employee_asocmodule_jabatan",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},{
							"name"		: "struktural_val",
							"text"		: "struktural_val",
							"value"		: Show_Popup_RefPegawai_User_Config.employee_asocmodule_jabatan.struktural_val,
							"editor"	: "",
							"group"		: "employee_asocmodule_jabatan",
							"editable"	: true,
							"status"	: true,
							"renderer"	: ""
						},
						//employee_asocmodule_jabatan - end
					]
				})
			]
		},{
			flex:2,
			split:true,
			iconCls: 'icon-gears',
			region:'center',
			layout: 'border',
			layout:'fit',
			items : Grid_PengaturanDashboard_Daftar
		}
    ]
}
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>
