<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	function qrcode_create($string = '',$dir,$px, $canvas,$tempDir = "./assets/qrcodes/tmp/")
	{
	    require_once('phpqrcode/qrlib.php'); 
	    //															px,canvas
	    QRcode::png($string, $tempDir.$string.'.png', QR_ECLEVEL_M, $px, $canvas);

	    return $dir.$string.'.png';
	}
?>