<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
function konf_timetonumber($v) {
	$num = str_replace(':', '' , $v);
	$num = intval($num);
	return $num;
}

function konf_numbertotime($v) {
	$str = strval($v);
	$str = str_pad($str, 4, "0", STR_PAD_LEFT);
	$srt = substr($str,0,2).':'.substr($str,-2);
	return $srt;
}

?>