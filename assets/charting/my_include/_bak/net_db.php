<?php
/**
 * @build_date	: 17-06-2008
 * @name		: net_db.php
 * @comment 	: dilindungi hak cipta :) 
 */
//if(ereg(basename(__FILE__),$PHP_SELF)) { header("location: ./"); die;}


/* funsi koneksi ke databse server */ 
function net_db_connect($server = DB_SERVER, $username = DB_SERVER_USERNAME, $password = DB_SERVER_PASSWORD, $database = DB_DATABASE, $link = 'db_link') 
{
    global $$link;

    if (USE_PCONNECT == 'true') {
      $$link = mysql_pconnect($server, $username, $password);
    } else {
      $$link = mysql_connect($server, $username, $password);
    }

    if ($$link) mysql_select_db($database);

    return $$link;
 }
 
/* tutup koneksi */
function net_db_close($link = 'db_link') 
{
    global $$link;
    return mysql_close($$link);
}
/* Menampilkan error query dari mysql  */
function net_db_error($query, $errno, $error)
{ 
    die('<font color="#000000"><b>' . $errno . ' - ' . $error . '<br><br>' . $query . '<br>
		 <br><small><font color="#ff0000">[NET STOP]</font></small><br><br></b></font>');
}
/* menggambl data query */
function net_db_query($query, $link = 'db_link') 
{
    global $$link;
    if (defined('DS_DB_TRANSACTIONS') && (DS_DB_TRANSACTIONS == 'true')) {
      error_log('QUERY ' . $query . "\n", 3, DS_PAGE_PARSE_TIME_LOG);
    }

    $result = mysql_query($query, $$link) or net_db_error($query, mysql_errno(), mysql_error());

    if (defined('DS_DB_TRANSACTIONS') && (DS_DB_TRANSACTIONS == 'true')) {
       $result_error = mysql_error();
       error_log('RESULT ' . $result . ' ' . $result_error . "\n", 3, DS_PAGE_PARSE_TIME_LOG);
    }

    return $result;
}

/* menambah dan mengupdate data mysql */
function net_db_perform($table, $data, $action = 'insert', $parameters = '', $link = 'db_link')
{
    reset($data);
    if ($action == 'insert') {
      $query = 'insert into ' . $table . ' (';
      while (list($columns, ) = each($data)) {
        $query .= '`'. $columns .'` , ';
      }
      $query = substr($query, 0, -2) . ') values (';
      reset($data);
      while (list(, $value) = each($data)) {
        switch ((string)$value) {
          case 'now()':
            $query .= 'now(), ';
            break;
          case 'null':
            $query .= 'null, ';
            break;
          default:
            $query .= '\'' . net_db_input($value) . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) . ')';
    } elseif ($action == 'update') {
      $query = 'update ' . $table . ' set ';
      while (list($columns, $value) = each($data)) {
        switch ((string)$value) {
          case 'now()':
            $query .= $columns . ' = now(), ';
            break;
          case 'null':
            $query .= $columns .= ' = null, ';
            break;
          default:
            $query .= $columns . ' = \'' . net_db_input($value) . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) . ' where ' . $parameters;
    }
	//print $query;
    return net_db_query($query, $link);
}

  function net_db_fetch_array($db_query) {
    return mysql_fetch_array($db_query, MYSQL_ASSOC);
  }

  function net_db_num_rows($db_query) {
    return mysql_num_rows($db_query);
  }

  function net_db_data_seek($db_query, $row_number) {
    return mysql_data_seek($db_query, $row_number);
  }

  function net_db_insert_id() {
    return mysql_insert_id();
  }

  function net_db_free_result($db_query) {
    return mysql_free_result($db_query);
  }

  function net_db_fetch_fields($db_query) {
    return mysql_fetch_field($db_query);
  }

  function net_db_output($string) {
    return htmlspecialchars($string);
  }

  function net_db_input($string) {
    return addslashes($string);
  }

  function net_sanitize_string($string) {
    $string = ereg_replace(' +', ' ', $string);

    return preg_replace("/[<>]/", '_', $string);
  }


  function net_db_prepare_input($query_str) {
    $query_str = urldecode($query_str);
	$query_str = str_replace('/', ' ', $query_str);
	$query_str = str_replace('\\', ' ', $query_str);
	$query_str = str_replace('\'', ' ', $query_str);
		
	return $query_str;

  }

function simple_query($table_name) 
{
	$kon = "WHERE $key_col = $key_val";
	$db_rs = net_db_query("SELECT * FROM $table_name ");
	$num_fields = mysql_num_fields($db_rs);
	if ($num_fields) {
		$row = mysql_fetch_assoc($db_rs);
		for ($i = 0; $i < $num_fields; $i++) 
		{
			//$simple_q[mysql_field_name($db_rs, $i)] = $row[mysql_field_name($db_rs, $i)];
			$simple_q[$i] = mysql_field_name($db_rs, $i);

		}
		return $simple_q;
	
	} 
	else 
	{
	return false;
	}
	net_db_free_result($db_rs);
}

function query_selcted_field($tabel,$arrfield,$kondisi='') 
{
	$sql = "select ";
		foreach($arrfield as $field)
		{
			$xfld[] =" ".$field." ";
		}
	$sql .=implode(',',$xfld);	
	$sql .=" from $tabel ";
	if(isset($kondisi))
	{
		$sql .= $kondisi ;
	}
	$sql .=" order by 1 asc ";
		$db_rs = net_db_query($sql);
		$num_fields = mysql_num_fields($db_rs);
		$num_rows = net_db_num_rows($db_rs);
		$p=0;	
		if ($num_rows) {
			while ($row = mysql_fetch_assoc($db_rs)) 
			{ $p++;
				$arrval[] = $row;
			}
		}		
		if ($num_fields) {
			$row = mysql_fetch_assoc($db_rs);
			for ($i = 0; $i < $num_fields; $i++) 
			{
				//$simple_q[mysql_field_name($db_rs, $i)] = $row[mysql_field_name($db_rs, $i)];
				$simple_q[$i] = mysql_field_name($db_rs, $i);
				$get_type[$i] = mysql_field_type($db_rs, $i);	
				$get_len[$i] = mysql_field_len($db_rs, $i);		
				$arr[mysql_field_name($db_rs, $i)] = $row[mysql_field_name($db_rs, $i)];
			}
		   if ($num_rows < 1) {		
			 $arrval=array($arr);
			}

		}
		return array($simple_q,$arrval,$num_rows,$get_type,$get_len);	

	net_db_free_result($db_rs);
}

function querycek_field($table_name,$kondisi='') 
{
	 $sqlx="SHOW FIELDS FROM $table_name ";
	 $rox=net_db_query($sqlx);
	 $dat=array();
	 while($dax=net_db_fetch_array($rox)){
		if(stristr($dax['Type'],'tinyint'))
		{
			$dat[$dax['Field']]=$dax['Default'];
		}
		 
	 }
	 
	if($kondisi) $kon = $kondisi;
	$db_rs = net_db_query("SELECT * FROM $table_name  $kon ");
	$num_fields = mysql_num_fields($db_rs);
	if ($num_fields) {
		$row = mysql_fetch_assoc($db_rs);
		for ($i = 0; $i < $num_fields; $i++) 
		{
			//$simple_q[mysql_field_name($db_rs, $i)] = $row[mysql_field_name($db_rs, $i)];
			$simple_q[$i] = mysql_field_name($db_rs, $i);

		}
		return array($simple_q,$dat);
	
	} 
	else 
	{
	return false;
	}
	net_db_free_result($db_rs);
}

############ TUSKA FUNCTION ###########

function lookUp($tableName,$fieldName,$criteria)
	{
	$resx= net_db_query("select $fieldName from $tableName where $criteria" );
	if ($rowx=mysql_fetch_array($resx))
		return $rowx[0];
	else
		return false;
	}
	
###############################################	

  
?>
