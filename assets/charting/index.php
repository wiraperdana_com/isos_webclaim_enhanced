<?php
/*
include 'my_include/net_function.php';
include "my_include/FusionCharts.php";

?>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/style.css">
<SCRIPT LANGUAGE="Javascript" SRC="FusionCharts.js"></SCRIPT>
</head>

<table width="120%" border="1" cellspacing="10" cellpadding="10" >
<tr align="left" valign="top" >
<td width="15%</h5>">
<?php 
	
	$sql="SELECT * FROM tView_Rekap_Golongan ";
	$exe=net_db_query($sql);
	$data=net_db_fetch_array($exe);
	
	$strXML1 = "<chart caption='GRAFIK REKAP GOLONGAN' shownames='1' showvalues='0' decimals='0' numberPrefix=''> >";
    $strXML1 .= "<categories>";
    $strXML1 .= " <category label='Golongan 1' />";
    $strXML1 .= " <category label='Golongan 2' />";
    $strXML1 .= " <category label='Golongan 3' />";
    $strXML1 .= " <category label='Golongan 4' />";
    $strXML1 .= " </categories>";
	$strXML1 .= "<dataset seriesName='A' color='AFD8F8' showValues='0'>
					<set label='IA' value='$data[jml_g11]'  />
					<set value='$data[jml_g21]' />
					<set value='$data[jml_g31]' />
					<set value='$data[jml_g41]' />			
				 </dataset>";
					
	$strXML1 .= "<dataset seriesName='B' color='F6BD0F' showValues='0'>
					<set value='$data[jml_g12]' />
					<set value='$data[jml_g22]' />
					<set value='$data[jml_g32]' />
					<set value='$data[jml_g42]' />
				 </dataset>";

	$strXML1 .= "<dataset seriesName='C' color='8BBA00' showValues='0'>
				    <set value='$data[jml_g13]' />
					<set value='$data[jml_g23]' />					
					<set value='$data[jml_g33]' />
					<set value='$data[jml_g43]' />				
				 </dataset>";
	
	$strXML1 .= "<dataset seriesName='D' color='C8A1D1' showValues='0'>
					<set value='$data[jml_g14]' />		
					<set value='$data[jml_g24]' />			
					<set value='$data[jml_g34]' />					
					<set value='$data[jml_g44]' />					
				 </dataset>";
					
    $strXML1 .= "</chart>";
	echo renderChartHTML("Charts/MSColumn3D.swf", "", "$strXML1", "Rekap_golongan", 560, 300, false);  
	?></td>
    
<td valign="top">
    <div class="grafiktitle"> LIST PEGAWAI YANG AKAN NAIK PANGKAT BULAN : <?=strtoupper(date('F'))?>  <?=date('Y')?> </div> <br>
	<div style="height:258px !important;overflow:auto !important;">
 
	<?php 
	$sql="SELECT * FROM tView_Trans_KP_Prediksi  WHERE tahun_prediksi='2013' AND MONTH(TMT_kpkt)=MONTH(CURRENT_DATE())  ";
	$nbv=net_db_query($sql);
	if(net_db_num_rows($nbv) < 1){		
		echo "<div class=\"grafiktitle\"> Pesan : Tidak ada Data Pegawai yang akan Naik Pangkat Bulan ".date('F')." ".date('Y')." </div>" ;
	}else{
		list_tabel_view($sql);
    } 
?>        	
		</div>
		</td>
</tr>
<tr align="left" >
<td valign="top">

<?php 
	
	$sql="SELECT * FROM tView_Rekap_Eselon ";
	$exe=net_db_query($sql);
	$data=net_db_fetch_array($exe);
	
	$strXML2 = "<chart caption='GRAFIK REKAP GOLONGAN' shownames='1' showvalues='0' decimals='0' numberPrefix=''> >";
    $strXML2 .= "<categories>";
    $strXML2 .= " <category label='Eselon 1' />";
    $strXML2 .= " <category label='Eselon 2' />";
    $strXML2 .= " <category label='Eselon 3' />";
    $strXML2 .= " <category label='Eselon 4' />";
	$strXML2 .= " <category label='Eselon 5' />";
    $strXML2 .= " <category label='Pelaksana' />";
	$strXML2 .= " <category label='Fungsional' />";
    $strXML2 .= " </categories>";
	$strXML2 .= "<dataset seriesName='Eselon' color='AFD8F8' showValues='0'>
					<set value='$data[jml_e1]'  />
					<set value='$data[jml_e2]' />
					<set value='$data[jml_e3]' />
					<set value='$data[jml_e4]' />			
					<set value='$data[jml_e5]' />			
					<set value='$data[jml_pelaksana]' />			
					<set value='$data[jml_fung]' />			
				 </dataset>";
	
    $strXML2.= "</chart>";
	echo renderChartHTML("Charts/MSColumn3D.swf", "", "$strXML2", "Rekap_eselon", 560, 300, false);  
	?></td>
    
<td valign="top">
<div class="grafiktitle"> LIST PEGAWAI YANG AKAN PENSIUN PADA BULAN : <?=strtoupper(date('F'))?>  <?=date('Y')?> </div><br>
	<div style="height:258px !important;overflow:auto !important;">
	<?php 
	$sql="SELECT * FROM tView_Trans_KP_Prediksi  WHERE YEAR(tmt_bup)=YEAR(CURRENT_DATE()) ";
	$nbv=net_db_query($sql);
	if(net_db_num_rows($nbv) < 1){
		echo "<div class=\"grafiktitle\"> Tidak ada Data Pegawai Pensiun Bulan ".date('F')." Tahun ".date('Y')." </div>" ;
	}else{
		list_tabel_view($sql);
	}
	?></td> 
    </div>
</tr>
<tr align="left" >
<td valign="top">
<?php 
	
	$sql="SELECT * FROM tView_Rekap_Pddk ";
	$exe=net_db_query($sql);
	$data=net_db_fetch_array($exe);
	
	$strXML2 = "<chart caption='GRAFIK REKAP GOLONGAN' shownames='1' showvalues='0' decimals='0' numberPrefix=''> >";
    $strXML2 .= "<categories>";
    $strXML2 .= " <category label='SD' />";
    $strXML2 .= " <category label='SMP' />";
    $strXML2 .= " <category label='SMA' />";
    $strXML2 .= " <category label='D1' />";
	$strXML2 .= " <category label='D2' />";
    $strXML2 .= " <category label='D3' />";
	$strXML2 .= " <category label='D4' />";	
	$strXML2 .= " <category label='S1' />";
	$strXML2 .= " <category label='S2' />";
    $strXML2 .= " <category label='S3' />";
    $strXML2 .= " </categories>";
	$strXML2 .= "<dataset seriesName='Pendidikan' color='F6BD0F' showValues='0'>
					<set value='$data[jml_sd]'  />
					<set value='$data[jml_smp]' />
					<set value='$data[jml_sma]' />
					<set value='$data[jml_d1]' />											
					<set value='$data[jml_d2]' />								
					<set value='$data[jml_d3]' />			
					<set value='$data[jml_d4]' />		
					<set value='$data[jml_s1]' />							
					<set value='$data[jml_s2]' />			
					<set value='$data[jml_s3]' />	
			 	</dataset>";
	
    $strXML2.= "</chart>";
	echo renderChartHTML("Charts/MSColumn3D.swf", "", "$strXML2", "Rekap_pendidikan", 560, 300, false);  
	echo '<p>';
?></td>

<td rowspan="2"> <div class="grafiktitle"> LIST PEGAWAI ULANG YANG BERULANG TAHUN BULAN : <?=strtoupper(date('F'))?>  <?=date('Y')?>  
</div>
  <br>
  <?php 
	?>
	<div style="height:604px !important;overflow:auto !important;">
	  <table width="100%" border="0" cellspacing="0" cellpadding="10" class="bordersilver">
	    <tr align="center" class="tdataheader" >
	      <td  align="center" >NIP</td>
            <td  align="center" >Nama Lengkap</td>
       	    <td align="center" >Unit-Organisasi &nbsp; </td>
		    <td align="center" >Unit-Kerja </td>
		    <td align="center" >Tanggal Ultah</td>
          </tr>
	    
	    <? 
		$sql="
SELECT NIP,f_namalengkap,nama_pangkat,nama_golru,nama_jab,nama_unor,nama_unker,DAY(tanggal_lahir) AS tgl_lahir  FROM  tView_Pegawai_Grid WHERE MONTH(tanggal_lahir)=MONTH(CURRENT_DATE()) 
order by  DAY(tanggal_lahir) asc  ";
		$exe=net_db_query($sql);
		
		while($data=net_db_fetch_array($exe)){
			$no=$no+1;
			($no % 2)? $cl="data0" : $cl="data1";
			
			echo "<tr class='$cl'>
				<td align='center' nowrap='nowrap'>$data[NIP]&nbsp;</td>
			  	<td align='left' nowrap='nowrap'>$data[f_namalengkap]&nbsp;</td>
				<td align='center' >$data[nama_unker]&nbsp;</td>
				<td align='center' >$data[nama_unor]&nbsp;</td>
				<td align='center' >$data[tgl_lahir]&nbsp;</td>
				";
			  	
		     echo"</td></tr>";
		}	
		?>
	    </table>
    </div></td>
</tr>
<tr align="left" >
<td valign="top">
<?	
	//echo 'GRAFIK USIA AGAMA <br>';
	$sql="SELECT * FROM tView_Rekap_Usia_Agama ";
	$exe=net_db_query($sql);
	$data=net_db_fetch_array($exe);
	
	$strXML3 = "<chart caption='GRAFIK REKAP USIA AGAMA' shownames='1' showvalues='0' decimals='0' numberPrefix=''> >";
    $strXML3 .= "<categories>";
    $strXML3 .= " <category label='USIA 20-29' />";
    $strXML3 .= " <category label='USIA 30-39' />";
	$strXML3 .= " <category label='USIA 40-49' />";
	$strXML3 .= " <category label='USIA 50-59' />";
	$strXML3 .= " <category label='USIA 60' />";
    $strXML3 .= " <category label='Islam' />";
	$strXML3 .= " <category label='Katolik' />";
	$strXML3 .= " <category label='Protestan' />";
	$strXML3 .= " <category label='Hindu' />";
	$strXML3 .= " <category label='Budha' />";

    $strXML3 .= " </categories>";
	$strXML3 .= "<dataset seriesName='' color='8BBA00' showValues='0'>
					<set value='$data[jml_20_29]'  />
					<set value='$data[jml_30_39]' />
					<set value='$data[jml_40_49]' />
					<set value='$data[jml_50_59]' />
					<set value='$data[jml_60_lbh]' />
					<set value='$data[jml_islam]' />
					<set value='$data[jml_katolik]' />
					<set value='$data[jml_protestan]' />
					<set value='$data[jml_hindu]' />
					<set value='$data[jml_budha]' />					
		 		</dataset>";
					
    $strXML3 .= "</chart>";
	echo renderChartHTML("Charts/MSColumn3D.swf", "", "$strXML3", "Rekap_usiaagama", 560, 300, false);  
	
	?></td>
</tr>
</table>
<?php
*/
echo 'Access Forbidden!';
?>
