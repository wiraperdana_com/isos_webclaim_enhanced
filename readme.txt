Generator ketika melakukan generate code akan dilangsungkan secara background process.
Jadi, ketika simpan kode cepat & kode belum muncul secara fisik file, ditunggu saja, sampai file selesai di create.
Ini salah satu upaya untuk menangani TIMEOUT request HTTP di server.
Kelemahannya, jadi tidak begitu tau ketika ada error ketika melakukan generate code, tapi kecil kemungkinan error ketika generate.

Untuk pengguna windows,
Set PHP Runtime di Path Environment Windows.
http://www.nextofwindows.com/how-to-addedit-environment-variables-in-windows-7
http://www.geeksengine.com/article/php-include-path.html
Misal untuk XAMPP :
C:\xampp\php\php.exe or C:\xampp\php
Untuk test bila PHP sudah executable secara CLI, bukan CMD, lalu ketika PHP -v