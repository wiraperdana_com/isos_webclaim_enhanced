-- --------------------------------------------------------
-- Host:                         10.102.21.2
-- Server version:               5.6.29 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for isos_card
CREATE DATABASE IF NOT EXISTS `isos_card` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `isos_card`;


-- Dumping structure for table isos_card.authassignment
CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.authassignment: ~3 rows (approximately)
/*!40000 ALTER TABLE `authassignment` DISABLE KEYS */;
INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
	('ABA', '2', NULL, 'N;'),
	('Admin', '1', NULL, 'N;'),
	('SOS', '3', NULL, 'N;');
/*!40000 ALTER TABLE `authassignment` ENABLE KEYS */;


-- Dumping structure for table isos_card.authitem
CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.authitem: ~43 rows (approximately)
/*!40000 ALTER TABLE `authitem` DISABLE KEYS */;
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
	('ABA', 2, 'ABA Officer', NULL, 'N;'),
	('Admin', 2, 'Super User', NULL, 'N;'),
	('Approve.*', 1, 'Approved Order', NULL, 'N;'),
	('Approve.Admin', 0, 'Approve - Admin', NULL, 'N;'),
	('Approve.No', 0, 'Approve - No', NULL, 'N;'),
	('Approve.Yes', 0, 'Approve - Yes', NULL, 'N;'),
	('Cards.*', 1, 'Cards', NULL, 'N;'),
	('Cards.Admin', 0, 'Cards - Admin', NULL, 'N;'),
	('Cards.Create', 0, 'Cards - Create', NULL, 'N;'),
	('Cards.Delete', 0, 'Cards - Delete', NULL, 'N;'),
	('Cards.Note', 0, 'Cards - Note', NULL, 'N;'),
	('Cards.Order', 0, 'Cards - Order', NULL, 'N;'),
	('Cards.Update', 0, 'Cards - Update', NULL, 'N;'),
	('Dashboard.*', 1, 'Dashboard', NULL, 'N;'),
	('Doc.*', 1, 'Documentation', NULL, 'N;'),
	('Edc.*', 1, 'Terminal', NULL, 'N;'),
	('EmailConfig.*', 1, 'Settings', NULL, 'N;'),
	('EmailConfig.Admin', 0, 'Settings - Admin', NULL, 'N;'),
	('EmailConfig.Update', 0, 'Settings - Update', NULL, 'N;'),
	('New User', 2, 'New User', NULL, 'N;'),
	('Rights.Assignment.*', 1, 'Assignment', NULL, 'N;'),
	('Rights.Assignment.Revoke', 0, 'Assignment - Revoke', NULL, 'N;'),
	('Rights.Assignment.User', 0, 'Assignment - View', NULL, 'N;'),
	('Rights.Assignment.View', 0, 'Assignment', NULL, 'N;'),
	('Rights.AuthItem.*', 1, 'Permissions & Roles', NULL, 'N;'),
	('Rights.AuthItem.Assign', 0, 'Permissions Assign', NULL, 'N;'),
	('Rights.AuthItem.Create', 0, 'Roles - Create', NULL, 'N;'),
	('Rights.AuthItem.Delete', 0, 'Roles - Delete', NULL, 'N;'),
	('Rights.AuthItem.Permissions', 0, 'Permissions', NULL, 'N;'),
	('Rights.AuthItem.RemoveChild', 0, 'Roles - Remove Child', NULL, 'N;'),
	('Rights.AuthItem.Revoke', 0, 'Permissions - Revoke', NULL, 'N;'),
	('Rights.AuthItem.Roles', 0, 'Roles', NULL, 'N;'),
	('Rights.AuthItem.Update', 0, 'Roles - Update', NULL, 'N;'),
	('SOS', 2, 'SOS Officer', NULL, 'N;'),
	('User.Admin.*', 1, 'Users', NULL, 'N;'),
	('User.Admin.Admin', 0, 'User - Admin', NULL, 'N;'),
	('User.Admin.Create', 0, 'User - Create', NULL, 'N;'),
	('User.Admin.Delete', 0, 'User - Delete', NULL, 'N;'),
	('User.Admin.Update', 0, 'User - Update', NULL, 'N;'),
	('User.Admin.View', 0, 'User - View', NULL, 'N;'),
	('User.Profile.*', 1, 'Profile User', NULL, 'N;'),
	('User.Profile.Changepassword', 0, 'Profile User - Change Password', NULL, 'N;'),
	('User.Profile.Edit', 0, 'Profile User - Edit', NULL, 'N;'),
	('User.Profile.Profile', 0, 'Profile User - Admin', NULL, 'N;');
/*!40000 ALTER TABLE `authitem` ENABLE KEYS */;


-- Dumping structure for table isos_card.authitemchild
CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.authitemchild: ~15 rows (approximately)
/*!40000 ALTER TABLE `authitemchild` DISABLE KEYS */;
INSERT INTO `authitemchild` (`parent`, `child`) VALUES
	('ABA', 'Approve.*'),
	('SOS', 'Cards.*'),
	('ABA', 'Cards.Admin'),
	('ABA', 'Dashboard.*'),
	('New User', 'Dashboard.*'),
	('SOS', 'Dashboard.*'),
	('SOS', 'Edc.*'),
	('ABA', 'User.Admin.Admin'),
	('SOS', 'User.Admin.Admin'),
	('ABA', 'User.Admin.Create'),
	('SOS', 'User.Admin.Create'),
	('ABA', 'User.Admin.View'),
	('SOS', 'User.Admin.View'),
	('ABA', 'User.Profile.*'),
	('New User', 'User.Profile.*'),
	('SOS', 'User.Profile.*');
/*!40000 ALTER TABLE `authitemchild` ENABLE KEYS */;


-- Dumping structure for table isos_card.cards
CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_order` varchar(45) NOT NULL,
  `id_client` int(11) NOT NULL,
  `descriptions` text NOT NULL,
  `upload_file` varchar(200) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `approved_at` datetime DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `note` varchar(150) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `amount_approved` int(11) DEFAULT NULL,
  `amount_rejected` int(11) DEFAULT NULL,
  `descr_rejected` varchar(200) DEFAULT NULL,
  `expected_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_order` (`no_order`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.cards: ~7 rows (approximately)
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` (`id`, `no_order`, `id_client`, `descriptions`, `upload_file`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `approved_at`, `approved_by`, `note`, `amount`, `amount_approved`, `amount_rejected`, `descr_rejected`, `expected_date`) VALUES
	(1, '00001', 1, 'Test', '00001.zip', 5, '2016-04-07 00:42:43', 1, '2016-04-07 00:43:29', 1, '2016-04-07 00:43:55', 1, 'Note Tambahan', 5, NULL, NULL, NULL, '2016-04-23'),
	(2, '00002', 1, 'order kartu 100', '00002.zip', 5, '2016-04-07 10:34:22', 3, '2016-04-07 10:34:51', 3, '2016-04-07 10:38:30', 2, NULL, 100, NULL, NULL, NULL, '2016-04-08'),
	(3, '00003', 1, 'Test', '00003.zip', 3, '2016-04-11 16:48:43', 1, '2016-04-11 16:48:48', 1, '2016-04-11 17:02:33', 1, NULL, 11, 3, 8, 'Test', '2016-04-15'),
	(4, '00004', 1, 'testing card order 13 APril 2016', '00004.zip', 3, '2016-04-13 11:15:26', 3, '2016-04-13 11:16:24', 3, '2016-04-13 11:58:27', 1, NULL, 100, 80, 20, 'Testing kartu yang di reject karena terdapat kata-kata yang salah pada nama.', '2016-04-21'),
	(6, '00005', 1, 'Tes Order 13 April 2016 ', '00005.zip', 2, '2016-04-13 14:13:35', 3, '2016-04-18 10:06:00', 1, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL),
	(7, '00007', 1, 'pesanan 100 kartu', '00007.zip', 5, '2016-04-14 10:17:30', 3, '2016-04-14 10:17:35', 3, '2016-04-14 10:19:46', 2, NULL, 100, 90, 10, 'ada 10 kartu yang double', '2016-04-15'),
	(8, '00008', 1, 'Tes Order Card 14 April 2016', '00008.zip', 1, '2016-04-14 11:02:01', 3, '2016-04-14 11:02:01', 3, NULL, NULL, NULL, 75, NULL, NULL, NULL, NULL),
	(9, '00009', 1, 'Pesan 200 kartu', '00009.zip', 5, '2016-04-14 14:51:07', 3, '2016-04-14 14:51:20', 3, '2016-04-14 14:52:40', 2, NULL, 200, 190, 10, 'dari 200 kartu ada 10 kartu yang dobel', '2016-04-15');
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;


-- Dumping structure for table isos_card.client
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `no_telp` varchar(25) NOT NULL,
  `pic` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.client: ~0 rows (approximately)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` (`id`, `nama`, `address`, `no_telp`, `pic`, `status`) VALUES
	(1, 'PT Freeport Indonesia', 'Plaza 89, Lt. 5\r\nJl. HR. Rasuna Said Kav. X-7 No. 6\r\nJakarta 12940 Indonesia', '+62-21-2591818', 'Nama', 1);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;


-- Dumping structure for table isos_card.email_config
CREATE TABLE IF NOT EXISTS `email_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `pesan` text NOT NULL,
  `status` int(11) NOT NULL,
  `fraudtime` int(11) DEFAULT NULL,
  `fraudcron` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.email_config: ~5 rows (approximately)
/*!40000 ALTER TABLE `email_config` DISABLE KEYS */;
INSERT INTO `email_config` (`id`, `kategori`, `email`, `judul`, `pesan`, `status`, `fraudtime`, `fraudcron`) VALUES
	(1, 1, 'firisco.satria@internationalsos.com,yusrizal@karyaanilo.com', '[MHC] Suspected Fraud Detection for Member# {id} ({cid});[MHC] Suspected Fraud Detection– Summary/Recapitulation per {date}', '<!Doctype>\r\n<html>\r\n<head>\r\n	<title>{judul}</title>\r\n</head>\r\n<body>\r\n	<p>\r\n		This is to report you that member uses the following card for more than 1 time transaction for the same type of services.\r\n		<br><br>\r\n		The details as below:\r\n	</p>\r\n	<table cellpadding="10" border="1">\r\n		<thead>\r\n			<tr bgcolor="#D5D7FF">\r\n				<th>Member ID</th>\r\n				<th>Member Name</th>\r\n				<th>DOB</th>\r\n				<th>Benefit Entitlement</th>\r\n				<th>Date and Time</th>\r\n				<th>Provider Name</th>\r\n				<th>Type of Services</th>\r\n			</tr>\r\n		</thead>			 					\r\n		<tbody>\r\n			<tr>\r\n				<td>{id}</td>\r\n				<td>{member}</td>\r\n				<td>{dob}</td>\r\n				<td>{class}</td>\r\n				<td>{dateTime}</td>\r\n				<td>{hospital}</td>\r\n				<td>{jenis}</td>\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n	<br>\r\n	<i>***Take note that this is an unmanned email account and cannot be reply**</i>\r\n</body>\r\n</html>', 1, 5, '08:00:00'),
	(2, 2, 'firisco.satria@internationalsos.com', 'No Order {no_order}', 'No Order {no_order}\r\n{description}', 1, NULL, NULL),
	(3, 3, 'firisco.satria@internationalsos.com', 'No order {no_order} Approved', 'No order {no_order} Approved\r\n{description}', 1, NULL, NULL),
	(4, 4, 'firisco.satria@internationalsos.com', 'No order {no_order} Failed', 'No order {no_order} Failed', 1, NULL, NULL),
	(5, 5, 'firisco.satria@internationalsos.com', 'No order {no_order} Done', 'No order {no_order} Done', 1, NULL, NULL);
/*!40000 ALTER TABLE `email_config` ENABLE KEYS */;


-- Dumping structure for table isos_card.mitra_agent
CREATE TABLE IF NOT EXISTS `mitra_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terminalId` varchar(10) DEFAULT NULL,
  `idmitra` int(50) DEFAULT NULL,
  `msisdn` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.mitra_agent: ~0 rows (approximately)
/*!40000 ALTER TABLE `mitra_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `mitra_agent` ENABLE KEYS */;


-- Dumping structure for table isos_card.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table isos_card.profiles: ~3 rows (approximately)
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` (`user_id`, `lastname`, `firstname`) VALUES
	(1, 'Admin', 'Admin'),
	(2, 'User', 'ABA'),
	(3, 'User', 'SOS');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;


-- Dumping structure for table isos_card.profiles_fields
CREATE TABLE IF NOT EXISTS `profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table isos_card.profiles_fields: ~2 rows (approximately)
/*!40000 ALTER TABLE `profiles_fields` DISABLE KEYS */;
INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
	(1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
	(2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);
/*!40000 ALTER TABLE `profiles_fields` ENABLE KEYS */;


-- Dumping structure for table isos_card.rights
CREATE TABLE IF NOT EXISTS `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`),
  CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.rights: ~0 rows (approximately)
/*!40000 ALTER TABLE `rights` DISABLE KEYS */;
/*!40000 ALTER TABLE `rights` ENABLE KEYS */;


-- Dumping structure for table isos_card.transaction
CREATE TABLE IF NOT EXISTS `transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `trxId` varchar(50) NOT NULL,
  `terminalId` varchar(10) NOT NULL,
  `merchantId` varchar(50) NOT NULL,
  `msisdn` varchar(15) NOT NULL,
  `traceNo` varchar(10) NOT NULL,
  `noCard` varchar(20) NOT NULL,
  `namaNasabah` varchar(50) NOT NULL,
  `jenisTransaksi` varchar(20) NOT NULL,
  `statusTrx` int(11) NOT NULL,
  `col1` varchar(400) NOT NULL,
  `col2` varchar(400) NOT NULL,
  `col3` varchar(400) NOT NULL,
  `col4` varchar(400) NOT NULL,
  `col5` varchar(400) NOT NULL,
  `dateTransaction` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;

-- Dumping data for table isos_card.transaction: ~118 rows (approximately)
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` (`ID`, `trxId`, `terminalId`, `merchantId`, `msisdn`, `traceNo`, `noCard`, `namaNasabah`, `jenisTransaksi`, `statusTrx`, `col1`, `col2`, `col3`, `col4`, `col5`, `dateTransaction`) VALUES
	(1, 'UO000001000120160414101018 ', 'UO000001', 'ANILO0000000001', '628176804481', '000120', '0000000001-0', 'Employee A', '502', 1, '3', 'PT. Freeport McMoran Indonesia', '', '', '', '2016-04-14 10:25:18'),
	(2, 'UO000001000121160414101140 ', 'UO000001', 'ANILO0000000001', '628176804481', '000121', '0000000001-1', 'undefined', '501', 2, '0', 'undefined', '', '', '', '2016-04-14 10:11:40'),
	(3, 'UO000001000124160414101752 ', 'UO000001', 'ANILO0000000001', '628176804481', '000124', '0000000001-0', 'Employee A', '504', 1, '1', 'PT Freeport Indonesia', '', '', '', '2016-04-14 10:17:52'),
	(4, 'UO000001000125160414103934 ', 'UO000001', 'ANILO0000000001', '628176804481', '000125', '0000000001-0', 'Employee A', '502', 1, '4', 'PT Freeport Indonesia', '', '', '', '2016-04-14 10:39:34'),
	(5, 'UO000001000126160414104436 ', 'UO000001', 'ANILO0000000001', '628176804481', '000126', '0000000001-0', 'Employee A', '502', 1, '5', 'PT Freeport Indonesia', '', '', '', '2016-04-14 10:44:36'),
	(6, 'UO000001000127160414104521 ', 'UO000001', 'ANILO0000000001', '628176804481', '000127', '0000000001-0', 'Employee A', '503', 1, '1', 'PT Freeport Indonesia', '', '', '', '2016-04-14 10:45:21'),
	(7, 'UO000001000128160414104619 ', 'UO000001', 'ANILO0000000001', '628176804481', '000128', '0000000001-1', 'Employee A Spouse', '502', 1, '1', 'PT Freeport Indonesia', '', '', '', '2016-04-14 10:46:19'),
	(8, 'UO000001000129160414104654 ', 'UO000001', 'ANILO0000000001', '628176804481', '000129', '0000000001-1', 'Employee A Spouse', '503', 1, '1', 'PT Freeport Indonesia', '', '', '', '2016-04-14 10:46:54'),
	(9, 'UO000001000131160414104839 ', 'UO000001', 'ANILO0000000001', '628176804481', '000131', '0000000001-2', 'undefined', '504', 2, '0', 'undefined', '', '', '', '2016-04-14 10:48:39'),
	(10, 'UO000001000132160414111317 ', 'UO000001', 'ANILO0000000001', '628176804481', '000132', '0000000001-1', 'Employee A Spouse', '503', 1, '2', 'PT Freeport Indonesia', '', '', '', '2016-04-14 11:13:17'),
	(11, 'UO000001000133160414112126 ', 'UO000001', 'ANILO0000000001', '628176804481', '000133', '0000000001-1', 'Employee A Spouse', '503', 1, '3', 'PT Freeport Indonesia', '', '', '', '2016-04-14 11:21:26'),
	(12, 'UO000001000135160414113657 ', 'UO000001', 'ANILO0000000001', '628176804481', '000135', '0000000001-1', 'Employee A Spouse', '502', 1, '2', 'PT Freeport Indonesia', '', '', '', '2016-04-14 11:36:57'),
	(13, 'UO000001000136160414113727 ', 'UO000001', 'ANILO0000000001', '628176804481', '000136', '0000000001-1', 'Employee A Spouse', '503', 1, '4', 'PT Freeport Indonesia', '', '', '', '2016-04-14 11:37:27'),
	(14, 'UO000001000137160414140328 ', 'UO000001', 'ANILO0000000001', '628176804481', '000137', '0000000001-0', 'Employee A', '502', 1, '6', 'PT Freeport Indonesia', '', '', '', '2016-04-14 14:03:28'),
	(15, 'UO000001000138160414141623 ', 'UO000001', 'ANILO0000000001', '628176804481', '000138', '0000000001-0', 'Employee A', '504', 1, '2', 'PT Freeport Indonesia', '', '', '', '2016-04-14 14:16:23'),
	(16, 'UO000001000139160414141730 ', 'UO000001', 'ANILO0000000001', '628176804481', '000139', '0000000001-1', 'Employee A Spouse', '502', 1, '3', 'PT Freeport Indonesia', '', '', '', '2016-04-14 14:17:30'),
	(17, 'UO000001000140160414142229 ', 'UO000001', 'ANILO0000000001', '628176804481', '000140', '0000000001-0', 'Employee A', '502', 1, '7', 'PT Freeport Indonesia', '', '', '', '2016-04-14 14:22:29'),
	(18, 'UO000001000141160418100224 ', 'UO000001', 'ANILO0000000001', '628176804481', '000141', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', '', '', '', '2016-04-18 09:50:24'),
	(19, 'UO000001000142160418100427 ', 'UO000001', 'ANILO0000000001', '628176804481', '000142', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '2', 'PT Freeport Indonesia', '', '', '', '2016-04-18 09:51:27'),
	(20, 'UO000001000143160418100848 ', 'UO000001', 'ANILO0000000001', '628176804481', '000143', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '3', 'PT Freeport Indonesia', '', '', '', '2016-04-18 10:08:48'),
	(21, 'UO000001000144160418103108 ', 'UO000001', 'ANILO0000000001', '628176804481', '000144', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '4', 'PT Freeport Indonesia', '', '', '', '2016-04-18 10:10:08'),
	(22, 'UO000001000145160418103308 ', 'UO000001', 'ANILO0000000001', '628176804481', '000145', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '5', 'PT Freeport Indonesia', '', '', '', '2016-04-18 10:33:08'),
	(23, 'UO000001000146160418103442 ', 'UO000001', 'ANILO0000000001', '628176804481', '000146', '0000000001-1', 'Employee A Spouse', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', '', '', '', '2016-04-18 10:11:42'),
	(24, 'UO000001000148160418103721 ', 'UO000001', 'ANILO0000000001', '628176804481', '000148', '0000000001-1', 'Employee A Spouse', 'Rawat Jalan', 1, '2', 'PT Freeport Indonesia', '', '', '', '2016-04-18 10:37:21'),
	(25, 'UO000001000149160418112419 ', 'UO000001', 'ANILO0000000001', '628176804481', '000149', '0000000001-0', 'Employee A', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-18 11:24:19'),
	(26, 'UO000001000002160418144118 ', 'UO000001', 'ANILO0000000001', '628176804481', '000002', '0000000001-2', 'undefined', 'Emergency', 2, '0', 'undefined', 'RS Abdi Waluyo', 'undefined', 'undefined', '2016-04-18 14:41:18'),
	(27, 'UO000001000012160419120627 ', 'UO000001', 'ANILO0000000001', '628176804481', '000012', '0000000001-2', 'Employee A Child', 'Rawat Jalan', 2, '0', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-19 12:06:27'),
	(28, 'UO000001000013160419120650 ', 'UO000001', 'ANILO0000000001', '628176804481', '000013', '0000000001-2', 'Employee A Child', 'Rawat Jalan', 2, '0', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-19 12:06:50'),
	(29, 'UO000001000014160419120727 ', 'UO000001', 'ANILO0000000001', '628176804481', '000014', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-19 12:07:27'),
	(30, 'UO000001000016160419121618 ', 'UO000001', 'ANILO0000000001', '628176804481', '000016', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-19 12:16:18'),
	(31, 'UO000001000017160419144631 ', 'UO000001', 'ANILO0000000001', '628176804481', '000017', '0000000001-0', 'Employee A', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-19 14:46:30'),
	(32, 'UO000001000002160420171349 ', 'UO000001', 'ANILO0000000001', '628176804481', '000002', '0000000001-2', 'Employee A Child', 'Emergency', 2, '0', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-20 17:13:49'),
	(33, 'UO000001000003160420171519 ', 'UO000001', 'ANILO0000000001', '628176804481', '000003', '0000000001-2', 'Employee A Child', 'Emergency', 2, '0', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-20 17:15:19'),
	(34, '3C039679000027160420175323 ', '3C039679', '0016282212119736', '6282212119736', '000027', '0000000001-1', 'Employee A Spouse', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'Development SOS', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-20 17:53:23'),
	(35, '3C039679000029160420175622 ', '3C039679', '0016282212119736', '6282212119736', '000029', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '5', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-20 17:56:22'),
	(36, '3C039679000030160420175759 ', '3C039679', '0016282212119736', '6282212119736', '000030', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-20 17:57:58'),
	(37, '3C039679000031160420175838 ', '3C039679', '0016282212119736', '6282212119736', '000031', '0000000001-0', 'Employee A', 'Rawat Gigi', 1, '3', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-20 17:58:38'),
	(38, '3C039679000032160420175933 ', '3C039679', '0016282212119736', '6282212119736', '000032', '0000000001-1', 'Employee A Spouse', 'Rawat Gigi', 1, '2', 'PT Freeport Indonesia', 'Development SOS', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-20 17:59:33'),
	(39, '3C039679000033160420180011 ', '3C039679', '0016282212119736', '6282212119736', '000033', '0000000001-1', 'Employee A Spouse', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'Development SOS', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-20 18:00:11'),
	(40, '3C039679000034160421091115 ', '3C039679', '0016282212119736', '6282212119736', '000034', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 09:11:14'),
	(41, '3C039679000036160421091343 ', '3C039679', '0016282212119736', '6282212119736', '000036', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 09:13:41'),
	(42, '3C039679000037160421092801 ', '3C039679', '0016282212119736', '6282212119736', '000037', '0000000001-0', 'Employee A', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 09:28:00'),
	(43, '3C039679000038160421092955 ', '3C039679', '0016282212119736', '6282212119736', '000038', '0000000001-2', 'Employee A Child', 'Emergency', 2, '0', 'PT Freeport Indonesia', 'Development SOS', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-21 09:29:54'),
	(44, '3C039679000039160421094842 ', '3C039679', '0016282212119736', '6282212119736', '000039', '0000000001-0', 'Employee A', 'Emergency', 1, '2', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 09:48:42'),
	(45, '3C039679000041160421095155 ', '3C039679', '0016282212119736', '6282212119736', '000041', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '2', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 09:51:55'),
	(46, '3C039679000042160421120415 ', '3C039679', '0016282212119736', '6282212119736', '000042', '0000000001-2', 'Employee A Child', 'Emergency', 2, '0', 'PT Freeport Indonesia', 'Development SOS', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-21 12:04:14'),
	(47, '3C039679000043160421120444 ', '3C039679', '0016282212119736', '6282212119736', '000043', '0000000001-0', 'Employee A', 'Emergency', 1, '3', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 12:04:44'),
	(48, '3C039679000044160421121048 ', '3C039679', '0016282212119736', '6282212119736', '000044', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '3', 'PT Freeport Indonesia', 'Development SOS', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 12:10:48'),
	(49, '3C039679000045160421121847 ', '3C039679', '0016282212119736', '6282212119736', '000045', '0000000002-0', 'Employee B', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'Development SOS', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-21 12:18:47'),
	(50, '12345679000046160421135606 ', '12345679', '123456789012345', '6282212119740', '000046', '0000000002-0', 'Employee B', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-21 13:56:05'),
	(51, '12345679000047160421140225 ', '12345679', '123456789012345', '6282212119740', '000047', '0000000002-0', 'Employee B', 'Rawat Jalan', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-21 14:02:25'),
	(52, '12345679000049160421140742 ', '12345679', '123456789012345', '6282212119740', '000049', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '4', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 14:07:42'),
	(53, '12345679000051160421140849 ', '12345679', '123456789012345', '6282212119740', '000051', '0000000001-1', 'Employee A Spouse', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-21 14:08:49'),
	(54, '12345688000002160421141005 ', '12345688', '123456789012345', '6282212119771', '000002', '0000000001-1', 'Employee A Spouse', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-21 14:10:04'),
	(55, '12345688000004160421141056 ', '12345688', '123456789012345', '6282212119771', '000004', '0000000001-1', 'Employee A Spouse', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-21 14:10:55'),
	(56, '3C039679000002160421141151 ', '3C039679', '0016282212119736', '6282212119736', '000002', '0000000002-0', 'Employee B', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'Development SOS', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-21 14:11:50'),
	(57, '3C039679000003160421141347 ', '3C039679', '0016282212119736', '6282212119736', '000003', '0000000002-0', 'Employee B', 'Rawat Jalan', 1, '3', 'PT Freeport Indonesia', 'Development SOS', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-21 14:13:47'),
	(58, '3C039679000004160421141538 ', '3C039679', '001628221211973', '6282212119736', '000004', '0000000001-1', 'Employee A Spouse', 'Rawat Gigi', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-21 14:15:37'),
	(59, '12345688000001160421141640 ', '12345688', '123456789012345', '6282212119771', '000001', '0000000001-1', 'Employee A Spouse', 'Rawat Inap', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-21 14:16:40'),
	(60, '12345688000002160421150743 ', '12345688', '123456789012345', '6282212119771', '000002', '0000000002-0', 'Employee B', 'Rawat Gigi', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-21 15:07:43'),
	(61, '3C039679000001160421152515 ', '3C039679', '001628221211973', '6282212119736', '000001', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP', '2016-04-21 15:25:15'),
	(62, '3C039679000002160421153037 ', '3C039679', '001628221211973', '6282212119736', '000002', '0000000001-0', 'Employee A', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-21 15:30:37'),
	(63, '3C039679000003160421153309 ', '3C039679', '001628221211973', '6282212119736', '000003', '0000000001-2', 'Employee A Child', 'Rawat Gigi', 2, '0', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-21 15:33:09'),
	(64, '3C039679000004160421153431 ', '3C039679', '001628221211973', '6282212119736', '000004', '0000000002-0', 'Employee B', 'Rawat Gigi', 1, '3', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-21 15:34:31'),
	(65, '3C039679000006160421153731 ', '3C039679', '001628221211973', '6282212119736', '000006', '0000000002-0', 'Employee B', 'Rawat Gigi', 1, '4', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-21 15:37:31'),
	(66, '12345688000003160421160244 ', '12345688', '123456789012345', '6282212119771', '000003', '0000000001-0', 'Employee A', 'Rawat Gigi', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-21 16:02:44'),
	(67, '12345688000004160421171049 ', '12345688', '123456789012345', '6282212119771', '000004', '0000000001-1', 'Employee A Spouse', 'Rawat Inap', 1, '3', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-21 17:10:49'),
	(68, '12345688000005160421171212 ', '12345688', '123456789012345', '6282212119771', '000005', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '5', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-21 17:12:12'),
	(69, '3C039679000015160422142201 ', '3C039679', '001628221211973', '6282212119736', '000015', '0000003280-0', 'Employee B', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 12:12:00'),
	(70, '3C039679000016160422142243 ', '3C039679', '001628221211973', '6282212119736', '000016', '0000003280-0', 'Employee B', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 12:12:23'),
	(71, '3C039679000016160422142245', '3C039679', '001628221211973', '6282212119736', '000017', '0000003280-0', 'Employee B', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 12:12:41'),
	(72, '3C039679000016160422142247', '3C039679', '001628221211973', '6282212119736', '000018', '0000003280-0', 'Employee B', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 12:12:50'),
	(73, 'NaN160425121411 ', 'undefined', 'undefined', 'undefined', 'undefined', '0000003280-0', '', 'Rawat Jalan', 3, '0', '', 'undefined', '', '', '2016-04-25 12:14:11'),
	(74, '12345679000001160425122148 ', '12345679', '123456789012345', '6282212119740', '000001', '0000000001-0', 'Employee A', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 12:21:48'),
	(75, '12345679000002160425125449 ', '12345679', '123456789012345', '6282212119740', '000002', '0000000001-0', 'Employee A', 'Emergency', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 12:54:49'),
	(76, '12345679000003160425125541 ', '12345679', '123456789012345', '6282212119740', '000003', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 12:55:41'),
	(77, '12345679000005160425125632 ', '12345679', '123456789012345', '6282212119740', '000005', '0000000001-0', 'Employee A', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 12:56:32'),
	(78, '12345679000006160425130249 ', '12345679', '123456789012345', '6282212119740', '000006', '0000000001-0', 'Employee A', 'Emergency', 1, '3', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 13:02:49'),
	(79, '12345679000007160425135051 ', '12345679', '123456789012345', '6282212119740', '000007', '0000000001-0', 'Employee A', 'Emergency', 1, '4', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 13:50:51'),
	(80, '12345679000008160425142543 ', '12345679', '123456789012345', '6282212119740', '000008', '0000000001-0', 'Employee A', 'Emergency', 1, '5', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 14:25:43'),
	(81, '12345679000009160425143401 ', '12345679', '123456789012345', '6282212119740', '000009', '0000000001-0', 'Employee A', 'Emergency', 1, '6', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 14:34:01'),
	(82, '12345679000010160425144544 ', '12345679', '123456789012345', '6282212119740', '000010', '0000000001-0', 'Employee A', 'Emergency', 1, '7', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 14:45:44'),
	(83, '12345679000011160425144812 ', '12345679', '123456789012345', '6282212119740', '000011', '0000000001-2', 'Employee A Child', 'Rawat Jalan', 2, '0', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-25 14:48:12'),
	(84, '12345679000013160425144908 ', '12345679', '123456789012345', '6282212119740', '000013', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 14:49:08'),
	(85, '12345679000014160425144938 ', '12345679', '123456789012345', '6282212119740', '000014', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 14:49:38'),
	(86, '12345679000015160425145032 ', '12345679', '123456789012345', '6282212119740', '000015', '0000000001-0', 'Employee A', 'Rawat Gigi', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-25 14:50:32'),
	(87, '12345679000016160425145458 ', '12345679', '123456789012345', '6282212119740', '000016', '0000000001-1', 'Employee A Spouse', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-25 14:54:58'),
	(88, '12345679000017160425145518 ', '12345679', '123456789012345', '6282212119740', '000017', '0000000001-1', 'Employee A Spouse', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-25 14:55:18'),
	(89, '12345679000018160425145540 ', '12345679', '123456789012345', '6282212119740', '000018', '0000000001-1', 'Employee A Spouse', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-25 14:55:40'),
	(90, '12345679000019160425145605 ', '12345679', '123456789012345', '6282212119740', '000019', '0000000001-1', 'Employee A Spouse', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-25 14:56:05'),
	(91, '12345679000020160425145916 ', '12345679', '123456789012345', '6282212119740', '000020', '0000003280-0', 'Employee B', 'Emergency', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 14:59:16'),
	(92, '12345679000021160425145944 ', '12345679', '123456789012345', '6282212119740', '000021', '0000003280-0', 'Employee B', 'Rawat Jalan', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 14:59:44'),
	(93, '12345679000022160425150009 ', '12345679', '123456789012345', '6282212119740', '000022', '0000003280-0', 'Employee B', 'Rawat Inap', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 15:00:09'),
	(94, '12345679000023160425150042 ', '12345679', '123456789012345', '6282212119740', '000023', '0000003280-0', 'Employee B', 'Rawat Gigi', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 15:00:42'),
	(95, '12345679000024160425160152 ', '12345679', '123456789012345', '6282212119740', '000024', '0000003280-0', 'MUHAMMAD DZIKRA RAMADHAN SUTARYONO', 'Emergency', 1, '3', 'PT Freeport Indonesia', 'RS Abdi Waluyo Tjb', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-25 16:01:52'),
	(96, '61814544000026160426095819 ', '61814544', '001628221211973', '6282212119736', '000026', '9999900033-0', 'Tester Employee 33', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 09:58:19'),
	(97, '61814544000027160426095831 ', '61814544', '001628221211973', '6282212119736', '000027', '9999900033-0', 'Tester Employee 33', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 09:58:30'),
	(98, '61814544000028160426095840 ', '61814544', '001628221211973', '6282212119736', '000028', '9999900033-0', 'Tester Employee 33', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 09:58:40'),
	(99, '61814544000029160426095854 ', '61814544', '001628221211973', '6282212119736', '000029', '9999900033-0', 'Tester Employee 33', 'Rawat Jalan', 3, '0', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 09:58:54'),
	(100, '12345678000002160426101550 ', '12345678', '123456789012345', '6282212119747', '000002', '9999900033-0', 'Tester Employee 33', 'Emergency', 1, '2', 'PT Freeport Indonesia', 'Klinik SOS Medika', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 10:15:50'),
	(101, '12345678000004160426101654 ', '12345678', '123456789012345', '6282212119747', '000004', '9999900033-0', 'Tester Employee 33', 'Rawat Jalan', 1, '2', 'PT Freeport Indonesia', 'Klinik SOS Medika', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 10:16:54'),
	(102, '61814544000030160426104756 ', '61814544', '001628221211973', '6282212119736', '000030', '9999900033-0', 'Tester Employee 33', 'Emergency', 1, '3', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 10:47:56'),
	(103, '61814544000031160426105003 ', '61814544', '001628221211973', '6282212119736', '000031', '9999900033-0', 'Tester Employee 33', 'Rawat Inap', 1, '2', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 10:50:03'),
	(104, '61814544000033160426105324 ', '61814544', '001628221211973', '6282212119736', '000033', '9999900033-0', 'Tester Employee 33', 'Emergency', 1, '4', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 10:53:24'),
	(105, '61814544000037160426114631 ', '61814544', '001628221211973', '6282212119736', '000037', '9999900033-0', 'Tester Employee 33', 'Rawat Jalan', 1, '3', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '07 MEI 1976 ', 'Kelas 1 Standar', '2016-04-26 11:46:31'),
	(106, '61814544000038160426114717 ', '61814544', '001628221211973', '6282212119736', '000038', '0000000001-0', 'Employee A', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-26 11:47:17'),
	(107, '12345678000005160426135337 ', '12345678', '123456789012345', '6282212119747', '000005', '0000000001-0', 'Employee A', 'Rawat Gigi', 1, '2', 'PT Freeport Indonesia', 'Klinik SOS Medika', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-26 13:53:37'),
	(108, '12345688000006160426151942 ', '12345688', '123456789012345', '6282212119771', '000006', '0000000001-1', 'Employee A Spouse', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-26 15:19:42'),
	(109, '61814544000043160426153434 ', '61814544', '001628221211973', '6282212119736', '000043', '0000000001-1', 'Employee A Spouse', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-26 15:34:34'),
	(110, '61814544000044160426153505 ', '61814544', '001628221211973', '6282212119736', '000044', '0000000001-1', 'Employee A Spouse', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '01 JAN 1964 ', 'Kelas VIP Standar', '2016-04-26 15:35:05'),
	(111, '12345678000009160426153700 ', '12345678', '123456789012345', '6282212119747', '000009', '0000000002-0', 'Employee B', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'Klinik SOS Medika', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-26 15:37:00'),
	(112, '61814544000045160426162431 ', '61814544', '001628221211973', '6282212119736', '000045', '0000000001-0', 'Employee A', 'Emergency', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-26 16:24:30'),
	(113, '61814544000046160426162602 ', '61814544', '001628221211973', '6282212119736', '000046', '0000000001-0', 'Employee A', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-26 16:26:02'),
	(114, '61814544000047160426162703 ', '61814544', '001628221211973', '6282212119736', '000047', '0000000001-0', 'Employee A', 'Rawat Inap', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-26 16:27:03'),
	(115, '61814544000048160426162941 ', '61814544', '001628221211973', '6282212119736', '000048', '0000000001-0', 'Employee A', 'Rawat Gigi', 1, '3', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '25 FEB 1962 ', '1 Kelas Diatas VIP Std', '2016-04-26 16:29:41'),
	(116, '61814544000049160426163041 ', '61814544', '001628221211973', '6282212119736', '000049', '0000000002-0', 'Employee B', 'Rawat Gigi', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-26 16:30:41'),
	(117, '61814544000050160426163254 ', '61814544', '001628221211973', '6282212119736', '000050', '0000000001-2', 'Employee A Child', 'Rawat Jalan', 2, '0', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '29 DES 1996 ', 'Kelas 1 Standar', '2016-04-26 16:32:54'),
	(118, '61814544000051160426163502 ', '61814544', '001628221211973', '6282212119736', '000051', '0000000002-0', 'Employee B', 'Rawat Jalan', 1, '1', 'PT Freeport Indonesia', 'RS Abdi Waluyo', '23 MAR 1970 ', 'Kelas 1 Standar', '2016-04-26 16:35:02');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;


-- Dumping structure for table isos_card.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(3) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table isos_card.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `userId`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit_at`, `superuser`, `status`) VALUES
	(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'muhamad.alfan01@yahoo.com', '951ad7c22bdeb0236a524e874260aa0a', '2014-08-19 16:46:46', '2016-04-25 13:05:21', 1, 1),
	(2, NULL, 'aba', '08fc2a20dddeb462daaffae48eebb2f0', 'aba@aba.com', 'a0bdf7ab13065832f5e756053525d5a4', '2016-03-22 17:09:00', '2016-04-19 14:50:22', 0, 1),
	(3, NULL, 'sos', '7745f1a3dcfd3c2e941b1e8c9a026c03', 'sos@sos.com', '1156d5666a04f6dc0d383e3657607e64', '2016-03-31 18:16:04', '2016-04-20 17:51:53', 0, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
