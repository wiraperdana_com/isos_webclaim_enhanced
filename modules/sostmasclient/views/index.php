<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_1', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "1",
		"name": "status_data",
		"sortDir": "ASC",
		"persist": false,
		"type": "smallint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "CURRENT_TIMESTAMP",
		"name": "createdDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "createdBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "Active",
		"name": "client_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "text",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_email",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_telp",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_address",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_codeId",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_1",
	"name": "data_model_1",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_5', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": true,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_5",
	"name": "data_model_5",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_8', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "Active",
		"name": "client_status",
		"sortDir": "ASC",
		"persist": true,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_8",
	"name": "data_model_8",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_13', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "1",
		"name": "status_data",
		"sortDir": "ASC",
		"persist": false,
		"type": "smallint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0000-00-00 00:00:00",
		"name": "createdDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "createdBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "text",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "providerId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0",
		"name": "clientProviderId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_13",
	"name": "data_model_13",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_1",
	"autoLoad": false,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasclient\/call_method\/select\/table\/sos_tmas_client",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "clientId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_2",
	"storeId": "data_store_2",
	"name": "data_store_2"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_5",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasclient\/call_method\/select\/table\/tvbrowsereflocation",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "location_name",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_6",
	"storeId": "data_store_6",
	"name": "data_store_6",
	listeners:{
		load:function(){
			this.insert(0,{location_name:'All'});
		}
	}
});
Ext.create('Ext.data.Store', {
	"model": "data_model_8",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasclient\/call_method\/select\/table\/tvclientstatus",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "client_status",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_9",
	"storeId": "data_store_9",
	"name": "data_store_9",
	listeners:{
		load:function(){
			this.insert(0,{client_status:'All'});
		}
	}
});
Ext.create('Ext.data.Store', {
	"model": "data_model_13",
	"autoLoad": false,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasclient\/call_method\/select\/table\/tvsostmasclientprovider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "clientProviderId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_14",
	"storeId": "data_store_14",
	"name": "data_store_14"
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [{
		"key": "Active",
		"val": "Active"
	}, {
		"key": "Inactive",
		"val": "Inactive"
	}],
	"id": "data_store_24",
	"storeId": "data_store_24",
	"name": "data_store_24"
});
//store define - end
var workspace_sostmasclient = {
	"w2_space": {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Pilih Client",
				"modulecode": "tvbrowserefclient",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "sostmasclient_module_real_general_form_16",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"semar": true,
				"width": 640,
				"height": 492,
				"modal": true,
				"bodyStyle": "padding:5px;",
				"layout": {
					"type": "fit"
				},
				"closeAction": "destroy",
				"id": "sostmasclient_module_real_general_browseref_33",
				"name": "sostmasclient_module_real_general_browseref_33",
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Pilih Location",
				"modulecode": "tvbrowsereflocation",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "sostmasclient_module_real_general_form_12",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"semar": true,
				"width": 640,
				"height": 492,
				"modal": true,
				"bodyStyle": "padding:5px;",
				"layout": {
					"type": "fit"
				},
				"closeAction": "destroy",
				"id": "sostmasclient_module_real_general_browseref_32",
				"name": "sostmasclient_module_real_general_browseref_32",
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "north",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "south",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "east",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Pilih Provider",
				"modulecode": "tvbrowsereftmasprovider",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "sostmasclient_module_real_general_form_16",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"semar": true,
				"width": 640,
				"height": 492,
				"modal": true,
				"bodyStyle": "padding:5px;",
				"layout": {
					"type": "fit"
				},
				"closeAction": "destroy",
				"id": "sostmasclient_module_real_general_browseref_34",
				"name": "sostmasclient_module_real_general_browseref_34",
				"items": []
			}]
		}]
	}
}
var new_tabpanel = {
	id: 'newtab_sostmasclient_module',
	title: 'Master Client',
	iconCls: 'semar-group',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 3,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"margins": {
				"top": 4,
				"right": 4,
				"bottom": 4,
				"left": 4,
				"height": 8,
				"width": 8
			},
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Master Client",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasclient_module_real_general_tabpanel_3",
				"semar": true,
				"xtype": "tabpanel",
				"name": "sostmasclient_module_real_general_tabpanel_3",
				"defaults": {
					"layout": {
						"type": "vbox",
						"align": "stretch",
						"pack": "start"
					},
					"flex": 1
				},
				"layout": {
					"type": "card"
				},
				"items": [{
					"title": "Client List",
					"id": "sostmasclient_module_real_general_tabpanel_3_tab_1",
					"name": "sostmasclient_module_real_general_tabpanel_3_tab_1",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"loadMask": true,
						"noCache": false,
						"autoHeight": true,
						"columnLines": false,
						"invalidateScrollerOnRefresh": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "sostmasclient_module_real_general_grid_4",
						"semar": true,
						"xtype": "grid",
						"tbar": [
							[{
								"id": "sostmasclient_module_real_general_grid_4_btn_tambah",
								"name": "sostmasclient_module_real_general_grid_4_btn_tambah",
								"itemId": "sostmasclient_module_real_general_grid_4_btn_tambah",
								"text": "Tambah",
								"iconCls": "semar-add",
								"configs": {
									"toolbar": "top",
									"text": "Tambah",
									"iconCls": "semar-add",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "sostmasclient_module_real_general_grid_4_btn_hapus",
								"name": "sostmasclient_module_real_general_grid_4_btn_hapus",
								"itemId": "sostmasclient_module_real_general_grid_4_btn_hapus",
								"text": "Hapus",
								"iconCls": "semar-delete",
								"configs": {
									"toolbar": "top",
									"text": "Hapus",
									"iconCls": "semar-delete",
									"type": "delete",
									"value": "sos_tmas_client",
									"split": true,
									"active": true
								}
							}, "-"],
							[{
								"id": "sostmasclient_module_real_general_grid_4_btn_excel",
								"name": "sostmasclient_module_real_general_grid_4_btn_excel",
								"itemId": "sostmasclient_module_real_general_grid_4_btn_excel",
								"text": "Excel",
								"iconCls": "semar-excel",
								"configs": {
									"toolbar": "top",
									"text": "Excel",
									"iconCls": "semar-excel",
									"type": "excel",
									"value": "sos_tmas_client",
									"split": true,
									"active": true
								}
							}, "-"],
							[{
								"id": "sostmasclient_module_real_general_grid_4_btn_resetfilter",
								"name": "sostmasclient_module_real_general_grid_4_btn_resetfilter",
								"itemId": "sostmasclient_module_real_general_grid_4_btn_resetfilter",
								"text": "ResetFilter",
								"iconCls": "semar-undo",
								"configs": {
									"toolbar": "top",
									"text": "ResetFilter",
									"iconCls": "semar-undo",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}]
						],
						"store": "data_store_2",
						"dockedItems": [{
							"id": "sostmasclient_module_real_general_grid_4_pagingtoolbar",
							"hidden": false,
							"xtype": "pagingtoolbar",
							"store": "data_store_2",
							"dock": "bottom",
							"displayInfo": true
						}, {
							"id": "sostmasclient_module_real_general_grid_4_searchfield",
							"dock": "top",
							"hidden": false,
							"xtype": "semarwidgetsearchtoolbar",
							"grid_id": "sostmasclient_module_real_general_grid_4",
							"store": "data_store_2",
							"text": "Search"
						}],
						"columns": [{
							"text": "clientId",
							"dataIndex": "clientId",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Client Code",
							"dataIndex": "client_codeId",
							"width": 51,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Nama Client",
							"dataIndex": "client_name",
							"width": 165,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Address",
							"dataIndex": "client_address",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Telp",
							"dataIndex": "client_telp",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Email",
							"dataIndex": "client_email",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Lokasi",
							"dataIndex": "location_name",
							"width": 124,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Description",
							"dataIndex": "description",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Status",
							"dataIndex": "client_status",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "createdBy",
							"dataIndex": "createdBy",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "createdDate",
							"dataIndex": "createdDate",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "updatedBy",
							"dataIndex": "updatedBy",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "updatedDate",
							"dataIndex": "updatedDate",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "status_data",
							"dataIndex": "status_data",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}],
						"selType": "checkboxmodel",
						"selModel": {
							"mode": "MULTI",
							"selType": "checkboxmodel"
						},
						"name": "sostmasclient_module_real_general_grid_4",
						"layout": {
							"type": "fit"
						},
						"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
					}]
				}, {
					"title": "Client Detail",
					"id": "sostmasclient_module_real_general_tabpanel_3_tab_2",
					"name": "sostmasclient_module_real_general_tabpanel_3_tab_2",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"width": 410,
						"url": "sostmasclient\/call_method\/insertupdate\/table\/sos_tmas_client",
						"method": "POST",
						"height": 708,
						"autoScroll": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "sostmasclient_module_real_general_form_12",
						"semar": true,
						"xtype": "form",
						"tbar": [
							[{
								"id": "sostmasclient_module_real_general_form_12_btn_simpan",
								"name": "sostmasclient_module_real_general_form_12_btn_simpan",
								"itemId": "sostmasclient_module_real_general_form_12_btn_simpan",
								"text": "Simpan",
								"iconCls": "semar-save",
								"configs": {
									"toolbar": "top",
									"text": "Simpan",
									"iconCls": "semar-save",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "sostmasclient_module_real_general_form_12_btn_ubah",
								"name": "sostmasclient_module_real_general_form_12_btn_ubah",
								"itemId": "sostmasclient_module_real_general_form_12_btn_ubah",
								"text": "Ubah",
								"iconCls": "semar-edit",
								"configs": {
									"toolbar": "top",
									"text": "Ubah",
									"iconCls": "semar-edit",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "sostmasclient_module_real_general_form_12_btn_batal",
								"name": "sostmasclient_module_real_general_form_12_btn_batal",
								"itemId": "sostmasclient_module_real_general_form_12_btn_batal",
								"text": "Batal",
								"iconCls": "semar-undo",
								"configs": {
									"toolbar": "top",
									"text": "Batal",
									"iconCls": "semar-undo",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}]
						],
						"name": "sostmasclient_module_real_general_form_12",
						"defaults": {
							"xtype": "panel",
							"layout": "anchor",
							"anchor": "100%",
							"flex": 1,
							"margin": 5,
							"defaults": {
								"anchor": "100%",
								"flex": 1
							}
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_text_17",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_text_17",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Client Code",
								"name": "client_codeId",
								"allowBlank": false,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_text_17",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_text_18",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_text_18",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": true,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Nama Client",
								"name": "client_name",
								"allowBlank": false,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_text_18",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_textarea_19",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_textarea_19",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": true,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Alamat",
								"name": "client_address",
								"allowBlank": true,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_textarea_19",
								"semar": true,
								"xtype": "textareafield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_text_20",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_text_20",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Telp",
								"name": "client_telp",
								"allowBlank": true,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_text_20",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_text_21",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_text_21",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Email",
								"name": "client_email",
								"allowBlank": true,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_text_21",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_text_22",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_text_22",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": true,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Lokasi",
								"name": "location_name",
								"allowBlank": false,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_text_22",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_textarea_23",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_textarea_23",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": true,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Deskripsi",
								"name": "description",
								"allowBlank": true,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_textarea_23",
								"semar": true,
								"xtype": "textareafield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_combo_25",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_combo_25",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Status",
								"name": "client_status",
								"allowBlank": false,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_combo_25",
								"semar": true,
								"xtype": "combobox",
								"store": "data_store_24",
								"displayField": "key",
								"valueField": "val",
								"queryMode": "local",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "sostmasclient_module_real_general_form_12_blk_form_hidden_26",
							"semar": true,
							"xtype": "panel",
							"name": "sostmasclient_module_real_general_form_12_blk_form_hidden_26",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": true,
								"labelWidth": 100,
								"labelAlign": "left",
								"name": "clientId",
								"allowBlank": true,
								"id": "sostmasclient_module_real_general_form_12_blk_i_form_hidden_26",
								"semar": true,
								"xtype": "hiddenfield",
								"items": []
							}]
						}]
					}]
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"width": 991,
			"region": "west",
			"height": 795,
			"autoScroll": true,
			"margins": {
				"top": 4,
				"right": 4,
				"bottom": 4,
				"left": 4,
				"height": 8,
				"width": 8
			},
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasclient_module_real_general_grid_7",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_6",
				"dockedItems": [{
					"id": "sostmasclient_module_real_general_grid_7_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_6",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "sostmasclient_module_real_general_grid_7_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "sostmasclient_module_real_general_grid_7",
					"store": "data_store_6",
					"text": "Search"
				}],
				"columns": [{
					"text": "Location",
					"dataIndex": "location_name",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "sostmasclient_module_real_general_grid_7",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasclient_module_real_general_grid_11",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_9",
				"dockedItems": [{
					"id": "sostmasclient_module_real_general_grid_11_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_9",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "sostmasclient_module_real_general_grid_11_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "sostmasclient_module_real_general_grid_11",
					"store": "data_store_9",
					"text": "Search"
				}],
				"columns": [{
					"text": "Status",
					"dataIndex": "client_status",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "sostmasclient_module_real_general_grid_11",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 3,
			"title": "",
			"region": "east",
			"autoScroll": true,
			"margins": {
				"top": 4,
				"right": 4,
				"bottom": 4,
				"left": 4,
				"height": 8,
				"width": 8
			},
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Client Provider List",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasclient_module_real_general_grid_15",
				"semar": true,
				"xtype": "grid",
				"tbar": [
					[{
						"id": "sostmasclient_module_real_general_grid_15_btn_tambah",
						"name": "sostmasclient_module_real_general_grid_15_btn_tambah",
						"itemId": "sostmasclient_module_real_general_grid_15_btn_tambah",
						"text": "Tambah",
						"iconCls": "semar-add",
						"configs": {
							"toolbar": "top",
							"text": "Tambah",
							"iconCls": "semar-add",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sostmasclient_module_real_general_grid_15_btn_hapus",
						"name": "sostmasclient_module_real_general_grid_15_btn_hapus",
						"itemId": "sostmasclient_module_real_general_grid_15_btn_hapus",
						"text": "Hapus",
						"iconCls": "semar-delete",
						"configs": {
							"toolbar": "top",
							"text": "Hapus",
							"iconCls": "semar-delete",
							"type": "delete",
							"value": "sos_tmas_client_provider",
							"split": true,
							"active": true
						}
					}, "-"],
					[{
						"id": "sostmasclient_module_real_general_grid_15_btn_excel",
						"name": "sostmasclient_module_real_general_grid_15_btn_excel",
						"itemId": "sostmasclient_module_real_general_grid_15_btn_excel",
						"text": "Excel",
						"iconCls": "semar-excel",
						"configs": {
							"toolbar": "top",
							"text": "Excel",
							"iconCls": "semar-excel",
							"type": "excel",
							"value": "tvsostmasclientprovider",
							"active": true
						}
					}]
				],
				"store": "data_store_14",
				"dockedItems": [{
					"id": "sostmasclient_module_real_general_grid_15_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_14",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "sostmasclient_module_real_general_grid_15_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "sostmasclient_module_real_general_grid_15",
					"store": "data_store_14",
					"text": "Search"
				}],
				"columns": [{
					"text": "clientProviderId",
					"dataIndex": "clientProviderId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "clientId",
					"dataIndex": "clientId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Nama Client",
					"dataIndex": "client_name",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "providerId",
					"dataIndex": "providerId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Nama Provider",
					"dataIndex": "provider_name",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Lokasi",
					"dataIndex": "location_name",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Deskripsi",
					"dataIndex": "description",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "createdBy",
					"dataIndex": "createdBy",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "createdDate",
					"dataIndex": "createdDate",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "updatedBy",
					"dataIndex": "updatedBy",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "updatedDate",
					"dataIndex": "updatedDate",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "status_data",
					"dataIndex": "status_data",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "checkboxmodel",
				"selModel": {
					"mode": "MULTI",
					"selType": "checkboxmodel"
				},
				"name": "sostmasclient_module_real_general_grid_15",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Client Provider Detail",
				"width": 412,
				"url": "sostmasclient\/call_method\/insertupdate\/table\/sos_tmas_client_provider",
				"method": "POST",
				"height": 760,
				"autoScroll": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasclient_module_real_general_form_16",
				"semar": true,
				"xtype": "form",
				"tbar": [
					[{
						"id": "sostmasclient_module_real_general_form_16_btn_simpan",
						"name": "sostmasclient_module_real_general_form_16_btn_simpan",
						"itemId": "sostmasclient_module_real_general_form_16_btn_simpan",
						"text": "Simpan",
						"iconCls": "semar-save",
						"configs": {
							"toolbar": "top",
							"text": "Simpan",
							"iconCls": "semar-save",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sostmasclient_module_real_general_form_16_btn_ubah",
						"name": "sostmasclient_module_real_general_form_16_btn_ubah",
						"itemId": "sostmasclient_module_real_general_form_16_btn_ubah",
						"text": "Ubah",
						"iconCls": "semar-edit",
						"configs": {
							"toolbar": "top",
							"text": "Ubah",
							"iconCls": "semar-edit",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sostmasclient_module_real_general_form_16_btn_batal",
						"name": "sostmasclient_module_real_general_form_16_btn_batal",
						"itemId": "sostmasclient_module_real_general_form_16_btn_batal",
						"text": "Batal",
						"iconCls": "semar-undo",
						"configs": {
							"toolbar": "top",
							"text": "Batal",
							"iconCls": "semar-undo",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}]
				],
				"name": "sostmasclient_module_real_general_form_16",
				"defaults": {
					"xtype": "panel",
					"layout": "anchor",
					"anchor": "100%",
					"flex": 1,
					"margin": 5,
					"defaults": {
						"anchor": "100%",
						"flex": 1
					}
				},
				"layout": {
					"type": "anchor"
				},
				"items": [{
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sostmasclient_module_real_general_form_16_blk_form_text_28",
					"semar": true,
					"xtype": "panel",
					"name": "sostmasclient_module_real_general_form_16_blk_form_text_28",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Provider",
						"name": "provider_name",
						"allowBlank": false,
						"id": "sostmasclient_module_real_general_form_16_blk_i_form_text_28",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sostmasclient_module_real_general_form_16_blk_form_textarea_29",
					"semar": true,
					"xtype": "panel",
					"name": "sostmasclient_module_real_general_form_16_blk_form_textarea_29",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Deskripsi",
						"name": "description",
						"allowBlank": true,
						"id": "sostmasclient_module_real_general_form_16_blk_i_form_textarea_29",
						"semar": true,
						"xtype": "textareafield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sostmasclient_module_real_general_form_16_blk_form_hidden_37",
					"semar": true,
					"xtype": "panel",
					"name": "sostmasclient_module_real_general_form_16_blk_form_hidden_37",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "clientProviderId",
						"allowBlank": true,
						"id": "sostmasclient_module_real_general_form_16_blk_i_form_hidden_37",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sostmasclient_module_real_general_form_16_blk_form_hidden_39",
					"semar": true,
					"xtype": "panel",
					"name": "sostmasclient_module_real_general_form_16_blk_form_hidden_39",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "providerId",
						"allowBlank": true,
						"id": "sostmasclient_module_real_general_form_16_blk_i_form_hidden_39",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sostmasclient_module_real_general_form_16_blk_form_hidden_40",
					"semar": true,
					"xtype": "panel",
					"name": "sostmasclient_module_real_general_form_16_blk_form_hidden_40",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "clientId",
						"allowBlank": true,
						"id": "sostmasclient_module_real_general_form_16_blk_i_form_hidden_40",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}]
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_sostmasclient, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_sostmasclient, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('sostmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('sostmasclient_module_real_general_grid_4');
					if (new String('clientId=clientId').match('=')) {
						var hasAfterCheckComp = new String('clientId=clientId').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						Ext.getCmp('sostmasclient_module_real_general_grid_15').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_15').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_12_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasclient_module_real_general_form_12'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_tabpanel_3').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_12').getForm().loadRecord((Ext.getCmp('sostmasclient_module_real_general_grid_4').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('sostmasclient_module_real_general_grid_4').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_4_itemdblclick = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_4')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_4');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemdblclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_4', 'itemdblclick');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_itemdblclick);
						} else {
							tempEl.on('itemdblclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_4', 'itemdblclick');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_itemdblclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasclient_module_real_general_form_12'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_12_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_12').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_tabpanel_3').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_4_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_tambah')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_4_btn_tambah', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_btn_tambah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_4_btn_tambah', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_btn_tambah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('sostmasclient_module_real_general_grid_4').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('clientId'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'sostmasclient/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'clientId',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_4_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_hapus')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_4_btn_hapus', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_btn_hapus_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_4_btn_hapus', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_btn_hapus_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('sostmasclient', //nama module
								Ext.getCmp('sostmasclient_module_real_general_grid_4').title, //title
								'sostmasclient_module_real_general_grid_4', //id grid
								'sos_tmas_client', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_4_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_excel')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_4_btn_excel', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_btn_excel_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_4_btn_excel', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_btn_excel_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_resetfilter', 'click', {
				key: 'removeParam',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().proxy.extraParams = {
						id_open: '1',
						semar: 'true'
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_resetfilter', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_resetfilter', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_11').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_4_btn_resetfilter', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_7').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_4_btn_resetfilter_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_resetfilter')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_4_btn_resetfilter');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_4_btn_resetfilter', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_btn_resetfilter_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_4_btn_resetfilter', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_4_btn_resetfilter_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_12_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_12').on({
						beforeaction: function() {
							Ext.getCmp('sostmasclient_module_real_general_form_12').body.mask('Loading...');
						}
					});
					Ext.getCmp('sostmasclient_module_real_general_form_12').getForm().submit({
						url: Ext.getCmp('sostmasclient_module_real_general_form_12').url,
						method: Ext.getCmp('sostmasclient_module_real_general_form_12').method,
						clientValidation: true,
						success: function(form, action) {
							Ext.getCmp('sostmasclient_module_real_general_form_12').body.unmask();
							Ext.Msg.alert('Success', action.result.msg);
							Ext.getCmp('sostmasclient_module_real_general_form_12').getForm().setValues(action.result.data);

							//more events - start
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
							//more events - end
						},
						failure: function(form, action) {
							Ext.getCmp('sostmasclient_module_real_general_form_12').body.unmask();
							switch (action.failureType) {
							case Ext.form.action.Action.CLIENT_INVALID:
								Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
								break;
							case Ext.form.action.Action.CONNECT_FAILURE:
								Ext.Msg.alert('Failure', 'Ajax communication failed');
								break;
							case Ext.form.action.Action.SERVER_INVALID:
								Ext.Msg.alert('Failure', action.result.msg);
							}
						}
					});
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_12_btn_simpan', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_tabpanel_3').setActiveTab(JSON.parse(0));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_12_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_form_12_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_12_btn_simpan')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_12_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_12_btn_simpan', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_btn_simpan_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_12_btn_simpan', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_btn_simpan_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_12_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_12_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_12_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasclient_module_real_general_form_12'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_form_12_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_12_btn_ubah')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_12_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_12_btn_ubah', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_btn_ubah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_12_btn_ubah', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_btn_ubah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_12_btn_batal', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_tabpanel_3').setActiveTab(JSON.parse(0));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_12_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_12_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_12_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_12').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_form_12_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_12_btn_batal')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_12_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_12_btn_batal', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_btn_batal_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_12_btn_batal', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_btn_batal_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_12_blk_i_form_text_22', 'click', {
				key: 'browserefShow',
				fn: function(callbackIndex) {
					doTestVisualBrowseref('sostmasclient_module_real_general_browseref_32', JSON.parse(true), callbackIndex);
				}
			});
			var intervale_sostmasclient_module_real_general_form_12_blk_i_form_text_22_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_text_22')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_text_22');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_12_blk_i_form_text_22', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_blk_i_form_text_22_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_12_blk_i_form_text_22', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_blk_i_form_text_22_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26', 'change', {
				key: 'reverseValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40').ismessagebox == true) {
						if (String(Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26').getValue()).length > 0) {
							SemarBOXPanelDialog(Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40'), {
								msg: Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26').getValue()
							});
						};
					} else {
						if (Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40').getValue() != Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26').getValue()) {
							Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40').setValue(Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26').getValue());
						};
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_form_12_blk_i_form_hidden_26_change = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('change' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26', 'change');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_blk_i_form_hidden_26_change);
						} else {
							tempEl.on('change', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26', 'change');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_12_blk_i_form_hidden_26_change);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_7', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('sostmasclient_module_real_general_grid_7');
					if (new String('location_name=location_name').match('=')) {
						var hasAfterCheckComp = new String('location_name=location_name').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().proxy.extraParams = {id_open:1,semar:true};
							Ext.getCmp('sostmasclient_module_real_general_grid_15').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
							Ext.getCmp('sostmasclient_module_real_general_grid_15').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
						
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_7', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().load();
					Ext.getCmp('sostmasclient_module_real_general_grid_15').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_7_itemclick = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_7')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_7');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_7', 'itemclick');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_7_itemclick);
						} else {
							tempEl.on('itemclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_7', 'itemclick');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_7_itemclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_11', 'click', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('sostmasclient_module_real_general_grid_11');
					if (new String('client_status=client_status').match('=')) {
						var hasAfterCheckComp = new String('client_status=client_status').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
						
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_11', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_11_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_11')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_11');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_11', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_11_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_11', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_11_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_15', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasclient_module_real_general_form_16'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_15', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_16_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_15', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_16').getForm().loadRecord((Ext.getCmp('sostmasclient_module_real_general_grid_15').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('sostmasclient_module_real_general_grid_15').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_15_itemdblclick = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_15')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_15');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemdblclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_15', 'itemdblclick');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_15_itemdblclick);
						} else {
							tempEl.on('itemdblclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_15', 'itemdblclick');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_15_itemdblclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_15_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasclient_module_real_general_form_16'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_15_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_16_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_grid_15_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_16').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_15_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_15_btn_tambah')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_15_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_15_btn_tambah', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_15_btn_tambah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_15_btn_tambah', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_15_btn_tambah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_15_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('sostmasclient_module_real_general_grid_15').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('clientProviderId'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('sostmasclient_module_real_general_grid_15_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'sostmasclient/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'clientProviderId',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('sostmasclient_module_real_general_grid_15').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_15_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_15_btn_hapus')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_15_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_15_btn_hapus', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_15_btn_hapus_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_15_btn_hapus', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_15_btn_hapus_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_grid_15_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('sostmasclient_module_real_general_grid_15_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('sostmasclient', //nama module
								Ext.getCmp('sostmasclient_module_real_general_grid_15').title, //title
								'sostmasclient_module_real_general_grid_15', //id grid
								'clientId', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_sostmasclient_module_real_general_grid_15_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_grid_15_btn_excel')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_grid_15_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_grid_15_btn_excel', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_15_btn_excel_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_grid_15_btn_excel', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_grid_15_btn_excel_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_16_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_16').on({
						beforeaction: function() {
							Ext.getCmp('sostmasclient_module_real_general_form_16').body.mask('Loading...');
						}
					});
					Ext.getCmp('sostmasclient_module_real_general_form_16').getForm().submit({
						url: Ext.getCmp('sostmasclient_module_real_general_form_16').url,
						method: Ext.getCmp('sostmasclient_module_real_general_form_16').method,
						clientValidation: true,
						success: function(form, action) {
							Ext.getCmp('sostmasclient_module_real_general_form_16').body.unmask();
							Ext.Msg.alert('Success', action.result.msg);
							Ext.getCmp('sostmasclient_module_real_general_form_16').getForm().setValues(action.result.data);

							//more events - start
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
							//more events - end
						},
						failure: function(form, action) {
							Ext.getCmp('sostmasclient_module_real_general_form_16').body.unmask();
							switch (action.failureType) {
							case Ext.form.action.Action.CLIENT_INVALID:
								Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
								break;
							case Ext.form.action.Action.CONNECT_FAILURE:
								Ext.Msg.alert('Failure', 'Ajax communication failed');
								break;
							case Ext.form.action.Action.SERVER_INVALID:
								Ext.Msg.alert('Failure', action.result.msg);
							}
						}
					});
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_16_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_grid_15').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_form_16_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_16_btn_simpan')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_16_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_16_btn_simpan', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_btn_simpan_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_16_btn_simpan', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_btn_simpan_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_16_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_16_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_16_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasclient_module_real_general_form_16'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_form_16_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_16_btn_ubah')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_16_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_16_btn_ubah', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_btn_ubah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_16_btn_ubah', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_btn_ubah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_16_btn_batal', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasclient_module_real_general_form_16'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_16_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_16_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasclient_module_real_general_form_16_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasclient_module_real_general_form_16').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_form_16_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_16_btn_batal')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_16_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_16_btn_batal', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_btn_batal_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_16_btn_batal', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_btn_batal_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_16_blk_i_form_text_28', 'click', {
				key: 'browserefShow',
				fn: function(callbackIndex) {
					doTestVisualBrowseref('sostmasclient_module_real_general_browseref_34', JSON.parse(true), callbackIndex);
				}
			});
			var intervale_sostmasclient_module_real_general_form_16_blk_i_form_text_28_click = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_text_28')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_text_28');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_16_blk_i_form_text_28', 'click');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_blk_i_form_text_28_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_16_blk_i_form_text_28', 'click');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_blk_i_form_text_28_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40', 'change', {
				key: 'getValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40').getValue() != Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26').getValue()) {
						Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40').setValue(Ext.getCmp('sostmasclient_module_real_general_form_12_blk_i_form_hidden_26').getValue());
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasclient_module_real_general_form_16_blk_i_form_hidden_40_change = setInterval(function() {
				if (Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40')) {
					var tempEl = Ext.getCmp('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('change' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40', 'change');
								};
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_blk_i_form_hidden_40_change);
						} else {
							tempEl.on('change', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostmasclient_module_real_general_form_16_blk_i_form_hidden_40', 'change');
							});
							clearInterval(intervale_sostmasclient_module_real_general_form_16_blk_i_form_hidden_40_change);
						};
					};
				};
			}, 100);
		}
	}
}

<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>