<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller digenerate oleh eXML Builder.
 * Controller untuk module dashboardsosv1.
 *
 */
include "assets/charting/my_include/FusionCharts.php";
class dashboardsosv1 extends CI_Controller
{

	/**
	 * __construct Controller untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_dashboardsosv1'));
	}


	/**
	 * widget_dashboard Controller untuk module $modulename.
	 */
	public function widget_dashboard()
	{
		if($this->input->post('id_open')){
			$data['jsscript'] = TRUE;
			$this->load->view('dashboard',$data);			
		}else{
			$this->load->view('dashboard'); 
		};
	}


	/**
	 * call_method Controller untuk module $modulename.
	 */
	public function call_method($querytype, $typex, $valuex)
	{
		echo json_encode( $this->model_dashboardsosv1->call_method( $typex, $querytype, $valuex ) );
	}
	
	public function chart()
    { 
        $this->w = $this->input->get('w'); 
		
		$Parray = $this->model_dashboardsosv1->dashboardgridchart();
		
        $strXML1  = "<chart legendPostion=''  xAxisName='Jenis Claim' yAxisName='Total' showValues='0' formatNumberScale='1' >";
		
		$strXML1 .= "<categories>"; 
		foreach ($Parray as $k => $val) {
          $strXML1 .= " <category label='".$val."' />"; 
 		}
		$strXML1 .= " </categories>"; 
		
		$strXML1 .= "<dataset seriesName='Week' color='e44a00' showValues='1'>";
		foreach ($Parray as $k => $val) {
 		   $service = $this->model_dashboardsosv1->chartservice_type($k, 'w');
		   $strXML1 .= " <set value='" . $service . "' />";
 		 
		 }
	    $strXML1 .= "</dataset>";
		
		$strXML1 .= "<dataset seriesName='Month' color='6baa01' showValues='1'>";
		foreach ($Parray as $k => $val) {
 		   $service = $this->model_dashboardsosv1->chartservice_type($k, 'm');
		   $strXML1 .= " <set value='" . $service . "' />";
 		 
		 }
	    $strXML1 .= "</dataset>";
		
		$strXML1 .= "<dataset seriesName='Year' color='33bdda' showValues='1'>";
		foreach ($Parray as $k => $val) {
 		   $service = $this->model_dashboardsosv1->chartservice_type($k, 'w');
		   $strXML1 .= " <set value='" . $service . "' />";
 		 
		 }
	    $strXML1 .= "</dataset>";	 
		
        $strXML1 .= "</chart>";
		  
        echo renderChartHTML(base_url("/assets/charting/Charts/MSColumn3D.swf"), "", "$strXML1", "", $this->w."%", '220', false);
 	}
	
	function ext_get_panel1($params=0, $params2=0){
		$this->load->helper('f_grid');
		$data['clientID'] = $params;
		$data['bln'] = $params2;
	    $data['result'] = $this->model_dashboardsosv1->panel1_AllData($params);
		$this->load->view('panel1_html', $data);	
 	}
	
	function ext_get_panel2($params=0, $params2=0){ 
		$this->load->helper('f_grid');
		$data['providerId'] = $params;
		$data['bln'] = $params2;
	    $data['result'] = $this->model_dashboardsosv1->panel2_AllData($params);
		$this->load->view('panel2_html', $data);	
 	} 
	
	function ext_get_panel3($params=0, $params2=0){
		$this->load->helper('f_grid');
		$data['clientID'] = $params;
		$data['bln'] = $params2;
	    $data['result'] = $this->model_dashboardsosv1->panel3_AllData($params);
		$this->load->view('panel3_html', $data);	
 	}
	
	function ext_get_panel4($params=0, $params2=0){ 
		$this->load->helper('f_grid');
		$data['providerId'] = $params;
		$data['bln'] = $params2;
	    $data['result'] = $this->model_dashboardsosv1->panel4_AllData($params);
		$this->load->view('panel4_html', $data);	
 	}
	
	function get_client(){
 	   $view = $this->db->from('sos_tmas_client')->where('status_data', 1)->get()->result_array();
	   $data=array();
	   
	   foreach($view as $key =>$value){
 		  $data[$key] = $value;   
	   }	  
	  echo json_encode(array(  "success" => true, "model" => $data));	
	}
	
	function get_provider(){
 	   $view = $this->db->from('sos_tmas_provider')->where('status_data', 1)->get()->result_array();
	   $data=array();
	   
	   foreach($view as $key =>$value){
 		  $data[$key] = $value;   
	   }	  
	  echo json_encode(array(  "success" => true, "model" => $data));	
	}
	
	function get_periode(){
 	   
	   $view = $this->db->select(
	   				    'MONTH(date_claim) as bln, MONTHNAME(date_claim) as bulan'
					   )->from('sos_ttrans_claim')
					   ->where('status_data', 1)
					   ->group_by('MONTHNAME(date_claim)')
					   ->order_by('date_claim','desc') 
					   ->get()->result_array();
	   $data=array();
	   //echo $this->db->last_query();
	   foreach($view as $key =>$value){
 		  $data[$key] = $value;   
	   }	  
	  echo json_encode(array(  "success" => true, "model" => $data));	
	}
	
	

}
