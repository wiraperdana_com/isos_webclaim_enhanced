
Ext.define('ClientModel', {
    extend: 'Ext.data.Model',
    fields: [
        'clientId', 'client_name'
    ]
});

var ComboClientM = Ext.create('Ext.data.Store', {
    model: 'ClientModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardsosv1/get_client/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
});


Ext.define('PeriodeModel', {
    extend: 'Ext.data.Model',
    fields: [
        'bln', 'bulan'
    ]
});

var ComboPeriodeM = Ext.create('Ext.data.Store', {
    model: 'PeriodeModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardsosv1/get_periode/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
});


var Grid_T_panel3 = Ext.create('Ext.Component', {
         html: '<iframe src= "<?php echo base_url();?>dashboardsosv1/ext_get_panel3/" width="100%" height="87%"></iframe>',
       
});

var Form_SearchBy = new Ext.create('Ext.form.Panel', {
    id: 'Form_SearchBy', 
    frame: true,
    bodyStyle: 'padding: 5px 5px 0 0;', 
    fieldDefaults: {
        labelAlign: 'left',
        msgTarget: 'side',
    }, 
    layout :'hbox',
    items: [
    {
      allowBlank: true,
      width: 300,
      xtype: 'combobox',
      valueField: 'clientId',
      displayField: 'client_name',
      triggerAction: 'all',
      id: 'ClientFrmData',
      store: ComboClientM,
      mode: 'local',
      name: 'ClientFrmData',
      fieldLabel: 'Client Name',
      lazyRender: true, 
      listeners: {
          'focus': {
              fn: function(comboField) {
                  comboField.doQuery(comboField.allQuery, true);
                  comboField.expand();
              },
              scope: this
          },
          select: function (combo) {
              Grid_T_panel3.update('<iframe src="<?php echo base_url();?>dashboardsosv1/ext_get_panel3/' + combo.getValue() + '"  width="100%" height="100%"></iframe>');
          }
      }
    },
    {
      allowBlank: true,
      width: 300,
      xtype: 'combobox',
      valueField: 'bln',
      displayField: 'bulan',
      triggerAction: 'all',
      id: 'PeriodeFrmData',
      store: ComboPeriodeM,
      mode: 'local',
      name: 'PeriodeFrmData',
      fieldLabel: 'Period',
      labelStyle :'padding-left:15px;',
      lazyRender: true, 
      listeners: {
          'focus': {
              fn: function(comboField) {
                  comboField.doQuery(comboField.allQuery, true);
                  comboField.expand();
              },
              scope: this
          },
          select: function (combo) {
              Grid_T_panel3.update('<iframe src="<?php echo base_url();?>dashboardsosv1/ext_get_panel3/' + Ext.getCmp('ClientFrmData').getValue() + '/' + combo.getValue() +'"  width="100%" height="87%"></iframe>');
          }
      }
    } 
    ]
 });
 
 
Ext.define('ProviderDsbModel', {
    extend: 'Ext.data.Model',
    fields: [
        'providerId', 'provider_name'
    ]
});

var ComboProviderM = Ext.create('Ext.data.Store', {
    model: 'ProviderDsbModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardsosv1/get_provider/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
}); 

Ext.define('PeriodePRModel', {
    extend: 'Ext.data.Model',
    fields: [
        'bln', 'bulan'
    ]
});

var ComboPeriodePRM = Ext.create('Ext.data.Store', {
    model: 'PeriodePRModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardsosv1/get_periodeprovider/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
});

var Grid_T_panel4 = Ext.create('Ext.Component', {
         html: '<iframe src= "<?php echo base_url();?>dashboardsosv1/ext_get_panel4/" width="100%" height="87%"></iframe>',
       
});

var Form_SearchBy2 = new Ext.create('Ext.form.Panel', {
    id: 'Form_SearchBy2', 
    frame: true,
    bodyStyle: 'padding: 5px 5px 0 0;', 
    fieldDefaults: {
        labelAlign: 'left',
        msgTarget: 'side',
    }, 
    layout :'hbox',
    items: [
    {
      allowBlank: true,
      width: 300,
      xtype: 'combobox',
      valueField: 'providerId',
      displayField: 'provider_name',
      triggerAction: 'all',
      id: 'ProviderFrmData',
      store: ComboProviderM,
      mode: 'local',
      name: 'ProviderFrmData',
      fieldLabel: 'Provider Name',
      lazyRender: true, 
      listeners: {
          'focus': {
              fn: function(comboField) {
                  comboField.doQuery(comboField.allQuery, true);
                  comboField.expand();
              },
              scope: this
          },
          select: function (combo) {
              Grid_T_panel4.update('<iframe src="<?php echo base_url();?>dashboardsosv1/ext_get_panel4/' + combo.getValue() + '"  width="100%" height="100%"></iframe>');
          }
      }
    },
    {
      allowBlank: true,
      width: 300,
      xtype: 'combobox',
      valueField: 'bln',
      displayField: 'bulan',
      triggerAction: 'all',
      id: 'PeriodePRFrmData',
      store: ComboPeriodePRM,
      mode: 'local',
      name: 'PeriodePRFrmData',
      fieldLabel: 'Period',
      labelStyle :'padding-left:15px;',
      lazyRender: true, 
      listeners: {
          'focus': {
              fn: function(comboField) {
                  comboField.doQuery(comboField.allQuery, true);
                  comboField.expand();
              },
              scope: this
          },
          select: function (combo) {
              Grid_T_panel4.update('<iframe src="<?php echo base_url();?>dashboardsosv1/ext_get_panel4/' + Ext.getCmp('ClientFrmData').getValue() + '/' + combo.getValue() +'"  width="100%" height="87%"></iframe>');
          }
      }
    } 
    ]
 });
