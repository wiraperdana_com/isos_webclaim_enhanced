<style>
 
.table { 
  font-size: 11px;
  font-family:Verdana, Geneva, sans-serif;
  width: 100%;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
  display: table;
  line-height: 20px; 
  color: #3b3b3b;
  -webkit-font-smoothing: antialiased;
  font-smoothing: antialiased; 
  border-collapse: collapse;
}
@media screen and (max-width: 580px) {
  .table {
    display: block;
  }
}

 
.row {
  display: table-row;
  background: #f6f6f6;   
}
.row:nth-of-type(odd) {
  background: #e9e9e9;
}
.row.header {
  font-weight: 900;
  color: #ffffff;
  background: #ea6153;
}
.row.green {
  background: #27ae60;
}
.row.blue {
  background: #2980b9;
}
@media screen and (max-width: 580px) {
  .row {
    padding: 8px 0;
    display: block;
  }
}

.cell {
  padding: 6px 12px;
  display: table-cell;
}
@media screen and (max-width: 580px) {
  .cell {
    padding: 2px 12px;
    display: block;
  }
}
</style>
 	 
 <table width="100%" border="1" cellpadding="0" cellspacing="0"  class="table">
  <tr class="row header red">
    <td colspan="7">&nbsp;Members who Swipe the Card More Than Once a Day (In-Patient case)</td>
   </tr>
  <tr class="row header blue">
    <td width="28%" class="cell">Client Name</td>
    <td width="19%" class="cell">&nbsp;</td>
    <td width="10%" class="cell">In-Patient</td>
    <td width="13%" class="cell">Out-Patient </td>
    <td width="10%" class="cell">Dental</td>
    <td width="10%" class="cell">Emergency</td>
    <td width="10%" class="cell">Total</td>
  </tr>
  <?php 
     
    foreach($result as $obj)
	{
		 $providerIdx =  $providerId != '0' ? $providerId : $obj->providerId; 
  ?>
  <tr>
    <td rowspan="3" valign="top">&nbsp;<?php echo $obj->provider_name;?></td>
    <td>&nbsp;Previous Month</td>
    <td align="center">&nbsp;<?php echo previous_ServiceType2(1, $providerIdx, $bln);?></td>
    <td align="center"><?php echo previous_ServiceType2(2, $providerIdx, $bln);?></td>
    <td align="center"><?php echo previous_ServiceType2(3, $providerIdx, $bln);?></td>
    <td align="center"><?php echo previous_ServiceType2(4, $providerIdx, $bln);?></td>
    <td align="center"><?php echo previous_ServiceType2(1, $providerIdx, $bln)+previous_ServiceType2(2, $providerIdx, $bln)+previous_ServiceType2(3, $providerIdx, $bln)+previous_ServiceType2(4, $providerIdx, $bln);?></td>
  </tr>
  <tr>
    <td>&nbsp;Current Month</td>
    <td align="center">&nbsp;<?php echo current_ServiceType2(1, $providerIdx, $bln);?></td>
    <td align="center"><?php echo current_ServiceType2(2, $providerIdx, $bln);?></td>
    <td align="center"><?php echo current_ServiceType2(3, $providerIdx, $bln);?></td>
    <td align="center"><?php echo current_ServiceType2(4, $providerIdx, $bln);?></td>
    <td align="center"><?php echo current_ServiceType2(1, $providerIdx, $bln)+current_ServiceType2(2, $providerIdx, $bln)+current_ServiceType2(3, $providerIdx, $bln)+current_ServiceType2(4, $providerIdx, $bln);?></td>
  </tr>
  <tr>
    <td>&nbsp;Today</td>
    <td align="center">&nbsp;<?php echo today_ServiceType2(1, $providerIdx, $bln);?></td>
    <td align="center"><?php echo today_ServiceType2(2, $providerIdx, $bln);?></td>
    <td align="center"><?php echo today_ServiceType2(3, $providerIdx, $bln);?></td>
    <td align="center"><?php echo today_ServiceType2(4, $providerIdx, $bln);?></td>
    <td align="center"><?php echo today_ServiceType2(1, $providerIdx, $bln)+today_ServiceType2(2, $providerIdx, $bln)+today_ServiceType2(3, $providerIdx, $bln)+today_ServiceType2(4, $providerIdx, $bln);?></td>
  </tr>
  <?php } ?>
 </table>

   