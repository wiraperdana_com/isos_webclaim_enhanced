<style>
 
.table { 
  font-size: 11px;
  font-family:Verdana, Geneva, sans-serif;
  width: 100%;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
  display: table;
  line-height: 20px; 
  color: #3b3b3b;
  -webkit-font-smoothing: antialiased;
  font-smoothing: antialiased; 
  border-collapse: collapse;
}
@media screen and (max-width: 580px) {
  .table {
    display: block;
  }
}

 
.row {
  display: table-row;
  background: #f6f6f6;   
}
.row:nth-of-type(odd) {
  background: #e9e9e9;
}
.row.header {
  font-weight: 900;
  color: #ffffff;
  background: #ea6153;
}
.row.green {
  background: #27ae60;
}
.row.blue {
  background: #2980b9;
}
@media screen and (max-width: 580px) {
  .row {
    padding: 8px 0;
    display: block;
  }
}

.cell {
  padding: 6px 12px;
  display: table-cell;
}
@media screen and (max-width: 580px) {
  .cell {
    padding: 2px 12px;
    display: block;
  }
}
</style>
 	 
 <table width="100%" border="1" cellpadding="0" cellspacing="0"  class="table">
  <tr class="row header red">
    <td colspan="7">&nbsp;Numbers of Active Members by Client</td>
   </tr>
  <tr class="row header blue">
    <td width="28%" class="cell">Client Name</td>
    <td width="19%" class="cell">&nbsp;</td>
    <td width="10%" class="cell">Main Insured</td>
    <td width="13%" class="cell">Dependents</td>
    <td width="10%" class="cell">Dental</td>
    <td width="10%" class="cell">Emergency</td>
    <td width="10%" class="cell">Total</td>
  </tr>
  <?php 
     
    foreach($result as $obj)
	{
		 $clientIDx =  $clientID != '0' ? $clientID : $obj->clientId; 
  ?>
  <tr>
    <td rowspan="3" valign="top">&nbsp;<?php echo $obj->client_name;?></td>
    <td>&nbsp;Previous Month</td>
    <td align="center">&nbsp;<?php echo previous_ServiceType(1, $clientIDx, $bln);?></td>
    <td align="center"><?php echo previous_ServiceType(2, $clientIDx, $bln);?></td>
    <td align="center"><?php echo previous_ServiceType(3, $clientIDx, $bln);?></td>
    <td align="center"><?php echo previous_ServiceType(4, $clientIDx, $bln);?></td>
    <td align="center"><?php echo previous_ServiceType(1, $clientIDx, $bln)+previous_ServiceType(2, $clientIDx, $bln)+previous_ServiceType(3, $clientIDx, $bln)+previous_ServiceType(4, $clientIDx, $bln);?></td>
  </tr>
  <tr>
    <td>&nbsp;Current Month</td>
    <td align="center">&nbsp;<?php echo current_ServiceType(1, $clientIDx, $bln);?></td>
    <td align="center"><?php echo current_ServiceType(2, $clientIDx, $bln);?></td>
    <td align="center"><?php echo current_ServiceType(3, $clientIDx, $bln);?></td>
    <td align="center"><?php echo current_ServiceType(4, $clientIDx, $bln);?></td>
    <td align="center"><?php echo current_ServiceType(1, $clientIDx, $bln)+current_ServiceType(2, $clientIDx, $bln)+current_ServiceType(3, $clientIDx, $bln)+current_ServiceType(4, $clientIDx, $bln);?></td>
  </tr>
  <tr>
  </tr>
  <?php } ?>
 </table>

   