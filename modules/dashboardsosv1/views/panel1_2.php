
Ext.define('ClientModel', {
    extend: 'Ext.data.Model',
    fields: [
        'clientId', 'client_name'
    ]
});

var ComboClientMpnl1 = Ext.create('Ext.data.Store', {
    model: 'ClientModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardsosv1/get_client/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
});


Ext.define('PeriodeModel', {
    extend: 'Ext.data.Model',
    fields: [
        'bln', 'bulan'
    ]
});

var ComboPeriodeMpnl1 = Ext.create('Ext.data.Store', {
    model: 'PeriodeModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardsosv1/get_periode/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
});


var Grid_T_panel1 = Ext.create('Ext.Component', {
         html: '<iframe src= "<?php echo base_url();?>dashboardsosv1/ext_get_panel1/" width="100%" height="100%"></iframe>',
       
});

var Form_SearchByPnl1 = new Ext.create('Ext.form.Panel', {
    id: 'Form_SearchByPnl1', 
    frame: true,
    bodyStyle: 'padding: 5px 5px 0 0;', 
    fieldDefaults: {
        labelAlign: 'left',
        msgTarget: 'side',
    }, 
    layout :'hbox',
    items: [
    {
      allowBlank: true,
      width: 300,
      xtype: 'combobox',
      valueField: 'clientId',
      displayField: 'client_name',
      triggerAction: 'all',
      id: 'ClientFrmDatapnl1',
      store: ComboClientMpnl1,
      mode: 'local',
      name: 'ClientFrmDatapnl1',
      fieldLabel: 'Client Name',
      lazyRender: true, 
      listeners: {
          'focus': {
              fn: function(comboField) {
                  comboField.doQuery(comboField.allQuery, true);
                  comboField.expand();
              },
              scope: this
          },
          select: function (combo) {
              Grid_T_panel1.update('<iframe src="<?php echo base_url();?>dashboardsosv1/ext_get_panel1/' + combo.getValue() + '"  width="100%" height="100%"></iframe>');
          }
      }
    },
    {
      allowBlank: true,
      width: 300,
      xtype: 'combobox',
      valueField: 'bln',
      displayField: 'bulan',
      triggerAction: 'all',
      id: 'PeriodeFrmDatapnl1',
      store: ComboPeriodeMpnl1,
      mode: 'local',
      name: 'PeriodeFrmDatapnl1',
      fieldLabel: 'Period',
      labelStyle :'padding-left:15px;',
      lazyRender: true, 
      listeners: {
          'focus': {
              fn: function(comboField) {
                  comboField.doQuery(comboField.allQuery, true);
                  comboField.expand();
              },
              scope: this
          },
          select: function (combo) {
              Grid_T_panel1.update('<iframe src="<?php echo base_url();?>dashboardsosv1/ext_get_panel1/' + Ext.getCmp('ClientFrmDatapnl1').getValue() + '/' + combo.getValue() +'"  width="100%" height="87%"></iframe>');
          }
      }
    } 
    ]
 });
 
 
Ext.define('ProviderDsbModel', {
    extend: 'Ext.data.Model',
    fields: [
        'providerId', 'provider_name'
    ]
});

var ComboProviderMpnl1 = Ext.create('Ext.data.Store', {
    model: 'ProviderDsbModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardsosv1/get_provider/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
}); 

Ext.define('PeriodePRModel', {
    extend: 'Ext.data.Model',
    fields: [
        'bln', 'bulan'
    ]
});

var ComboPeriodePRMpnl1 = Ext.create('Ext.data.Store', {
    model: 'PeriodePRModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardsosv1/get_periodeprovider/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
});

var Grid_T_panel2 = Ext.create('Ext.Component', {
         html: '<iframe src= "<?php echo base_url();?>dashboardsosv1/ext_get_panel2/" width="100%" height="87%"></iframe>',
       
});

var Form_SearchByPnl2 = new Ext.create('Ext.form.Panel', {
    id: 'Form_SearchByPnl2', 
    frame: true,
    bodyStyle: 'padding: 5px 5px 0 0;', 
    fieldDefaults: {
        labelAlign: 'left',
        msgTarget: 'side',
    }, 
    layout :'hbox',
    items: [
    {
      allowBlank: true,
      width: 300,
      xtype: 'combobox',
      valueField: 'providerId',
      displayField: 'provider_name',
      triggerAction: 'all',
      id: 'ProviderFrmDatapnl1',
      store: ComboProviderMpnl1,
      mode: 'local',
      name: 'ComboPeriodePRMpnl1',
      fieldLabel: 'Provider Name',
      lazyRender: true, 
      listeners: {
          'focus': {
              fn: function(comboField) {
                  comboField.doQuery(comboField.allQuery, true);
                  comboField.expand();
              },
              scope: this
          },
          select: function (combo) {
              Grid_T_panel2.update('<iframe src="<?php echo base_url();?>dashboardsosv1/ext_get_panel2/' + combo.getValue() + '"  width="100%" height="100%"></iframe>');
          }
      }
    },
    {
      allowBlank: true,
      width: 300,
      xtype: 'combobox',
      valueField: 'bln',
      displayField: 'bulan',
      triggerAction: 'all',
      id: 'PeriodePRFrmDatapnl1',
      store: ComboPeriodePRMpnl1,
      mode: 'local',
      name: 'PeriodePRFrmDatapnl1',
      fieldLabel: 'Period',
      labelStyle :'padding-left:15px;',
      lazyRender: true, 
      listeners: {
          'focus': {
              fn: function(comboField) {
                  comboField.doQuery(comboField.allQuery, true);
                  comboField.expand();
              },
              scope: this
          },
          select: function (combo) {
              Grid_T_panel2.update('<iframe src="<?php echo base_url();?>dashboardsosv1/ext_get_panel2/' + Ext.getCmp('ClientFrmDatapnl1').getValue() + '/' + combo.getValue() +'"  width="100%" height="87%"></iframe>');
          }
      }
    } 
    ]
 });
