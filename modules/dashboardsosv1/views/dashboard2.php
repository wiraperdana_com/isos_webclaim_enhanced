<?php  $this->load->view('panel1_2');?>
<?php  $this->load->view('panel3_4');?>
var new_tabpanel_DS2 = Ext.create('Ext.panel.Panel', {
width: 500,
height: 300,
layout: 'border',
autoscroll:true,
items: [ {
    region: 'center',  
    xtype: 'panel',
    height: 300,
    split: true, 
    autoScroll: true,  
    items:[
            {
                xtype: 'fieldset',defaults: {anchor: '100%'},  padding: '0 0 0 0',   flex:1,
                style: 'padding: 10px 5px 3px 5px; border:none;', 
                items:[
                   { xtype :'panel', title :'Numbers of Active Members by Client', items:[],height: 290  }
                ]
            },
			{
                xtype: 'fieldset',defaults: {anchor: '100%'},  padding: '0 0 0 0',   flex:1,
                style: 'padding: 10px 5px 3px 5px; border:none;', 
                items:[
                   { xtype :'panel', title :'Members who Swipe the Card More Than Once a Day (In-Patient case)', items:[],height: 290  }
                ]
            },
			{
                xtype: 'fieldset',defaults: {anchor: '100%'},  padding: '0 0 0 0',   flex:1,
                style: 'padding: 10px 5px 3px 5px; border:none;', 
                items:[
                   { xtype :'panel', title :'Numbers of Transaction (Swiping Card) by Client and by Type of Case', items:[Form_SearchBy, Grid_T_panel3],height: 290  }
                ]
            },
            {
                 xtype: 'fieldset', defaults: {anchor: '100%'}, autoScroll: true,  padding: '0 0 0 0',  flex:1,
                 style: 'padding: 10px 5px 3px 5px; border:none;', 
				  items:[
                       { xtype :'panel', title :'Numbers of Transaction (Swiping Card) by Top 10 Providers and by Type of Case', items:[Form_SearchBy2,Grid_T_panel4],height: 290, } 
                   ]
            }]
}], 
});
 