// TABEL GRID BERITA  ------------------------------------------------- START
var Params_T_D_Panduan;
Ext.define('MT_D_Panduan', {extend: 'Ext.data.Model',
  fields: ['panduanId', 'nama_panduan', 'file_panduan', 'createdBy','createdDate', 'updatedBy','updatedDate']
});
var Reader_T_D_Panduan = new Ext.create('Ext.data.JsonReader', {
  id: 'Reader_T_D_Panduan', root: 'results', totalProperty: 'total', idProperty: 'panduanId'  	
});

var Proxy_T_D_Panduan = new Ext.create('Ext.data.AjaxProxy', {
  id: 'Proxy_T_D_Panduan', url: BASE_URL + 'dashboardproviderv1/panduan', actionMethods: {read:'POST'}, extraParams: {id_open:1},
  reader: Reader_T_D_Panduan,
  afterRequest: function(request, success) {
   	Params_T_D_Panduan = request.operation.params;
  }
});

var Data_T_D_Panduan = new Ext.create('Ext.data.Store', {
	id: 'Data_T_D_Panduan', model: 'MT_D_Panduan', pageSize: 20, noCache: false, autoLoad: false,
  proxy: Proxy_T_D_Panduan
});

 
var Grid_T_D_Dashboard2 = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_D_Dashboard2', store: Data_T_D_Panduan, frame: true, border: true, loadMask: true, noCache: false,
    style: 'margin:0 auto;', autoHeight: true, height:'100%', columnLines: true,  
 	columns: [
  	{header:'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .5px;'}, 
  	{header: "Subject", dataIndex: 'nama_panduan', width: 440}, 
  	{header: "Attachement File", dataIndex: 'file_panduan', width: 220,
        renderer: function(value, metaData, record, rowIndex, colIndex, store) { 
            if(record.get('file_panduan') != null){
            var URL = BASE_URL;
                URL+="dashboardproviderv1/downloadPanduan/";
                URL+= record.get('panduanId');
            	return '<a href='+URL+' target=_blank>' + decode_base64(value) +'</a>';
            } 
       }  
     },  	  	 
  	{header: "Date upload", dataIndex: 'createdDate', width:125, format:'d-m-Y'},
    {header: "User Upload", dataIndex: 'createdBy', width: 190}, 
    {header: "Date Modify", dataIndex: 'updatedDate', width: 125, format:'d-m-Y'}, 
    {header: "User Modify", dataIndex: 'updatedBy', flex:1}, 
  ],  
  dockedItems: [{xtype: 'pagingtoolbar', store: Data_T_D_Panduan, dock: 'bottom', displayInfo: true,
  }]
  /*listeners: {
    'beforerender' : function(grid) {
         //console.log(grid.columns[0])
         var cond = '<?php echo count(@$Provider) == 0 ? @$client : @$Provider;?>';
         if(cond){ 
         	grid.columns[3].hidden = true;
         	grid.columns[4].hidden = true; 
         	grid.columns[5].hidden = true; 
         	grid.columns[6].hidden = true;
         }  
    }
   }*/ 
});

function findColumnByDataIndex(grid, dataIndex) {
    var selector = "gridcolumn[dataIndex=" + dataIndex + "]";
    return selector;
};

function decode_base64 (s)
{
    var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
    var n = [[65, 91], [97, 123], [48, 58], [43, 44], [47, 48]];

    for (z in n)
    {
        for (i = n[z][0]; i < n[z][1]; i++)
        {
            v.push(w(i));
        }
    }
    for (i = 0; i < 64; i++)
    {
        e[v[i]] = i;
    }

    for (i = 0; i < s.length; i+=72)
    {
        var b = 0, c, x, l = 0, o = s.substring(i, i+72);
        for (x = 0; x < o.length; x++)
        {
            c = e[o.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8)
            {
                r += w((b >>> (l -= 8)) % 256);
            }
         }
    }
    return r;
}