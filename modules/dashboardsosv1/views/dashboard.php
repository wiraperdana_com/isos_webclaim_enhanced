<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

// TABEL GRID MEMBER  ------------------------------------------------- START
var Params_T_D_Dependent;
Ext.define('MT_D_Dependent', {extend: 'Ext.data.Model',
  fields: ['memberId','client_name','member_cardNo', 'employee_ID', 'member_name',
  		   'relationship_type','jenis_kelamin', 'tgl_lahir',
           'location_name','location_region','plan_type','room_class','employment_status',
           'status_card','dental_limit'  ]
});
var Reader_T_D_Dependent = new Ext.create('Ext.data.JsonReader', {
  id: 'Reader_T_D_Dependent', root: 'results', totalProperty: 'total', idProperty: 'memberId'  	
});

var Proxy_T_D_Dependent = new Ext.create('Ext.data.AjaxProxy', {
  id: 'Proxy_T_D_Dependent', url: BASE_URL + 'dashboardproviderv1/getMemberdependent', actionMethods: {read:'POST'}, extraParams: {id_open:1},
  reader: Reader_T_D_Dependent,
  afterRequest: function(request, success) {
   	Params_T_D_Dependent = request.operation.params;
  }
});

var Data_T_D_Dependent = new Ext.create('Ext.data.Store', {
	id: 'Data_T_D_Dependent', model: 'MT_D_Dependent', pageSize: 20, noCache: false, autoLoad: false,
  proxy: Proxy_T_D_Dependent
});
 
var Grid_T_D_Dependent = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_D_Dependent',  store: Data_T_D_Dependent, frame: true, border: true, loadMask: true, noCache: false,
    style: 'margin:0 auto;', autoHeight: true, columnLines: true,  
	columns: [
  	{header:'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .5px;'}, 
  	{header: "Client", dataIndex: 'client_name', width: 240}, 
    {header: "No Member Card", dataIndex: 'member_cardNo', width: 140}, 
    {header: "Employee ID", dataIndex: 'employee_ID', width: 140}, 
    {header: "Member Name", dataIndex: 'member_name', width: 240}, 
    {header: "Relationship type", dataIndex: 'relationship_type', width: 140}, 
    {header: "Jns Kelamin", dataIndex: 'jenis_kelamin', width: 50}, 
  	{header: "Tgl Lahir", dataIndex: 'tgl_lahir', width: 120, format:'d-m-Y'},
    {header: "Location", dataIndex: 'location_region', width: 170}, 
    {header: "Region", dataIndex: 'location_region', width: 170}, 
    {header: "Plan type", dataIndex: 'plan_type',  width: 140}, 
    {header: "Room Class", dataIndex: 'room_class',  width: 140}, 
    {header: "Employment Status", dataIndex: 'employment_status',  width: 100}, 
    {header: "Status Card", dataIndex: 'status_card', flex:1}, 
    {header: "Dental Limit", dataIndex: 'cladental_limitim_status', flex:1}, 
  ],  
  dockedItems: [{xtype: 'pagingtoolbar', store: Data_T_D_Dependent, dock: 'bottom', displayInfo: true,
  listeners :{
      beforechange: function() { 					     
	   Data_T_D_Dependent.getProxy().extraParams.member_cardNo = Ext.getCmp('t_member_cardNo').getValue();  
  	}   
  }
  }], 
});


//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_4', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "totservicetypelastweek",
		"sortDir": "ASC",
		"persist": false,
		"type": "bigint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "totservicetypelastmonth",
		"sortDir": "ASC",
		"persist": false,
		"type": "bigint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "totservicetypelastyear",
		"sortDir": "ASC",
		"persist": false,
		"type": "bigint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "service_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_4",
	"name": "data_model_4",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_4",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>dashboardsosv1\/call_method\/select\/table\/tvdashboardgridchart",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "service_type",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_5",
	"storeId": "data_store_5",
	"name": "data_store_5"
});
//store define - end

//Search Card Variant=================
var cardSearch =  {    
	layout: 'fit',
	border: false,
	items:[{
            "frame": false,
            "border": false,
            "disabled": false,                    
            "flex": 1, 
            "id": "dashboardproviderv1_form_blk_form_text_5",
            "semar": true,
            "xtype": "panel",
            "name": "dashboardproviderv1_form_blk_form_text_5",
            "defaults": {  
               pack: "center",
               align: "center"
            },
            "layout": {
                "type": "hbox"
            },
            "items": [                      
                {
                 xtype:'component',
                 html :'<span  style="font-size:14px; text-align:right;"><b>No Member Card  &nbsp;</b></span>'                                  },{
                xtype : 'textfield', 
                width:300,
                name : 'member_cardNo',
                id : 'member_cardNo', 
                fieldStyle:'font-size:16px; height:25px;', 
             }, 
              {
                 xtype: 'button',
                 name: 'search_pegawai',
                 id: 'search_pegawai_ST',
				 "iconCls": "semar-view",
                 //text: 'Proses', 
                 margins: '0 5 5 5',
                 height:25,
                 handler: function() {
                    Ext.getCmp('layout-body').body.mask("Proses...", "x-mask-loading");
                    Ext.Ajax.request({
                    url: BASE_URL + 'dashboardproviderv1/getMember/',
                    params:{ member_cardNo : Ext.getCmp('member_cardNo').getValue(), 
                    	     client_name : Ext.getCmp('s_client_name').getValue() },
                    callback: function(options, success, response) {
                         var row = Ext.JSON.decode(response.responseText); 
                        Ext.getCmp('layout-body').body.unmask();
                        if(row.total > 0){
                           //CEK Data No Member Card ada namun status_card = Inactive atau Pending
                           if(row.model.status_card == 'Inactive'){
                               var Error = 'Maaf Member Card Inactive <br /> Hubungi Intl SOS 14 Hour 021 - 7659163';
                               Ext.fly('NohaveInformasiMember').update(Error);
                               Ext.getCmp('modul_fail_claim').setValue('Inactive');
                               DeActive();
                           }
                           if(row.model.status_card == 'Pending'){
                               var Error = 'Maaf Member Card Pending <br /> Hubungi Intl SOS 14 Hour 021 - 7659163';
                               Ext.fly('NohaveInformasiMember').update(Error);
                               Ext.getCmp('modul_fail_claim').setValue('Pending');
                               DeActive();
                           }
                           
                           if(row.model.status_card == 'Active'){ 
                                Ext.getCmp('t_member_cardNo').setValue(row.model.member_cardNo);
                                Ext.getCmp('t_member_name').setValue(row.model.member_name);
                                Ext.getCmp('t_jenis_kelamin').setValue(row.model.jenis_kelamin);
                                Ext.getCmp('t_member_address').setValue(row.model.member_address);
                                Ext.getCmp('t_member_telp').setValue(row.model.member_telp);
                                Ext.getCmp('t_plan_type').setValue(row.model.plan_desc);
                                Ext.getCmp('t_client_name').setValue(row.model.client_name);
                                Ext.getCmp('t_relationship_type').setValue(row.model.relationship_type);
                                Ext.getCmp('t_employment_status').setValue(row.model.employment_status);
                                Ext.getCmp('t_status_card').setValue(row.model.status_card);
                                Ext.getCmp('t_activation_date').setValue(row.model.activation_date);
                                Ext.getCmp('t_tanggal_lahir').setValue(row.model.tgl_lahir);
                                Ext.getCmp('t_dental_limit').setValue(row.model.dental_limit); 
                               
                                Data_T_D_Dependent.load({params:{ member_cardNo : row.model.member_cardNo}}); 
                                Ext.getCmp('form_view_result').show(); 
                                Ext.fly('dependent_fieldset').show();
                                Ext.getCmp('content_fail_claim').hide();
                          }
                      }else{                              
                           var Error = 'Maaf Member Card Tidak Terdaftar <br /> Hubungi Intl SOS 14 Hour 021 - 7659163';
                           Ext.fly('NohaveInformasiMember').update(Error);
                           DeActive();
                       }
                    }//callback
                     
                    })//extjs
                  }
                }
                          
             ]//========
      }]
 }
 
//========SEARCH CLIENT
var ClientSearchName = {
   
	layout: 'fit',
	border: false,
	items:[{
            "frame": false,
            "border": false,
            "disabled": false,                    
            "flex": 1, 
            "id": "ClientSearchName",
            "semar": true,
            "xtype": "panel",
            "name": "ClientSearchName",
            "defaults": {  
               pack: "center",
               align: "center"
            },
            "layout": {
                "type": "hbox"
            },
            "items": [                      
                {
                 xtype:'component',
                 html :'<span  style="font-size:14px; text-align:right; padding-right:40px;"><b>Client Name</b></span>'
                  },{
                xtype : 'textfield', 
                width:300,
                name : 's_client_name',
                id : 's_client_name',
                fieldStyle:'font-size:16px; height:25px;', 
               }, 
               {
                 xtype: 'button',
                 name: 'btns_client_name',
                 id: 'btns_client_name',
                 text: '....', 
                 margins: '0 5 5 5',
                 height:25,
                 handler: function() {
                     Load_Panel_Ref('win_popup_Refclient', 'ref_client', 'dashboardproviderv1_form');
                 }
                 
               } 
              ]//========
      }]
 }
 
var workspace_dashboardsosv1 = []
var new_tabpanel_DS1 = {
	id: 'new_tabpanel_dashboard_dashboardsosv1_module',
	layout: 'border',
	border: false,
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Pencarian Data Member",
				"url": "dashboardsosv1\/call_method\/insertupdate\/table\/value",
				"method": "POST",
				"autoScroll": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "dashboardproviderv1_form",
				"semar": true,
				"xtype": "form",
				"name": "dashboardproviderv1_form",
				"defaults": {
					"xtype": "panel",
					"layout": "anchor",
					"anchor": "100%",
					"flex": 1,
					"margin": 5,
					"defaults": {
						"anchor": "100%",
						"flex": 1
					}
				},
				"layout": {
					"type": "anchor"
				},
				"items": [
                {
                  xtype: 'fieldcontainer',  
                  combineErrors: false,
                  layout: 'hbox',
                  msgTarget: 'side',
                  items: [ //CT1
                     /////START KOLOM LEFT////////////////////////////////////////////////////
                    {
                     xtype: 'fieldset',
                     defaults: {
                       anchor: '100%'
                     },
                      margins: '0 5px 0 0',
                      style: 'padding: 0 0 0 0; border-width: 0px;', 
                      flex: 1,
                      items: [
                             ///START COMPONENT///////////////////////////
                             {
                                      xtype :'component',
                                      html : '<p align="right"><b>Operator  : <?php echo $this->session->userdata("user_zs_exmldashboard");?></b></center> <br />'
                             },
                             {
                                      xtype :'component',
                                      html : '<center><b>WEB CLAIM <br />  <?php echo count(@$Provider) == 0 ? @$client : @$Provider;?></b></center> <hr /> '
                             }]///END COMPONENT///////////////////////////
                    }] 
 				},
                {
                xtype :'panel', 
                border: false,
                defaults: {
                  border: false,
                  autoscroll:true, 
                },
                layout:'column',
                    items: [{ columnWidth: .10, html: '&nbsp;' },
                            { columnWidth: .80, items:[ ClientSearchName ]      
                           }]
               },
               {
                xtype :'container',
                title :'dasd',                 
                border: false,
                defaults: {
                  border: false,
                  autoscroll:true,
                  autoHeight:true
                },
                layout:'column', 
                    items: [{ columnWidth: .10, html: '&nbsp;' },
                            { columnWidth: .80, items:[ cardSearch ]      
                           }]
               },
               {
                      xtype:'component',
                      id : 'content_fail_claim',    
                      hidden :true,                  
                      html :'<br /><br /><center><span id="NohaveInformasiMember"></span></center>',					  
                 },
                 {
                     xtype :'hidden',
                     id : 'modul_fail_claim'
                 },
              {
              xtype: 'fieldset',
              hidden:true,
              margins: '0 5px 0 0',
              style: 'padding: 5px 5px 3px 5px; border:none;',
              defaults: {
                  labelWidth: 120,
                  fieldStyle:"border:none 0px"
              },
              defaultType: 'textfield',
              id :'form_view_result',
              flex: 1,
              items: [
                     {
                          xtype: 'textfield',
                          fieldLabel: 'No Member Card',
                          name: 't_member_cardNo',
                          id: 't_member_cardNo',
                          anchor : '100%', 
                          readOnly: true,
                      },
                      {
                          xtype: 'textfield',
                          fieldLabel: 'Nama',
                          name: 't_member_name',
                          id: 't_member_name', 
                          anchor : '100%',
                          readOnly: true,
                      },
                      {
                          xtype: 'textfield',
                          fieldLabel: 'Jenis Kelamin',
                          name: 't_jenis_kelamin',
                          id: 't_jenis_kelamin',
                          anchor : '100%',
                          readOnly: true,
                      },
                       {
                          xtype: 'textfield',
                          fieldLabel: 'Tgl Lahir',
                          name: 't_tanggal_lahir',
                          id: 't_tanggal_lahir',
                          anchor : '100%',
                          readOnly: true,
                      },
                      {
                          xtype: 'textarea',
                          fieldLabel: 'Alamat',
                          name: 't_member_address',
                          id: 't_member_address',
                          anchor : '100%',
                          readOnly: true,
                      },
                      {
                          xtype: 'textfield',
                          fieldLabel: 'Telp',
                          name: 't_member_telp',
                          id: 't_member_telp',
                          anchor : '100%',
                          readOnly: true,
                      },
                      {
                          xtype: 'textfield',
                          fieldLabel: 'Hak Akses Pelayanan',
                          name: 't_plan_type',
                          id: 't_plan_type',
                          anchor : '100%',
                          readOnly: true,
                      },
                      {
                         xtype: 'textfield',
                         fieldLabel: 'Perusahaan',
                         name: 't_client_name',
                         id: 't_client_name',
                         anchor : '100%',
                         readOnly: true,                                                
                     },
                     {
                         xtype: 'textfield',
                         fieldLabel: 'Relationship',
                         name: 't_relationship_type',
                         id: 't_relationship_type',
                         anchor : '100%',
                         readOnly: true,
                     },
                     {
                         xtype: 'textfield',
                         fieldLabel: 'Status Pegawai',
                         name: 't_employment_status',
                         id: 't_employment_status',
                         anchor : '100%',
                         readOnly: true,
                     },
                     {
                         xtype: 'textfield',
                         fieldLabel: 'Status Card',
                         name: 't_status_card',
                         id: 't_status_card',
                         anchor : '100%',
                         readOnly: true,
                     },
                     {
                         xtype: 'textfield',
                         fieldLabel: 'Tanggal Aktif',
                         name: 't_activation_date',
                         id: 't_activation_date',
                         anchor : '100%',
                         readOnly: true,
                     },  
                      {
                          xtype: 'textfield',
                          fieldLabel: 'Dental Limit',
                          name: 't_dental_limit',
                          id: 't_dental_limit',
                          anchor : '100%',
                          readOnly: true,
                      } ,
                     {
                        xtype: 'fieldset',  
                        id :'dependent_fieldset',
                        title :'<b>DEPENDENT MEMBER LIST</b>',
                        margins: '5px 5px 5px 5px',
                        style: 'padding: 0 5px 3px 5px;',
                        defaults: {
                                 labelWidth: 120
                                },
                        defaultType: 'textfield',
                        flex: 1,
                        items: [ Grid_T_D_Dependent ]
                    },//END FIELDSET DEPENT 
                  ]
                }]
			 }
             ]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Chart Transaksi",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "dashboardsosv1_module_real_general_chart_1",
				"semar": true,
				"xtype": "exmlwidgetevochart",
				"name": "dashboardsosv1_module_real_general_chart_1",
				"layout": {
					"type": "autocontainer"
				},
				"items": [{  xtype :'component', 
                             html :'<div id="grafik_trans"></div>', 
                          }],
                          listeners: {
                              afterlayout:function(){
                                Ext.fly('grafik_trans').update('<iframe src="<?php echo base_url();?>dashboardsosv1/chart?w=100" style="height:240px; width:100%;">');
                              }
                          }
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "History Transaksi",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "dashboardsosv1_module_real_general_grid_6",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_5",
				"columns": [{
					"text": "Service Type",
					"dataIndex": "service_type",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Last 1 Year",
					"dataIndex": "totservicetypelastyear",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Last 1 Month",
					"dataIndex": "totservicetypelastmonth",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Last 7 Days",
					"dataIndex": "totservicetypelastweek",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "dashboardsosv1_module_real_general_grid_6",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_dashboardsosv1, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_dashboardsosv1, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('dashboardsosv1_module_real_general_chart_1', 'afterrender', {
				key: 'updateChart',
				fn: function(callbackIndex) {

					if (Ext.getCmp('dashboardsosv1_module_real_general_chart_1').ischart == true) {
						Ext.getCmp('dashboardsosv1_module_real_general_chart_1').updateVisual();
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			runRegisterEvent('dashboardsosv1_module_real_general_chart_1', 'afterrender');
		}
	}
}

var Tab1_T_Dashbord_1 = {
	id: 'Tab1_T_Dashbord_1', title: 'Dashboard History Transaksi', border: false, collapsible: false,
	layout: 'fit', items: [new_tabpanel_DS1]
};

<?php  $this->load->view('dashboard2');?>

var Tab2_T_Dashbord_2 = {
	id: 'Tab2_T_Dashbord_2', title: 'Dashboard Reporting', border: false, collapsible: false,
	layout: 'fit', items: [new_tabpanel_DS2]
}; 

var  new_tabpanel_dashboard = new Ext.createWidget('tabpanel', {
	id: 'new_tabpanel_dashboard', layout: 'fit', resizeTabs: true,  enableTabScroll: false, 
    deferredRender: true, border: false, defaults: {autoScroll:true},  
    items: [Tab1_T_Dashbord_1, Tab2_T_Dashbord_2], 
    listeners: {
  	'tabchange': function(tabPanel, tab){ 
          
  	}
  }   
});

function DeActive(){ 
  Ext.getCmp('form_view_result').hide();
  Ext.getCmp('content_fail_claim').show();
  Ext.fly('dependent_fieldset').hide();   
}

<?php }else{ echo "var new_tabpanel_dashboard = 'GAGAL';"; } ?>