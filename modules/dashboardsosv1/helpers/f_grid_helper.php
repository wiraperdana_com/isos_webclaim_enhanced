<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	function previous_ServiceType($ID, $clientID, $bln)
	{
	     $CI = & get_instance(); 
		 $CI->db->where('clientId',$clientID);
		 if($bln!=0 && $bln>0){ $CI->db->where('MONTH(date_claim) = MONTH("'.date("Y-".$bln."-01").'" - INTERVAL 1 MONTH)'); }
		 else{ $CI->db->where('MONTH(date_claim) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)');  }
		 
		 $CI->db->where('servicetypeId', $ID);
 		 $CI->db->select('count(*) as jml'); 
 		 $v = $CI->db->from('sos_ttrans_claim')->get()->row(); 
		 $c = isset($v->jml) ? $v->jml : 0;
		 //echo $CI->db->last_query(); 
		 return $c;
	}
	
	function current_ServiceType($ID, $clientID, $bln)
	{
	     $CI = & get_instance(); 
		 $CI->db->where('clientId',$clientID);
		 if($bln!=0 && $bln>0){ $CI->db->where('MONTH(date_claim) = MONTH("'.date("Y-".$bln."-01").'")'); }
		 else{ $CI->db->where('MONTH(date_claim) = MONTH(CURRENT_DATE)');  }
		 
		 $CI->db->where('servicetypeId', $ID);
 		 $CI->db->select('count(*) as jml'); 
 		 $v = $CI->db->from('sos_ttrans_claim')->get()->row(); 
		 $c = isset($v->jml) ? $v->jml : 0;
		 //echo $CI->db->last_query(); 
		 return $c;
	}
	
	function today_ServiceType($ID, $providerId, $bln)
	{
	     $CI = & get_instance(); 
		 $CI->db->where('providerId',$providerId);
 		 $CI->db->where('DATE_FORMAT(date_claim,"%Y-%m-%d") = CURDATE()');
		 
		 $CI->db->where('servicetypeId', $ID);
 		 $CI->db->select('count(*) as jml'); 
 		 $v = $CI->db->from('sos_ttrans_claim')->get()->row(); 
		 $c = isset($v->jml) ? $v->jml : 0;
		 //echo $CI->db->last_query(); 
		 return $c;
	}
	
	
	//////////////////////////PROVIDER//////////////////////////////////////////////
	function previous_ServiceType2($ID, $providerId, $bln)
	{
	     $CI = & get_instance(); 
		 $CI->db->where('providerId',$providerId);
		 if($bln!=0 && $bln>0){ $CI->db->where('MONTH(date_claim) = MONTH("'.date("Y-".$bln."-01").'" - INTERVAL 1 MONTH)'); }
		 else{ $CI->db->where('MONTH(date_claim) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)');  }
		 
		 $CI->db->where('servicetypeId', $ID);
 		 $CI->db->select('count(*) as jml'); 
 		 $v = $CI->db->from('sos_ttrans_claim')->get()->row(); 
		 $c = isset($v->jml) ? $v->jml : 0;
		 //echo $CI->db->last_query(); 
		 return $c;
	}
	
	function current_ServiceType2($ID, $providerId, $bln)
	{
	     $CI = & get_instance(); 
		 $CI->db->where('providerId',$providerId);
		 if($bln!=0 && $bln>0){ $CI->db->where('MONTH(date_claim) = MONTH("'.date("Y-".$bln."-01").'")'); }
		 else{ $CI->db->where('MONTH(date_claim) = MONTH(CURRENT_DATE)');  }
		 
		 $CI->db->where('servicetypeId', $ID);
 		 $CI->db->select('count(*) as jml'); 
 		 $v = $CI->db->from('sos_ttrans_claim')->get()->row(); 
		 $c = isset($v->jml) ? $v->jml : 0;
		 //echo $CI->db->last_query(); 
		 return $c;
	}
	
	function today_ServiceType2($ID, $providerId, $bln)
	{
	     $CI = & get_instance(); 
		 $CI->db->where('providerId',$providerId);
 		 $CI->db->where('DATE_FORMAT(date_claim,"%Y-%m-%d") = CURDATE()');
		 
		 $CI->db->where('servicetypeId', $ID);
 		 $CI->db->select('count(*) as jml'); 
 		 $v = $CI->db->from('sos_ttrans_claim')->get()->row(); 
		 $c = isset($v->jml) ? $v->jml : 0;
		 //echo $CI->db->last_query(); 
		 return $c;
	}
?>