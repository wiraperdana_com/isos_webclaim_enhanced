<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_1', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "flag_fraud_email",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "providerId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "etdate(",
		"name": "lastvisitDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "etdate(",
		"name": "registerDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "kode_unker",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0",
		"name": "NIP",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "email",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "fullname",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "pass",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "user",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "group",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0",
		"name": "ID_User",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_1",
	"name": "data_model_1",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_12', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_12",
	"name": "data_model_12",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_14', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "providerId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_14",
	"name": "data_model_14",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_1",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tviewuser\/call_method\/select\/table\/tView_User",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "ID_User",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_2",
	"storeId": "data_store_2",
	"name": "data_store_2"
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [{
		"key": "1",
		"val": "SUPER ADMIN"
	}, {
		"key": "2",
		"val": "ADMIN"
	}, {
		"key": "3",
		"val": "SOS"
	}, {
		"key": "4",
		"val": "ABA"
	}, {
		"key": "5",
		"val": "CLIENT"
	}, {
		"key": "6",
		"val": "PROVIDER"
	}],
	"id": "data_store_10",
	"storeId": "data_store_10",
	"name": "data_store_10"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_12",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tviewuser\/call_method\/select\/table\/sos_tmas_client",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "clientId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_13",
	"storeId": "data_store_13",
	"name": "data_store_13"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_14",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tviewuser\/call_method\/select\/table\/sos_tmas_provider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "providerId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_15",
	"storeId": "data_store_15",
	"name": "data_store_15"
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [{
		"key": "0",
		"val": "0"
	}, {
		"key": "1",
		"val": "1"
	}],
	"id": "data_store_fraud",
	"storeId": "data_store_fraud",
	"name": "data_store_fraud"
});
//store define - end
var workspace_tviewuser = {
	"w2_space": {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "f_pass",
				"invokeCall": "f_pass",
				"invokeReturn": true,
				"invokeParams": "[{\"position\":\"1\",\"set\":\"get\",\"type\":\"string\",\"value\":\"tviewuser_module_real_general_form_4_blk_i_form_text_6\"}]",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tviewuser_module_real_general_invoke_20",
				"semar": true,
				"xtype": "panel",
				"name": "tviewuser_module_real_general_invoke_20",
				"layout": {
					"type": "fit"
				},
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "north",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "south",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "east",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}]
	}
}
var new_tabpanel = {
	id: 'newtab_tviewuser_module',
	title: 'Pengguna Login',
	iconCls: 'semar-human',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Daftar Pengguna Form",
				"width": 485,
				"url": "tviewuser\/call_method\/insertupdate\/table\/tUser",
				"method": "POST",
				"height": 478,
				"autoScroll": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tviewuser_module_real_general_form_4",
				"semar": true,
				"xtype": "form",
				"tbar": [
					[{
						"id": "tviewuser_module_real_general_form_4_btn_simpan",
						"name": "tviewuser_module_real_general_form_4_btn_simpan",
						"itemId": "tviewuser_module_real_general_form_4_btn_simpan",
						"text": "Simpan",
						"iconCls": "semar-save",
						"configs": {
							"toolbar": "top",
							"text": "Simpan",
							"iconCls": "semar-save",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "tviewuser_module_real_general_form_4_btn_ubah",
						"name": "tviewuser_module_real_general_form_4_btn_ubah",
						"itemId": "tviewuser_module_real_general_form_4_btn_ubah",
						"text": "Ubah",
						"iconCls": "semar-edit",
						"configs": {
							"toolbar": "top",
							"text": "Ubah",
							"iconCls": "semar-edit",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "tviewuser_module_real_general_form_4_btn_batal",
						"name": "tviewuser_module_real_general_form_4_btn_batal",
						"itemId": "tviewuser_module_real_general_form_4_btn_batal",
						"text": "Batal",
						"iconCls": "semar-undo",
						"configs": {
							"toolbar": "top",
							"text": "Batal",
							"iconCls": "semar-undo",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}]
				],
				"name": "tviewuser_module_real_general_form_4",
				"defaults": {
					"xtype": "panel",
					"layout": "anchor",
					"anchor": "100%",
					"flex": 1,
					"margin": 5,
					"defaults": {
						"anchor": "100%",
						"flex": 1
					}
				},
				"layout": {
					"type": "anchor"
				},
				"items": [{
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_text_5",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_text_5",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Pengguna",
						"name": "user",
						"allowBlank": false,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_text_5",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_text_6",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_text_6",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Password",
						"name": "passtxt",
						"value": "MHC",
						"allowBlank": false,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_text_6",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_text_7",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_text_7",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Nama Lengkap",
						"name": "fullname",
						"allowBlank": false,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_text_7",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_text_8",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_text_8",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Email",
						"name": "email",
						"allowBlank": true,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_text_8",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_text_9",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_text_9",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "NIP",
						"name": "NIP",
						"allowBlank": false,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_text_9",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_combo_11",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_combo_11",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Group",
						"name": "group",
						"allowBlank": false,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_combo_11",
						"semar": true,
						"xtype": "combobox",
						"store": "data_store_10",
						"displayField": "val",
						"valueField": "key",
						"queryMode": "local",
						"items": [],
						listeners:{
							change:function(){
								var temp = 
									Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_combo_11').getValue();
									Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_combo_16').setVisible(false);
									Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_combo_17').setVisible(false);
									if( String( temp ) == "5" ){
										Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_combo_16').setVisible(true);
									};
									if( String( temp ) == "6" ){
										Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_combo_17').setVisible(true);
									};
								}
						}
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_combo_16",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_combo_16",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Client",
						"name": "clientId",
						"allowBlank": true,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_combo_16",
						"semar": true,
						"xtype": "combobox",
						"store": "data_store_13",
						"displayField": "client_name",
						"valueField": "clientId",
						"queryMode": "local",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_combo_17",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_combo_17",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Provider",
						"name": "providerId",
						"allowBlank": true,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_combo_17",
						"semar": true,
						"xtype": "combobox",
						"store": "data_store_15",
						"displayField": "provider_name",
						"valueField": "providerId",
						"queryMode": "local",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_combo_171",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_combo_171",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Flag Fraud Email",
						"name": "flag_fraud_email",
						"allowBlank": true,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_combo_171",
						"semar": true,
						"xtype": "combobox",
						"store": "data_store_fraud",
						"displayField": "key",
						"valueField": "val",
						"queryMode": "local",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_hidden_18",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_hidden_18",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "ID_User",
						"allowBlank": false,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_hidden_18",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewuser_module_real_general_form_4_blk_form_hidden_19",
					"semar": true,
					"xtype": "panel",
					"name": "tviewuser_module_real_general_form_4_blk_form_hidden_19",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "pass",
						"allowBlank": true,
						"id": "tviewuser_module_real_general_form_4_blk_i_form_hidden_19",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Daftar Pengguna Login",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tviewuser_module_real_general_grid_3",
				"semar": true,
				"xtype": "grid",
				"tbar": [
					[{
						"id": "tviewuser_module_real_general_grid_3_btn_tambah",
						"name": "tviewuser_module_real_general_grid_3_btn_tambah",
						"itemId": "tviewuser_module_real_general_grid_3_btn_tambah",
						"text": "Tambah",
						"iconCls": "semar-add",
						"configs": {
							"toolbar": "top",
							"text": "Tambah",
							"iconCls": "semar-add",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "tviewuser_module_real_general_grid_3_btn_hapus",
						"name": "tviewuser_module_real_general_grid_3_btn_hapus",
						"itemId": "tviewuser_module_real_general_grid_3_btn_hapus",
						"text": "Hapus",
						"iconCls": "semar-delete",
						"configs": {
							"toolbar": "top",
							"text": "Hapus",
							"iconCls": "semar-delete",
							"type": "delete",
							"value": "tUser",
							"split": true,
							"active": true
						}
					}, "-"],
					[{
						"id": "tviewuser_module_real_general_grid_3_btn_excel",
						"name": "tviewuser_module_real_general_grid_3_btn_excel",
						"itemId": "tviewuser_module_real_general_grid_3_btn_excel",
						"text": "Excel",
						"iconCls": "semar-excel",
						"configs": {
							"toolbar": "top",
							"text": "Excel",
							"iconCls": "semar-excel",
							"type": "excel",
							"value": "tView_User",
							"active": true
						}
					}]
				],
				"store": "data_store_2",
				"dockedItems": [{
					"id": "tviewuser_module_real_general_grid_3_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_2",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "tviewuser_module_real_general_grid_3_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "tviewuser_module_real_general_grid_3",
					"store": "data_store_2",
					"text": "Search"
				}],
				"columns": [{
					"text": "providerId",
					"dataIndex": "providerId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "clientId",
					"dataIndex": "clientId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "No",
					"dataIndex": "ID_User",
					"width": 40,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Group",
					"dataIndex": "group",
					"width": 110,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "User",
					"dataIndex": "user",
					"width": 104,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Pass",
					"dataIndex": "pass",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Nama Lengkap",
					"dataIndex": "fullname",
					"width": 190,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Email",
					"dataIndex": "email",
					"width": 134,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "NIP",
					"dataIndex": "NIP",
					"width": 138,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "kode_unker",
					"dataIndex": "kode_unker",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Tgl. Terdaftar",
					"dataIndex": "registerDate",
					"width": 121,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Kunjungan Terakhir",
					"dataIndex": "lastvisitDate",
					"width": 138,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "type",
					"dataIndex": "type",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Status",
					"dataIndex": "status",
					"width": 68,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Flag Fraud Email",
					"dataIndex": "flag_fraud_email",
					"width": 68,
					"height": null,
					"flex": null,
					"filter": {
						"type": "int"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "checkboxmodel",
				"selModel": {
					"mode": "MULTI",
					"selType": "checkboxmodel"
				},
				"name": "tviewuser_module_real_general_grid_3",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_tviewuser, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_tviewuser, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_simpan', 'click', {
				key: 'invokeCall',
				fn: function(callbackIndex) {
					var invokeComponent = doGetWindowWorkspace(workspace_tviewuser, 'tviewuser_module_real_general_invoke_20');
					if (invokeComponent != undefined && Ext.isObject(invokeComponent)) {
						var tempParams = Ext.clone(invokeComponent.invokeParams);
						var procParams = {};
						if (tempParams) {
							if (tempParams.length > 0) {
								while (!Ext.isArray(tempParams)) {
									tempParams = Ext.decode(tempParams);
								};
								if (Ext.isArray(tempParams)) {
									tempParams = tempParams;
									for (var pr in tempParams) {
										if (Ext.isNumeric(pr)) {
											if (tempParams[pr]) {
												if (tempParams[pr]['set'] == 'get') {
													if (Ext.getCmp(tempParams[pr]['value'])) {
														tempParams[pr]['value'] = Ext.getCmp(tempParams[pr]['value']).getValue();
													};
												} else if (tempParams[pr]['set'] == 'grid') {
													var tempSetValFromGrid = tempParams[pr]['value'].split('=');
													if (tempSetValFromGrid.length == 2) {
														tempParams[pr]['value'] = doGetGridSelectionSingle(tempSetValFromGrid[0], tempSetValFromGrid[1], ',', 'string');
													};
												}
											}
										}
									};
									procParams = {
										params: Ext.encode(tempParams)
									};
								};
							};
						};
						var procName = invokeComponent.invokeCall;
						var procReturn = invokeComponent.invokeReturn;
						if (new String(procName).length > 0) {
							Ext.getBody().mask('Please wait...');
							var joinUri = 'select/function';
							if (procReturn == false) {
								joinUri = 'call/procedure';
							};
							Ext.Ajax.request({
								url: BASE_URL + 'tviewuser/call_method/' + joinUri + '/' + procName,
								method: 'POST',
								params: procParams,
								success: function(response) {
									if (new String(response.responseText).length > 0) {
										var obj = Ext.decode(response.responseText);
										if (Ext.isObject(obj)) {
											if (obj.data != null && obj.data != undefined) {
												//if function and butuh di set, dimari.
												if ('tviewuser_module_real_general_form_4_blk_i_form_hidden_19' != 'true' && new String('tviewuser_module_real_general_form_4_blk_i_form_hidden_19').length > 0 && Ext.isString('tviewuser_module_real_general_form_4_blk_i_form_hidden_19')) {
													//spesial untuk messagebox
													if (Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_hidden_19').ismessagebox == true) {
														if (String(obj.data[procName]).length > 0) {
															SemarBOXPanelDialog(Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_hidden_19'), {
																msg: obj.data[procName]
															});
														};
													} else {
														if (Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_hidden_19')) {
															Ext.getCmp('tviewuser_module_real_general_form_4_blk_i_form_hidden_19').setValue(obj.data[procName]);
														}
													};
												}
											}
										};
									};

									if (Ext.isFunction(callbackIndex)) {
										callbackIndex()
									};
								},
								failure: function(response) {
									Ext.MessageBox.show({
										title: 'Warning !',
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.ERROR
									});
								},
								callback: function() {
									Ext.getBody().unmask();
								}
							});
						} else {
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
						};
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4').on({
						beforeaction: function() {
							Ext.getCmp('tviewuser_module_real_general_form_4').body.mask('Loading...');
						}
					});
					Ext.getCmp('tviewuser_module_real_general_form_4').getForm().submit({
						url: Ext.getCmp('tviewuser_module_real_general_form_4').url,
						method: Ext.getCmp('tviewuser_module_real_general_form_4').method,
						clientValidation: true,
						success: function(form, action) {
							Ext.getCmp('tviewuser_module_real_general_form_4').body.unmask();
							Ext.Msg.alert('Success', action.result.msg);
							Ext.getCmp('tviewuser_module_real_general_form_4').getForm().setValues(action.result.data);

							//more events - start
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
							//more events - end
						},
						failure: function(form, action) {
							Ext.getCmp('tviewuser_module_real_general_form_4').body.unmask();
							switch (action.failureType) {
							case Ext.form.action.Action.CLIENT_INVALID:
								Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
								break;
							case Ext.form.action.Action.CONNECT_FAILURE:
								Ext.Msg.alert('Failure', 'Ajax communication failed');
								break;
							case Ext.form.action.Action.SERVER_INVALID:
								Ext.Msg.alert('Failure', action.result.msg);
							}
						}
					});
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_grid_3').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_simpan', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tviewuser_module_real_general_form_4_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('tviewuser_module_real_general_form_4_btn_simpan')) {
					var tempEl = Ext.getCmp('tviewuser_module_real_general_form_4_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('tviewuser_module_real_general_form_4_btn_simpan', 'click');
						});
						clearInterval(intervale_tviewuser_module_real_general_form_4_btn_simpan_click);
					};
				};
			}, 100);
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tviewuser_module_real_general_form_4'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tviewuser_module_real_general_form_4_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('tviewuser_module_real_general_form_4_btn_ubah')) {
					var tempEl = Ext.getCmp('tviewuser_module_real_general_form_4_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('tviewuser_module_real_general_form_4_btn_ubah', 'click');
						});
						clearInterval(intervale_tviewuser_module_real_general_form_4_btn_ubah_click);
					};
				};
			}, 100);
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_form_4_btn_batal', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tviewuser_module_real_general_form_4'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tviewuser_module_real_general_form_4_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('tviewuser_module_real_general_form_4_btn_batal')) {
					var tempEl = Ext.getCmp('tviewuser_module_real_general_form_4_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('tviewuser_module_real_general_form_4_btn_batal', 'click');
						});
						clearInterval(intervale_tviewuser_module_real_general_form_4_btn_batal_click);
					};
				};
			}, 100);
			pushRegisterEvent('tviewuser_module_real_general_grid_3', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4_btn_ubah').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_grid_3', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4').getForm().loadRecord((Ext.getCmp('tviewuser_module_real_general_grid_3').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('tviewuser_module_real_general_grid_3').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_grid_3', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_grid_3', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tviewuser_module_real_general_form_4'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tviewuser_module_real_general_grid_3_itemdblclick = setInterval(function() {
				if (Ext.getCmp('tviewuser_module_real_general_grid_3')) {
					var tempEl = Ext.getCmp('tviewuser_module_real_general_grid_3');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('itemdblclick', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('tviewuser_module_real_general_grid_3', 'itemdblclick');
						});
						clearInterval(intervale_tviewuser_module_real_general_grid_3_itemdblclick);
					};
				};
			}, 100);
			pushRegisterEvent('tviewuser_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4_btn_ubah').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewuser_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewuser_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tviewuser_module_real_general_form_4'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tviewuser_module_real_general_grid_3_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('tviewuser_module_real_general_grid_3_btn_tambah')) {
					var tempEl = Ext.getCmp('tviewuser_module_real_general_grid_3_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('tviewuser_module_real_general_grid_3_btn_tambah', 'click');
						});
						clearInterval(intervale_tviewuser_module_real_general_grid_3_btn_tambah_click);
					};
				};
			}, 100);
			pushRegisterEvent('tviewuser_module_real_general_grid_3_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('tviewuser_module_real_general_grid_3').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('ID_User'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('tviewuser_module_real_general_grid_3_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'tviewuser/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'ID_User',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('tviewuser_module_real_general_grid_3').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_tviewuser_module_real_general_grid_3_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('tviewuser_module_real_general_grid_3_btn_hapus')) {
					var tempEl = Ext.getCmp('tviewuser_module_real_general_grid_3_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('tviewuser_module_real_general_grid_3_btn_hapus', 'click');
						});
						clearInterval(intervale_tviewuser_module_real_general_grid_3_btn_hapus_click);
					};
				};
			}, 100);
			pushRegisterEvent('tviewuser_module_real_general_grid_3_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('tviewuser_module_real_general_grid_3_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('tviewuser', //nama module
								Ext.getCmp('tviewuser_module_real_general_grid_3').title, //title
								'tviewuser_module_real_general_grid_3', //id grid
								'ID_User', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_tviewuser_module_real_general_grid_3_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('tviewuser_module_real_general_grid_3_btn_excel')) {
					var tempEl = Ext.getCmp('tviewuser_module_real_general_grid_3_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('tviewuser_module_real_general_grid_3_btn_excel', 'click');
						});
						clearInterval(intervale_tviewuser_module_real_general_grid_3_btn_excel_click);
					};
				};
			}, 100);
		}
	}
}

<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>