<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller digenerate oleh eXML Builder.
 * Controller untuk module tvbrowsereftmasprovider.
 *
 */
class tvbrowsereftmasprovider extends CI_Controller
{

	/**
	 * __construct Controller untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_tvbrowsereftmasprovider'));
	}


	/**
	 * widget_browseref Controller untuk module $modulename.
	 */
	public function widget_browseref()
	{
		if($this->input->post('id_open')){
			$data['jsscript'] = TRUE;
			$this->load->view('browseref',$data);
		}else{
			$this->load->view('browseref');
		};
	}


	/**
	 * call_method Controller untuk module $modulename.
	 */
	public function call_method($querytype, $typex, $valuex)
	{
		echo json_encode( $this->model_tvbrowsereftmasprovider->call_method( $typex, $querytype, $valuex ) );
	}

}
