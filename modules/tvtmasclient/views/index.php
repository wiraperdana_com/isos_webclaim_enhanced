<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_2', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "Active",
		"name": "client_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "text",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_email",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_telp",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_address",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_codeId",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_2",
	"name": "data_model_2",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_5', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_5",
	"name": "data_model_5",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_8', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "Active",
		"name": "client_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_8",
	"name": "data_model_8",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_17', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "locationId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_17",
	"name": "data_model_17",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_2",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasclient\/call_method\/select\/table\/tvtmasclient",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "clientId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_3",
	"storeId": "data_store_3",
	"name": "data_store_3"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_5",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasclient\/call_method\/select\/table\/tvtmasclient",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "location_name",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_6",
	"storeId": "data_store_6",
	"name": "data_store_6"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_8",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasclient\/call_method\/select\/table\/tvtmasclient",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "client_status",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_9",
	"storeId": "data_store_9",
	"name": "data_store_9"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_17",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasclient\/call_method\/select\/table\/sos_tref_location",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "locationId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_18",
	"storeId": "data_store_18",
	"name": "data_store_18"
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [{
		"key": "Active",
		"val": "Active"
	}, {
		"key": "Inactive",
		"val": "Inactive"
	}],
	"id": "data_store_23",
	"storeId": "data_store_23",
	"name": "data_store_23"
});
//store define - end
var workspace_tvtmasclient = {
	"w2_space": {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Pilih Location",
				"modulecode": "tvbrowsereflocation",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "tvtmasclient_module_real_general_form_11",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"semar": true,
				"width": 640,
				"height": 492,
				"modal": true,
				"bodyStyle": "padding:5px;",
				"layout": {
					"type": "fit"
				},
				"closeAction": "destroy",
				"id": "tvtmasclient_module_real_general_browseref_20",
				"name": "tvtmasclient_module_real_general_browseref_20",
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "north",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "south",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "east",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}]
	}
}
var new_tabpanel = {
	id: 'newtab_tvtmasclient_module',
	title: 'Master Client',
	iconCls: 'icon-gears',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 7,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"margins": {
				"top": 4,
				"right": 4,
				"bottom": 4,
				"left": 4,
				"height": 8,
				"width": 8
			},
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Master Client",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvtmasclient_module_real_general_tabpanel_1",
				"semar": true,
				"xtype": "tabpanel",
				"name": "tvtmasclient_module_real_general_tabpanel_1",
				"defaults": {
					"layout": {
						"type": "vbox",
						"align": "stretch",
						"pack": "start"
					},
					"flex": 1
				},
				"layout": {
					"type": "card"
				},
				"items": [{
					"title": "Client List",
					"id": "tvtmasclient_module_real_general_tabpanel_1_tab_1",
					"name": "tvtmasclient_module_real_general_tabpanel_1_tab_1",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"loadMask": true,
						"noCache": false,
						"autoHeight": true,
						"columnLines": false,
						"invalidateScrollerOnRefresh": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "tvtmasclient_module_real_general_grid_4",
						"semar": true,
						"xtype": "grid",
						"tbar": [
							[{
								"id": "tvtmasclient_module_real_general_grid_4_btn_tambah",
								"name": "tvtmasclient_module_real_general_grid_4_btn_tambah",
								"itemId": "tvtmasclient_module_real_general_grid_4_btn_tambah",
								"text": "Tambah",
								"iconCls": "semar-add",
								"configs": {
									"toolbar": "top",
									"text": "Tambah",
									"iconCls": "semar-add",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvtmasclient_module_real_general_grid_4_btn_hapus",
								"name": "tvtmasclient_module_real_general_grid_4_btn_hapus",
								"itemId": "tvtmasclient_module_real_general_grid_4_btn_hapus",
								"text": "Hapus",
								"iconCls": "semar-delete",
								"configs": {
									"toolbar": "top",
									"text": "Hapus",
									"iconCls": "semar-delete",
									"type": "normal",
									"value": "sos_tmas_client",
									"split": true,
									"active": true
								}
							}, "-"],
							[{
								"id": "tvtmasclient_module_real_general_grid_4_btn_excel",
								"name": "tvtmasclient_module_real_general_grid_4_btn_excel",
								"itemId": "tvtmasclient_module_real_general_grid_4_btn_excel",
								"text": "Excel",
								"iconCls": "semar-excel",
								"configs": {
									"toolbar": "top",
									"text": "Excel",
									"iconCls": "semar-excel",
									"type": "excel",
									"value": "tvtmasclient",
									"active": true
								}
							}]
						],
						"store": "data_store_3",
						"dockedItems": [{
							"id": "tvtmasclient_module_real_general_grid_4_pagingtoolbar",
							"hidden": false,
							"xtype": "pagingtoolbar",
							"store": "data_store_3",
							"dock": "bottom",
							"displayInfo": true
						}, {
							"id": "tvtmasclient_module_real_general_grid_4_searchfield",
							"dock": "top",
							"hidden": false,
							"xtype": "semarwidgetsearchtoolbar",
							"grid_id": "tvtmasclient_module_real_general_grid_4",
							"store": "data_store_3",
							"text": "Search"
						}],
						"columns": [{
							"text": "clientId",
							"dataIndex": "clientId",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Client Code",
							"dataIndex": "client_codeId",
							"width": 96,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Client Name",
							"dataIndex": "client_name",
							"width": 319,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "client_address",
							"dataIndex": "client_address",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "client_telp",
							"dataIndex": "client_telp",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "client_email",
							"dataIndex": "client_email",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Location",
							"dataIndex": "location_name",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "description",
							"dataIndex": "description",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Status",
							"dataIndex": "client_status",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}],
						"selType": "checkboxmodel",
						"selModel": {
							"mode": "MULTI",
							"selType": "checkboxmodel"
						},
						"name": "tvtmasclient_module_real_general_grid_4",
						"layout": {
							"type": "fit"
						},
						"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
					}]
				}, {
					"title": "Client Detail",
					"id": "tvtmasclient_module_real_general_tabpanel_1_tab_2",
					"name": "tvtmasclient_module_real_general_tabpanel_1_tab_2",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"width": 849,
						"url": "tvtmasclient\/call_method\/insertupdate\/table\/sos_tmas_client",
						"method": "POST",
						"height": 708,
						"autoScroll": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "tvtmasclient_module_real_general_form_11",
						"semar": true,
						"xtype": "form",
						"tbar": [
							[{
								"id": "tvtmasclient_module_real_general_form_11_btn_simpan",
								"name": "tvtmasclient_module_real_general_form_11_btn_simpan",
								"itemId": "tvtmasclient_module_real_general_form_11_btn_simpan",
								"text": "Simpan",
								"iconCls": "semar-save",
								"configs": {
									"toolbar": "top",
									"text": "Simpan",
									"iconCls": "semar-save",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvtmasclient_module_real_general_form_11_btn_ubah",
								"name": "tvtmasclient_module_real_general_form_11_btn_ubah",
								"itemId": "tvtmasclient_module_real_general_form_11_btn_ubah",
								"text": "Ubah",
								"iconCls": "semar-edit",
								"configs": {
									"toolbar": "top",
									"text": "Ubah",
									"iconCls": "semar-edit",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvtmasclient_module_real_general_form_11_btn_batal",
								"name": "tvtmasclient_module_real_general_form_11_btn_batal",
								"itemId": "tvtmasclient_module_real_general_form_11_btn_batal",
								"text": "Batal",
								"iconCls": "semar-undo",
								"configs": {
									"toolbar": "top",
									"text": "Batal",
									"iconCls": "semar-undo",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}]
						],
						"name": "tvtmasclient_module_real_general_form_11",
						"defaults": {
							"xtype": "panel",
							"layout": "anchor",
							"anchor": "100%",
							"flex": 1,
							"margin": 5,
							"defaults": {
								"anchor": "100%",
								"flex": 1
							}
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_text_12",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_text_12",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Client Code",
								"name": "client_codeId",
								"allowBlank": false,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_text_12",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_text_13",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_text_13",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Client Name",
								"name": "client_name",
								"allowBlank": false,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_text_13",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_textarea_14",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_textarea_14",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "TextArea 14",
								"fieldLabel": "Client Address",
								"name": "client_address",
								"allowBlank": true,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_textarea_14",
								"semar": true,
								"xtype": "textareafield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_text_15",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_text_15",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Client Telp",
								"name": "client_telp",
								"allowBlank": true,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_text_15",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_text_16",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_text_16",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Client Email",
								"name": "client_email",
								"allowBlank": true,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_text_16",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_text_21",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_text_21",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Location",
								"name": "location_name",
								"allowBlank": false,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_text_21",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_textarea_22",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_textarea_22",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Description",
								"name": "description",
								"allowBlank": true,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_textarea_22",
								"semar": true,
								"xtype": "textareafield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_combo_24",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_combo_24",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Client Status",
								"name": "client_status",
								"allowBlank": true,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_combo_24",
								"semar": true,
								"xtype": "combobox",
								"store": "data_store_23",
								"displayField": "val",
								"valueField": "key",
								"queryMode": "local",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasclient_module_real_general_form_11_blk_form_hidden_25",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasclient_module_real_general_form_11_blk_form_hidden_25",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"name": "clientId",
								"allowBlank": true,
								"id": "tvtmasclient_module_real_general_form_11_blk_i_form_hidden_25",
								"semar": true,
								"xtype": "hiddenfield",
								"items": []
							}]
						}]
					}]
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvtmasclient_module_real_general_grid_7",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_6",
				"columns": [{
					"text": "Location",
					"dataIndex": "location_name",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvtmasclient_module_real_general_grid_7",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvtmasclient_module_real_general_grid_10",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_9",
				"columns": [{
					"text": "Status",
					"dataIndex": "client_status",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvtmasclient_module_real_general_grid_10",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_tvtmasclient, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_tvtmasclient, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_tabpanel_1').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvtmasclient_module_real_general_form_11'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_form_11').getForm().loadRecord((Ext.getCmp('tvtmasclient_module_real_general_grid_4').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('tvtmasclient_module_real_general_grid_4').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasclient_module_real_general_grid_4_itemdblclick = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_grid_4')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_grid_4');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemdblclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_grid_4', 'itemdblclick');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_4_itemdblclick);
						} else {
							tempEl.on('itemdblclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_grid_4', 'itemdblclick');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_4_itemdblclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_form_11').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvtmasclient_module_real_general_form_11'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_tabpanel_1').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasclient_module_real_general_grid_4_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_grid_4_btn_tambah')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_grid_4_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_tambah', 'click');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_4_btn_tambah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_tambah', 'click');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_4_btn_tambah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('tvtmasclient_module_real_general_grid_4').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('clientId'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('tvtmasclient_module_real_general_grid_4_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'tvtmasclient/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'clientId',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('tvtmasclient_module_real_general_grid_4').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_tvtmasclient_module_real_general_grid_4_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_grid_4_btn_hapus')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_grid_4_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_hapus', 'click');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_4_btn_hapus_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_hapus', 'click');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_4_btn_hapus_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('tvtmasclient_module_real_general_grid_4_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('tvtmasclient', //nama module
								Ext.getCmp('tvtmasclient_module_real_general_grid_4').title, //title
								'tvtmasclient_module_real_general_grid_4', //id grid
								'clientId', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_tvtmasclient_module_real_general_grid_4_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_grid_4_btn_excel')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_grid_4_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_excel', 'click');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_4_btn_excel_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_grid_4_btn_excel', 'click');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_4_btn_excel_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_btn_simpan', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_tabpanel_1').setActiveTab(JSON.parse(0));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_form_11').on({
						beforeaction: function() {
							Ext.getCmp('tvtmasclient_module_real_general_form_11').body.mask('Loading...');
						}
					});
					Ext.getCmp('tvtmasclient_module_real_general_form_11').getForm().submit({
						url: Ext.getCmp('tvtmasclient_module_real_general_form_11').url,
						method: Ext.getCmp('tvtmasclient_module_real_general_form_11').method,
						clientValidation: true,
						success: function(form, action) {
							Ext.getCmp('tvtmasclient_module_real_general_form_11').body.unmask();
							Ext.Msg.alert('Success', action.result.msg);
							Ext.getCmp('tvtmasclient_module_real_general_form_11').getForm().setValues(action.result.data);

							//more events - start
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
							//more events - end
						},
						failure: function(form, action) {
							Ext.getCmp('tvtmasclient_module_real_general_form_11').body.unmask();
							switch (action.failureType) {
							case Ext.form.action.Action.CLIENT_INVALID:
								Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
								break;
							case Ext.form.action.Action.CONNECT_FAILURE:
								Ext.Msg.alert('Failure', 'Ajax communication failed');
								break;
							case Ext.form.action.Action.SERVER_INVALID:
								Ext.Msg.alert('Failure', action.result.msg);
							}
						}
					});
				}
			});
			var intervale_tvtmasclient_module_real_general_form_11_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_simpan')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_form_11_btn_simpan', 'click');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_form_11_btn_simpan_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_form_11_btn_simpan', 'click');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_form_11_btn_simpan_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvtmasclient_module_real_general_form_11'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasclient_module_real_general_form_11_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_ubah')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_form_11_btn_ubah', 'click');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_form_11_btn_ubah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_form_11_btn_ubah', 'click');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_form_11_btn_ubah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_btn_batal', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvtmasclient_module_real_general_form_11'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_form_11').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasclient_module_real_general_form_11_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_batal')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_form_11_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_form_11_btn_batal', 'click');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_form_11_btn_batal_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_form_11_btn_batal', 'click');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_form_11_btn_batal_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_form_11_blk_i_form_text_21', 'click', {
				key: 'browserefShow',
				fn: function(callbackIndex) {
					doTestVisualBrowseref('tvtmasclient_module_real_general_browseref_20', JSON.parse(true), callbackIndex);
				}
			});
			var intervale_tvtmasclient_module_real_general_form_11_blk_i_form_text_21_click = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_form_11_blk_i_form_text_21')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_form_11_blk_i_form_text_21');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_form_11_blk_i_form_text_21', 'click');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_form_11_blk_i_form_text_21_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_form_11_blk_i_form_text_21', 'click');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_form_11_blk_i_form_text_21_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_grid_7', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_grid_7', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('tvtmasclient_module_real_general_grid_7');
					if (new String('location_name=location_name').match('=')) {
						var hasAfterCheckComp = new String('location_name=location_name').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						Ext.getCmp('tvtmasclient_module_real_general_grid_4').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasclient_module_real_general_grid_7_itemclick = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_grid_7')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_grid_7');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_grid_7', 'itemclick');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_7_itemclick);
						} else {
							tempEl.on('itemclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_grid_7', 'itemclick');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_7_itemclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasclient_module_real_general_grid_10', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasclient_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasclient_module_real_general_grid_10', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('tvtmasclient_module_real_general_grid_10');
					if (new String('client_status=client_status').match('=')) {
						var hasAfterCheckComp = new String('client_status=client_status').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						Ext.getCmp('tvtmasclient_module_real_general_grid_4').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasclient_module_real_general_grid_10_itemclick = setInterval(function() {
				if (Ext.getCmp('tvtmasclient_module_real_general_grid_10')) {
					var tempEl = Ext.getCmp('tvtmasclient_module_real_general_grid_10');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasclient_module_real_general_grid_10', 'itemclick');
								};
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_10_itemclick);
						} else {
							tempEl.on('itemclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasclient_module_real_general_grid_10', 'itemclick');
							});
							clearInterval(intervale_tvtmasclient_module_real_general_grid_10_itemclick);
						};
					};
				};
			}, 100);
		}
	}
}

<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>