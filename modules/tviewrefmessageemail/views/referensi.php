<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_1', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "message",
		"sortDir": "ASC",
		"persist": false,
		"type": "text",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "emailmsg_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0",
		"name": "emailmsg_typeId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_1",
	"name": "data_model_1",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_5', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "emailmsg_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "emailmsg_typeId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_5",
	"name": "data_model_5",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_1",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tviewrefmessageemail\/call_method\/select\/table\/tviewrefmessageemail",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "emailmsg_typeId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_2",
	"storeId": "data_store_2",
	"name": "data_store_2"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_5",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tviewrefmessageemail\/call_method\/select\/table\/sos_tref_message_email",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "emailmsg_typeId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_6",
	"storeId": "data_store_6",
	"name": "data_store_6"
});
//store define - end
var workspace_tviewrefmessageemail = []
var new_tabpanel_MD = {
	id: 'new_tabpanel_MD_tviewrefmessageemail_module',
	title: 'Ref. Message Email',
	iconCls: 'icon-gears',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Ref. Message Email Form",
				"width": 485,
				"url": "tviewrefmessageemail\/call_method\/insertupdate\/table\/sos_tref_message_email",
				"method": "POST",
				"height": 760,
				"autoScroll": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tviewrefmessageemail_module_real_general_form_4",
				"semar": true,
				"xtype": "form",
				"tbar": [
					[{
						"id": "tviewrefmessageemail_module_real_general_form_4_btn_simpan",
						"name": "tviewrefmessageemail_module_real_general_form_4_btn_simpan",
						"itemId": "tviewrefmessageemail_module_real_general_form_4_btn_simpan",
						"text": "Simpan",
						"iconCls": "semar-save",
						"configs": {
							"toolbar": "top",
							"text": "Simpan",
							"iconCls": "semar-save",
							"type": "normal",
							"active": true
						}
					}],
					[{
						"id": "tviewrefmessageemail_module_real_general_form_4_btn_ubah",
						"name": "tviewrefmessageemail_module_real_general_form_4_btn_ubah",
						"itemId": "tviewrefmessageemail_module_real_general_form_4_btn_ubah",
						"text": "Ubah",
						"iconCls": "semar-edit",
						"configs": {
							"toolbar": "top",
							"text": "Ubah",
							"iconCls": "semar-edit",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "tviewrefmessageemail_module_real_general_form_4_btn_batal",
						"name": "tviewrefmessageemail_module_real_general_form_4_btn_batal",
						"itemId": "tviewrefmessageemail_module_real_general_form_4_btn_batal",
						"text": "Batal",
						"iconCls": "semar-undo",
						"configs": {
							"toolbar": "top",
							"text": "Batal",
							"iconCls": "semar-undo",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}]
				],
				"name": "tviewrefmessageemail_module_real_general_form_4",
				"defaults": {
					"xtype": "panel",
					"layout": "anchor",
					"anchor": "100%",
					"flex": 1,
					"margin": 5,
					"defaults": {
						"anchor": "100%",
						"flex": 1
					}
				},
				"layout": {
					"type": "anchor"
				},
				"items": [{
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewrefmessageemail_module_real_general_form_4_blk_form_combo_7",
					"semar": true,
					"xtype": "panel",
					"name": "tviewrefmessageemail_module_real_general_form_4_blk_form_combo_7",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Message Type",
						"name": "emailmsg_type",
						"allowBlank": true,
						"id": "tviewrefmessageemail_module_real_general_form_4_blk_i_form_combo_7",
						"semar": true,
						"xtype": "combobox",
						"store": "data_store_6",
						"displayField": "emailmsg_type",
						"valueField": "emailmsg_type",
						"queryMode": "local",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewrefmessageemail_module_real_general_form_4_blk_form_textarea_8",
					"semar": true,
					"xtype": "panel",
					"name": "tviewrefmessageemail_module_real_general_form_4_blk_form_textarea_8",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Message",
						"name": "message",
						"allowBlank": false,
						"id": "tviewrefmessageemail_module_real_general_form_4_blk_i_form_textarea_8",
						"semar": true,
						"xtype": "textareafield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "tviewrefmessageemail_module_real_general_form_4_blk_form_hidden_9",
					"semar": true,
					"xtype": "panel",
					"name": "tviewrefmessageemail_module_real_general_form_4_blk_form_hidden_9",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "emailmsg_typeId",
						"allowBlank": true,
						"id": "tviewrefmessageemail_module_real_general_form_4_blk_i_form_hidden_9",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Ref. Message Email List",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tviewrefmessageemail_module_real_general_grid_3",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_2",
				"dockedItems": [{
					"id": "tviewrefmessageemail_module_real_general_grid_3_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_2",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "tviewrefmessageemail_module_real_general_grid_3_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "tviewrefmessageemail_module_real_general_grid_3",
					"store": "data_store_2",
					"text": "Search"
				}],
				"columns": [{
					"text": "ID",
					"dataIndex": "emailmsg_typeId",
					"width": 41,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Message Type",
					"dataIndex": "emailmsg_type",
					"width": 190,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Message",
					"dataIndex": "message",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "checkboxmodel",
				"selModel": {
					"mode": "MULTI",
					"selType": "checkboxmodel"
				},
				"name": "tviewrefmessageemail_module_real_general_grid_3",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_tviewrefmessageemail, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_tviewrefmessageemail, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewrefmessageemail_module_real_general_grid_3').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').on({
						beforeaction: function() {
							Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').body.mask('Loading...');
						}
					});
					Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').getForm().submit({
						url: Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').url,
						method: Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').method,
						clientValidation: true,
						success: function(form, action) {
							Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').body.unmask();
							Ext.Msg.alert('Success', action.result.msg);
							Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').getForm().setValues(action.result.data);

							//more events - start
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
							//more events - end
						},
						failure: function(form, action) {
							Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').body.unmask();
							switch (action.failureType) {
							case Ext.form.action.Action.CLIENT_INVALID:
								Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
								break;
							case Ext.form.action.Action.CONNECT_FAILURE:
								Ext.Msg.alert('Failure', 'Ajax communication failed');
								break;
							case Ext.form.action.Action.SERVER_INVALID:
								Ext.Msg.alert('Failure', action.result.msg);
							}
						}
					});
				}
			});
			var intervale_tviewrefmessageemail_module_real_general_form_4_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_simpan')) {
					var tempEl = Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_simpan', 'click');
								};
							});
							clearInterval(intervale_tviewrefmessageemail_module_real_general_form_4_btn_simpan_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_simpan', 'click');
							});
							clearInterval(intervale_tviewrefmessageemail_module_real_general_form_4_btn_simpan_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_blk_i_form_combo_7').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tviewrefmessageemail_module_real_general_form_4'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tviewrefmessageemail_module_real_general_form_4_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_ubah')) {
					var tempEl = Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_ubah', 'click');
								};
							});
							clearInterval(intervale_tviewrefmessageemail_module_real_general_form_4_btn_ubah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_ubah', 'click');
							});
							clearInterval(intervale_tviewrefmessageemail_module_real_general_form_4_btn_ubah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_batal', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tviewrefmessageemail_module_real_general_form_4'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tviewrefmessageemail_module_real_general_form_4_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_batal')) {
					var tempEl = Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_batal', 'click');
								};
							});
							clearInterval(intervale_tviewrefmessageemail_module_real_general_form_4_btn_batal_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tviewrefmessageemail_module_real_general_form_4_btn_batal', 'click');
							});
							clearInterval(intervale_tviewrefmessageemail_module_real_general_form_4_btn_batal_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tviewrefmessageemail_module_real_general_grid_3', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewrefmessageemail_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewrefmessageemail_module_real_general_grid_3', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tviewrefmessageemail_module_real_general_form_4'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tviewrefmessageemail_module_real_general_grid_3', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('tviewrefmessageemail_module_real_general_form_4').getForm().loadRecord((Ext.getCmp('tviewrefmessageemail_module_real_general_grid_3').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('tviewrefmessageemail_module_real_general_grid_3').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tviewrefmessageemail_module_real_general_grid_3_itemdblclick = setInterval(function() {
				if (Ext.getCmp('tviewrefmessageemail_module_real_general_grid_3')) {
					var tempEl = Ext.getCmp('tviewrefmessageemail_module_real_general_grid_3');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemdblclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tviewrefmessageemail_module_real_general_grid_3', 'itemdblclick');
								};
							});
							clearInterval(intervale_tviewrefmessageemail_module_real_general_grid_3_itemdblclick);
						} else {
							tempEl.on('itemdblclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tviewrefmessageemail_module_real_general_grid_3', 'itemdblclick');
							});
							clearInterval(intervale_tviewrefmessageemail_module_real_general_grid_3_itemdblclick);
						};
					};
				};
			}, 100);
		}
	}
};

<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>