<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller digenerate oleh eXML Builder.
 * Controller untuk module sostrefpanduan.
 *
 */
class sostrefpanduan extends CI_Controller
{

	/**
	 * __construct Controller untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_sostrefpanduan'));
	}


	/**
	 * widget_referensi Controller untuk module $modulename.
	 */
	public function widget_referensi()
	{
		if($this->input->post('id_open')){
			$data['jsscript'] = TRUE;
			$this->load->view('referensi',$data);
		}else{
			$this->load->view('referensi');
		};
	}


	/**
	 * call_method Controller untuk module $modulename.
	 */
	public function call_method($querytype, $typex, $valuex)
	{
		echo json_encode( $this->model_sostrefpanduan->call_method( $typex, $querytype, $valuex ) );
	}
	
	
	function insert_arsip(){
 		$config['upload_path'] = './assets/filemanager/panduan';
 	    
		 
			$config['allowed_types'] = 'pdf|doc';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$upload = $this->upload->do_upload('panduan_file');
		 
		//$config['max_size'] = '5024'; 
 		 $imgs = $this->upload->data(); 
		$attachment1='';
		if($upload==FALSE):
		   //die();
		else:
		   $imgs = $this->upload->data(); 
		   $attachment1 = $imgs['file_name'];
  		endif;
		
		$options = array('panduanId' => $this->input->post('panduanId'));
		$Q = $this->db->get_where('sos_tref_panduan', $options,1);	
		@unlink($config['upload_path'].'/'.base64_decode($Q->row()->file_panduan));
		$data['file_panduan'] = base64_encode($attachment1);
		 
 		
 		if($Q->num_rows() > 0){
 			$data_ID = array('panduanId' =>  $this->input->post('panduanId'));  
			$obj = $this->db->update('sos_tref_panduan',$data, $data_ID);
			$id = $this->input->post('panduanId');
 		}else{  
			$obj = $this->db->insert('sos_tref_panduan' , $data);
			$id = $this->db->insert_id();
		}
		
		
		
  	    if($obj){
		    echo "{success:true, errors: { reason: 'SUKSES', panduanId: ".$id." }}";
		}else{
			echo "{success:false, errors: { reason: 'Gagal Upload !' }}";	
		}
	}
	
	 function ext_insert(){   
	    
 		$Status = $this->model_sostrefpanduan->Insert_Data();	 
		if($Status == "Exist"){
			echo "{success:false, info: { reason: 'Panduan sudah ada !' }}";
		}elseif($Status == "Updated"){
			echo "{success:true, info: { reason: 'Success merubah data !' }}";
		}elseif(is_numeric($Status)){
			echo "{success:true, info: { reason: '".$Status."' }}";
		}else{
			echo "{success:false, info: { reason: 'Gagal menambah Data !' }}";
		}  
   }

}
