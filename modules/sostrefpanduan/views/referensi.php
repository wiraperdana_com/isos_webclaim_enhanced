<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_1', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "1",
		"name": "status_data",
		"sortDir": "ASC",
		"persist": false,
		"type": "smallint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "CURRENT_TIMESTAMP",
		"name": "createdDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "createdBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "file_panduan",
		"sortDir": "ASC",
		"persist": false,
		"type": "text",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "nama_panduan",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "panduanId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_1",
	"name": "data_model_1",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_1",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostrefpanduan\/call_method\/select\/table\/sos_tref_panduan",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "panduanId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_2",
	"storeId": "data_store_2",
	"name": "data_store_2"
});
//store define - end


//FORM ARSIP PDF
var Form_Panduan_PDF = new Ext.create('Ext.form.Panel', {
  id: 'Form_Panduan_PDF', url: BASE_URL + 'sostrefpanduan/insert_arsip/pdf', fileUpload: true,  border:false,
  frame: false, width: '100%', height: 100, margins: '0 5 0 0', defaults: {anchor: '100%', allowBlank: true, msgTarget: 'side',labelWidth: 50},
  items:[ 
      {
       xtype:'fieldset', title :'PDF Files',width: 625, 
       items: [{xtype: 'fieldcontainer', layout: 'hbox', defaults: {hideLabel: true}, combineErrors: false,
                items:[{xtype : 'hidden', name :'panduanId', id :'panduanId_pdf'},
                        {xtype: 'filefield', name: 'panduan_file', id:'filepanduan', emptyText: 'Upload Files', disabled:true,
                        buttonText: '', buttonConfig: {iconCls: 'icon-image_add'}, margins: '0 5 0 0', width: 425,
                        listeners: {
                          'change': function(){
                            if(Form_Panduan_PDF.getForm().isValid()){                                
                              Form_Panduan_PDF.getForm().submit({
                                waitMsg: 'Sedang meng-upload ...',
                                success: function(form, action) {
                                  obj = Ext.decode(action.response.responseText);
                                 
                                  if(obj.errors.reason == 'SUKSES'){
                                      Ext.getCmp('panduanId_pdf').setValue(obj.errors.panduanId); 
                                      Ext.getCmp('panduanId').setValue(obj.errors.panduanId);
                                      Ext.MessageBox.show({title:'Informasi !', msg: 'Sukses upload Data !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});
                                  } 
                                },
                                failure: function(form, action){
                                  obj = Ext.decode(action.response.responseText);
                                  Ext.MessageBox.show({title:'Gagal Upload !', msg: obj.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                                }, 
                                scope: this
                              });
                            }
                          }
                        }
                      },
                      {xtype: 'button', text: 'Hapus', id:'Btn_Hapus_panduan', tooltip: {text: 'Hapus Arsip Digital'}, handler: function() {Reset_panduan();}, margins: '0 5 0 0'},
                      {xtype: 'button', text: 'Download', id:'Btn_Download_panduan', target: '_blank', tooltip: {text: 'Download Arsip Digital'}, handler: function() { Download_panduan('files');}
                      }]//fieldcontainer 
      }]//Fieldset
   }]
});

var workspace_sostrefpanduan = []
var new_tabpanel_MD = {
	id: 'new_tabpanel_MD_sostrefpanduan_module',
	title: 'Ref. Panduan',
	iconCls: 'icon-gears',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Ref. Panduan Detail",
				"width": 485,
				"url": "sostrefpanduan/ext_insert",
				"method": "POST",
				"height": 760,
				"autoScroll": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostrefpanduan_module_real_general_form_4",
				"semar": true,
				"xtype": "form",
				"tbar": [
					[
                     {
						"id": "sostrefpanduan_module_real_general_form_4_btn_simpan",
						"name": "sostrefpanduan_module_real_general_form_4_btn_simpan",
						"itemId": "sostrefpanduan_module_real_general_form_4_btn_simpan",
						"text": "Simpan",
						"iconCls": "semar-save",
						"configs": {
							"toolbar": "top",
							"text": "Simpan",
							"iconCls": "semar-save",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sostrefpanduan_module_real_general_form_4_btn_ubah",
						"name": "sostrefpanduan_module_real_general_form_4_btn_ubah",
						"itemId": "sostrefpanduan_module_real_general_form_4_btn_ubah",
						"text": "Ubah",
						"iconCls": "semar-edit",
						"configs": {
							"toolbar": "top",
							"text": "Ubah",
							"iconCls": "semar-edit",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sostrefpanduan_module_real_general_form_4_btn_batal",
						"name": "sostrefpanduan_module_real_general_form_4_btn_batal",
						"itemId": "sostrefpanduan_module_real_general_form_4_btn_batal",
						"text": "Batal",
						"iconCls": "semar-undo",
						"configs": {
							"toolbar": "top",
							"text": "Batal",
							"iconCls": "semar-undo",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}]
				],
				"name": "sostrefpanduan_module_real_general_form_4",
				"defaults": {
					"xtype": "panel",
					"layout": "anchor",
					"anchor": "100%",
					"flex": 1,
					"margin": 5,
					"defaults": {
						"anchor": "100%",
						"flex": 1
					}
				},
				"layout": {
					"type": "anchor"
				},
				"items": [{
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sostrefpanduan_module_real_general_form_4_blk_form_text_5",
					"semar": true,
					"xtype": "panel",
					"name": "sostrefpanduan_module_real_general_form_4_blk_form_text_5",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{xtype : 'hidden', name :'panduanId', id :'panduanId'}, 
                       {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Nama Panduan",
						"name": "nama_panduan",
						"allowBlank": false,
						"id": "sostrefpanduan_module_real_general_form_4_blk_i_form_text_5",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sostrefpanduan_module_real_general_form_4_blk_form_textarea_10",
					"semar": true,
					"xtype": "panel",
					"name": "sostrefpanduan_module_real_general_form_4_blk_form_textarea_10",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Description",
						"name": "description",
						"allowBlank": false,
						"id": "sostrefpanduan_module_real_general_form_4_blk_i_form_textarea_10",
						"semar": true,
						"xtype": "htmleditor",
						"items": []
					}]
				}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "ttransinboundmanifest_module_real_general_form_22_ar_0_general_form_22_blk_form_file_35",
								"semar": true,
								"xtype": "panel",
								"name": "ttransinboundmanifest_module_real_general_form_22_ar_0_general_form_22_blk_form_file_35",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [Form_Panduan_PDF]
					 
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Ref. Panduan List",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostrefpanduan_module_real_general_grid_3",
				"semar": true,
				"xtype": "grid",
				"tbar": [
					[{
						"id": "sostrefpanduan_module_real_general_grid_3_btn_tambah",
						"name": "sostrefpanduan_module_real_general_grid_3_btn_tambah",
						"itemId": "sostrefpanduan_module_real_general_grid_3_btn_tambah",
						"text": "Tambah",
						"iconCls": "semar-add",
						"configs": {
							"toolbar": "top",
							"text": "Tambah",
							"iconCls": "semar-add",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sostrefpanduan_module_real_general_grid_3_btn_hapus",
						"name": "sostrefpanduan_module_real_general_grid_3_btn_hapus",
						"itemId": "sostrefpanduan_module_real_general_grid_3_btn_hapus",
						"text": "Hapus",
						"iconCls": "semar-delete",
						"configs": {
							"toolbar": "top",
							"text": "Hapus",
							"iconCls": "semar-delete",
							"type": "delete",
							"value": "sos_tref_panduan",
							"split": true,
							"active": true
						}
					}, "-"],
					[{
						"id": "sostrefpanduan_module_real_general_grid_3_btn_excel",
						"name": "sostrefpanduan_module_real_general_grid_3_btn_excel",
						"itemId": "sostrefpanduan_module_real_general_grid_3_btn_excel",
						"text": "Excel",
						"iconCls": "semar-excel",
						"configs": {
							"toolbar": "top",
							"text": "Excel",
							"iconCls": "semar-excel",
							"type": "excel",
							"value": "sos_tref_panduan",
							"active": true
						}
					}]
				],
				"store": "data_store_2",
				"dockedItems": [{
					"id": "sostrefpanduan_module_real_general_grid_3_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_2",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "sostrefpanduan_module_real_general_grid_3_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "sostrefpanduan_module_real_general_grid_3",
					"store": "data_store_2",
					"text": "Search"
				}],
				"columns": [{
					"text": "panduanId",
					"dataIndex": "panduanId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Nama Panduan",
					"dataIndex": "nama_panduan",
					"width": 205,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "File Panduan",
					"dataIndex": "file_panduan",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Description",
					"dataIndex": "description",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "createdBy",
					"dataIndex": "createdBy",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "createdDate",
					"dataIndex": "createdDate",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "updatedBy",
					"dataIndex": "updatedBy",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "updatedDate",
					"dataIndex": "updatedDate",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "status_data",
					"dataIndex": "status_data",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "checkboxmodel",
				"selModel": {
					"mode": "MULTI",
					"selType": "checkboxmodel"
				},
				"name": "sostrefpanduan_module_real_general_grid_3",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_sostrefpanduan, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_sostrefpanduan, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostrefpanduan_module_real_general_grid_3').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('sostrefpanduan_module_real_general_form_4').on({
						beforeaction: function() {
							Ext.getCmp('sostrefpanduan_module_real_general_form_4').body.mask('Loading...');
						}
					});
					Ext.getCmp('sostrefpanduan_module_real_general_form_4').getForm().submit({          			
                            success: function(form, action){
                                Ext.getCmp('sostrefpanduan_module_real_general_form_4').body.unmask(); 
                                obj = Ext.decode(action.response.responseText);
                                if(IsNumeric(obj.info.reason)){ 
                                    Ext.MessageBox.show({title:'Informasi !', msg: 'Sukses menambah data !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});  			
                                }else{
                                    Ext.MessageBox.show({title:'Informasi !', msg: obj.info.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});
                                }
                                Ext.getCmp('sostrefpanduan_module_real_general_grid_3').getStore().load();
								Ext.getCmp('filepanduan').disable(true);
                            },
                            failure: function(form, action){
                                Ext.getCmp('sostrefpanduan_module_real_general_form_4').body.unmask();
                              if (action.failureType == 'server') {
                                obj = Ext.decode(action.response.responseText);
                                Ext.MessageBox.show({title:'Peringatan !', msg: obj.info.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                              }else{
                                if (typeof(action.response) == 'undefined') {
                                    Ext.MessageBox.show({title:'Peringatan !', msg: 'Silahkan isi dengan benar !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                                }else{
                                    Ext.MessageBox.show({title:'Peringatan !', msg: 'Server tidak dapat dihubungi !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                                }
                              }
                            }
                          });
				}
			});
			var intervale_sostrefpanduan_module_real_general_form_4_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_simpan')) {
					var tempEl = Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_simpan', 'click');
									
								};
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_form_4_btn_simpan_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_simpan', 'click');
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_form_4_btn_simpan_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(false));
					
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					Ext.getCmp('filepanduan').enable(true);
					semarDeactiveAllX(Ext.getCmp('sostrefpanduan_module_real_general_form_4'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostrefpanduan_module_real_general_form_4_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_ubah')) {
					var tempEl = Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_ubah', 'click');
								};
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_form_4_btn_ubah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_ubah', 'click');
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_form_4_btn_ubah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_batal', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostrefpanduan_module_real_general_form_4'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sostrefpanduan_module_real_general_form_4').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostrefpanduan_module_real_general_form_4_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_batal')) {
					var tempEl = Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_batal', 'click');
								};
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_form_4_btn_batal_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostrefpanduan_module_real_general_form_4_btn_batal', 'click');
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_form_4_btn_batal_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostrefpanduan_module_real_general_grid_3', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostrefpanduan_module_real_general_grid_3', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostrefpanduan_module_real_general_form_4'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostrefpanduan_module_real_general_grid_3', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
                 var rec = Ext.getCmp('sostrefpanduan_module_real_general_grid_3').getSelectionModel().getSelection()[0];
 				 Ext.getCmp('panduanId_pdf').setValue(rec.get('panduanId')); 
                 	Ext.getCmp('sostrefpanduan_module_real_general_form_4').getForm().loadRecord((Ext.getCmp('sostrefpanduan_module_real_general_grid_3').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('sostrefpanduan_module_real_general_grid_3').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostrefpanduan_module_real_general_grid_3_itemdblclick = setInterval(function() {
				if (Ext.getCmp('sostrefpanduan_module_real_general_grid_3')) {
					var tempEl = Ext.getCmp('sostrefpanduan_module_real_general_grid_3');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemdblclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostrefpanduan_module_real_general_grid_3', 'itemdblclick');
								};
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_grid_3_itemdblclick);
						} else {
							tempEl.on('itemdblclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostrefpanduan_module_real_general_grid_3', 'itemdblclick');
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_grid_3_itemdblclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostrefpanduan_module_real_general_form_4_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostrefpanduan_module_real_general_form_4'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sostrefpanduan_module_real_general_form_4').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostrefpanduan_module_real_general_grid_3_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('sostrefpanduan_module_real_general_grid_3_btn_tambah')) {
					var tempEl = Ext.getCmp('sostrefpanduan_module_real_general_grid_3_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_tambah', 'click');
								};
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_grid_3_btn_tambah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_tambah', 'click');
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_grid_3_btn_tambah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('sostrefpanduan_module_real_general_grid_3').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('panduanId'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('sostrefpanduan_module_real_general_grid_3_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'sostrefpanduan/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'panduanId',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('sostrefpanduan_module_real_general_grid_3').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_sostrefpanduan_module_real_general_grid_3_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('sostrefpanduan_module_real_general_grid_3_btn_hapus')) {
					var tempEl = Ext.getCmp('sostrefpanduan_module_real_general_grid_3_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_hapus', 'click');
								};
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_grid_3_btn_hapus_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_hapus', 'click');
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_grid_3_btn_hapus_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('sostrefpanduan_module_real_general_grid_3_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('sostrefpanduan', //nama module
								Ext.getCmp('sostrefpanduan_module_real_general_grid_3').title, //title
								'sostrefpanduan_module_real_general_grid_3', //id grid
								'panduanId', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_sostrefpanduan_module_real_general_grid_3_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('sostrefpanduan_module_real_general_grid_3_btn_excel')) {
					var tempEl = Ext.getCmp('sostrefpanduan_module_real_general_grid_3_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_excel', 'click');
								};
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_grid_3_btn_excel_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('sostrefpanduan_module_real_general_grid_3_btn_excel', 'click');
							});
							clearInterval(intervale_sostrefpanduan_module_real_general_grid_3_btn_excel_click);
						};
					};
				};
			}, 100);
		}
	}
};

<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>