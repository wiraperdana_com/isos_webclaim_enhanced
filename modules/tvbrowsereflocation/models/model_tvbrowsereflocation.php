<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Model digenerate oleh eXML Builder.
 * Model untuk module tvbrowsereflocation.
 *
 */
class model_tvbrowsereflocation extends MY_Model
{

	/**
	 * __construct Model untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * _load_model Model untuk module $modulename.
	 */
	public function _load_model($model_name)
	{
		$CI =& get_instance();
		$CI->load->model($model_name);
		return $CI->$model_name;
	}


	/**
	 * call_method Model untuk module $modulename.
	 */
	public function call_method($typex, $querytype, $valuex)
	{
		$result = array( 'status' => false, 'data' => array(), 'total' => 0 );
		if( $typex != null ) {
			switch( $typex ) {
				case ( 'table' ) :
				case ( 'view' ) :
					$result = $this->doTableView( $querytype, $valuex );
					break;
				case ( 'procedure' ) :
				case ( 'function' ) :
					$result = $this->doProc( $querytype, $valuex );
					break;
			};
		};
		return $result;
	}


	/**
	 * _do_select Model untuk module $modulename.
	 */
	public function _do_select($valuex)
	{
		$this->db->select('*');
		$this->db->from( $valuex );
		$fields = $this->db->field_data( $valuex );
		foreach( $fields as $k => $v ) {
			if( isset( $_REQUEST[ $v->name ] ) ){
				$this->db->where( $v->name, $_REQUEST[ $v->name ] );
			};
		};
		$Filters_Model = $this->_load_model('filters_model');
		$Filters_Model->get_SORT();
		$Filters_Model->get_SORT_Get();
		$Filters_Model->get_FILTER();
	}


	/**
	 * _do_insertupdate_data Model untuk module $modulename.
	 */
	public function _do_insertupdate_data($valuex)
	{
		$result = array( 'success' => false, 'msg' => 'Unknow', 'data' => [] );
		if( $this->db->table_exists( $valuex ) ) {
			$fields = $this->db->field_data( $valuex );
			$primary_key = array();
			$fields_keys = array();
			foreach( $fields as $k => $v ) {
				if( $v->primary_key > 0 ) {
					$primary_key[] = $v->name;
				};
				$fields_keys[] = $v->name;
			};
			$data = array();
			foreach( $fields_keys as $fk ){
				$temp = $this->input->post( $fk );
				if( $temp && $temp != $fk ){
					if( strlen( $temp ) > 0 ) {
						$data[ $fk ] = $temp;
					};
				};
			};
			$where_data = array();
			if( count( $primary_key ) > 0 ) {
				foreach( $primary_key as $pk ){
					if( isset( $data[ $pk ] ) ) {
						$where_data[ $pk ] = $data[ $pk ];
						unset( $data[ $pk ] );
					};
				};
			};
			$merge_all = array_merge( $data, $where_data );
			if( count( $where_data ) > 0 ) {
				$this->db->update( $valuex, $data, $where_data );
				$result = array( 'success' => true, 'msg' => 'Success Update Data', 'data' => $merge_all );
			}else{
				$pre_merge = array();
				$this->db->insert( $valuex, $data );
				$last_id = $this->db->insert_id();
				if( count( $primary_key ) > 0 ) {
					foreach( $primary_key as $pk ){
						$pre_merge[ $pk ] = $last_id;
					};
				};
				$merge_all = array_merge( $merge_all, $pre_merge );
				$result = array( 'success' => true, 'msg' => 'Success Insert Data', 'data' => $merge_all );
			};
		};
		return $result;
	}


	/**
	 * _do_delete_data Model untuk module $modulename.
	 */
	public function _do_delete_data($valuex)
	{
		$result = array( 'success' => false, 'msg' => 'Unknow');
		if( $this->db->table_exists( $valuex ) ) {
			$fields = $this->db->field_data( $valuex );
			$primary_key = array();
			$fields_keys = array();
			foreach( $fields as $k => $v ) {
				$fields_keys[] = $v->name;
			};
			if( in_array( $this->input->post('where'), $fields_keys) && $this->input->post('postdata') ) {
				$this->db->where_in( $this->input->post('where'), explode('-',$this->input->post('postdata')) );
				$this->db->delete( $valuex );
				$result = array( 'success' => true, 'msg' => 'Success Delete Data' );
			};
		};
		return $result;
	}


	/**
	 * _do_all_data Model untuk module $modulename.
	 */
	public function _do_all_data($valuex)
	{
		$data = array();
		$this->_do_select( $valuex );
		$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
		$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 20;
		$this->db->limit($limit, $start);
		$Q = $this->db->get();
		foreach ($Q->result() as $obj) {
			$data[] = $obj;
		};
		return $data;
	}


	/**
	 * _do_count_data Model untuk module $modulename.
	 */
	public function _do_count_data($valuex)
	{
		$this->_do_select( $valuex );
		return $this->db->count_all_results();
	}


	/**
	 * doTableView Model untuk module $modulename.
	 */
	public function doTableView($querytype, $valuex)
	{
		$result = array( 'status' => false, 'data' => array(), 'total' => 0 );
		switch( $querytype ) {
			case 'select' :
				$result = array( 'status' => true, 'data' => $this->_do_all_data( $valuex ), 'total' => $this->_do_count_data( $valuex ) );
				break;
			case 'insertupdate' :
				$result = $this->_do_insertupdate_data( $valuex );
				break;
			case 'delete' :
				$result = $this->_do_delete_data( $valuex );
				break;
		};
		return $result;
	}


	/**
	 * doProc Model untuk module $modulename.
	 */
	public function doProc($querytype, $valuex)
	{
		$datax = null;
		$params = array();
		if( $this->input->post('params' ) ){
			$tempparams = @json_decode( $this->input->post('params' ) );
			foreach( $tempparams as $k => $v ) {
				settype( $v->value, $v->type );
				$v->value = ( ( $v->type == 'string' ) ? '"' . $v->value . '"' : $v->value );
				$params[ ( int ) $v->position - 1 ] = $v->value;
			};
		};
		if( $querytype == 'select' ){
			$sqlBuild = 'select '.$valuex.'( '. implode( $params, ',' ) .' ) as ' . $valuex;
			$temp = $this->db->query( $sqlBuild )->result();
			foreach( $temp as $k => $v ){
				$datax = array( $valuex => $v->{$valuex} );
			};
		}else if( $querytype == 'call' ){
			$sqlBuild = 'call '.$valuex.'( '. implode( $params, ',' ) .' )';
			$this->db->query( $sqlBuild );
		}
		$result = array( 'status' => false, 'data' => $datax, 'total' => 0 );
		return $result;
	}

}
