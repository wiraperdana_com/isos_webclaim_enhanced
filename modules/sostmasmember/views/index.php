<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//==================================== IMPORT DATA ABSENSI ============================================ START

            // Ext.define('Form_ImportDataAbsensi',{
            //     extend:'Ext.window.Window',
            //     id: 'Form_ImportDataAbsensi',
            //     border:false,
            //     title:'Import Master Absensi',
            //     width:410,
            //     modal:true,
            //     items:[{xtype: 'Win_Absensi_ImportDataAbsensi', id:'Form_Import_Absensi'}],
            //     bbar: new Ext.ux.StatusBar({text: 'Ready', id: 'sbWin_Import_Absensi', iconCls: 'x-status-valid'})
            // });
            // 
            function showForm_Import_member() {
                // Ext.getCmp('items-body').body.mask("Loading...", "x-mask-loading"); 

                var Form_Import_Absensi = new Ext.create('Ext.form.Panel', {
                    // Ext.define('Win_Absensi_ImportDataAbsensi',{
                    // extend:'Ext.form.Panel',
                    // xtype: 'Win_Absensi_ImportDataAbsensi',
                    id: 'Form_Import_Absensi',
                    url: BASE_URL + 'sostmasmember/import_member',
                    bodyStyle: 'padding:10px',
                    width: 400,
                    fieldDefaults: {
                        msgTarget: 'side',
                        labelWidth: 125
                    },
                    items: [
                   
                        {
                            xtype: 'fileuploadfield',
                            name: 'file_upload',
                            emptyText: '-',
                            fieldLabel: 'File Excel',
                            width: 350
                        }],
                    buttons: [{
                            text: 'Import',
                            handler: function() {
                                Ext.getCmp('Form_Import_Absensi').body.mask();
                                Panel_Form_Import_Absensi.on({
                                    beforeaction: function() {
                                        Ext.getCmp('sbWin_Import_Absensi').showBusy();
                                    }
                                });
                                Ext.getCmp('Form_Import_Absensi').submit({
                                    success: function(form, action) {
                                        Ext.getCmp('Form_Import_Absensi').body.unmask();
                              
                                        obj = Ext.decode(action.response.responseText);
                                        if (obj.success){
											Ext.getStore('data_store_2').load();
                                        };
                                        var status = (obj.success) ? 'x-status-valid' : 'x-status-error';
                                        Ext.getCmp('sbWin_Import_Absensi').setStatus({text: obj.info.message, iconCls: status});
                                    },
                                    failure: function(form, action) {
                                        Ext.getCmp('Form_Import_Absensi').body.unmask();
                                        if (action.failureType == 'server') {
                                            obj = Ext.decode(action.response.responseText);
                                            Ext.getCmp('sbWin_Import_Absensi').setStatus({text: obj.info.message, iconCls: 'x-status-error'});
                                        } else {
                                            if (typeof (action.response) == 'undefined') {
                                                Ext.getCmp('sbWin_Import_Absensi').setStatus({text: 'Silahkan isi dengan benar !', iconCls: 'x-status-error'});
                                            } else {
                                                Ext.getCmp('sbWin_Import_Absensi').setStatus({text: 'Server tidak dapat dihubungi !', iconCls: 'x-status-error'});
                                            }
                                        }
                                    }
                                });
                            },
                        }]
                });

                var Panel_Form_Import_Absensi = new Ext.create('Ext.Window', {
                    id: 'Panel_Form_Import_Absensi',
                    border: false,
                    title: 'Import Master Member',
                    width: 410,
                    modal: true,
                    items: [Form_Import_Absensi],
                    bbar: new Ext.ux.StatusBar({text: 'Ready', id: 'sbWin_Import_Absensi', iconCls: 'x-status-valid'})
                }).show();

            }

            //======================================================================================================


//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_1', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "1",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	},{
		"dateFormat": null,
		"defaultValue": "1",
		"name": "status_data",
		"sortDir": "ASC",
		"persist": false,
		"type": "smallint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "CURRENT_TIMESTAMP",
		"name": "createdDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "createdBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "dental_limit",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "update_code",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "activation_date",
		"sortDir": "ASC",
		"persist": false,
		"type": "date",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "status_card",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "employment_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "room_class",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "plan_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_region",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_email",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_telp",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_address",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "tanggal_lahir",
		"sortDir": "ASC",
		"persist": false,
		"type": "date",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "jenis_kelamin",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "relationship_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "employee_ID",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_cardNo",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "memberId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_1",
	"name": "data_model_1",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_4', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "region_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "regionId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_4",
	"name": "data_model_4",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_8', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_8",
	"name": "data_model_8",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_14', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "relationship_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "relationshiptypeId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_14",
	"name": "data_model_14",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_24', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "locationId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_24",
	"name": "data_model_24",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_29', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "plan_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "plantypeId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_29",
	"name": "data_model_29",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_33', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "employment_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "employmentstatusId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_33",
	"name": "data_model_33",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_1",
	"autoLoad": false,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 100,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/tvtmasmember",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "memberId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_2",
	"storeId": "data_store_2",
	"name": "data_store_2"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_4",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/sos_tref_region",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "id",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_5",
	"storeId": "data_store_5",
	"name": "data_store_5",
	listeners:{
		load:function(){
			this.insert(0,{region_name:'All'});
		}
	}
});
Ext.create('Ext.data.Store', {
	"model": "data_model_8",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/sos_tmas_client",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "id",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_9",
	"storeId": "data_store_9",
	"name": "data_store_9"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_14",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/sos_tref_relationship_type",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "id",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_15",
	"storeId": "data_store_15",
	"name": "data_store_15"
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [{
		"key": "L",
		"val": "Laki - laki"
	}, {
		"key": "P",
		"val": "Perempuan"
	}],
	"id": "data_store_18",
	"storeId": "data_store_18",
	"name": "data_store_18"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_24",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/sos_tref_location",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "id",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_25",
	"storeId": "data_store_25",
	"name": "data_store_25"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_29",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/sos_tref_plan_type",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "id",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_30",
	"storeId": "data_store_30",
	"name": "data_store_30"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_33",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/sos_tref_employment_status",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "id",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_34",
	"storeId": "data_store_34",
	"name": "data_store_34",
	listeners:{
		load:function(){
			this.insert(0,{employment_status:'All'});
		}
	}
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [
	{
		"key": "All",
		"val": "All"
	}, {
		"key": "Inactive",
		"val": "Inactive"
	}, {
		"key": "Active",
		"val": "Active"
	}, {
		"key": "Pending",
		"val": "Pending"
	}],
	"id": "data_store_36",
	"storeId": "data_store_36",
	"name": "data_store_36"
});
//store define - end

// TABEL GRID MEMBER  ------------------------------------------------- START
var Params_T_D_Family;
Ext.define('MT_D_Family', {extend: 'Ext.data.Model',
  fields: ['memberId','client_name','member_cardNo', 'employee_ID', 'member_name',
  		   'relationship_type','jenis_kelamin', 'tgl_lahir',
           'location_name','location_region','plan_desc','room_class','employment_status',
           'status_card','dental_limit'  ]
});
var Reader_T_D_Family = new Ext.create('Ext.data.JsonReader', {
  id: 'Reader_T_D_Family', root: 'results', totalProperty: 'total', idProperty: 'memberId'  	
});

var Proxy_T_D_Family = new Ext.create('Ext.data.AjaxProxy', {
  id: 'Proxy_T_D_Family', url: BASE_URL + 'dashboardproviderv1/getMemberdependent', actionMethods: {read:'POST'}, extraParams: {id_open:1},
  reader: Reader_T_D_Family,
  afterRequest: function(request, success) {
   	Params_T_D_Family = request.operation.params;
  }
});

var Data_T_D_Family = new Ext.create('Ext.data.Store', {
	id: 'Data_T_D_Family', model: 'MT_D_Family', pageSize: 20, noCache: false, autoLoad: false,
  proxy: Proxy_T_D_Family
});
 
var Grid_T_D_Family = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_D_Family',  store: Data_T_D_Family, frame: true, border: true, loadMask: true, noCache: false,
    style: 'margin:0 auto;', autoHeight: true, columnLines: true,  
	columns: [
  	{header:'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .5px;'}, 
  	{header: "Client", dataIndex: 'client_name', width: 240}, 
    {header: "No. Kartu Member", dataIndex: 'member_cardNo', width: 140}, 
    {header: "NIK", dataIndex: 'employee_ID', width: 140, hidden: true}, 
    {header: "Nama Member", dataIndex: 'member_name', width: 240}, 
    {header: "Tipe Relasi", dataIndex: 'relationship_type', width: 140}, 
    {header: "Jns Kelamin", dataIndex: 'jenis_kelamin', width: 50}, 
  	{header: "Tgl Lahir", dataIndex: 'tgl_lahir', width: 120, format:'d-m-Y'},
    {header: "Lokasi", dataIndex: 'location_region', width: 170}, 
    {header: "Wilayah", dataIndex: 'location_region', width: 170}, 
    {header: "Hak Kelas Perawatan", dataIndex: 'plan_desc',  width: 140}, 
    {header: "Kelas Ruangan", dataIndex: 'room_class',  width: 140, hidden: true}, 
    {header: "Status Pekerjaan", dataIndex: 'employment_status',  width: 100}, 
    {header: "Status Kartu", dataIndex: 'status_card', flex:1}, 
    {header: "Hak Rawat Gigi", dataIndex: 'dental_limit', flex:1}, 
  ],  
  dockedItems: [{xtype: 'pagingtoolbar', store: Data_T_D_Family, dock: 'bottom', displayInfo: true,
  listeners :{
      beforechange: function() { 					     
	   Data_T_D_Family.getProxy().extraParams.member_cardNo = Ext.getCmp('t_member_cardNo').getValue();  
  	}   
  }
  }]  
});

var workspace_sostmasmember = []
var new_tabpanel = {
	id: 'newtab_sostmasmember_module',
	title: 'Master Member',
	iconCls: 'semar-group',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 8,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"margins": {
				"top": 4,
				"right": 4,
				"bottom": 4,
				"left": 4,
				"height": 8,
				"width": 8
			},
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasmember_module_real_general_tabpanel_3",
				"semar": true,
				"xtype": "tabpanel",
				"name": "sostmasmember_module_real_general_tabpanel_3",
				"defaults": {
					"layout": {
						"type": "vbox",
						"align": "stretch",
						"pack": "start"
					},
					"flex": 1
				},
				"layout": {
					"type": "card"
				},
				"items": [{
					"frame": false,
					"border": true,
					"disabled": false,
					"flex": 1,
					"title": "Member List",
					"loadMask": true,
					"noCache": false,
					"autoHeight": true,
					"columnLines": false,
					"invalidateScrollerOnRefresh": true,
					"margins": {
						"top": 0,
						"right": 0,
						"bottom": 0,
						"left": 0,
						"height": 0,
						"width": 0
					},
					"id": "sostmasmember_module_real_general_grid_3",
					"semar": true,
					"xtype": "grid",
					"tbar": [
						/*[{
							"id": "sostmasmember_module_real_general_grid_3_btn_tambah",
							"name": "sostmasmember_module_real_general_grid_3_btn_tambah",
							"itemId": "sostmasmember_module_real_general_grid_3_btn_tambah",
							"text": "Tambah",
							"iconCls": "semar-add",
							"configs": {
								"toolbar": "top",
								"text": "Tambah",
								"iconCls": "semar-add",
								"type": "normal",
								"value": "true",
								"active": true
							}
						}],*/
						/*[{
							"id": "sostmasmember_module_real_general_grid_3_btn_hapus",
							"name": "sostmasmember_module_real_general_grid_3_btn_hapus",
							"itemId": "sostmasmember_module_real_general_grid_3_btn_hapus",
							"text": "Hapus",
							"iconCls": "semar-delete",
							"configs": {
								"toolbar": "top",
								"text": "Hapus",
								"iconCls": "semar-delete",
								"type": "delete",
								"value": "sos_tmas_member",
								"split": true,
								"active": true
							}
						}],*/
						[{
							"id": "sostmasmember_module_real_general_grid_3_btn_excel",
							"name": "sostmasmember_module_real_general_grid_3_btn_excel",
							"itemId": "sostmasmember_module_real_general_grid_3_btn_excel",
							"text": "Excel",
							"iconCls": "semar-excel",
							"configs": {
								"toolbar": "top",
								"text": "Excel",
								"iconCls": "semar-excel",
								"type": "excel",
								"value": "sos_tmas_member",
								"active": true
							}
						},"-"],
						[{
							"id": "sostmasmember_module_real_general_grid_3_btn_upload",
							"name": "sostmasmember_module_real_general_grid_3_btn_upload",
							"itemId": "sostmasmember_module_real_general_grid_3_btn_upload",
							"text": "Upload",
							"iconCls": "icon-menu_import",
							"listeners": {
									click: function() {
										// Obj_Form_ImportDataAbsensi= new Form_ImportDataAbsensi();
										// Obj_Form_ImportDataAbsensi.show();
										showForm_Import_member();
									}
								}
							
						}],['->'],
						[{
							"id": "sostmasmember_module_real_general_grid_3_btn_resetfilter",
							"name": "sostmasmember_module_real_general_grid_3_btn_resetfilter",
							"itemId": "sostmasmember_module_real_general_grid_3_btn_resetfilter",
							"text": "ResetFilter",
							"iconCls": "semar-undo",
							"configs": {
								"toolbar": "top",
								"text": "Upload",
								"iconCls": "semar-undo",
								"type": "normal",
								"value": "true",
								"active": true
							}
						}]
					],
					"store": "data_store_2",
					"dockedItems": [{
						"id": "sostmasmember_module_real_general_grid_3_pagingtoolbar",
						"hidden": false,
						"xtype": "pagingtoolbar",
						"store": "data_store_2",
						"dock": "bottom",
						"displayInfo": true
					}, {
						"id": "sostmasmember_module_real_general_grid_3_searchfield",
						"dock": "top",
						"hidden": false,
						"xtype": "semarwidgetsearchtoolbar",
						"grid_id": "sostmasmember_module_real_general_grid_3",
						"store": "data_store_2",
						"text": "Search"
					}],
					"columns": [{
						"text": "Client",
						"dataIndex": "client_name",
						"width": 200,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					},{
						"text": "ID",
						"dataIndex": "memberId",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Client ID",
						"dataIndex": "clientId",
						"width": 37,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "No. Kartu",
						"dataIndex": "member_cardNo",
						"width": 111,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "NIK",
						"dataIndex": "employee_ID",
						"width": 104,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Nama Member",
						"dataIndex": "member_name",
						"width": 109,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Tipe Relasi",
						"dataIndex": "relationship_type",
						"width": 97,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Jns Kelamin",
						"dataIndex": "jenis_kelamin",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Tgl Lahir",
						"dataIndex": "tanggal_lahir",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "member_address",
						"dataIndex": "member_address",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "member_telp",
						"dataIndex": "member_telp",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "member_email",
						"dataIndex": "member_email",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Lokasi",
						"dataIndex": "location_name",
						"width": 123,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Region",
						"dataIndex": "location_region",
						"width": 134,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "description",
						"dataIndex": "description",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Plan Type",
						"dataIndex": "plan_type",
						"width": 102,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Hak Akses Pelayanan",
						"dataIndex": "room_class",
						"width": 102,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Status Karyawan",
						"dataIndex": "employment_status",
						"width": 109,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Status Kartu",
						"dataIndex": "status_card",
						"width": 64,
						"height": null,
						"flex": null,
						"filter": {
							"type": "string"
						},
						"search": true,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "activation_date",
						"dataIndex": "activation_date",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "update_code",
						"dataIndex": "update_code",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "Dental Limit",
						"dataIndex": "dental_limit",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "createdBy",
						"dataIndex": "createdBy",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "createdDate",
						"dataIndex": "createdDate",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "updatedBy",
						"dataIndex": "updatedBy",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "updatedDate",
						"dataIndex": "updatedDate",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}, {
						"text": "status_data",
						"dataIndex": "status_data",
						"width": 100,
						"height": null,
						"flex": 1,
						"filter": {
							"type": "string"
						},
						"hidden": true,
						"search": false,
						"xtype": "gridcolumn",
						"format": null
					}],
					"selType": "checkboxmodel",
					"selModel": {
						"mode": "MULTI",
						"selType": "checkboxmodel"
					},
					"name": "sostmasmember_module_real_general_grid_3",
					"layout": {
						"type": "fit"
					},
					"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
				}, {
					"frame": false,
					"border": true,
					"disabled": false,
					"flex": 1,
					"title": "Member Detail",
					"url": "sostmasmember\/call_method\/insertupdate\/table\/sos_tmas_member",
					"method": "POST",
					"autoScroll": true,
					"margins": {
						"top": 0,
						"right": 0,
						"bottom": 0,
						"left": 0,
						"height": 0,
						"width": 0
					},
					"id": "sostmasmember_module_real_general_form_7",
					"semar": true,
					"xtype": "form",
					"tbar": [
						/*[{
							"id": "sostmasmember_module_real_general_form_7_btn_simpan",
							"name": "sostmasmember_module_real_general_form_7_btn_simpan",
							"itemId": "sostmasmember_module_real_general_form_7_btn_simpan",
							"text": "Simpan",
							"iconCls": "semar-save",
							"configs": {
								"toolbar": "top",
								"text": "Simpan",
								"iconCls": "semar-save",
								"type": "normal",
								"value": "true",
								"active": true
							}
						}],
						[{
							"id": "sostmasmember_module_real_general_form_7_btn_ubah",
							"name": "sostmasmember_module_real_general_form_7_btn_ubah",
							"itemId": "sostmasmember_module_real_general_form_7_btn_ubah",
							"text": "Ubah",
							"iconCls": "semar-edit",
							"configs": {
								"toolbar": "top",
								"text": "Ubah",
								"iconCls": "semar-edit",
								"type": "normal",
								"value": "true",
								"active": true
							}
						}],
						[{
							"id": "sostmasmember_module_real_general_form_7_btn_batal",
							"name": "sostmasmember_module_real_general_form_7_btn_batal",
							"itemId": "sostmasmember_module_real_general_form_7_btn_batal",
							"text": "Batal",
							"iconCls": "semar-undo",
							"configs": {
								"toolbar": "top",
								"text": "Batal",
								"iconCls": "semar-undo",
								"type": "normal",
								"value": "true",
								"active": true
							}
						}]*/
					],
					"name": "sostmasmember_module_real_general_form_7",
					"defaults": {
						"xtype": "panel",
						"layout": "anchor",
						"anchor": "100%",
						"flex": 1,
						"margin": 5,
						"defaults": {
							"anchor": "100%",
							"flex": 1
						}
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_combo_10",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_combo_10",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Client",
							"name": "clientId",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_combo_10",
							"semar": true,
							"xtype": "combobox",
							"store": "data_store_9",
							"displayField": "client_name",
							"valueField": "clientId",
							"queryMode": "local",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_text_11",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_text_11",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "No. Kartu",
							"name": "member_cardNo",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_text_11",
							"semar": true,
							"xtype": "textfield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_combo_17",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_combo_17",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Tipe Relasi",
							"name": "relationship_type",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_combo_17",
							"semar": true,
							"xtype": "combobox",
							"store": "data_store_15",
							"displayField": "relationship_type",
							"valueField": "relationship_type",
							"queryMode": "local",
							"items": [],
							listeners:{
								change:function(){
									var temp = 
										Ext.getCmp('sostmasmember_module_real_general_form_7_blk_i_form_combo_17').getValue();
										Ext.getCmp('sostmasmember_module_real_general_form_7_blk_i_form_text_12').setReadOnly(true);
										if( String( temp ) == "Employee" ){
										Ext.getCmp('sostmasmember_module_real_general_form_7_blk_i_form_text_12').setReadOnly(false);
										};
								}
							}
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_text_13",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_text_13",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Nama Member",
							"name": "member_name",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_text_13",
							"semar": true,
							"xtype": "textfield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_text_12",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_text_12",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "NIK",
							"name": "employee_ID",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_text_12",
							"semar": true,
							"xtype": "hiddenfield",
							"items": [],
							listeners:{
								render:function(cmp){
									cmp.getEl().on('click',function(){
									var temps = 
										Ext.getCmp('sostmasmember_module_real_general_form_7_blk_i_form_combo_17').getValue();
											if( String( temps ) != "Employee" ){
												doManualTestVisualBrowseref_Fragment( 
													'id_window_browser_sostmasmember_module_real_general_form_7_blk_form_text_13', 
													true, 
													function(){
														console.log('fungsi yg jalan setelah memilih data');
													},
													{
														"title": "Daftar Member",
														componentTarget : 'sostmasmember_module_real_general_form_7',
														"modulecode": "tvbrowsreftmasmember",
														"browserefHeight": 364,
														"browserefWidth": 683,
														"browserefClick": 2
													},
														function( dynamcID, componentTarget, selectedx ){
														Ext.getCmp( componentTarget ).getForm().setValues( selectedx.data );
														Ext.getCmp(dynamcID).destroy();
													});
											}
									});
								}
							}
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_combo_19",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_combo_19",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Jenis Kelamin",
							"name": "jenis_kelamin",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_combo_19",
							"semar": true,
							"xtype": "combobox",
							"store": "data_store_18",
							"displayField": "val",
							"valueField": "key",
							"queryMode": "local",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_date_20",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_date_20",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Tgl. Lahir",
							"format": "Y\/m\/d",
							"name": "tanggal_lahir",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_date_20",
							"semar": true,
							"xtype": "datefield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_textarea_21",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_textarea_21",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Alamat",
							"name": "member_address",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_textarea_21",
							"semar": true,
							"xtype": "textareafield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_text_22",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_text_22",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Telp.",
							"name": "member_telp",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_text_22",
							"semar": true,
							"xtype": "textfield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_text_23",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_text_23",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Email",
							"name": "member_email",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_text_23",
							"semar": true,
							"xtype": "textfield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_combo_26",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_combo_26",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Lokasi",
							"name": "location_name",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_combo_26",
							"semar": true,
							"xtype": "combobox",
							"store": "data_store_25",
							"displayField": "location_name",
							"valueField": "location_name",
							"queryMode": "local",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_combo_27",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_combo_27",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Region",
							"name": "location_region",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_combo_27",
							"semar": true,
							"xtype": "combobox",
							"store": "data_store_5",
							"displayField": "region_name",
							"valueField": "region_name",
							"queryMode": "local",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_textarea_28",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_textarea_28",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Deskripsi",
							"name": "description",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_textarea_28",
							"semar": true,
							"xtype": "textareafield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_combo_31",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_combo_31",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Plan Type",
							"name": "plan_type",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_combo_31",
							"semar": true,
							"xtype": "hiddenfield",
							"store": "data_store_30",
							"displayField": "plan_type",
							"valueField": "plan_type",
							"queryMode": "local",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_text_32",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_text_32",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Hak Akses Pelayanan",
							"name": "room_class",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_text_32",
							"semar": true,
							"xtype": "textfield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_combo_35",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_combo_35",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Status Karyawan",
							"name": "employment_status",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_combo_35",
							"semar": true,
							"xtype": "combobox",
							"store": "data_store_34",
							"displayField": "employment_status",
							"valueField": "employment_status",
							"queryMode": "local",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_combo_37",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_combo_37",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Status Kartu",
							"name": "status_card",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_combo_37",
							"semar": true,
							"xtype": "combobox",
							"store": "data_store_36",
							"displayField": "val",
							"valueField": "key",
							"queryMode": "local",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_date_38",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_date_38",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Tgl. Aktivasi",
							"format": "Y\/m\/d",
							"name": "activation_date",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_date_38",
							"semar": true,
							"xtype": "datefield",
							"items": []
						}]
					}, {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_date_381",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_date_381",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"emptyText": "-",
							"fieldLabel": "Dental Limit",
							"name": "dental_limit",
							"allowBlank": true,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_text_381",
							"semar": true,
							"xtype": "textfield",
							"items": []
						},
                        {
                            xtype: 'fieldset',   
                            title :'<b>FAMILY MEMBER LIST</b>',
                            margins: '5px 5px 5px 5px',
                            style: 'padding: 0 5px 3px 5px;',  
                            flex: 1,
                            items: [ Grid_T_D_Family ]
                   		 }]
					},  {
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"isformitems": true,
						"id": "sostmasmember_module_real_general_form_7_blk_form_hidden_39",
						"semar": true,
						"xtype": "panel",
						"name": "sostmasmember_module_real_general_form_7_blk_form_hidden_39",
						"defaults": {
							"anchor": "100%",
							"flex": 1
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"title": "",
							"margin": "10 5 3 10",
							"padding": "0 0 0 0",
							"readOnly": false,
							"labelWidth": 100,
							"labelAlign": "left",
							"name": "memberId",
							"allowBlank": false,
							"id": "sostmasmember_module_real_general_form_7_blk_i_form_hidden_39",
							"semar": true,
							"xtype": "hiddenfield",
							"items": []
						}]
					}]
				}, {
					"title": "Advanced Search",
					"id": "sostmasmember_module_real_general_tabpanel_3_tab_3",
					"name": "sostmasmember_module_real_general_tabpanel_3_tab_3",
					layout:'fit',
					bodyPadding:10,
					"items": [
						{
						xtype:'form',
						border:1,
						id:'form_Advance_Search_04',
						fieldDefaults: {
				            msgTarget: 'side'
				        },
						layout:'vbox',
						semar:true,
						bodyPadding:10,
						defaults:{
							xtype:'textfield',
							labelSeparator:'',
							labelWidth:150,
							width:450
						},
						items:[ 
							{
								xtype:'combo',	
								fieldLabel:'Client',
								editable:false,
                                id:'clientId',
								name:'clientId',
								store: {
									fields:['clientId','client_name'],
									autoLoad:true,
									"proxy": {
										"type": "ajax",
										"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/tvbrowserefclient",
										"extraParams": {
											"id_open": "1",
											"semar": "true"
										},
										reader:{
											root:'data'
										}
									}
								},
								queryMode: 'local',
								displayField: 'client_name',
								valueField: 'clientId',
							},
							{
								fieldLabel:'Member Card',
								name:'member_cardNo_'
							},
							{
								fieldLabel:'Member Name',
								name:'member_name_'
							},
							{
								xtype:'combo',	
								fieldLabel:'Relation Type',
								editable:false,
								name:'relationship_type',
								store: {
									fields:['relationship_type'],
									autoLoad:true,
									"proxy": {
										"type": "ajax",
										"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/sos_tref_relationship_type",
										"extraParams": {
											"id_open": "1",
											"semar": "true"
										},
										reader:{
											root:'data'
										}
									}
								},
								queryMode: 'local',
								displayField: 'relationship_type',
								valueField: 'relationship_type',
							},
							{
								fieldLabel:'Tanggal Lahir',
								name:'tanggal_lahir_',
								xtype:'datefield'
							},
							{
								fieldLabel:'Location',
								name:'location_name_'
							},
							{
								fieldLabel:'Region',
								name:'location_region_'
							},
							{
								xtype:'combo',	
								fieldLabel:'Hak Akses Pelayanan',
								editable:false,
								name:'plan_type',
								store: {
									fields:['plan_type','description'],
									autoLoad:true,
									"proxy": {
										"type": "ajax",
										"url": "<?php echo base_url();?>sostmasmember\/call_method\/select\/table\/sos_tref_plan_type",
										"extraParams": {
											"id_open": "1",
											"semar": "true"
										},
										reader:{
											root:'data'
										}
									}
								},
								queryMode: 'local',
								displayField: 'description',
								valueField: 'plan_type',
							},
							{	
								xtype:'combo',	
								fieldLabel:'Member Status',
								editable:false,
								name:'employment_status',
								store: ['Active','Inactive','Pending'],
								queryMode: 'local',
								displayField: 'employment_status',
								valueField: 'employment_status'
							},
							{	
								xtype:'combo',	
								fieldLabel:'Status Card',
								editable:false,
								name:'status_card',
								store: ['Active','Inactive','Pending'],
								queryMode: 'local',
								displayField: 'status_card',
								valueField: 'status_card'
							}
						],
						buttons:[
							{
								width:50,
								text:'Search',
								handler:function(){
									var myParams = Ext.getCmp('form_Advance_Search_04').getForm().getValues();
									for(var key in myParams) {
										var val = myParams[key];
										if(!val){
											delete myParams[key];
										}else{
											Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().getProxy().extraParams[key] = myParams[key];
										}
									}
									Ext.getCmp('sostmasmember_module_real_general_tabpanel_3').setActiveTab(0);
									Ext.getStore('data_store_2').load();
									//Ext.getStore('data_store_20').load({params:myParams});
								}
							},
							{
								xtype:'button',
								text:'Reset',
								handler:function(){
									Ext.getCmp('form_Advance_Search_04').getForm().reset();
									Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().getProxy().extraParams = {id_open:1,semar:true};
									Ext.getStore('data_store_2').load();
								}
							}
						]
					}]
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasmember_module_real_general_grid_6",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_5",
				"columns": [{
					"text": "regionId",
					"dataIndex": "regionId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Region",
					"dataIndex": "region_name",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "sostmasmember_module_real_general_grid_6",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasmember_module_real_general_grid_40",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_36",
				"columns": [{
					"text": "Status Card",
					"dataIndex": "val",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "key",
					"dataIndex": "key",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "sostmasmember_module_real_general_grid_40",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sostmasmember_module_real_general_grid_41",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_34",
				"columns": [{
					"text": "employmentstatusId",
					"dataIndex": "employmentstatusId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Employment Status",
					"dataIndex": "employment_status",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "sostmasmember_module_real_general_grid_41",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_sostmasmember, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_sostmasmember, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('sostmasmember_module_real_general_grid_3', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasmember_module_real_general_form_7'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
                    
                    var member_cardNo = Ext.getCmp('sostmasmember_module_real_general_form_7_blk_i_form_text_11').getValue(); 
                    Data_T_D_Family.load({params:{ member_cardNo : member_cardNo}}); 
				}
			});
			/*pushRegisterEvent('sostmasmember_module_real_general_grid_3', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_form_7_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			pushRegisterEvent('sostmasmember_module_real_general_grid_3', 'itemdblclick', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_tabpanel_3').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasmember_module_real_general_grid_3', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_form_7').getForm().loadRecord((Ext.getCmp('sostmasmember_module_real_general_grid_3').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('sostmasmember_module_real_general_grid_3').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasmember_module_real_general_grid_3_itemdblclick = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_grid_3')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_grid_3');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('itemdblclick', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_grid_3', 'itemdblclick');
						});
						clearInterval(intervale_sostmasmember_module_real_general_grid_3_itemdblclick);
					};
				};
			}, 100);
			pushRegisterEvent('sostmasmember_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_form_7').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasmember_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasmember_module_real_general_form_7'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			/*pushRegisterEvent('sostmasmember_module_real_general_grid_3_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_form_7_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			var intervale_sostmasmember_module_real_general_grid_3_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_tambah')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_grid_3_btn_tambah', 'click');
						});
						clearInterval(intervale_sostmasmember_module_real_general_grid_3_btn_tambah_click);
					};
				};
			}, 100);
			pushRegisterEvent('sostmasmember_module_real_general_grid_3_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('sostmasmember_module_real_general_grid_3').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('memberId'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'sostmasmember/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'memberId',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_sostmasmember_module_real_general_grid_3_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_hapus')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_grid_3_btn_hapus', 'click');
						});
						clearInterval(intervale_sostmasmember_module_real_general_grid_3_btn_hapus_click);
					};
				};
			}, 100);
			pushRegisterEvent('sostmasmember_module_real_general_grid_3_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('sostmasmember', //nama module
								Ext.getCmp('sostmasmember_module_real_general_grid_3').title, //title
								'sostmasmember_module_real_general_grid_3', //id grid
								'memberId', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_sostmasmember_module_real_general_grid_3_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_excel')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_grid_3_btn_excel', 'click');
						});
						clearInterval(intervale_sostmasmember_module_real_general_grid_3_btn_excel_click);
					};
				};
			}, 100);
			pushRegisterEvent('sostmasmember_module_real_general_grid_3_btn_resetfilter', 'click', {
				key: 'manual remove param',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().proxy.extraParams = {
						id_open: '1',
						semar: 'true'
					};
					Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasmember_module_real_general_grid_3_btn_resetfilter_click = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_resetfilter')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_grid_3_btn_resetfilter');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_grid_3_btn_resetfilter', 'click');
						});
						clearInterval(intervale_sostmasmember_module_real_general_grid_3_btn_resetfilter_click);
					};
				};
			}, 100);
			
			pushRegisterEvent('sostmasmember_module_real_general_grid_6', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('sostmasmember_module_real_general_grid_6');
					if (new String('location_region=region_name').match('=')) {
						var hasAfterCheckComp = new String('location_region=region_name').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasmember_module_real_general_grid_6', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasmember_module_real_general_grid_6_itemclick = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_grid_6')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_grid_6');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('itemclick', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_grid_6', 'itemclick');
						});
						clearInterval(intervale_sostmasmember_module_real_general_grid_6_itemclick);
					};
				};
			}, 100);
			pushRegisterEvent('sostmasmember_module_real_general_grid_40', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('sostmasmember_module_real_general_grid_40');
					if (new String('status_card=val').match('=')) {
						var hasAfterCheckComp = new String('status_card=val').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasmember_module_real_general_grid_40', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasmember_module_real_general_grid_40_itemclick = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_grid_40')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_grid_40');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('itemclick', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_grid_40', 'itemclick');
						});
						clearInterval(intervale_sostmasmember_module_real_general_grid_40_itemclick);
					};
				};
			}, 100);
			pushRegisterEvent('sostmasmember_module_real_general_grid_41', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('sostmasmember_module_real_general_grid_41');
					if (new String('employment_status=employment_status').match('=')) {
						var hasAfterCheckComp = new String('employment_status=employment_status').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasmember_module_real_general_grid_41', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasmember_module_real_general_grid_41_itemclick = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_grid_41')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_grid_41');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('itemclick', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_grid_41', 'itemclick');
						});
						clearInterval(intervale_sostmasmember_module_real_general_grid_41_itemclick);
					};
				};
			}, 100);
			/*pushRegisterEvent('sostmasmember_module_real_general_form_7_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_grid_3').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sostmasmember_module_real_general_form_7_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_form_7').on({
						beforeaction: function() {
							Ext.getCmp('sostmasmember_module_real_general_form_7').body.mask('Loading...');
						}
					});
					Ext.getCmp('sostmasmember_module_real_general_form_7').getForm().submit({
						url: Ext.getCmp('sostmasmember_module_real_general_form_7').url,
						method: Ext.getCmp('sostmasmember_module_real_general_form_7').method,
						clientValidation: true,
						success: function(form, action) {
							Ext.getCmp('sostmasmember_module_real_general_form_7').body.unmask();
							Ext.Msg.alert('Success', action.result.msg);
							Ext.getCmp('sostmasmember_module_real_general_form_7').getForm().setValues(action.result.data);

							//more events - start
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
							//more events - end
						},
						failure: function(form, action) {
							Ext.getCmp('sostmasmember_module_real_general_form_7').body.unmask();
							switch (action.failureType) {
							case Ext.form.action.Action.CLIENT_INVALID:
								Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
								break;
							case Ext.form.action.Action.CONNECT_FAILURE:
								Ext.Msg.alert('Failure', 'Ajax communication failed');
								break;
							case Ext.form.action.Action.SERVER_INVALID:
								Ext.Msg.alert('Failure', action.result.msg);
							}
						}
					});
				}
			});
			var intervale_sostmasmember_module_real_general_form_7_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_form_7_btn_simpan')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_form_7_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_form_7_btn_simpan', 'click');
						});
						clearInterval(intervale_sostmasmember_module_real_general_form_7_btn_simpan_click);
					};
				};
			}, 100);
			pushRegisterEvent('sostmasmember_module_real_general_form_7_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasmember_module_real_general_form_7'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			/*pushRegisterEvent('sostmasmember_module_real_general_form_7_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_form_7_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			/*var intervale_sostmasmember_module_real_general_form_7_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_form_7_btn_ubah')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_form_7_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_form_7_btn_ubah', 'click');
						});
						clearInterval(intervale_sostmasmember_module_real_general_form_7_btn_ubah_click);
					};
				};
			}, 100);*/
			/*pushRegisterEvent('sostmasmember_module_real_general_form_7_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_form_7_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			/*pushRegisterEvent('sostmasmember_module_real_general_form_7_btn_batal', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sostmasmember_module_real_general_form_7'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			/*pushRegisterEvent('sostmasmember_module_real_general_form_7_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sostmasmember_module_real_general_form_7').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sostmasmember_module_real_general_form_7_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('sostmasmember_module_real_general_form_7_btn_batal')) {
					var tempEl = Ext.getCmp('sostmasmember_module_real_general_form_7_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sostmasmember_module_real_general_form_7_btn_batal', 'click');
						});
						clearInterval(intervale_sostmasmember_module_real_general_form_7_btn_batal_click);
					};
				};
			}, 100);*/
		}
	}
}


<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>