<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller digenerate oleh eXML Builder.
 * Controller untuk module sostmasmember.
 *
 */
class sostmasmember extends CI_Controller
{

	/**
	 * __construct Controller untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_sostmasmember'));
	}


	/**
	 * index Controller untuk module $modulename.
	 */
	public function index()
	{
		if($this->input->post('id_open')){
			$data['jsscript'] = TRUE;
			$this->load->view('index',$data);
		}else{
			$this->load->view('index');
		};
	}


	/**
	 * call_method Controller untuk module $modulename.
	 */
	public function call_method($querytype, $typex, $valuex)
	{
		echo json_encode( $this->model_sostmasmember->call_method( $typex, $querytype, $valuex ) );
	}
	 
	 public function getMemberdependent(){
		if($this->input->post("id_open")){
			$data = $this->model_sostmasmember->get_AllDataDPDNT();	  			 	
			$total = $this->model_sostmasmember->get_CountDataDPDNT();	  
   		echo '({total:'. $total . ',results:'.json_encode($data).'})';
  	    }  
	}
	

	function import_member() {
        $result = array(
            'isSuccess' => true,
            'message' => 'Gagal melakukan import master absensi.',
        );

        $this->load->library('upload');
        $this->upload->initialize(array(
            'upload_path' => './files/',
            'allowed_types' => 'xls|xlsx|csv',
            'max_size' => '5000',
            'file_name' => 'member.xls',
            'overwrite' => true
        ));

        $this->load->library('excel');

        if (!$this->upload->do_upload('file_upload')) {
            $result = array(
                'isSuccess' => false,
                'message' => $this->upload->display_errors('', '')
            );
        } else {
            $inputFileName = './files/member.xls';
            $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            if (!empty($sheetData)) {
                if ($result['isSuccess']) {
                    if ($this->model_sostmasmember->import_kehadiran($sheetData)) {
                        $result = array(
                            'isSuccess' => true,
                            'message' => 'Berhasil melakukan import data absensi.',
                        );
                    }
                }
            }
          
        }

        $return = array(
            "success" => $result['isSuccess'],
            "info" => array(
                "message" => $result['message'],
            ),
        );

        echo json_encode($return);
   }

}
