<?php
class Panel_Ref extends CI_Controller {
	function __construct() {
		parent::__construct();
 		if ($this->my_usession->logged_in == FALSE){
 			echo "window.location = '".base_url()."user/index';";
 			exit;
       }
    }
   
  function index(){echo '';}
  
  function ref_pegawai(){
	  if($this->input->post('id_open')){
		  $data['jsscript'] = TRUE;
		  $this->load->view('panel_ref/ref_pegawai_view', $data);
	  }else{
		  $this->load->view('panel_ref/ref_pegawai_view');
	  }
  }
  function ref_client(){
	  if($this->input->post('id_open')){
		  $data['jsscript'] = TRUE;
		  $this->load->view('panel_ref/ref_client_view', $data);
	  }else{
		  $this->load->view('panel_ref/ref_client_view');
	  }
  }
   
}
?>