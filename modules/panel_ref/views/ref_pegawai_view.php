<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

// Catatan :
// VKEY = sPgwPilih
// VKEY1 = sDupeg
// VKEY2 = 
 
var title_plus = "Referensi Member"; 

//var Str_Cur_Form_Caller_Pegawai = '';
//var Cur_Form_Caller_Pegawai = '', 
var fmode = '';
var veselon = 0;

// POPUP REFERENSI PEGAWAI ---------------------------------------------------- START
Ext.define('MSearch_RefPegawai', {extend: 'Ext.data.Model',
   fields: ['memberId', 'member_cardNo', 'member_name', 'relationship_type']
});
var Reader_Search_RefPegawai = new Ext.create('Ext.data.JsonReader', {
 	id: 'Reader_Search_RefPegawai', root: 'results', totalProperty: 'total', idProperty: 'memberId'  	
});
var Proxy_Search_RefPegawai = new Ext.create('Ext.data.AjaxProxy', {
   url: BASE_URL + 'tvbrowsereftmasmember/ext_get_all_pegawai', actionMethods: {read:'POST'}, extraParams :{id_open: 1}, reader: Reader_Search_RefPegawai
});
var Data_Search_RefPegawai = new Ext.create('Ext.data.Store', {
	id: 'Data_Search_RefPegawai', model: 'MSearch_RefPegawai', pageSize: 10,	noCache: false, autoLoad: true,
  proxy: Proxy_Search_RefPegawai
});

var Search_RefPegawai = new Ext.create('Ext.ux.form.SearchField', {id: 'Search_RefPegawai', store: Data_Search_RefPegawai, emptyText: 'Pencarian Nama Member ...', width: 450});
var tbSearch_RefPegawai = new Ext.create('Ext.toolbar.Toolbar', { id: 'tbSearch_RefPegawai',
	items:[ 		
		Search_RefPegawai, '->', {text: 'PILIH', iconCls: 'icon-check', id: 'PILIH_pegawai', handler: function(){SetTo_Form_Caller_Pegawai();}}
  ]
});

var filters_Search_RefPegawai = new Ext.create('Ext.ux.grid.filter.Filter', {
  	ftype: 'filters', autoReload: true, local: false, store: Data_Search_RefPegawai,
    filters: [
    	{type: 'string', dataIndex: 'member_cardNo'},
    	{type: 'string', dataIndex: 'member_name'},    
    ]
});

var cbGrid_Search_RefPegawai = new Ext.create('Ext.selection.CheckboxModel');
var Grid_Search_RefPegawai = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_Search_RefPegawai', store: Data_Search_RefPegawai, frame: true, border: true, loadMask: true, noCache: false, style: 'margin:0 auto;', height: '100%', width: '100%', selModel: cbGrid_Search_RefPegawai, columnLines: true,
	columns: [
 		{header: "No Member Card ", dataIndex: 'member_cardNo', width: 170}, 
 		{header: "Member Name", dataIndex: 'member_name', flex:1}, 
 	], 
  features: [filters_Search_RefPegawai],
 	bbar: tbSearch_RefPegawai,
 	dockedItems: [{xtype: 'pagingtoolbar', store: Data_Search_RefPegawai, dock: 'bottom', displayInfo: true}],
 	listeners: {
 		itemdblclick: function(dataview, record, item, index, e) {
 			Ext.getCmp('PILIH_pegawai').handler.call(Ext.getCmp("PILIH_pegawai").scope);
 		}
 	}
});
	
var win_popup_RefPegawai = new Ext.create('widget.window', {
 	id: 'win_popup_RefPegawai', title: 'Referensi Pegawai ' + title_plus, iconCls: 'icon-human',
 	modal:true, plain:true, closable: true, width: 650, height: 400, layout: 'fit', bodyStyle: 'padding: 5px;',
 	items: [Grid_Search_RefPegawai]
});

var new_popup_ref = win_popup_RefPegawai;

// POPUP REFERENSI PEGAWAI ---------------------------------------------------- END

// FUNCTION REF PEGAWAI ------------------------------------------------------- START 
function Funct_win_popup_RefPegawai(form_name, vfmode){
	//Str_Cur_Form_Caller_Pegawai = form_name;
	Cur_Form_Caller_Pegawai = window[form_name]; 
	fmode = vfmode;
	win_popup_RefPegawai.show();
}

function SetTo_Form_Caller_Pegawai(){
	var sm = Grid_Search_RefPegawai.getSelectionModel(), sel = sm.getSelection();
	  
	if(sel.length == 1) {
      var s = sel[0].get('member_cardNo').split('-');        
Ext.getCmp('dashboardproviderv1_module_real_general_form_1').getForm().findField('member_cardNo_separate').setValue(s[1]);
Ext.getCmp('dashboardproviderv1_module_real_general_form_1').getForm().findField('member_cardNo').setValue(s[0]);
Ext.getCmp('dashboardproviderv1_module_real_general_form_1').getForm().findField('s_member_name').setValue(sel[0].get('member_name')); 
         
 	     win_popup_RefPegawai.close();
	}else{
		Ext.MessageBox.show({title:'Peringatan !', msg: 'Silahkan pilih salah satu !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
	}
}

function setParams_Data_Search_RefPegawai(p_eselon){
	Search_RefPegawai.onTrigger1Click();
	Data_Search_RefPegawai.changeParams({params :{id_open: 1}});
}

// FUNCTION REF PEGAWAI ------------------------------------------------------- END

<?php }else{ echo "var new_popup_ref = 'GAGAL';"; } ?>