<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>
	
<?php if(isset($jsscript) && $jsscript == TRUE){ ?>
 
var title_plus = "Referensi Client"; 
 
var fmode = '';
var veselon = 0;

// POPUP REFERENSI PEGAWAI ---------------------------------------------------- START
Ext.define('MSearch_RefClient', {extend: 'Ext.data.Model',
   fields: ['clientId', 'client_name','client_telp']
});
var Reader_Search_RefClient = new Ext.create('Ext.data.JsonReader', {
 	id: 'Reader_Search_RefClient', root: 'results', totalProperty: 'total', idProperty: 'clientId'  	
});
var Proxy_Search_RefClient = new Ext.create('Ext.data.AjaxProxy', {
   url: BASE_URL + 'tvbrowserefclient/ext_get_all_client', 
   actionMethods: {read:'POST'}, extraParams :{id_open: 1}, 
   reader: Reader_Search_RefClient
});
var Data_Search_RefClient = new Ext.create('Ext.data.Store', {
	id: 'Data_Search_RefClient', model: 'MSearch_RefClient', pageSize: 10,	noCache: false, autoLoad: true,
    proxy: Proxy_Search_RefClient
});

var Search_RefClient = new Ext.create('Ext.ux.form.SearchField', {id: 'Search_RefClient', 
					   store: Data_Search_RefClient, emptyText: 'Pencarian Nama CLient ...', width: 450});
var tbSearch_RefClient = new Ext.create('Ext.toolbar.Toolbar', { id: 'tbSearch_RefClient',
	items:[ 		
		Search_RefClient, '->', {text: 'PILIH', iconCls: 'icon-check', id: 'PILIH_Client', handler: function(){SetTo_Form_Caller_client();}}
  ]
});
 

var cbGrid_Search_RefClient = new Ext.create('Ext.selection.CheckboxModel');
var Grid_Search_RefClient = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_Search_RefClient', store: Data_Search_RefClient, frame: true, border: true, loadMask: true, noCache: false, 
    style: 'margin:0 auto;', height: '100%', width: '100%', selModel: cbGrid_Search_RefClient, columnLines: true,
	columns: [
 		{header: "Nama Client ", dataIndex: 'client_name', flex:1}, 
 		{header: "Alamat", dataIndex: 'client_address', flex:1, hidden: true}, 
        {header: "Telp", dataIndex: 'client_telp', flex:1, hidden: true}, 
 	],    
 	bbar: tbSearch_RefClient,
 	dockedItems: [{xtype: 'pagingtoolbar', store: Data_Search_RefClient, dock: 'bottom', displayInfo: true}],
 	listeners: {
 		itemdblclick: function(dataview, record, item, index, e) {
 			Ext.getCmp('PILIH_Client').handler.call(Ext.getCmp("PILIH_Client").scope);
 		}
 	}
});
function Funct_win_popup_Refclient(form_name, vfmode){
	//Str_Cur_Form_Caller_Pegawai = form_name;
	Cur_Form_Caller_Pegawai = window[form_name]; 
	fmode = vfmode;
	win_popup_Refclient.show();
}
	
var win_popup_Refclient = new Ext.create('widget.window', {
 	id: 'win_popup_Refclient', title: title_plus, iconCls: 'icon-human',
 	modal:true, plain:true, closable: true, width: 650, height: 400, layout: 'fit', bodyStyle: 'padding: 5px;',
 	items: [Grid_Search_RefClient]
});

var new_popup_ref = win_popup_Refclient;

// POPUP REFERENSI CLIENT ---------------------------------------------------- END
 
function SetTo_Form_Caller_client(){
	var sm = Grid_Search_RefClient.getSelectionModel(), sel = sm.getSelection();
	  
	if(sel.length == 1) {
 		Ext.getCmp('dashboardproviderv1_form').getForm().findField('member_cardNo').setValue(sel[0].get('member_cardNo')); 
		Ext.getCmp('dashboardproviderv1_form').getForm().findField('s_client_name').setValue(sel[0].get('client_name')); 
        win_popup_Refclient.close();
	}else{
		Ext.MessageBox.show({title:'Peringatan !', msg: 'Silahkan pilih salah satu !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
	}
}

function setParams_Data_Search_RefClient(p_eselon){
	Search_RefClient.onTrigger1Click();
	Data_Search_RefClient.changeParams({params :{id_open: 1}});
}

// FUNCTION REF PEGAWAI ------------------------------------------------------- END

<?php }else{ echo "var new_popup_ref = 'GAGAL';"; } ?>