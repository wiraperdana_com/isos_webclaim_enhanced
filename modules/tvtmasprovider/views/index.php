<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_1', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "time",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "Active",
		"name": "provider_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_email",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_telp",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_address",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_codeId",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0",
		"name": "providerId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_1",
	"name": "data_model_1",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_6', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_6",
	"name": "data_model_6",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_9', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_9",
	"name": "data_model_9",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_12', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "Active",
		"name": "provider_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_12",
	"name": "data_model_12",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_17', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "providertypeId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_17",
	"name": "data_model_17",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_1",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasprovider\/call_method\/select\/table\/tvtmasprovider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "providerId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_2",
	"storeId": "data_store_2",
	"name": "data_store_2"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_6",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasprovider\/call_method\/select\/table\/tvtmasprovider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "location_name",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_7",
	"storeId": "data_store_7",
	"name": "data_store_7"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_9",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasprovider\/call_method\/select\/table\/tvtmasprovider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "provider_type",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_10",
	"storeId": "data_store_10",
	"name": "data_store_10"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_12",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasprovider\/call_method\/select\/table\/tvtmasprovider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "provider_status",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_13",
	"storeId": "data_store_13",
	"name": "data_store_13"
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [{
		"key": "Active",
		"val": "Active"
	}, {
		"key": "Inactive",
		"val": "Inactive"
	}],
	"id": "data_store_16",
	"storeId": "data_store_16",
	"name": "data_store_16"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_17",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvtmasprovider\/call_method\/select\/table\/sos_tref_provider_type",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "providertypeId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_18",
	"storeId": "data_store_18",
	"name": "data_store_18"
});
//store define - end
var workspace_tvtmasprovider = {
	"w2_space": {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Pilih Location",
				"modulecode": "tvbrowsereflocation",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "tvtmasprovider_module_real_general_form_19",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvtmasprovider_module_real_general_browseref_33",
				"semar": true,
				"xtype": "panel",
				"name": "tvtmasprovider_module_real_general_browseref_33",
				"layout": {
					"type": "fit"
				},
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Pilih Client",
				"modulecode": "tvbrowserefclient",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "tvtmasprovider_module_real_general_form_19",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"semar": true,
				"width": 640,
				"height": 492,
				"modal": true,
				"bodyStyle": "padding:5px;",
				"layout": {
					"type": "fit"
				},
				"closeAction": "destroy",
				"id": "tvtmasprovider_module_real_general_browseref_30",
				"name": "tvtmasprovider_module_real_general_browseref_30",
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "north",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "south",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "east",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}]
	}
}
var new_tabpanel = {
	id: 'newtab_tvtmasprovider_module',
	title: 'Master Provider',
	iconCls: 'icon-gears',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 7,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"margins": {
				"top": 4,
				"right": 4,
				"bottom": 4,
				"left": 4,
				"height": 8,
				"width": 8
			},
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Master Provider",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvtmasprovider_module_real_general_tabpanel_3",
				"semar": true,
				"xtype": "tabpanel",
				"name": "tvtmasprovider_module_real_general_tabpanel_3",
				"defaults": {
					"layout": {
						"type": "vbox",
						"align": "stretch",
						"pack": "start"
					},
					"flex": 1
				},
				"layout": {
					"type": "card"
				},
				"items": [{
					"title": "Provider List",
					"id": "tvtmasprovider_module_real_general_tabpanel_3_tab_1",
					"name": "tvtmasprovider_module_real_general_tabpanel_3_tab_1",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"loadMask": true,
						"noCache": false,
						"autoHeight": true,
						"columnLines": false,
						"invalidateScrollerOnRefresh": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "tvtmasprovider_module_real_general_grid_5",
						"semar": true,
						"xtype": "grid",
						"tbar": [
							[{
								"id": "tvtmasprovider_module_real_general_grid_5_btn_tambah",
								"name": "tvtmasprovider_module_real_general_grid_5_btn_tambah",
								"itemId": "tvtmasprovider_module_real_general_grid_5_btn_tambah",
								"text": "Tambah",
								"iconCls": "semar-add",
								"configs": {
									"toolbar": "top",
									"text": "Tambah",
									"iconCls": "semar-add",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvtmasprovider_module_real_general_grid_5_btn_hapus",
								"name": "tvtmasprovider_module_real_general_grid_5_btn_hapus",
								"itemId": "tvtmasprovider_module_real_general_grid_5_btn_hapus",
								"text": "Hapus",
								"iconCls": "semar-delete",
								"configs": {
									"toolbar": "top",
									"text": "Hapus",
									"iconCls": "semar-delete",
									"type": "delete",
									"value": "sos_tmas_provider",
									"split": true,
									"active": true
								}
							}, "-"],
							[{
								"id": "tvtmasprovider_module_real_general_grid_5_btn_excel",
								"name": "tvtmasprovider_module_real_general_grid_5_btn_excel",
								"itemId": "tvtmasprovider_module_real_general_grid_5_btn_excel",
								"text": "Excel",
								"iconCls": "semar-excel",
								"configs": {
									"toolbar": "top",
									"text": "Excel",
									"iconCls": "semar-excel",
									"type": "excel",
									"value": "tvtmasprovider",
									"active": true
								}
							}]
						],
						"store": "data_store_2",
						"dockedItems": [{
							"id": "tvtmasprovider_module_real_general_grid_5_pagingtoolbar",
							"hidden": false,
							"xtype": "pagingtoolbar",
							"store": "data_store_2",
							"dock": "bottom",
							"displayInfo": true
						}, {
							"id": "tvtmasprovider_module_real_general_grid_5_searchfield",
							"dock": "top",
							"hidden": false,
							"xtype": "semarwidgetsearchtoolbar",
							"grid_id": "tvtmasprovider_module_real_general_grid_5",
							"store": "data_store_2",
							"text": "Search"
						}],
						"columns": [{
							"text": "providerId",
							"dataIndex": "providerId",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "clientId",
							"dataIndex": "clientId",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Client",
							"dataIndex": "client_name",
							"width": 213,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Provider Code",
							"dataIndex": "provider_codeId",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Provider Name",
							"dataIndex": "provider_name",
							"width": 192,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Provider Type",
							"dataIndex": "provider_type",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "provider_address",
							"dataIndex": "provider_address",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "provider_telp",
							"dataIndex": "provider_telp",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "provider_email",
							"dataIndex": "provider_email",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Location",
							"dataIndex": "location_name",
							"width": 141,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Status",
							"dataIndex": "provider_status",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "description",
							"dataIndex": "description",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}],
						"selType": "checkboxmodel",
						"selModel": {
							"mode": "MULTI",
							"selType": "checkboxmodel"
						},
						"name": "tvtmasprovider_module_real_general_grid_5",
						"layout": {
							"type": "fit"
						},
						"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
					}]
				}, {
					"title": "Provider Detail",
					"id": "tvtmasprovider_module_real_general_tabpanel_3_tab_2",
					"name": "tvtmasprovider_module_real_general_tabpanel_3_tab_2",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"width": 849,
						"url": "tvtmasprovider\/call_method\/insertupdate\/table\/sos_tmas_provider",
						"method": "POST",
						"height": 708,
						"autoScroll": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "tvtmasprovider_module_real_general_form_19",
						"semar": true,
						"xtype": "form",
						"tbar": [
							[{
								"id": "tvtmasprovider_module_real_general_form_19_btn_simpan",
								"name": "tvtmasprovider_module_real_general_form_19_btn_simpan",
								"itemId": "tvtmasprovider_module_real_general_form_19_btn_simpan",
								"text": "Simpan",
								"iconCls": "semar-save",
								"configs": {
									"toolbar": "top",
									"text": "Simpan",
									"iconCls": "semar-save",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvtmasprovider_module_real_general_form_19_btn_ubah",
								"name": "tvtmasprovider_module_real_general_form_19_btn_ubah",
								"itemId": "tvtmasprovider_module_real_general_form_19_btn_ubah",
								"text": "Ubah",
								"iconCls": "semar-edit",
								"configs": {
									"toolbar": "top",
									"text": "Ubah",
									"iconCls": "semar-edit",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvtmasprovider_module_real_general_form_19_btn_batal",
								"name": "tvtmasprovider_module_real_general_form_19_btn_batal",
								"itemId": "tvtmasprovider_module_real_general_form_19_btn_batal",
								"text": "Batal",
								"iconCls": "semar-undo",
								"configs": {
									"toolbar": "top",
									"text": "Batal",
									"iconCls": "semar-undo",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}]
						],
						"name": "tvtmasprovider_module_real_general_form_19",
						"defaults": {
							"xtype": "panel",
							"layout": "anchor",
							"anchor": "100%",
							"flex": 1,
							"margin": 5,
							"defaults": {
								"anchor": "100%",
								"flex": 1
							}
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_text_20",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_text_20",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Client",
								"name": "client_name",
								"allowBlank": false,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_text_20",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_text_21",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_text_21",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Provider Code",
								"name": "provider_codeId",
								"allowBlank": false,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_text_21",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_text_22",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_text_22",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Provider Name",
								"name": "provider_name",
								"allowBlank": false,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_text_22",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_combo_23",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_combo_23",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Provider Type",
								"name": "provider_type",
								"allowBlank": false,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_combo_23",
								"semar": true,
								"xtype": "combobox",
								"store": "data_store_18",
								"displayField": "provider_type",
								"valueField": "provider_type",
								"queryMode": "local",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_textarea_24",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_textarea_24",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Provider Address",
								"name": "provider_address",
								"allowBlank": true,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_textarea_24",
								"semar": true,
								"xtype": "textareafield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_text_25",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_text_25",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Provider Telp",
								"name": "provider_telp",
								"allowBlank": true,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_text_25",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_text_26",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_text_26",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Provider Email",
								"name": "provider_email",
								"allowBlank": true,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_text_26",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_text_27",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_text_27",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Location",
								"name": "location_name",
								"allowBlank": false,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_text_27",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_combo_28",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_combo_28",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Provider Status",
								"name": "provider_status",
								"allowBlank": true,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_combo_28",
								"semar": true,
								"xtype": "combobox",
								"store": "data_store_16",
								"displayField": "val",
								"valueField": "key",
								"queryMode": "local",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_textarea_29",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_textarea_29",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Description",
								"name": "description",
								"allowBlank": true,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_textarea_29",
								"semar": true,
								"xtype": "textareafield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_hidden_36",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_hidden_36",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"name": "clientId",
								"allowBlank": false,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_hidden_36",
								"semar": true,
								"xtype": "hiddenfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvtmasprovider_module_real_general_form_19_blk_form_hidden_37",
							"semar": true,
							"xtype": "panel",
							"name": "tvtmasprovider_module_real_general_form_19_blk_form_hidden_37",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"name": "providerId",
								"allowBlank": false,
								"id": "tvtmasprovider_module_real_general_form_19_blk_i_form_hidden_37",
								"semar": true,
								"xtype": "hiddenfield",
								"items": []
							}]
						}]
					}]
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvtmasprovider_module_real_general_grid_8",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_7",
				"columns": [{
					"text": "Location",
					"dataIndex": "location_name",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvtmasprovider_module_real_general_grid_8",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvtmasprovider_module_real_general_grid_11",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_10",
				"columns": [{
					"text": "Type",
					"dataIndex": "provider_type",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvtmasprovider_module_real_general_grid_11",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvtmasprovider_module_real_general_grid_15",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_13",
				"columns": [{
					"text": "Status",
					"dataIndex": "provider_status",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvtmasprovider_module_real_general_grid_15",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_tvtmasprovider, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_tvtmasprovider, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvtmasprovider_module_real_general_form_19'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_form_19').getForm().loadRecord((Ext.getCmp('tvtmasprovider_module_real_general_grid_5').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('tvtmasprovider_module_real_general_grid_5').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5', 'itemdblclick', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_tabpanel_3').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasprovider_module_real_general_grid_5_itemdblclick = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_grid_5')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_grid_5');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemdblclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_grid_5', 'itemdblclick');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_grid_5_itemdblclick);
						} else {
							tempEl.on('itemdblclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_grid_5', 'itemdblclick');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_grid_5_itemdblclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvtmasprovider_module_real_general_form_19'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_form_19').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_tambah', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_tabpanel_3').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasprovider_module_real_general_grid_5_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_grid_5_btn_tambah')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_grid_5_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_tambah', 'click');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_grid_5_btn_tambah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_tambah', 'click');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_grid_5_btn_tambah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('tvtmasprovider_module_real_general_grid_5').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('providerId'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('tvtmasprovider_module_real_general_grid_5_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'tvtmasprovider/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'providerId',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('tvtmasprovider_module_real_general_grid_5').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_tvtmasprovider_module_real_general_grid_5_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_grid_5_btn_hapus')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_grid_5_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_hapus', 'click');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_grid_5_btn_hapus_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_hapus', 'click');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_grid_5_btn_hapus_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('tvtmasprovider_module_real_general_grid_5_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('tvtmasprovider', //nama module
								Ext.getCmp('tvtmasprovider_module_real_general_grid_5').title, //title
								'tvtmasprovider_module_real_general_grid_5', //id grid
								'providerId', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_tvtmasprovider_module_real_general_grid_5_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_grid_5_btn_excel')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_grid_5_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_excel', 'click');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_grid_5_btn_excel_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_grid_5_btn_excel', 'click');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_grid_5_btn_excel_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_grid_5').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_simpan', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_tabpanel_3').setActiveTab(JSON.parse(0));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_form_19').on({
						beforeaction: function() {
							Ext.getCmp('tvtmasprovider_module_real_general_form_19').body.mask('Loading...');
						}
					});
					Ext.getCmp('tvtmasprovider_module_real_general_form_19').getForm().submit({
						url: Ext.getCmp('tvtmasprovider_module_real_general_form_19').url,
						method: Ext.getCmp('tvtmasprovider_module_real_general_form_19').method,
						clientValidation: true,
						success: function(form, action) {
							Ext.getCmp('tvtmasprovider_module_real_general_form_19').body.unmask();
							Ext.Msg.alert('Success', action.result.msg);
							Ext.getCmp('tvtmasprovider_module_real_general_form_19').getForm().setValues(action.result.data);

							//more events - start
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
							//more events - end
						},
						failure: function(form, action) {
							Ext.getCmp('tvtmasprovider_module_real_general_form_19').body.unmask();
							switch (action.failureType) {
							case Ext.form.action.Action.CLIENT_INVALID:
								Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
								break;
							case Ext.form.action.Action.CONNECT_FAILURE:
								Ext.Msg.alert('Failure', 'Ajax communication failed');
								break;
							case Ext.form.action.Action.SERVER_INVALID:
								Ext.Msg.alert('Failure', action.result.msg);
							}
						}
					});
				}
			});
			var intervale_tvtmasprovider_module_real_general_form_19_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_simpan')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_simpan', 'click');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_btn_simpan_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_simpan', 'click');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_btn_simpan_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvtmasprovider_module_real_general_form_19'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasprovider_module_real_general_form_19_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_ubah')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_ubah', 'click');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_btn_ubah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_ubah', 'click');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_btn_ubah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_batal', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvtmasprovider_module_real_general_form_19'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tvtmasprovider_module_real_general_form_19').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvtmasprovider_module_real_general_form_19_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_batal')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_form_19_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_batal', 'click');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_btn_batal_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_form_19_btn_batal', 'click');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_btn_batal_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_blk_i_form_text_20', 'click', {
				key: 'browserefShow',
				fn: function(callbackIndex) {
					doTestVisualBrowseref('tvtmasprovider_module_real_general_browseref_30', JSON.parse(true), callbackIndex);
				}
			});
			var intervale_tvtmasprovider_module_real_general_form_19_blk_i_form_text_20_click = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_form_19_blk_i_form_text_20')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_form_19_blk_i_form_text_20');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_form_19_blk_i_form_text_20', 'click');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_blk_i_form_text_20_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_form_19_blk_i_form_text_20', 'click');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_blk_i_form_text_20_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvtmasprovider_module_real_general_form_19_blk_i_form_text_27', 'click', {
				key: 'browserefShow',
				fn: function(callbackIndex) {
					doTestVisualBrowseref('tvtmasprovider_module_real_general_browseref_33', JSON.parse(true), callbackIndex);
				}
			});
			var intervale_tvtmasprovider_module_real_general_form_19_blk_i_form_text_27_click = setInterval(function() {
				if (Ext.getCmp('tvtmasprovider_module_real_general_form_19_blk_i_form_text_27')) {
					var tempEl = Ext.getCmp('tvtmasprovider_module_real_general_form_19_blk_i_form_text_27');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvtmasprovider_module_real_general_form_19_blk_i_form_text_27', 'click');
								};
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_blk_i_form_text_27_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvtmasprovider_module_real_general_form_19_blk_i_form_text_27', 'click');
							});
							clearInterval(intervale_tvtmasprovider_module_real_general_form_19_blk_i_form_text_27_click);
						};
					};
				};
			}, 100);
		}
	}
}

<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>