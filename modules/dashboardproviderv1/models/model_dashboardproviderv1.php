<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Model digenerate oleh eXML Builder.
 * Model untuk module dashboardproviderv1.
 *
 */
class model_dashboardproviderv1 extends MY_Model
{

	/**
	 * __construct Model untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * _load_model Model untuk module $modulename.
	 */
	public function _load_model($model_name)
	{
		$CI =& get_instance();
		$CI->load->model($model_name);
		return $CI->$model_name;
	}


	/**
	 * call_method Model untuk module $modulename.
	 */
	public function call_method($typex, $querytype, $valuex)
	{
		$result = array( 'status' => false, 'data' => array(), 'total' => 0 );
		if( $typex != null ) {
			switch( $typex ) {
				case ( 'table' ) :
				case ( 'view' ) :
					$result = $this->doTableView( $querytype, $valuex );
					break;
				case ( 'procedure' ) :
				case ( 'function' ) :
					$result = $this->doProc( $querytype, $valuex );
					break;
			};
		};
		return $result;
	}

    public function get_provider(){
		 $this->db->where('tUser.user', $this->session->userdata("user_zs_exmldashboard"));
		 return $this->db->select('tUser.providerId, sos_tmas_provider.provider_name')->from('tUser')
		 				 ->join('sos_tmas_provider','sos_tmas_provider.providerId = tUser.providerId')
		 				 ->get()->row(); 
		 echo $this->db->last_query();
	}
	
	public function get_Client(){
		 $this->db->where('tUser.user', $this->session->userdata("user_zs_exmldashboard"));
		 return $this->db->select('tUser.clientId, sos_tmas_client.client_name')->from('tUser')
		 				 ->join('sos_tmas_client','sos_tmas_client.clientId = tUser.clientId')
		 				 ->get()->row(); 
		  
	}
	
	/**
	 * _do_select Model untuk module $modulename.
	 */
	public function _do_select($valuex)
	{
		$this->db->select('*');
		$this->db->from( $valuex );
		$fields = $this->db->field_data( $valuex );
		foreach( $fields as $k => $v ) {
			if( isset( $_REQUEST[ $v->name ] ) ){
				$this->db->where( $v->name, $_REQUEST[ $v->name ] );
			};
		};
		$Filters_Model = $this->_load_model('filters_model');
		$Filters_Model->get_SORT();
		$Filters_Model->get_SORT_Get();
		$Filters_Model->get_FILTER();
	}


	/**
	 * _do_insertupdate_data Model untuk module $modulename.
	 */
	function Insert_Data(){
		if($this->input->post('t_date_claim') == "dd/mm/yyyy"){
			$tgl_claim = NULL;
		}else{
			list($hari,$bulan,$tahun) = explode("/",$this->input->post('t_date_claim'));
			$format_tgl = "$tahun-$bulan-$hari";
			$tgl_claim = date("Y-m-d", strtotime($format_tgl));
		}
		// echo $tgl_claim.' '.date("G:i:s");die; 
		$data_s_insert = array('createdBy' => $this->session->userdata("user_zs_exmldashboard"), 'createdDate' => date("Y-m-d H:i:s"));
		$data_s_update = array('updatedBy' => $this->session->userdata("user_zs_exmldashboard"), 'updatedDate' => date("Y-m-d H:i:s"));
		
		$data = array(
			'memberId' => $this->input->post('t_memberId'),
			'clientId' => $this->input->post('t_clientId') == '' ? 0 : $this->input->post('t_clientId'),
			'providerId' => $this->input->post('t_providerId') == '' ? 0 : $this->input->post('t_providerId'),
			'servicetypeId' => $this->input->post('t_jenis_claim'),
			'relationship_type' => $this->input->post('t_f_relationship_type'),
			'date_claim' => $tgl_claim.' '.date("H:i:s"),
			'description' => $this->input->post('t_description'),
			//'room_class' => $this->input->post('t_room_class'),  
			'claim_status' => 'success'
		);
		
			if($this->input->post('t_claimId')){
				$data_ID = array('claimId' => $this->input->post('t_claimId'));
				$this->db->trans_start();
				$this->db->update('sos_ttrans_claim',array_merge($data, $data_s_update), $data_ID);
				$this->db->trans_complete(); 
				return "Updated"; exit;
			}else{
				$this->db->trans_start();
				$this->db->insert('sos_ttrans_claim', array_merge($data, $data_s_insert));
				$id = $this->db->insert_id();
				$this->db->trans_complete(); 
				return $id; 
			}
	  
	}
 	
	
	function sendmailMember($status='', $eto=''){
		$config = Array(
			'protocol' => 'smtp', 
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => '465',
			'smtp_user' => 'int.sos.managed.healthcare@gmail.com', // change it to yours
			'smtp_pass' => 'sos123456', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
	   );
		 
		if($status =='Active'){
			$this->db->where('emailmsg_type', 'verification_success' );
		}
		if($status =='Inactive'){
			$this->db->where('emailmsg_type', 'verification_memberinactive' );
		}
		if($status =='' || $status =='Pending'){
			$this->db->where('emailmsg_type', 'verification_memberfail' );
		}
		
		if($status =='' || $status =='fail'){
			$this->db->where('emailmsg_type', 'verification_fraud_minutes_mem' );
		}
		
		$this->db->where('status_data', 1);
		$msg = $this->db->from('sos_tref_message_email')->get()->row();
		
		$message = $msg->message;
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('noreply@int.sos.managed.healthcare'); // change it to yours
		$this->email->to($eto);// change it to yours
		$this->email->subject('Notifikasi Verifikasi Klaim Int SOS Managed Healthcare');
		$this->email->message($message);
		if(!$this->email->send()){
	      //show_error($this->email->print_debugger());
	     }
	
	}
	
	function Delete_Data(){
		$sesi_type = $this->session->userdata("opd_zs_simpeg");
		$records = explode('-', $this->input->post('postdata'), -1);
		$data = $this->getBy_ArrayID($records);
		if(count($data)){
    	$this->db->trans_start();
    	foreach($data as $key => $list){ 
    	    $this->db->delete('sos_ttrans_claim', array('claimId' => $list['claimId'])); 
     	}
  		$this->db->trans_complete();
		}
	}
	
	function getBy_ArrayID($ID = array()){
		if(count($ID)){
			$data = array();
			$this->db->select('*');
			$this->db->from("sos_ttrans_claim");
			$this->db->where_in('claimId',$ID);
			$Q = $this->db->get('');
			if($Q->num_rows() > 0){
				foreach ($Q->result_array() as $row){
					$data[] = $row;
			}
			}
			$Q->free_result();
			return $data;
		 }
	}
	
	
	function get_AllData(){
		$data = array();
		
		if($this->input->post('providerId') != ''){
			$this->db->where('providerId', $this->input->post('providerId'));
		}
		if($this->input->post('clientId') != ''){
			$this->db->where('clientId', $this->input->post('clientId'));
		}
		$this->db->where('memberId', $this->input->post('memberId'));
		$this->db->select('*');
		$this->db->from('tvttransclaim2');
		$this->db->order_by("dates_claim", "desc");
		 
		$this->get_FILTER();
		
			
			$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
			$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 20;
			
		$this->db->limit($limit, $start);
		$this->db->order_by('date_claim', 'desc');
		
		$Q = $this->db->get(); 
		//echo $this->db->last_query();die;
		foreach ($Q->result() as $obj)
		{
			$data[] = $obj;
		}  
		
		  		
		return $data;
	}
	
	function get_FILTER(){
    $filter = $this->input->get_post('filter');
    if (is_array($filter)){    	    	
    	for ($i=0; $i < count($filter); $i++){
    		switch($filter[$i]['data']['type']){ 
    			case 'string' : $this->db->like($filter[$i]['field'], $filter[$i]['data']['value']); break;    		 
    			case 'date' : 
    				switch($filter[$i]['data']['comparison']){
    					case "ne":
    						$this->db->where($filter[$i]['field'] . " !=", date("d/m/Y", strtotime($filter[$i]['data']['value']))); 
    						break;
    					case "lt":
    						$this->db->where($filter[$i]['field'] . " <=", date("d/m/Y", strtotime($filter[$i]['data']['value']))); 
    						break;
    					case "gt":
    						$this->db->where($filter[$i]['field'] . " >=", date("d/m/Y", strtotime($filter[$i]['data']['value']))); 
    						break;
    					case "eq":
    						$this->db->where($filter[$i]['field'], date("d/m/Y", strtotime($filter[$i]['data']['value']))); 
    						break;
    				}
    				break;    			  			
    		}
    	} 
    }
	}
	function get_CountData(){
		if($this->input->post('providerId') != ''){
			$this->db->where('providerId', $this->input->post('providerId'));
		}
		if($this->input->post('clientId') != ''){
			$this->db->where('clientId', $this->input->post('clientId'));
		}
		$this->db->where('memberId', $this->input->post('memberId'));
		$this->db->select('*');
		$this->db->from('tvttransclaim2');
		return $this->db->count_all_results();
	}
	
	
	function get_AllDataDPDNT(){
		$data = array();
		$this->db->where('employee_ID = (SELECT employee_ID FROM tvtmasmember WHERE member_cardNo = "'.$this->input->post('member_cardNo').'")'); 
		//$this->db->where('member_cardNo != ', $this->input->post('member_cardNo')); 
 		$this->db->select('*');
		$this->db->from('tvtmasmember');
			
			$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
			$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 20;
			
		$this->db->limit($limit, $start);
		$Q = $this->db->get(); 
		foreach ($Q->result() as $obj)
		{
			$data[] = $obj;
		}    		
		return $data;
	}
	
	function get_CountDataDPDNT(){ 
		$this->db->where('employee_ID', $this->input->post('member_cardNo'));
		$this->db->where('member_cardNo != ', $this->input->post('member_cardNo'));
		$this->db->select('*');
		$this->db->from('tvtmasmember');
		return $this->db->count_all_results();
	}
	
	
	function get_News(){
		$data = array();
		 
		$this->db->select('*');
		$this->db->from('sos_ttrans_news');
 			$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
			$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 20;
			
		$this->db->limit($limit, $start);
		$this->db->order_by('newsId', 'desc');
		$Q = $this->db->get(); 
		foreach ($Q->result() as $obj)
		{
			$data[] = $obj;
		}    		
		return $data;
	}
	
	function get_CountNews(){
 		$this->db->select('*');
		$this->db->from('sos_ttrans_news');
		return $this->db->count_all_results();
	}
	
	function get_Panduan(){
		$data = array();
		 
		$this->db->select('*');
		$this->db->from('sos_tref_panduan');
 			$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
			$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 20;
			
		$this->db->limit($limit, $start);
		$this->db->order_by('panduanId', 'desc');
		$Q = $this->db->get(); 
		foreach ($Q->result() as $obj )
		{
			$data[] = $obj; 
		}    		
		return $data;
	}
	
	function get_CountPanduan(){
 		$this->db->select('*');
		$this->db->from('sos_tref_panduan');
		return $this->db->count_all_results();
	}
	
 }
