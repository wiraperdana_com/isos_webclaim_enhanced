// TABEL GRID BERITA  ------------------------------------------------- START
var Params_T_D_News;
Ext.define('MT_D_News', {extend: 'Ext.data.Model',
  fields: ['newsId','news_date', 'news_judul', 'news_file', 'createdBy','createdDate', 'updatedBy','updatedDate']
});
var Reader_T_D_News = new Ext.create('Ext.data.JsonReader', {
  id: 'Reader_T_D_News', root: 'results', totalProperty: 'total', idProperty: 'NewsId'  	
});

var Proxy_T_D_News = new Ext.create('Ext.data.AjaxProxy', {
  id: 'Proxy_T_D_News', url: BASE_URL + 'dashboardproviderv1/berita_page', actionMethods: {read:'POST'}, extraParams: {id_open:1},
  reader: Reader_T_D_News,
  afterRequest: function(request, success) {
   	Params_T_D_News = request.operation.params;
  }
});

var Data_T_D_News = new Ext.create('Ext.data.Store', {
	id: 'Data_T_D_News', model: 'MT_D_News', pageSize: 20, noCache: false, autoLoad: false,
  proxy: Proxy_T_D_News
});

 
var Grid_T_D_News = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_D_News', store: Data_T_D_News, frame: true, border: true, loadMask: true, noCache: false,
    style: 'margin:0 auto;', autoHeight: true, height:'100%', columnLines: true,  
 	columns: [
  	{header:'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .5px;'}, 
  	{header: "Subject", dataIndex: 'news_judul', width: 440}, 
  	{header: "Attachement File", dataIndex: 'news_file', width: 220,
        renderer: function(value, metaData, record, rowIndex, colIndex, store) { 
            if(record.get('news_file') != null){
            var URL = BASE_URL;
                URL+="dashboardproviderv1/download/";
                URL+= record.get('newsId');
            	return '<a href='+URL+' target=_blank>' + decode_base64(value) +'</a>';
            } else{
               return '';
            }
       }  
     },  	  	 
  	{header: "Date upload", dataIndex: 'createdDate', width:125, format:'d-m-Y'},
    {header: "User Upload", dataIndex: 'createdBy', width: 190}, 
    {header: "Date Modify", dataIndex: 'updatedDate', width: 125, format:'d-m-Y'}, 
    {header: "User Modify", dataIndex: 'updatedBy', flex:1}, 
  ],  
  dockedItems: [{xtype: 'pagingtoolbar', store: Data_T_D_News, dock: 'bottom', displayInfo: true,
  }],
  listeners: {
        'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
            var zRec = iView.getRecord(iRowEl);
            if(zRec.data.newsId !=''){             
              PdfViewerPanel(zRec.data.newsId)
             }
        }
    },
 /* listeners: {
    'beforerender' : function(grid) {
         //console.log(grid.columns[0])
         var cond = '<?php echo count(@$Provider) == 0 ? @$client : @$Provider;?>';
         if(cond){ 
         	grid.columns[3].hidden = true;
         	grid.columns[4].hidden = true; 
         	grid.columns[5].hidden = true; 
         	grid.columns[6].hidden = true;
         }  
    }
   } */
});

function findColumnByDataIndex(grid, dataIndex) {
    var selector = "gridcolumn[dataIndex=" + dataIndex + "]";
    return selector;
};
 
function PdfViewerPanel (value){ 
    dynamicPanel = new Ext.Component({
     autoEl: {
        tag: 'iframe',
        style: 'height: 100%; width: 100%; border: none',
        src: BASE_URL + 'dashboardproviderv1/loadpdfNews/' + value
    },   
    width: '100%', 
   });
   if(dynamicPanel){ 
      Ext.getCmp('pim-view-export-panel-id').removeAll();
      Ext.getCmp('pim-view-export-panel-id').add(dynamicPanel);
    }
}
  
 
var panelNewsEvent = new Ext.create('Ext.Panel', {
    width: '100%',
    height: 800, 
    layout: {                        
        type: 'vbox',
        align: 'center',        
    }, 
    items: [{                        
        xtype: 'panel', 
        width: '100%',
        flex: 2,
        items :[Grid_T_D_News]                      
    },{
        xtype: 'panel',
        title: 'PDF Viewer',
        width: '100%',        
        flex: 4, 
        id: 'pim-view-export-panel-id' 
     }] 
});

