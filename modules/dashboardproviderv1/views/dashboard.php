<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
//store define - end


// TABEL GRID MEMBER  ------------------------------------------------- START
var Params_T_D_Claim;
Ext.define('MT_D_Claim', {extend: 'Ext.data.Model',
  fields: ['claimId','member_name', 'client_name', 'provider_name',
  		   'servicetypeId','service_type', 'relationshiptype','claim_status','description','date_claim','room_class'  ]
});
var Reader_T_D_Claim = new Ext.create('Ext.data.JsonReader', {
  id: 'Reader_T_D_Claim', root: 'results', totalProperty: 'total', idProperty: 'claimId'  	
});

var Proxy_T_D_Claim = new Ext.create('Ext.data.AjaxProxy', {
  id: 'Proxy_T_D_Claim', url: BASE_URL + 'dashboardproviderv1/ext_get_all', actionMethods: {read:'POST'}, extraParams: {id_open:1, },
  reader: Reader_T_D_Claim,
  afterRequest: function(request, success) {
   	Params_T_D_Claim = request.operation.params;
  }
});

var Data_T_D_Claim = new Ext.create('Ext.data.Store', {
	id: 'Data_T_D_Claim', model: 'MT_D_Claim', pageSize: 20, noCache: false, autoLoad: false,
    proxy: Proxy_T_D_Claim ,
    listeners :{
        datachanged: function(store,records){ 
            store.getProxy().extraParams.memberId = Ext.getCmp('t_memberId').getValue(); 
        }  
    }
});


var Filters_CLaim = new Ext.create('Ext.ux.grid.filter.Filter', {
    ftype: 'filters',
    autoReload: true,
    local: false,
    store: Data_T_D_Claim,
    filters: [{
              type: 'string',
              dataIndex: 'service_type'
              },
              {
               type:'date',
               dataIndex : 'date_claim',
              }],
}); 

var cbGrid_T_D_Claim = new Ext.create('Ext.selection.CheckboxModel');
var Grid_T_D_Claim = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_D_Claim', hidden:true, store: Data_T_D_Claim, frame: true, border: true, loadMask: true, noCache: false,
    style: 'margin:0 auto;', autoHeight: true, height:'100%', columnLines: true, title :'Klaim Historis',
    selModel: cbGrid_T_D_Claim,
    
	columns: [
  	{header:'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .5px;'},      
  	{header: "Nama", dataIndex: 'member_name', width: 240}, 
  	{header: "Jenis Claim", dataIndex: 'service_type', filter: { type: "string" }, width: 120},  	  	 
  	{header: "Tgl. Claim", dataIndex: 'date_claim', width:125, format:'d-m-Y' },
    {header: "Provider", dataIndex: 'provider_name', width: 190, hidden: true}, 
    {header: "Deskripsi", dataIndex: 'description', width: 370}, 
    {header: "Status Claim", dataIndex: 'claim_status', flex:1}, 
  ], 
  features: [Filters_CLaim],
  dockedItems: [{xtype: 'pagingtoolbar', store: Data_T_D_Claim, dock: 'bottom', displayInfo: true,
  listeners :{
      beforechange: function() { 					   
 	   Data_T_D_Claim.getProxy().extraParams.memberId = Ext.getCmp('t_memberId').getValue(); 
	   Data_T_D_Claim.getProxy().extraParams.providerId = Ext.getCmp('t_providerId').getValue();  
       Data_T_D_Claim.getProxy().extraParams.clientId = Ext.getCmp('t_clientId').getValue(); 
  	}    
  }
  } 
   ],
  listeners: {
  	 selectionchange : function(grid, selected){
        if(selected.length) { 
               clearInput(); 
               ActiveInput();
               //Ext.getCmp('Update_claim').enable(true);
               //Ext.getCmp('Hapus_claim').enable(true);
               Ext.getCmp('ongridStat').setValue(1);
          } else { DeactiveInput();}
     } 
   }
});

function win_popup_print (val,title, id){
   var win_popup_Struk = new Ext.create('widget.window', {
 	id: 'win_popup_Struk', title: 'Cetak Struk ' + title , iconCls: 'icon-human',
 	modal:true, plain:true, closable: true, width: 650, height: 500, layout: 'fit', bodyStyle: 'padding: 5px;',
 	items: [
        {  xtype :'component', 
		   html :'<div id="cetak_inactive"></div>', 
	    }
    ]
   });
   win_popup_Struk.show();
    
   Ext.fly('cetak_inactive').update('<iframe src="<?php echo base_url();?>dashboardproviderv1/print_struk?type='+val+'&id='+id+'" style="height:100%; width:100%;">');
    
}


var Btn_InformasiMember = new Ext.create('Ext.toolbar.Toolbar', {
    layout: {
        overflowHandler: 'Menu'
    },
    items: [{xtype:'hidden', name:'ongridStat', id:'ongridStat'},
         '-', {
            text: 'Cetak Struk',
            disabled : true,
            id: 'CetakStruk', 
            iconCls: 'icon-print',
            handler: function(){
                    //////////////////////////-------------PRINT MANUAL JIKA STATUS FAIL DI GRID----------/////////////////////        
                    if (Ext.getCmp('modul_fail_claim').getValue() == 'Inactive') {
                        Ext.getCmp('ongridStat').setValue('');
                        win_popup_print('Inactive','Inactive','');
                    }
                    
                    if (Ext.getCmp('modul_fail_claim').getValue() == 'Pending') {     
                        Ext.getCmp('ongridStat').setValue('');
                        win_popup_print('Pending','Pending','');
                    }
                    
					// YANG CETAK LANGSUNG KE dashboardprovidercetakstruklangsung
					// YANG CETAK DARI GRID KE dashboardprovidercetakstruk
                    if(Ext.getCmp('ongridStat').getValue() == 1){
                        doSemarDoc('doc',{
                                    title:'Cetak Struk',
                                    Template_ID:'dashboardprovidercetakstruk',
                                    Data_ID:'claimId',
                                    Grid_ID:'Grid_T_D_Claim'
                        },[]);
                    } 

                    /*if(Ext.getCmp('ongridStat').getValue() == 1) {
                      doSemarDoc('doc',{
                                    title:'Cetak Struk',
                                    Template_ID:'dashboardprovidercetakstruk',
                                    Data_ID:'claimId',
                                    Grid_ID:'Grid_T_D_Claim'
                        },[]);
                    }*/    
                        
                      
                }
        }, '-', {
            text: 'Proses Claim',
            disabled : true,
            id: 'Proses_Claim', 
            iconCls: 'icon-edit',
            handler: function() {
   			   Ext.getCmp('t_date_claim').show(true);
               Ext.getCmp('t_description').show(true);
               //Ext.getCmp('t_room_class').show(true);
               //Ext.getCmp('Update_claim').disable(true);
               //Ext.getCmp('Hapus_claim').disable(true);
               Ext.getCmp('simpan_Claim').enable(true);
               ActiveInput();
               clearInput();	                
            }
        }, 
          '-',   {
            text: 'Simpan',
            disabled : true,
            id: 'simpan_Claim', 
            iconCls: 'icon-save',
            handler: function() {
                 Ext.getCmp('layout-body').body.mask("cek transaksi...", "x-mask-loading");
                Form_T_D_verifikasi.getForm().submit({            			
                    success: function(form, action){
                        Ext.getCmp('Form_T_D_verifikasi').body.unmask(); 
                        Ext.getCmp('layout-body').body.unmask();
                        obj = Ext.decode(action.response.responseText);
                          
                        if(IsNumeric(obj.info.reason)){ 
                            Ext.MessageBox.show({title:'Informasi !', msg: 'Sukses menambah data !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});  			
                        }else{
                             Ext.MessageBox.show({title:'Informasi !', msg: obj.info.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});
                        }
                      Data_T_D_Claim.load({params:{memberId : Ext.getCmp('t_memberId').getValue() ,
                      								providerId : Ext.getCmp('t_providerId').getValue(),
                                                    clientId : Ext.getCmp('t_clientId').getValue()
                                                    }}); 
                      
                      
                      if(obj.info.reject ==  false ){
                          if(Ext.getCmp('t_jenis_claim').getValue() == 1){                            
                             win_popup_print(Ext.getCmp('t_jenis_claim').getValue(),'Rawat Inap', obj.info.reason);
                          }
                          if(Ext.getCmp('t_jenis_claim').getValue() == 2){                            
                             win_popup_print(Ext.getCmp('t_jenis_claim').getValue(),'Rawat Jalan', obj.info.reason);
                          }
                          if(Ext.getCmp('t_jenis_claim').getValue() == 3){                            
                             win_popup_print(Ext.getCmp('t_jenis_claim').getValue(),'Rawat Gigi', obj.info.reason);
                          }
                          if(Ext.getCmp('t_jenis_claim').getValue() == 4){                            
                             win_popup_print(Ext.getCmp('t_jenis_claim').getValue(),'Emergency', obj.info.reason);
                          }
                      }
                      DeactiveInput(); 
                      Ext.getCmp('batal_Claim').disable(true);
                	  Ext.getCmp('simpan_Claim').disable(true);
                      //Ext.getCmp('Update_claim').disable(true);
                	  //Ext.getCmp('Hapus_claim').disable(true);
                    },
                    failure: function(form, action){
                      Ext.getCmp('Form_T_D_verifikasi').body.unmask();
                      Ext.getCmp('layout-body').body.unmask();
                      if (action.failureType == 'server') {
                        obj = Ext.decode(action.response.responseText);
                        Ext.MessageBox.show({title:'Peringatan !', msg: obj.info.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                      }else{
                        if (typeof(action.response) == 'undefined') {
                            Ext.MessageBox.show({title:'Peringatan !', msg: 'Silahkan isi dengan benar !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                        }else{
                            Ext.MessageBox.show({title:'Peringatan !', msg: 'Server tidak dapat dihubungi !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                        }
                      }
                    }
                  });  
            }
        },'-',
        /*{   text: 'Ubah',  
            id: 'Update_claim', 
            disabled : true,
            iconCls: 'icon-edit', 
            handler: function(){                  
                Ext.getCmp('simpan_Claim').enable(true);
                Ext.getCmp('Update_claim').disable(true);
                Ext.getCmp('t_date_claim').show(true);
                Ext.getCmp('t_description').show(true);
                //Ext.getCmp('t_room_class').show(true); 
                var sm = Grid_T_D_Claim.getSelectionModel(), sel = sm.getSelection();
                if(sel.length > 0){ 
                   Ext.getCmp('t_claimId').setValue( sel[0].get('claimId'));  
                   Ext.getCmp('t_date_claim').setValue(new Date(sel[0].get('date_claim'))); 
                   Ext.getCmp('t_jenis_claim').setValue( sel[0].get('servicetypeId'));               
                   Ext.getCmp('t_description').setValue( sel[0].get('description'));
                   //Ext.getCmp('t_room_class').setValue( sel[0].get('room_class'));
                   
                }
            }
        }, '-',
  	   {   text: 'Hapus', 
           id: 'Hapus_claim',  
           disabled : true,  
           iconCls: 'icon-delete', 
           handler: function(){                
                  var sm = Grid_T_D_Claim.getSelectionModel(), sel = sm.getSelection();
                  if(sel.length > 0){
                        Ext.Msg.show({
                        title: 'Konfirmasi', msg: 'Apakah Anda yakin untuk menghapus ?',
                        buttons: Ext.Msg.YESNO, icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn == 'yes') {
                                var data = '';
                                for (i = 0; i < sel.length; i++) {
                                data = data + sel[i].get('claimId') + '-';
                                    }
                                    Ext.Ajax.request({
                                    url: BASE_URL + 'dashboardproviderv1/ext_delete', method: 'POST',
                                    params: { postdata: data },
                               		success: function(response){
                                    Data_T_D_Claim.load(
                                    				{params:{memberId : Ext.getCmp('t_memberId').getValue() ,
                      								providerId : Ext.getCmp('t_providerId').getValue(),
                                                    clientId : Ext.getCmp('t_clientId').getValue()
                                                    }
                                                    });
                                      DeactiveInput();
                                    },
                                    failure: function(response){ Ext.MessageBox.show({title:'Peringatan !', msg: obj.errors.reason,
                                    buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR}); }
                            });
                            }
                        }
                    });
                  }
 
           }
       },'-',*/
       {
            text: 'Batal Claim',
            disabled : true,
            id: 'batal_Claim', 
            iconCls: 'icon-undo',
            handler: function() {
                DeactiveInput();  
            }
        }, '->',
		
		{
            text: 'Logout',
            disabled : false,
            id: 'logout', 
            iconCls: 'icon-minus-circle',
            handler: function() {
                        do_logout();
                    }
        }, 
           
    ]
});




Ext.define('service_typeModel', {
    extend: 'Ext.data.Model',
    fields: [
        'servicetypeId', 'service_type'
    ]
});
var data_store_service_type = Ext.create('Ext.data.Store', {
    model: 'service_typeModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardproviderv1/get_serviceType/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: true,
    LoadMask: true
});


Ext.define('Plan_typeModel', {
    extend: 'Ext.data.Model',
    fields: [
        'plan_type', 'description'
    ]
});
var data_store_Plan_type = Ext.create('Ext.data.Store', {
    model: 'Plan_typeModel',
    autoDestroy: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        api: {
            read: BASE_URL + 'dashboardproviderv1/get_PlanType/',
        },
        reader: {
            type: 'json',
            root: 'model',
            successProperty: 'success'
        }
    },
    autoLoad: false,
    LoadMask: true
});

var Form_T_D_verifikasi = new Ext.create('Ext.form.Panel', {
 	id: 'Form_T_D_verifikasi', url: BASE_URL + 'dashboardproviderv1/ext_insert',
      frame: true, bodyStyle: 'padding: 5px 5px 0 0;', height: '100%',
      fieldDefaults: {labelAlign: 'left', msgTarget: 'side'}, defaultType: 'textfield',
      defaultType: 'textfield', defaults: {allowBlank: false}, buttonAlign:'left', autoScroll:true,
      items: [
      {
      xtype: 'fieldset',
      id :'JenisClaimFieldset', 
      hidden : true,  
      margins: '15px 15px 15px 15px',
      style: 'padding: 10 5px 3px 5px;border:none;',                       
      defaults: {
               labelWidth: 120, 
              }, 
      flex: 1,
      items: [{
                 xtype: 'hidden', 
                 name: 't_claimId',
                 id: 't_claimId',  
                 readOnly: true,                                                
             },{
                 xtype: 'hidden', 
                 name: 't_providerId',
                 id: 't_providerId',
                 value : '<?php echo @$providerId;?>', 
                 readOnly: true,                                                
             },{
                 xtype: 'hidden', 
                 name: 't_clientId',
                 id: 't_clientId',
                 value : '<?php echo @$clientId;?>', 
                 readOnly: true,                                                
             },{
                 xtype: 'hidden', 
                 name: 't_memberId',
                 id: 't_memberId', 
                 readOnly: true,                                                
             }, 
             {
                 xtype: 'hidden', 
                 name: 't_f_relationship_type',
                 id: 't_f_relationship_type', 
             },
             {
              id		: 't_jenis_claim',
              xtype		: 'combobox',
              allowBlank: false,
              store		: data_store_service_type,
              displayField: 'service_type',
              valueField  : 'servicetypeId',
              fieldLabel	: 'Jenis Layanan Claim',  
              name		: 't_jenis_claim',
              queryMode	: 'local',
              anchor :'50%',
              listeners	: {
                 change : function(val){
                    if(val.getValue() != ''){
                      Ext.getCmp('Proses_Claim').enable(true); 
                    }else{
                      Ext.getCmp('Proses_Claim').disable(true);                                           }
                 }
              }  
           },
           {
              xtype: 'datefield',
              hidden : true,
              fieldLabel	: 'Tanggal Claim', 
              name: 't_date_claim',
              id: 't_date_claim', 
              height: 22,
              anchor :'50%', 
              format: 'd/m/Y', emptyText: 'dd/mm/yyyy', maskRe: /[\d\/\;]/, altFormats: 'dmY|Y-m-d',
              value : new Date(),
              flex: 2
          },
          {
              xtype: 'textarea',
              hidden : true,
              fieldLabel	: 'Deskripsi', 
              name: 't_description',
              id: 't_description', 
              anchor :'50%',
              height: 52, 
              flex: 2
          },
          /*
          {
              xtype: 'combo',
              hidden : true,
              fieldLabel : 'Room Class', 
              name: 't_room_class',
              id: 't_room_class', 
              anchor :'50%',
              height: 22,
              emptyText: 'Pilih Room Class',
              queryMode: 'local', typeAhead: true, forceSelection: true, allowBlank: true, 
              store: data_store_Plan_type,
              displayField: 'description',
              valueField  : 'plan_type',
              flex: 2
          } 
          */
          ]
     },Grid_T_D_Claim
  ]
  
});



//Search Card Variant=================
var cardSearch =  {    
	layout: 'fit',
	border: false,
	items:[{
            "frame": false,
            "border": false,
            "disabled": false,                    
            "flex": 1, 
            "id": "dashboardproviderv1_form_blk_form_text_5",
            "semar": true,
            "xtype": "panel",
            "name": "dashboardproviderv1_form_blk_form_text_5",
            "defaults": {  
               pack: "center",
               align: "center"
            },
            "layout": {
                "type": "hbox"
            },
            "items": [                      
                {
                 xtype:'component',
                 html :'<span  style="font-size:14px; text-align:right;"><b>No Member Card  &nbsp;</b></span>'                                  },{
                xtype : 'textfield', 
                width:300,
                name : 'member_cardNo',
                id : 'member_cardNo', 
                fieldStyle:'font-size:16px; height:25px;', 
				enableKeyEvents: true,
				listeners: {
					keypress : function(textfield,eventObject){
						if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
							Ext.getCmp('search_pegawai_ST').handler.call(Ext.getCmp('search_pegawai_ST').scope);
						}
					}
				}
             }, 
              {
                 xtype: 'button',
                 name: 'search_pegawai',
                 id: 'search_pegawai_ST',
				 "iconCls": "semar-view",
                 //text: 'Proses', 
                 margins: '0 5 5 5',
                 height:25,
                 handler: function() {
                    Ext.getCmp('layout-body').body.mask("Proses...", "x-mask-loading");
                    Ext.Ajax.request({
                    url: BASE_URL + 'dashboardproviderv1/getMember/',
                    params:{ member_cardNo : Ext.getCmp('member_cardNo').getValue(), 
                    	     client_name : Ext.getCmp('s_client_name').getValue() },
                    callback: function(options, success, response) {
                         var row = Ext.JSON.decode(response.responseText); 
                        Ext.getCmp('layout-body').body.unmask();
                        if(row.total > 0){
                           //CEK Data No Member Card ada namun status_card = Inactive atau Pending
                           if(row.model.status_card == 'Inactive'){
                               var Error = '<font size="6">Maaf Member Card Inactive <br /> Hubungi Intl SOS 24 Hours 021 - 7659163</font>';
                               Ext.fly('NohaveInformasiMember').update(Error);
                               Ext.getCmp('modul_fail_claim').setValue('Inactive');                                                           DeActive();
                           }
                           if(row.model.status_card == 'Pending'){
                               var Error = '<font size="6">Maaf Member Card Pending <br /> Hubungi Intl SOS 24 Hours 021 - 7659163</font>';
                               Ext.fly('NohaveInformasiMember').update(Error);
                               Ext.getCmp('modul_fail_claim').setValue('Pending');
                               DeActive();
                           }
                           
                           if(row.model.status_card == 'Active'){
                                                                   
                           Data_T_D_Claim.load({params:{memberId : row.model.memberId ,
                      								providerId : Ext.getCmp('t_providerId').getValue(),
                                                    clientId : Ext.getCmp('t_clientId').getValue()
                                                    }}); 
                             
                            Ext.getCmp('t_memberId').setValue(row.model.memberId);
                            Ext.getCmp('t_clientId').setValue(row.model.clientId);
                            Ext.getCmp('t_f_relationship_type').setValue(row.model.relationship_type);
                            
                            Ext.getCmp('t_member_cardNo').setValue(row.model.member_cardNo);
                            Ext.getCmp('t_member_name').setValue(row.model.member_name);
                            Ext.getCmp('t_jenis_kelamin').setValue(row.model.jenis_kelamin);
                            Ext.getCmp('t_member_address').setValue(row.model.member_address);
                            Ext.getCmp('t_member_telp').setValue(row.model.member_telp);
                            Ext.getCmp('t_plan_type').setValue(row.model.plan_desc);
                            Ext.getCmp('t_client_name').setValue(row.model.client_name);
                            Ext.getCmp('t_relationship_type').setValue(row.model.relationship_type);
                            Ext.getCmp('t_employment_status').setValue(row.model.employment_status);
                            Ext.getCmp('t_status_card').setValue(row.model.status_card);
                            Ext.getCmp('t_activation_date').setValue(row.model.activation_date);
                            Ext.getCmp('t_tanggal_lahir').setValue(formatDate(row.model.tgl_lahir));
                            Ext.getCmp('t_dental_limit').setValue(Ext.util.Format.number(row.model.dental_limit, 'Rp 0,00.00/i')); 
                             
                            data_store_Plan_type.load({params:{plan_type : row.model.plan_type}});
                            Active();
                              
                            Data_T_D_Dependent.load({params:{ member_cardNo : row.model.member_cardNo}}); 
                             
                          }
                      }else{                              
                           var Error = '<font size="6">Maaf Member Card Tidak Terdaftar <br /> Hubungi Intl SOS 24 Hours 021 - 7659163</font>';
                           Ext.fly('NohaveInformasiMember').update(Error);
                           DeActive();
                       }
                    }//callback
                     
                    })//extjs
                  }
                }
                          
             ]//========
      }]
 }

 
function formatDate(value){
	if(value !=""){
		if(Ext.isDate(value)){
			return value.dateFormat('M d, Y');
		}else{
		  return value;
	} 
}
return value;
}

//========SEARCH CLIENT
var ClientSearchName = {
   
	layout: 'fit',
	border: false,
	items:[{
            "frame": false,
            "border": false,
            "disabled": false,                    
            "flex": 1, 
            "id": "ClientSearchName",
            "semar": true,
            "xtype": "panel",
            "name": "ClientSearchName",
            "defaults": {  
               pack: "center",
               align: "center"
            },
            "layout": {
                "type": "hbox"
            },
            "items": [                      
                {
                 xtype:'component',
                 html :'<span  style="font-size:14px; text-align:right; padding-right:40px;"><b>Client Name</b></span>'
                  },{
                xtype : 'textfield', 
                width:300,
                name : 's_client_name',
                id : 's_client_name',
				"value": "PT Freeport Indonesia",
                fieldStyle:'font-size:16px; height:25px;', 
               }, 
               {
                 xtype: 'button',
                 name: 'btns_client_name',
                 id: 'btns_client_name',
                 text: '....', 
                 margins: '0 5 5 5',
                 height:25,
                 handler: function() {
                     Load_Panel_Ref('win_popup_Refclient', 'ref_client', 'dashboardproviderv1_form');
                 }
                 
               } 
              ]//========
      }]
 }
 
//Search Member Name Variant=================
/*
var cardSearchName =  {    
	layout: 'fit',
	border: false,
	items:[{
            "frame": false,
            "border": false,
            "disabled": false,                    
            "flex": 1, 
            "id": "cardSearchName",
            "semar": true,
            "xtype": "panel",
            "name": "cardSearchName",
            "defaults": {  
               pack: "center",
               align: "center"
            },
            "layout": {
                "type": "hbox"
            },
            "items": [                      
                {
                 xtype:'component',
                 html :'<span  style="font-size:14px; width:180px; text-align:right;"><b>Member Name &nbsp; &nbsp; &nbsp;</b></span>'                                  },{
                xtype : 'textfield', 
                readOnly: true,
                width:200,
                name : 's_member_name',
                id : 's_member_name',
                fieldStyle:'font-size:16px; height:25px;', 
             }, 
              {
                 xtype: 'button',
                 name: 'btns_member_name',
                 id: 'btns_member_name',
                 text: '....', 
                 margins: '0 5 5 5',
                 height:25,
                 handler: function() {
                     Load_Panel_Ref('win_popup_RefPegawai', 'ref_pegawai', 'dashboardproviderv1_form');
                 }
                 
              }
                          
             ]//========
      }]
 }
*/
 

// TABEL GRID MEMBER  ------------------------------------------------- START
var Params_T_D_Dependent;
Ext.define('MT_D_Dependent', {extend: 'Ext.data.Model',
  fields: ['memberId','client_name','member_cardNo', 'employee_ID', 'member_name',
  		   'relationship_type','jenis_kelamin', 'tgl_lahir',
           'location_name','location_region','plan_desc','room_class','employment_status',
           'status_card','dental_limit'  ]
});
var Reader_T_D_Dependent = new Ext.create('Ext.data.JsonReader', {
  id: 'Reader_T_D_Dependent', root: 'results', totalProperty: 'total', idProperty: 'memberId'  	
});

var Proxy_T_D_Dependent = new Ext.create('Ext.data.AjaxProxy', {
  id: 'Proxy_T_D_Dependent', url: BASE_URL + 'dashboardproviderv1/getMemberdependent', actionMethods: {read:'POST'}, extraParams: {id_open:1},
  reader: Reader_T_D_Dependent,
  afterRequest: function(request, success) {
   	Params_T_D_Dependent = request.operation.params;
  }
});

 

var Data_T_D_Dependent = new Ext.create('Ext.data.Store', {
	id: 'Data_T_D_Dependent', model: 'MT_D_Dependent', pageSize: 20, noCache: false, autoLoad: false,
  proxy: Proxy_T_D_Dependent
});
 
var Grid_T_D_Dependent = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_D_Dependent',  store: Data_T_D_Dependent, frame: true, border: true, loadMask: true, noCache: false,
    style: 'margin:0 auto;', autoHeight: true, columnLines: true,  
	columns: [
  	{header:'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .5px;'}, 
  	{header: "Client", dataIndex: 'client_name', hidden: true, width: 240}, 
    {header: "No. Kartu Member", dataIndex: 'member_cardNo', width: 100}, 
    {header: "NIK", dataIndex: 'employee_ID', hidden: true, width: 140}, 
    {header: "Nama Member", dataIndex: 'member_name', width: 240}, 
	{header: "Tgl Lahir", dataIndex: 'tgl_lahir', width: 90},
	{header: "Jns Kelamin", dataIndex: 'jenis_kelamin', width: 50}, 
	{header: "Hak Kelas Perawatan", dataIndex: 'plan_desc',  width: 140}, 
	{header: "Hak Rawat Gigi", dataIndex: 'dental_limit', width: 100, format: 'Rp 0,00.00', xtype: 'numbercolumn'}, 
    {header: "Relasi", dataIndex: 'relationship_type', width: 90}, 
    {header: "Lokasi", dataIndex: 'location_region', width: 90}, 
    {header: "Provinsi", dataIndex: 'location_region', width: 90}, 
    {header: "Kelas Ruangan", dataIndex: 'room_class',  width: 90, hidden: true}, 
    {header: "Status Member", dataIndex: 'employment_status',flex:1}, 
    {header: "Status Kartu", dataIndex: 'status_card', hidden: true, width: 100}, 
    
  ],  
  dockedItems: [{xtype: 'pagingtoolbar', store: Data_T_D_Dependent, dock: 'bottom', displayInfo: true,
  listeners :{
      beforechange: function() { 					     
	   Data_T_D_Dependent.getProxy().extraParams.member_cardNo = Ext.getCmp('t_member_cardNo').getValue();  
  	}   
  }
  }], 
});

var workspace_dashboardproviderv1 = []
var Form_Verification = {
	id: 'Form_Verification',    
	layout: 'border',
	border: false,
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1, 
            "height" : '35%',
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
            bbar : Btn_InformasiMember,
			"items": [
                 {
                      xtype:'component',
                      id : 'content_fail_claim',    
                      hidden :true,                  
                      html :'<br /><br /><center><span id="NohaveInformasiMember"></span></center>',					  
                 },
                 {
                     xtype :'hidden',
                     id : 'modul_fail_claim'
                 },
                 {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "INFORMASI MEMBER", 
				"method": "POST",
				"autoScroll": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "dashboardproviderv1_module_real_general_form_2",
                hidden :true,
				"semar": true,
				"xtype": "form",
				"name": "dashboardproviderv1_module_real_general_form_2",
				"defaults": {
					"xtype": "panel",
					"layout": "anchor",
					"anchor": "100%",
					"flex": 1,
					"margin": 5,
					"defaults": {
						"anchor": "100%",
						"flex": 1
					}
				},
				"layout": {
					"type": "anchor"
				},  
                              
				"items": [
                    { //START--------
                        xtype: 'fieldcontainer',
                        hidden : true,
                        id :'fieldContainerInformasiMember',
                        combineErrors: false,
                        layout: 'hbox',
                        msgTarget: 'side',
                        items: [ //CT1
                             /////START KOLOM LEFT////////////////////////////////////////////////////
                            {
                             	xtype: 'fieldset',
                                defaults: {
                                    anchor: '100%'
                                },
                                margins: '0 5px 0 0',
                                style: 'padding: 0 0 0 0; border-width: 0px;', 
                                flex: 1,
                                items: [{
                                        xtype: 'fieldset',
                                        margins: '0 5px 0 0',
                                        style: 'padding: 5px 5px 3px 5px; border:none;',
                                        defaults: {
                                            labelWidth: 120,
                                            fieldStyle:"border:none 0px"
                                        },
                                        defaultType: 'textfield',
                                        flex: 1,
                                        items: [
                                               {
                                                    xtype: 'textfield',
                                                    fieldLabel: 'No. Kartu Member',
                                                    name: 't_member_cardNo',
                                                    id: 't_member_cardNo',
                                                    anchor : '100%', 
                                                    readOnly: true,
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    fieldLabel: 'Nama',
                                                    name: 't_member_name',
                                                    id: 't_member_name', 
                                                    anchor : '100%',
                                                    readOnly: true,
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    fieldLabel: 'Jenis Kelamin',
                                                    name: 't_jenis_kelamin',
                                                    id: 't_jenis_kelamin',
                                                    anchor : '100%',
                                                    readOnly: true,
                                                },
                                                 {
                                                    xtype: 'textfield',
                                                    fieldLabel: 'Tgl Lahir',
                                                    name: 't_tanggal_lahir',
                                                    id: 't_tanggal_lahir',
                                                    anchor : '100%',
                                                    readOnly: true,
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    fieldLabel: 'Hak Kelas Perawatan',
                                                    name: 't_plan_type',
                                                    id: 't_plan_type',
                                                    anchor : '100%',
                                                    readOnly: true,
                                                },
												{
                                                    xtype: 'hiddenfield',
                                                    fieldLabel: 'Alamat',
                                                    name: 't_member_address',
                                                    id: 't_member_address',
                                                    anchor : '100%',
                                                    readOnly: true,
                                                },
                                                {
                                                    xtype: 'hiddenfield',
                                                    fieldLabel: 'Telp',
                                                    name: 't_member_telp',
                                                    id: 't_member_telp',
                                                    anchor : '100%',
                                                    readOnly: true,
                                                },
                                            ]
                                        
                                       }]
                            }, 
                            //////////////////////////////////RIGTH///////////////////////////////////////
                            {
                                xtype: 'fieldcontainer',
                                combineErrors: false,
                                layout: 'hbox',
                                msgTarget: 'side',
                                items: [{
                                    xtype: 'fieldset', 
                                    margins: '0 5px 0 0',
                                    style: 'padding: 0 5px 3px 5px; border:none;',
                                    defaults: {
                                        labelWidth: 120,
                                        fieldStyle:"border:none 0px"
                                    },
                                    defaultType: 'textfield',
                                    flex: 1,
                                    items: [{
                                               xtype: 'textfield',
                                               fieldLabel: 'Perusahaan',
                                               name: 't_client_name',
                                               id: 't_client_name',
                                               anchor : '100%',
                                               readOnly: true,                                                
                                           },
                                           {
                                               xtype: 'textfield',
                                               fieldLabel: 'Relasi',
                                               name: 't_relationship_type',
                                               id: 't_relationship_type',
                                               anchor : '100%',
                                               readOnly: true,
                                           },
										   {
                                                xtype: 'textfield',
                                                fieldLabel: 'Hak Rawat Gigi',
                                                name: 't_dental_limit',
                                                id: 't_dental_limit',
                                                anchor : '100%',
                                                readOnly: true,
												decimalSeparator: ',', 
												listeners: {
													change: function(field) {
														
													} 
												}
                                            },  
											{
                                               xtype: 'hiddenfield',
                                               fieldLabel: 'Status Pegawai',
                                               name: 't_employment_status',
                                               id: 't_employment_status',
                                               anchor : '100%',
                                               readOnly: true,
                                           },
                                           {
                                               xtype: 'hiddenfield',
                                               fieldLabel: 'Status Card',
                                               name: 't_status_card',
                                               id: 't_status_card',
                                               anchor : '100%',
                                               readOnly: true,
                                           },
                                           {
                                               xtype: 'hiddenfield',
                                               fieldLabel: 'Tanggal Aktif',
                                               name: 't_activation_date',
                                               id: 't_activation_date',
                                               anchor : '100%',
                                               readOnly: true,
                                           },
                                     ]
                                    
                                  }]//END RIGHT//////////////////////////////////////
                            }] 
                     }//END CONTAINER
                     ,
                     {
                        xtype: 'fieldset',  
                        id :'dependent_fieldset',
                        title :'<b>FAMILY MEMBER LIST</b>',
                        margins: '5px 5px 5px 5px',
                        style: 'padding: 0 5px 3px 5px;',
                        defaults: {
                                 labelWidth: 120
                                },
                        defaultType: 'textfield',
                        flex: 1,
                        items: [ Grid_T_D_Dependent ]
                    },//END FIELDSET DEPENT 
                    Form_T_D_verifikasi
                 ]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "north",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": false,
				"disabled": false,
				//"flex": 1,
				"title": "Verifikasi Member",
				"width": 981, 
				"method": "POST",				
				"autoScroll": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "dashboardproviderv1_form",
				"semar": true,
				"xtype": "form",
				"name": "dashboardproviderv1_form",
				"defaults": {
					"xtype": "panel",
					"layout": "anchor",
					"anchor": "100%",
					"flex": 1,
					"margin": 5,
					"defaults": {
						"anchor": "100%",
						"flex": 1
					}
				},
				"layout": {
					"type": "anchor"
				},
				"items": [
                {
                  xtype: 'fieldcontainer',  
                  combineErrors: false,
                  layout: 'hbox',
                  msgTarget: 'side',
                  items: [ //CT1
                     /////START KOLOM LEFT////////////////////////////////////////////////////
                    {
                     xtype: 'fieldset',
                     defaults: {
                       anchor: '100%'
                     },
                      margins: '0 5px 0 0',
                      style: 'padding: 0 0 0 0; border-width: 0px;', 
                      flex: 1,
                      items: [
                             ///START COMPONENT///////////////////////////
                             {
                                      xtype :'component',
                                      html : '<p align="right"><b>Operator  : <?php echo $this->session->userdata("fullname_zs_exmldashboard");?></b></center> <br />'
                             },
                             {
                                      xtype :'component',
                                      html : '<center><b>WEB CLAIM <br />  <?php echo count(@$Provider) == 0 ? @$client : @$Provider;?></b></center> <hr /> '
                             }]///END COMPONENT///////////////////////////
                    }] 
 				},
                {
                xtype :'panel', 
                border: false,
                defaults: {
                  border: false,
                  autoscroll:true
                },
                layout:'column',
                    items: [{ columnWidth: .30, html: '&nbsp;' },
                            { columnWidth: .70, items:[ ClientSearchName ]      
                           }]
               },
               {
                xtype :'panel',                 
                border: false,
                defaults: {
                  border: false,
                  autoscroll:true,
                  autoHeight:true
                },
                layout:'column',
                    items: [{ columnWidth: .30, html: '&nbsp;' },
                            { columnWidth: .70, items:[ cardSearch ]      
                           }]
               }
               ]
               
			}]
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_dashboardproviderv1, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_dashboardproviderv1, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
		}
	}
}

var Form_T_D_Verified = new Ext.create('Ext.form.Panel', {
 	  id: 'Form_T_D_Verified', url: BASE_URL + 'diklat/diklat_teknis/ext_insert',
      frame: false,  
      fieldDefaults: {labelAlign: 'left', msgTarget: 'side'},  
      defaultType: 'textfield', defaults: {allowBlank: false}, buttonAlign:'left', autoScroll:true,
      items: [
      {xtype:'textfield', id:'x'},
      Form_Verification
      ]      
});
 
<?php echo $this->load->view('berita');?>
///////////////////////////////////////////////BERITA//////////////////////////////////////////////
 var Form_T_D_Berita = new Ext.create('Ext.Panel', {
 	  id: 'Form_T_D_Berita', 
      frame: true,  
      autoScroll:true,
      title :'&nbsp; Berita Health Care',
      items: [panelNewsEvent]      
});

<?php echo $this->load->view('panduan');?>
 
//////////////////////////////////////////////PANDUAN//////////////////////////////////////////// 
var Form_T_D_Panduan = new Ext.create('Ext.Panel', {
 	  id: 'Form_T_D_Panduan', 
      frame: true,  
      autoScroll:true,
      title :'&nbsp; Panduan SOS Health Care',
      items: [panelPanduanEvent]      
});
  
var Tab1_T_D_Verification = {
	id: 'Tab1_T_D_Verification', title: 'Verification', border: false, collapsible: false,
	layout: 'fit', items: [Form_Verification]
};

var Tab2_T_D_Panduan = {
	id: 'Tab2_T_D_panduan', title: 'Panduan', border: false, collapsible: false,
	layout: 'fit', items: [Form_T_D_Panduan]
}; 

var Tab2_T_D_Berita = {
	id: 'Tab2_T_D_Berita', title: 'Berita', border: false, collapsible: false,
	layout: 'fit', items: [Form_T_D_Berita ]
};

var  new_tabpanel_dashboard = new Ext.createWidget('tabpanel', {
	id: 'new_tabpanel_dashboard', layout: 'fit', resizeTabs: true,  enableTabScroll: false, 
    deferredRender: true, border: false, defaults: {autoScroll:true},  
    items: [Tab1_T_D_Verification, Tab2_T_D_Panduan, Tab2_T_D_Berita], 
    listeners: {
  	'tabchange': function(tabPanel, tab){
  		 if(tab.title = 'Berita' ) { Data_T_D_News.load() } 
         if(tab.title = 'Panduan' ) { Data_T_D_Panduan.load() }
  	}
  }   
});
 

function Active(){
  Ext.getCmp('fieldContainerInformasiMember').show();
  Ext.getCmp('dashboardproviderv1_module_real_general_form_2').show();
  Ext.getCmp('content_fail_claim').hide();
  Ext.fly('NohaveInformasiMember').update('');                                         
  Ext.getCmp('CetakStruk').enable(true); 
  Ext.getCmp('t_jenis_claim').show(); 
  Ext.getCmp('dependent_fieldset').show();
  Ext.getCmp('JenisClaimFieldset').show();
  Ext.getCmp('Grid_T_D_Claim').show(); 
}

function DeActive(){
  Ext.getCmp('fieldContainerInformasiMember').hide();
  Ext.getCmp('dashboardproviderv1_module_real_general_form_2').hide();
  Ext.getCmp('content_fail_claim').show();
  Ext.fly('dependent_fieldset').hide(); 
  Ext.getCmp('CetakStruk').enable(true); 
  Ext.getCmp('t_jenis_claim').hide(); 
  Ext.getCmp('dependent_fieldset').hide();
  Ext.getCmp('JenisClaimFieldset').hide(); 
  Ext.getCmp('Grid_T_D_Claim').hide(); 
}

function ActiveInput(){  
  Ext.getCmp('Proses_Claim').disable(true);
  Ext.getCmp('batal_Claim').enable(true);
}



function clearInput(){  
  Ext.getCmp('t_claimId').setValue('');        
  //Ext.getCmp('t_date_claim').setValue('');
  Ext.getCmp('t_description').setValue('');
  //Ext.getCmp('t_room_class').setValue('');
}

function DeactiveInput(){
	Ext.getCmp('t_date_claim').hide(true);
    Ext.getCmp('t_description').hide(true);
    //Ext.getCmp('t_room_class').hide(true); 
    Ext.getCmp('Proses_Claim').enable(true);
    Ext.getCmp('simpan_Claim').disable(true); 
    //Ext.getCmp('Update_claim').disable(true); 
    //Ext.getCmp('Hapus_claim').disable(true); 
    Ext.getCmp('t_claimId').setValue('');
    Grid_T_D_Claim.getSelectionModel().deselectAll(); 
}
 
<?php }else{ echo "var new_tabpanel_dashboard = 'GAGAL';"; } ?>