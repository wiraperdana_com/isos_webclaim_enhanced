<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller digenerate oleh eXML Builder.
 * Controller untuk module dashboardproviderv1.
 *
 */
class dashboardproviderv1 extends CI_Controller
{

	/**
	 * __construct Controller untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_dashboardproviderv1'));		 
	}


	/**
	 * widget_dashboard Controller untuk module $modulename.
	 */
	public function widget_dashboard()
	{
		
		if($this->input->post('id_open')){
			$data['jsscript'] = TRUE; 
			$PV =  $this->model_dashboardproviderv1->get_provider() ; 
			$CL =  $this->model_dashboardproviderv1->get_Client() ; 
			  
			if(count(@$PV->provider_name)  > 0){
				$data['Provider'] = @$PV->provider_name;
				$data['providerId'] = @$PV->providerId;
			}
			if(count(@$CL->client_name)  > 0){
				$data['client'] = @$CL->client_name; //disini jadi client
				$data['clientId'] = @$CL->clientId;//disini jadi client
			}
 			
			if(count(@$PV->provider_name)  > 0 && count(@$CL->client_name)  > 0){
				$data['Provider'] = '';
				$data['providerId'] ='';
				$data['client'] = '';
				$data['clientId'] = '';
			}
			
			$this->load->view('dashboard',$data);
		}else{
			$this->load->view('dashboard');
		};
	}


	/**
	 * call_method Controller untuk module $modulename.
	 */
	public function call_method($querytype, $typex, $valuex)
	{
		echo json_encode( $this->model_dashboardproviderv1->call_method( $typex, $querytype, $valuex ) );
	}
	
	public function getMember(){
		 
		if($this->input->post('client_name') !='' ){
			$this->db->where('client_name', $this->input->post('client_name'));	
		}
		$this->db->where('member_cardNo', $this->input->post('member_cardNo'));	
		$this->db->where_in('status_card', array('Active','Inactive','Pending' ));
		$this->db->where('CURRENT_DATE() >= activation_date');
		$member = $this->db->from('tvtmasmember')->get()->row();
		
		if($member){
		  //$this->model_dashboardproviderv1->sendMail(@$member->status_card, $member->member_email);
		}
 		echo json_encode(array("model" => $member, 'total' => count($member)));  
	}
	
	
	
	public function getMemberdependent(){
		if($this->input->post("id_open")){
			$data = $this->model_dashboardproviderv1->get_AllDataDPDNT();	  			 	
			$total = $this->model_dashboardproviderv1->get_CountDataDPDNT();	  
   		echo '({total:'. $total . ',results:'.json_encode($data).'})';
  	    }  
	}
	
	public function get_PlanType(){
		if($this->input->post('plan_type') !=''){
			$this->db->where('plan_type', $this->input->get('plan_type'));
		}
		$plan = $this->db->select('plan_type, description')->from('sos_tref_plan_type')->get()->result();
		$data = array();
		foreach($plan as $key =>$value){
 		  $data[] = $value;   
	    }	 	  
	    echo json_encode(array(  "success" => true, "model" => $data));
	}
	
	public function get_serviceType(){
		$service = $this->db->select('servicetypeId, service_type')->from('sos_tref_service_type')->get()->result();
		$data = array();
		foreach($service as $key =>$value){
 		  $data[] = $value;   
	    }	 	  
	    echo json_encode(array(  "success" => true, "model" => $data));
	}
	
	
	
	function ext_insert(){  	
		$Status = $this->model_dashboardproviderv1->Insert_Data();	 
		if($Status == "Updated"){
			echo "{success:true, info: { reject: false,reason: 'Sukses Merubah data !' }}";
		}elseif(is_numeric($Status)){
			echo "{success:true, info: { reject: false,reason: '".$Status."' }}";
			$this->sendmailAdmin( $this->input->post('t_memberId')); 
		}elseif( $Status == 'reject'){
			echo "{success:true, info: { reject: true, reason: 'Maaf lakukan lagi transaksi setelah 5 menit' }}";
			$this->sendmailAdmin( $this->input->post('t_memberId')); 
		}else{
			echo "{success:false, info: { reject: false, reason: 'Gagal menambah Data !' }}";
		}
  }
  
  function sendmailAdmin($memberId){
		$config = Array(
			'protocol' => 'smtp', 
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => '465',
			'smtp_user' => 'int.sos.managed.healthcare@gmail.com', // change it to yours
			'smtp_pass' => 'sos123456', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
	   );
 		
		$this->db->where('memberId', $memberId);
		$this->db->where('DATE_FORMAT(dates_claim,"%Y-%m-%d")', date("Y-m-d"));
		$content = $this->db->from('tvttransclaim2')->get();			 
		$data['data'] = $content->result();		 	
		$html = $this->load->view('admin_email', $data, true);
		 
		$this->db->where('ID_User', $this->session->userdata("iduser_zs_exmldashboard"));			 
		$eto= $this->db->from('tUser')->get()->row();
 	  
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
 		$this->email->from('noreply@int.sos.managed.healthcare'); // change it to yours
		$this->email->to($eto->email);// change it to yours		
		$judul = '[MHC] Suspected Fraud Detection forMember# '.@$content->row()->NIK.' ( '.@$content->row()->client_name.' )';
		$this->email->subject($judul);
		$this->email->message($html);
		$this->email->set_mailtype('html');
		
		if(!$this->email->send()){
	      //show_error($this->email->print_debugger());
	     }
	
	}   
  
  function ext_delete(){
		$this->model_dashboardproviderv1->Delete_Data();	
  }
  
  function ext_get_all(){
		if($this->input->post("id_open")){
			$data = $this->model_dashboardproviderv1->get_AllData();	  			 	
			$total = $this->model_dashboardproviderv1->get_CountData();	  
   		    echo '({total:'. $total . ',results:'.json_encode($data).'})';
  	    }
  }
  
  function berita_page(){
	 if($this->input->post("id_open")){
			$data = $this->model_dashboardproviderv1->get_News();	  			 	
			$total = $this->model_dashboardproviderv1->get_CountNews();	  
   		    echo '({total:'. $total . ',results:'.json_encode($data).'})';
    }
  }
  
  function panduan(){
	 if($this->input->post("id_open")){
			$data = $this->model_dashboardproviderv1->get_Panduan();	  			 	
			$total = $this->model_dashboardproviderv1->get_CountPanduan();	  
   		    echo '({total:'. $total . ',results:'.json_encode($data).'})';
    }
  }
 
  
  //PRINT STRUK////////////////////////////////////////////////////////////////////////////////////////////////////////
	function print_struk(){
		 
		switch($this->input->get('type')){
		   
		   case 'Inactive':
		     $this->struk_inactive();
		   break;
		   
		   case 'Pending':
		     $this->struk_pending();
		   break;
 		   
		   case  1: //rawat inap
		      $this->struk_claim($this->input->get('id'));
		   break;
		   
		   case  2: //rawat inap
		      $this->struk_claim($this->input->get('id'));
		   break; 
		   
		   case  3: //rawat inap
		      $this->struk_claim($this->input->get('id'));
		   break;  

			case  4: //emergency
		      $this->struk_claim($this->input->get('id'));
		   break;  		   
		}
	}
	function struk_inactive(){
	   $config['upload_path'] = "../../templates/docs/dashboardprovidercetakstrukfail/template_code/";
	   $data['data_cetak'] = $this->db->from('tvttransclaim2')->get()->result();
	   $this->load->view($config['upload_path'].'pdf_dashboardprovidercetakstruk', $data);	
	}
	
	function struk_pending(){
	   $config['upload_path'] = "../../templates/docs/dashboardprovidercetakstrukfail/template_code/";
	   $data['data_cetak'] = $this->db->from('tvttransclaim2')->get()->result();
	   $this->load->view($config['upload_path'].'pdf_dashboardprovidercetakstruk', $data);	
	}
	
	function struk_claim($claimId){
 		$config['upload_path'] = "../../templates/docs/dashboardprovidercetakstruklangsung/template_code/";	   
	    $data['data_cetak'] = $this->db->from('tvttransclaim2')->where('claimId', $claimId)->get()->result();
	    $this->load->view($config['upload_path'].'pdf_dashboardprovidercetakstruk', $data);
 	}
	
	
	function download(){
	    $this->load->helper('download');  
		$files = $this->db->select('news_file')->from('sos_ttrans_news')->where('newsId', (int)$this->uri->segment(3,0))->get()->row();
        $image_name = "./assets/filemanager/news/".base64_decode($files->news_file);
        $data = file_get_contents($image_name); // Read the file's contents

        force_download($image_name, $data);	
	}
	
	function downloadPanduan(){
	    $this->load->helper('download');  
		$files = $this->db->select('file_panduan')->from('sos_tref_panduan')->where('panduanId', (int)$this->uri->segment(3,0))->get()->row();
        $image_name = "./assets/filemanager/panduan/".base64_decode($files->file_panduan);
        $data = file_get_contents($image_name); // Read the file's contents

        force_download($image_name, $data);	
	}
 	 
	
	function loadpdfNews($id)
	{ 
	   $files = $this->db->select('news_file')->from('sos_ttrans_news')->where('newsId', $id)->get()->row();
	   $myfiles['file_data'] = base_url().'assets/filemanager/news/'.base64_decode($files->news_file);
	   $this->load->view('dashboardproviderv1/loadpdf', $myfiles);  
 	}
	
	function loadpdfPanduan($id)
	{ 
	   $files = $this->db->select('file_panduan')->from('sos_tref_panduan')->where('panduanId', $id)->get()->row();
	   $myfiles['file_data'] = base_url().'assets/filemanager/panduan/'.base64_decode($files->file_panduan);
	   $this->load->view('dashboardproviderv1/loadpdf', $myfiles);  
 	}
	
}
