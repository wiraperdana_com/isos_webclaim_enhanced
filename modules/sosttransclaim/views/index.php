<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start

 Ext.apply(Ext.form.field.VTypes, {
	daterange: function(val, field) {
		var date = field.parseDate(val);

		if (!date) {
			return false;
		}
		if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
			var start = field.up('form').down('#' + field.startDateField);
			start.setMaxValue(date);
			start.validate();
			this.dateRangeMax = date;
		}
		else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
			var end = field.up('form').down('#' + field.endDateField);
			end.setMinValue(date);
			end.validate();
			this.dateRangeMin = date;
		}
		/*
		 * Always return true since we're only using this vtype to set the
		 * min/max allowed values (these are tested for after the vtype test)
		 */
		return true;
	}
});
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_1', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "activation_date",
		"sortDir": "ASC",
		"persist": false,
		"type": "date",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "status_card",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "employment_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "room_class",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "plan_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_region",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_email",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_telp",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_address",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "tanggal_lahir",
		"sortDir": "ASC",
		"persist": false,
		"type": "date",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "jenis_kelamin",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "relationship_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "employee_ID",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_cardNo",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "memberId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_1",
	"name": "data_model_1",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_11', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "1",
		"name": "status_data",
		"sortDir": "ASC",
		"persist": false,
		"type": "smallint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "CURRENT_TIMESTAMP",
		"name": "createdDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "createdBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "claim_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "room_class",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "date_claim",
		"sortDir": "ASC",
		"persist": false,
		"type": "datetime",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "date_claim_time",
		"sortDir": "ASC",
		"persist": false,
		"type": "datetime",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "relationship_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "servicetypeId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "providerId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "memberId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "claimId",
		"sortDir": "ASC",
		"persist": true,
		"type": "bigint",
		"useNull": true
	}],
	"id": "data_model_11",
	"name": "data_model_11",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_16', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "service_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "servicetypeId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_16",
	"name": "data_model_16",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_19', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "1",
		"name": "status_data",
		"sortDir": "ASC",
		"persist": false,
		"type": "smallint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0000-00-00 00:00:00",
		"name": "createdDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "createdBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "claim_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "room_class",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "date_claim",
		"sortDir": "ASC",
		"persist": false,
		"type": "datetime",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "date_claim_time",
		"sortDir": "ASC",
		"persist": false,
		"type": "datetime",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "relationship_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "servicetypeId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "providerId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "dental_limit",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "jenis_kelamin",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "tanggal_lahir",
		"sortDir": "ASC",
		"persist": false,
		"type": "datetime",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "NIK",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "employee_ID",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "memberId",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "claimId",
		"sortDir": "ASC",
		"persist": true,
		"type": "bigint",
		"useNull": true
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "relationshiptype",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "servicetype",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "member_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_19",
	"name": "data_model_19",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_53', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "providerId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": true
	}],
	"id": "data_model_53",
	"name": "data_model_53",
	"extend": "Ext.data.Model"
});
/*Ext.define('data_model_browsereflocation', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": true,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_browsereflocation",
	"name": "data_model_browsereflocation",
	"extend": "Ext.data.Model"
});*/
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
/*Ext.create('Ext.data.Store', {
	"model": "data_model_browsereflocation",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sostmasclient\/call_method\/select\/table\/tvbrowsereflocation",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "location_name",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_browsereflocation",
	"storeId": "data_store_browsereflocation",
	"name": "data_store_browsereflocation"
});*/
Ext.create('Ext.data.Store', {
	"model": "data_model_1",
	"autoLoad": false,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 100,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/tvbrowserrefmasmember2",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "memberId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_2",
	"storeId": "data_store_2",
	"name": "data_store_2"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_11",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/sos_ttrans_claim",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "claimId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_12",
	"storeId": "data_store_12",
	"name": "data_store_12"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_16",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/sos_tref_service_type",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "servicetypeId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_17",
	"storeId": "data_store_17",
	"name": "data_store_17"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_19",
	"autoLoad": false,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 100,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/tvttransclaim",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "claimId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_20",
	"storeId": "data_store_20",
	"name": "data_store_20"
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [{
		"key": "Success",
		"val": "Success"
	}, {
		"key": "Fail",
		"val": "Fail"
	}],
	"id": "data_store_25",
	"storeId": "data_store_25",
	"name": "data_store_25"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_53",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/sos_tmas_provider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "providerId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_54",
	"storeId": "data_store_54",
	"name": "data_store_54"
});
//store define - end
var workspace_sosttransclaim = {
	"w2_space": {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Document 61",
				"isdocument": true,
				"Type_Doc": "pdf",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sosttransclaim_module_real_general_document_61",
				"semar": true,
				"xtype": "panel",
				"name": "sosttransclaim_module_real_general_document_61",
				"layout": {
					"type": "fit"
				},
				"items": []
			}]
		}, /*{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Pilih Location",
				"modulecode": "tvbrowsereflocation",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "form_Advance_Search",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"semar": true,
				"width": 640,
				"height": 492,
				"modal": true,
				"bodyStyle": "padding:5px;",
				"layout": {
					"type": "fit"
				},
				"closeAction": "destroy",
				"id": "location_name_browseref_32",
				"name": "location_name_browseref_32",
				"items": []
			}]
		},*/ {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Invoke 63",
				"invokeCall": "f_cek",
				"invokeReturn": true,
				"invokeParams":[{
									//claimId
									"position":"1",
									"set":"get",
									"type":"integer",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_hidden_56"
								}, {
									//memberId
									"position":"2",
									"set":"get",
									"type":"integer",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57"
								}, {
									//clientId
									"position":"3",
									"set":"get",
									"type":"integer",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58"
								}, {
									//providerId
									"position":"4",
									"set":"get",
									"type":"integer",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_combo_55"
								}, {
									//servicetypeId
									"position":"5",
									"set":"get",
									"type":"integer",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_combo_18"
								}, {
									//relationshiptype
									"position":"6",
									"set":"get",
									"type":"string",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59"
								}, {
									//dateclaim
									"position":"7",
									"set":"get",
									"type":"string",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_date_22"
								}, {
									//description
									"position":"8",
									"set":"get",
									"type":"string",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_textarea_23"
								}, {
									//roomclass
									"position":"9",
									"set":"get",
									"type":"string",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_text_24"
								}, {
									//claimstatus
									"position":"10",
									"set":"get",
									"type":"string",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_combo_26"
								}],
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sosttransclaim_module_real_general_invoke_63",
				"semar": true,
				"xtype": "panel",
				"name": "sosttransclaim_module_real_general_invoke_63",
				"layout": {
					"type": "fit"
				},
				"items": []
			},{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Invoke proses",
				"invokeCall": "f_tes",
				"invokeReturn": true,
				"invokeParams":[{
									//memberId
									"position":"1",
									"set":"get",
									"type":"integer",
									"value":"sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51"
								}, {
									//clientId
									"position":"2",
									"set":"get",
									"type":"integer",
									"value":"sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29"
								}, {
									//servicetypeId
									"position":"3",
									"set":"get",
									"type":"integer",
									"value":"sosttransclaim_module_real_general_form_14_blk_i_form_combo_18"
								}, {
									//relationshiptype
									"position":"4",
									"set":"get",
									"type":"string",
									"value":"sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35"
								}, {
									//roomclass
									"position":"5",
									"set":"get",
									"type":"string",
									"value":"sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44"
								}],
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sosttransclaim_module_real_general_invoke_proses",
				"semar": true,
				"xtype": "panel",
				"name": "sosttransclaim_module_real_general_invoke_proses",
				"layout": {
					"type": "fit"
				},
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "north",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "south",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "east",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [
				[],
				[]
			]
		}]
	}
}
var new_tabpanel = {
	id: 'newtab_sosttransclaim_module',
	title: 'Trans. Claim',
	iconCls: 'icon-gears',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Claim History",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sosttransclaim_module_real_general_grid_21",
				"semar": true,
				"xtype": "grid",
				"tbar": [
					/*[{
						"id": "sosttransclaim_module_real_general_grid_21_btn_tambah",
						"name": "sosttransclaim_module_real_general_grid_21_btn_tambah",
						"itemId": "sosttransclaim_module_real_general_grid_21_btn_tambah",
						"text": "Tambah",
						"iconCls": "semar-add",
						"configs": {
							"toolbar": "top",
							"text": "Tambah",
							"iconCls": "semar-add",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sosttransclaim_module_real_general_grid_21_btn_hapus",
						"name": "sosttransclaim_module_real_general_grid_21_btn_hapus",
						"itemId": "sosttransclaim_module_real_general_grid_21_btn_hapus",
						"text": "Hapus",
						"iconCls": "semar-delete",
						"configs": {
							"toolbar": "top",
							"text": "Hapus",
							"iconCls": "semar-delete",
							"type": "delete",
							"value": "sos_ttrans_claim",
							"split": true,
							"active": true
						}
					}, "-"],*/
					[{
						"id": "sosttransclaim_module_real_general_grid_21_btn_excel",
						"name": "sosttransclaim_module_real_general_grid_21_btn_excel",
						"itemId": "sosttransclaim_module_real_general_grid_21_btn_excel",
						"text": "Excel",
						"iconCls": "semar-excel",
						"configs": {
							"toolbar": "top",
							"text": "Excel",
							"iconCls": "semar-excel",
							"type": "excel",
							"value": "sos_ttrans_claim",
							"active": true
						}
					}],'->',{
						"id": "sosttransclaim_module_real_general_grid_21_btn_reset",
						"name": "sosttransclaim_module_real_general_grid_21_btn_reset",
						"itemId": "sosttransclaim_module_real_general_grid_21_btn_reset",
						"text": "Reset",
						"iconCls": "semar-undo",
						"handler":function(){
							Ext.getCmp('sosttransclaim_module_real_general_grid_21').getStore().getProxy().extraParams = {"id_open": "1","semar": "true"};
							Ext.getStore('data_store_20').removeAll();
						}
					}
				],
				"store": "data_store_20",
				"dockedItems": [{
					"id": "sosttransclaim_module_real_general_grid_21_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_20",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "sosttransclaim_module_real_general_grid_21_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "sosttransclaim_module_real_general_grid_21",
					"store": "data_store_20",
					"text": "Search"
				}],
				"columns": [{
					"text": "Member Name",
					"dataIndex": "member_name",
					"width": 141,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Client Name",
					"dataIndex": "client_name",
					"width": 170,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Provider Name",
					"dataIndex": "provider_name",
					"width": 127,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Service Type",
					"dataIndex": "servicetype",
					"width": 158,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Relationship Type",
					"dataIndex": "relationshiptype",
					"width": 103,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Claim ID",
					"dataIndex": "claimId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "memberId",
					"dataIndex": "memberId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "employee_ID",
					"dataIndex": "employee_ID",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": false,
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "NIK",
					"dataIndex": "NIK",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": false,
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "tanggal_lahir",
					"dataIndex": "tanggal_lahir",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "jenis_kelamin",
					"dataIndex": "jenis_kelamin",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "dental_limit",
					"dataIndex": "dental_limit",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "clientId",
					"dataIndex": "clientId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "providerId",
					"dataIndex": "providerId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "servicetypeId",
					"dataIndex": "servicetypeId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "relationship_type",
					"dataIndex": "relationship_type",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Date Claim",
					"dataIndex": "date_claim",
					"width": 103,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Date Claim",
					"dataIndex": "date_claim_time",
					"width": 103,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "description",
					"dataIndex": "description",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Room Class",
					"dataIndex": "room_class",
					"width": 98,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Claim Status",
					"dataIndex": "claim_status",
					"width": 76,
					"height": null,
					"flex": null,
					"filter": {
						"type": "string"
					},
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "createdBy",
					"dataIndex": "createdBy",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "createdDate",
					"dataIndex": "createdDate",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "updatedBy",
					"dataIndex": "updatedBy",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "updatedDate",
					"dataIndex": "updatedDate",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "status_data",
					"dataIndex": "status_data",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "checkboxmodel",
				"selModel": {
					"mode": "MULTI",
					"selType": "checkboxmodel"
				},
				"name": "sosttransclaim_module_real_general_grid_21",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Claim Information",
				"width": 485,
				"url": "sosttransclaim\/call_method\/insertupdate\/table\/sos_ttrans_claim",
				"method": "POST",
				"height": 478,
				"autoScroll": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sosttransclaim_module_real_general_form_14",
				"semar": true,
				"xtype": "form",
				"tbar": [
					[{
						"id": "sosttransclaim_module_real_general_form_14_btn_simpan",
						"name": "sosttransclaim_module_real_general_form_14_btn_simpan",
						"itemId": "sosttransclaim_module_real_general_form_14_btn_simpan",
						"text": "Simpan",
						"iconCls": "semar-save",
						"configs": {
							"toolbar": "top",
							"text": "Simpan",
							"iconCls": "semar-save",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sosttransclaim_module_real_general_form_14_btn_ubah",
						"name": "sosttransclaim_module_real_general_form_14_btn_ubah",
						"itemId": "sosttransclaim_module_real_general_form_14_btn_ubah",
						"text": "Ubah",
						"iconCls": "semar-edit",
						"configs": {
							"toolbar": "top",
							"text": "Ubah",
							"iconCls": "semar-edit",
							"type": "normal",
							"value": "true",
							"active": true
						}
					}],
					[{
						"id": "sosttransclaim_module_real_general_form_14_btn_batal",
						"name": "sosttransclaim_module_real_general_form_14_btn_batal",
						"itemId": "sosttransclaim_module_real_general_form_14_btn_batal",
						"text": "Batal",
						"iconCls": "semar-undo",
						"configs": {
							"toolbar": "top",
							"text": "Batal",
							"iconCls": "semar-undo",
							"type": "normal",
							"value": "true",
							"split": true,
							"active": true
						}
					}, "-"],
					[{
						"id": "sosttransclaim_module_real_general_form_14_btn_cetak",
						"name": "sosttransclaim_module_real_general_form_14_btn_cetak",
						"itemId": "sosttransclaim_module_real_general_form_14_btn_cetak",
						"text": "Cetak",
						"iconCls": "semar-pdf",
						"configs": {
							"toolbar": "top",
							"text": "Cetak",
							"iconCls": "semar-pdf",
							"type": "normal",
							"value": "tvttransclaim",
							"active": true
						},
						handler: function() {
							doSemarDoc('doc',{
								title:'Cetak Struk',
								Template_ID:'dashboardprovidercetakstruk',
								Data_ID:'claimId',
								Grid_ID:'sosttransclaim_module_real_general_grid_21'
							},[]);
						}
					}]
				],
				"name": "sosttransclaim_module_real_general_form_14",
				"defaults": {
					"xtype": "panel",
					"layout": "anchor",
					"anchor": "100%",
					"flex": 1,
					"margin": 5,
					"defaults": {
						"anchor": "100%",
						"flex": 1
					}
				},
				"layout": {
					"type": "anchor"
				},
				"items": [{
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_combo_55",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_combo_55",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Provider",
						"name": "providerId",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_combo_55",
						"semar": true,
						"xtype": "combobox",
						"store": "data_store_54",
						"displayField": "provider_name",
						"valueField": "providerId",
						"queryMode": "local",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_22",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_22",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Tanggal Claim",
						"format": "d-m-Y",
						"name": "date_claim",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_22",
						"semar": true,
						"xtype": "datefield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_221",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_221",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Waktu Claim",
						"format": "h:i:s",
						"name": "date_claim_time",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_221",
						"semar": true,
						"xtype": "datefield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_222",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_222",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Claim Tipe",
						"name": "servicetype",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_222",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_223",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_223",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Nama Client",
						"name": "client_name",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_223",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_224",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_224",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Nama Member",
						"name": "member_name",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_224",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_226",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_226",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Member ID",
						"name": "employee_ID",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_226",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_227",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_227",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Tgl. Lahir",
						"name": "tanggal_lahir",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_227",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_228",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_228",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Jns Kelamin",
						"name": "jenis_kelamin",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_228",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_date_229",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_date_229",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "NIK",
						"name": "NIK",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_date_229",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_text_24",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_text_24",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Hak Akses Claim",
						"name": "room_class",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_text_24",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_text_241",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_text_241",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Dental Limit",
						"name": "dental_limit",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_text_241",
						"semar": true,
						"xtype": "textfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_textarea_23",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_textarea_23",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Description",
						"name": "description",
						"allowBlank": true,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_textarea_23",
						"semar": true,
						"xtype": "textareafield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_combo_26",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_combo_26",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": true,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": true,
						"labelWidth": 100,
						"labelAlign": "left",
						"emptyText": "-",
						"fieldLabel": "Claim Status",
						"name": "claim_status",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_combo_26",
						"semar": true,
						"xtype": "hiddenfield",
						"store": "data_store_25",
						"displayField": "val",
						"valueField": "key",
						"queryMode": "local",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_hidden_56",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_hidden_56",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "claimId",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_hidden_56",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_hidden_57",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_hidden_57",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "memberId",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_hidden_58",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_hidden_58",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "clientId",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}, {
					"frame": false,
					"border": false,
					"disabled": false,
					"flex": 1,
					"isformitems": true,
					"id": "sosttransclaim_module_real_general_form_14_blk_form_hidden_59",
					"semar": true,
					"xtype": "panel",
					"name": "sosttransclaim_module_real_general_form_14_blk_form_hidden_59",
					"defaults": {
						"anchor": "100%",
						"flex": 1
					},
					"layout": {
						"type": "anchor"
					},
					"items": [{
						"frame": false,
						"border": false,
						"disabled": false,
						"flex": 1,
						"title": "",
						"margin": "10 5 3 10",
						"padding": "0 0 0 0",
						"readOnly": false,
						"labelWidth": 100,
						"labelAlign": "left",
						"name": "relationship_type",
						"allowBlank": false,
						"id": "sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59",
						"semar": true,
						"xtype": "hiddenfield",
						"items": []
					}]
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Transaction",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sosttransclaim_module_real_general_tabpanel_3",
				"semar": true,
				"xtype": "tabpanel",
				"name": "sosttransclaim_module_real_general_tabpanel_3",
				"defaults": {
					"layout": {
						"type": "vbox",
						"align": "stretch",
						"pack": "start"
					},
					"flex": 1
				},
				"layout": {
					"type": "card"
				},
				"items": [{
					"title": "Transaction List",
					"id": "sosttransclaim_module_real_general_tabpanel_3_tab_1",
					"name": "sosttransclaim_module_real_general_tabpanel_3_tab_1",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"loadMask": true,
						"noCache": false,
						"autoHeight": true,
						"columnLines": false,
						"invalidateScrollerOnRefresh": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "sosttransclaim_module_real_general_grid_4",
						"semar": true,
						"xtype": "grid",
						"store": "data_store_2",
						"dockedItems": [{
							"id": "sosttransclaim_module_real_general_grid_4_pagingtoolbar",
							"hidden": false,
							"xtype": "pagingtoolbar",
							"store": "data_store_2",
							"dock": "bottom",
							"displayInfo": true
						}, {
							xtype:'toolbar',
							"dock": "top",
							layout:'hbox',
							items:[
								{
									"id": "sosttransclaim_module_real_general_grid_4_searchfield",
									"hidden": false,
									flex:1,
									"xtype": "semarwidgetsearchtoolbar",
									"grid_id": "sosttransclaim_module_real_general_grid_4",
									"store": "data_store_2",
									"text": "Search"
								},
								{
									text:'Reset',
									"iconCls": "semar-undo",
									width:60,
									handler:function(){
										Ext.getCmp('form_Advance_Search').getForm().reset();
										Ext.getCmp('end_date').setDisabled(true);
										Ext.getStore('data_store_2').load();
									}
								}
							]
							
						}],
						"columns": [{
							"text": "ID",
							"dataIndex": "memberId",
							"width": 38,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Client ID",
							"dataIndex": "clientId",
							"width": 65,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "No. Member Card",
							"dataIndex": "member_cardNo",
							"width": 149,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Employee ID",
							"dataIndex": "employee_ID",
							"width": 134,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Member Name",
							"dataIndex": "member_name",
							"width": 179,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Relationship Type",
							"dataIndex": "relationship_type",
							"width": 103,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Jns Kelamin",
							"dataIndex": "jenis_kelamin",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Tgl Lahir",
							"dataIndex": "tanggal_lahir",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Address",
							"dataIndex": "member_address",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Telp",
							"dataIndex": "member_telp",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "member_email",
							"dataIndex": "member_email",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Location",
							"dataIndex": "location_name",
							"width": 131,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Region",
							"dataIndex": "location_region",
							"width": 108,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "description",
							"dataIndex": "description",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "plan_type",
							"dataIndex": "plan_type",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "room_class",
							"dataIndex": "room_class",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "employment_status",
							"dataIndex": "employment_status",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "status_card",
							"dataIndex": "status_card",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "activation_date",
							"dataIndex": "activation_date",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}],
						"selType": "checkboxmodel",
						"selModel": {
							"mode": "MULTI",
							"selType": "checkboxmodel"
						},
						"name": "sosttransclaim_module_real_general_grid_4",
						"layout": {
							"type": "fit"
						},
						"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
					}]
				}, {
					"title": "Member Info",
					"id": "sosttransclaim_module_real_general_tabpanel_3_tab_2",
					"name": "sosttransclaim_module_real_general_tabpanel_3_tab_2",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"url": "sosttransclaim\/call_method\/insertupdate\/table\/sos_tmas_member",
						"method": "POST",
						"autoScroll": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "sosttransclaim_module_real_general_form_28",
						"semar": true,
						"xtype": "form",
						"tbar": [
							/*[{
								"id": "sosttransclaim_module_real_general_form_28_btn_proses",
								"name": "sosttransclaim_module_real_general_form_28_btn_proses",
								"itemId": "sosttransclaim_module_real_general_form_28_btn_proses",
								"text": "Proses",
								"iconCls": "semar-check",
								"configs": {
									"toolbar": "top",
									"text": "Proses",
									"iconCls": "semar-check",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}]*/
						],
						"name": "sosttransclaim_module_real_general_form_28",
						"defaults": {
							"xtype": "panel",
							"layout": "anchor",
							"autoScroll": true,
							"defaults": {
								"xtype": "panel",
								"layout": "anchor",
								"anchor": "100%",
								"flex": 1,
								"margin": 5,
								"defaults": {
									"anchor": "100%",
									"flex": 1
								}
							},
							"flex": 1
						},
						"layout": {
							"type": "hbox",
							"align": "stretch"
						},
						"items": [{
							"frame": false,
							"border": true,
							"disabled": false,
							"flex": 1,
							"title": "",
							"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28",
							"autoScroll": true,
							"margins": {
								"top": 0,
								"right": 0,
								"bottom": 0,
								"left": 0,
								"height": 0,
								"width": 0
							},
							"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28",
							"semar": true,
							"xtype": "panel",
							"defaults": {
								"xtype": "panel",
								"layout": "anchor",
								"anchor": "100%",
								"flex": 1,
								"margin": 5,
								"defaults": {
									"anchor": "100%",
									"flex": 1
								}
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_33",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_33",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Employee ID",
									"name": "employee_ID",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_33",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_35",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_35",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Relationship Type",
									"name": "relationship_type",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_37",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_37",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Tgl Lahir",
									"name": "tanggal_lahir",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_37",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_40",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_40",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Email",
									"name": "member_email",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_40",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_42",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_42",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Region",
									"name": "location_region",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_42",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_44",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_44",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Hak Akses Pelayanan",
									"name": "room_class",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_47",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_47",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Status Card",
									"name": "status_card",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_47",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_textarea_49",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_textarea_49",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Address",
									"name": "member_address",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_textarea_49",
									"semar": true,
									"xtype": "textareafield",
									"items": []
								}]
							}, {
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"isformitems": true,
									"id": "sosttransclaim_module_real_general_form_14_blk_form_combo_18",
									"semar": true,
									"xtype": "panel",
									"name": "sosttransclaim_module_real_general_form_14_blk_form_combo_18",
									"defaults": {
										"anchor": "100%",
										"flex": 1
									},
									"layout": {
										"type": "anchor"
									},
									"items": [{
										"frame": false,
										"border": false,
										"disabled": false,
										"flex": 1,
										"title": "",
										"margin": "10 5 3 10",
										"padding": "0 0 0 0",
										"readOnly": false,
										"labelWidth": 100,
										"labelAlign": "left",
										"emptyText": "-",
										"fieldLabel": "Service Type",
										"name": "servicetypeId",
										"allowBlank": false,
										"id": "sosttransclaim_module_real_general_form_14_blk_i_form_combo_18",
										"semar": true,
										"xtype": "hiddenfield",
										"store": "data_store_17",
										"displayField": "service_type",
										"valueField": "servicetypeId",
										"queryMode": "local",
										"items": []
									}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_hidden_51",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_hidden_51",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": false,
									"labelWidth": 100,
									"labelAlign": "left",
									"name": "memberId",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51",
									"semar": true,
									"xtype": "hiddenfield",
									"items": []
								}]
							}]
						}, {
							"frame": false,
							"border": true,
							"disabled": false,
							"flex": 1,
							"title": "",
							"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28",
							"autoScroll": true,
							"margins": {
								"top": 0,
								"right": 0,
								"bottom": 0,
								"left": 0,
								"height": 0,
								"width": 0
							},
							"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28",
							"semar": true,
							"xtype": "panel",
							"defaults": {
								"xtype": "panel",
								"layout": "anchor",
								"anchor": "100%",
								"flex": 1,
								"margin": 5,
								"defaults": {
									"anchor": "100%",
									"flex": 1
								}
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_32",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_32",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "No. Member Card",
									"name": "member_cardNo",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_text_32",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_34",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_34",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Member Name",
									"name": "member_name",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_text_34",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_36",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_36",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Jns Kelamin",
									"name": "jenis_kelamin",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_text_36",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_39",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_39",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Telp",
									"name": "member_telp",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_text_39",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_41",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_41",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Location",
									"name": "location_name",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_text_41",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_46",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_46",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Employment Status",
									"name": "employment_status",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_text_46",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_48",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_48",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Activation Date",
									"name": "activation_date",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_text_48",
									"semar": true,
									"xtype": "textfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_textarea_50",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_textarea_50",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Description",
									"name": "description",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_textarea_50",
									"semar": true,
									"xtype": "textareafield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_43",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_form_text_43",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Plan Type",
									"name": "plan_type",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_1_general_form_28_blk_i_form_text_43",
									"semar": true,
									"xtype": "hiddenfield",
									"items": []
								}]
							}, {
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"isformitems": true,
								"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_29",
								"semar": true,
								"xtype": "panel",
								"name": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_form_text_29",
								"defaults": {
									"anchor": "100%",
									"flex": 1
								},
								"layout": {
									"type": "anchor"
								},
								"items": [{
									"frame": false,
									"border": false,
									"disabled": false,
									"flex": 1,
									"title": "",
									"margin": "10 5 3 10",
									"padding": "0 0 0 0",
									"readOnly": true,
									"labelWidth": 100,
									"labelAlign": "left",
									"emptyText": "-",
									"fieldLabel": "Client ID",
									"name": "clientId",
									"allowBlank": false,
									"id": "sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29",
									"semar": true,
									"xtype": "hiddenfield",
									"items": []
								}]
							}]
						}]
					}]
				},{
					"title": "Advance Search",
					"id": "sosttransclaim_module_real_general_tabpanel_3_tab_3",
					"name": "sosttransclaim_module_real_general_tabpanel_3_tab_3",
					layout:'fit',
					bodyPadding:10,
					"items": [{
						xtype:'form',
						border:1,
						id:'form_Advance_Search',
						fieldDefaults: {
				            msgTarget: 'side'
				        },
						layout:'vbox',
						semar:true,
						bodyPadding:10,
						defaults:{
							xtype:'textfield',
							labelSeparator:'',
							labelWidth:150,
							width:450
						},
						items:[
							{
								xtype:'combo',	
								fieldLabel:'Client',
								editable:false,
								name:'client_name',
								store: {
									fields:['client_name'],
									autoLoad:true,
									"proxy": {
										"type": "ajax",
										"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/tvbrowserefclient",
										"extraParams": {
											"id_open": "1",
											"semar": "true"
										},
										reader:{
											root:'data'
										}
									}
								},
								queryMode: 'local',
								displayField: 'client_name',
								valueField: 'client_name',
							},
							{
								xtype:'combo',	
								fieldLabel:'Provider Name',
								editable:false,
								name:'providerId',
								store: {
									fields:['providerId','provider_name'],
									autoLoad:true,
									"proxy": {
										"type": "ajax",
										"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/tvbrowsereftmasprovider",
										"extraParams": {
											"id_open": "1",
											"semar": "true"
										},
										reader:{
											root:'data'
										}
									}
								},
								queryMode: 'local',
								displayField: 'provider_name',
								valueField: 'providerId',
							},
							{	
								xtype:'combo',	
								fieldLabel:'Provider Location',
								editable:false,
								name:'location_name_',
								store: {
									fields:['location_name'],
									autoLoad:true,
									"proxy": {
										"type": "ajax",
										"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/tvbrowsereflocation",
										"extraParams": {
											"id_open": "1",
											"semar": "true"
										},
										reader:{
											root:'data'
										}
									}
								},
								queryMode: 'local',
								displayField: 'location_name',
								valueField: 'location_name'
							},
							{
								xtype:'combo',	
								fieldLabel:'Service Type',
								editable:false,
								name:'servicetypeId',
								store: {
									fields:['service_type', 'servicetypeId'],
									autoLoad:true,
									"proxy": {
										"type": "ajax",
										"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/sos_tref_service_type",
										"extraParams": {
											"id_open": "1",
											"semar": "true"
										},
										reader:{
											root:'data'
										}
									}
								},
								queryMode: 'local',
								displayField: 'service_type',
								valueField: 'servicetypeId',
							},
							{
								fieldLabel:'From',
								editable:false,
								name:'start_date',
								id:'start_date',
								vtype:'daterange',
								format: 'd-m-Y',
								xtype:'datefield',
								endDateField:'end_date',
								listeners:{
									change:function(){
										Ext.getCmp('end_date').setDisabled(false);
									}
								}
							},
							{
								fieldLabel:'To',
								editable:false,
								disabled:true,
								id:'end_date',
								name:'end_date',
								vtype:'daterange',
								format: 'd-m-Y',
								xtype:'datefield',
								startDateField:'start_date'
							},
							{
								fieldLabel:'Member Card',
								name:'member_cardNo'
							},
							{
								fieldLabel:'Member Name',
								name:'member_name_'
							},
							{
								xtype:'combo',	
								fieldLabel:'Relation Type',
								editable:false,
								name:'relationship_type',
								store: {
									fields:['relationship_type'],
									autoLoad:true,
									"proxy": {
										"type": "ajax",
										"url": "<?php echo base_url();?>sosttransclaim\/call_method\/select\/table\/sos_tref_relationship_type",
										"extraParams": {
											"id_open": "1",
											"semar": "true"
										},
										reader:{
											root:'data'
										}
									}
								},
								queryMode: 'local',
								displayField: 'relationship_type',
								valueField: 'relationship_type',
							},
						],
						buttons:[
							{
								width:50,
								text:'Search',
								handler:function(){
									var myParams = Ext.getCmp('form_Advance_Search').getForm().getValues();
									var myParamsMember = [];
									for(var key in myParams) {
										var val = myParams[key];
										if(!val){
											delete myParams[key];
										}else{
											Ext.getStore('data_store_20').getProxy().extraParams[key] = myParams[key];
											switch(key) {
												case "member_cardNo":
													myParamsMember.push("member_cardNo");
													break;
												case "member_name_":
													myParamsMember.push("member_name_");
													break;
												case "relationship_type":
													myParamsMember.push("relationship_type");
													break;
											}
										}
									}
									Ext.getStore('data_store_20').load({callback:function(){
										if(myParamsMember.length > 0){
											Ext.getCmp('sosttransclaim_module_real_general_tabpanel_3').setActiveTab(0);
											Ext.getStore('data_store_2').load({params:myParams});
										}
									}});
								}
							},
							{
								xtype:'button',
								text:'Reset',
								handler:function(){
									Ext.getCmp('form_Advance_Search').getForm().reset();
									Ext.getCmp('end_date').setDisabled(true);
									Ext.getStore('data_store_2').load();
									Ext.getCmp('sosttransclaim_module_real_general_grid_21').getStore().getProxy().extraParams = {"id_open": "1","semar": "true"};
									Ext.getStore('data_store_20').removeAll();
								}
							}
						]
					}]
				}]
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Informasi Proses",
				"MessageBOX_Body": "MessagexBOX Body",
				"MessageBOX_Value": "MessagexBOX Default Value",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sosttransclaim_module_real_general_messagebox_62",
				"semar": true,
				"xtype": "exmlwidgetmessageboxpanel",
				"ismessagebox": true,
				"MessageBOX_Multiline": false,
				"MessageBOX_Prompt": false,
				"MessageBOX_Buttons": "OK",
				"MessageBOX_Icon": "INFO",
				"html": "<div style=\"padding:10px;\"><center><h3>MessagexBOX Body<\/h3><\/center><\/div>",
				"name": "sosttransclaim_module_real_general_messagebox_62",
				"layout": {
					"type": "fit"
				},
				"hidden": true
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Informasi Claim",
				"MessageBOX_Body": "MessagexBOX Body",
				"MessageBOX_Value": "MessagexBOX Default Value",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "sosttransclaim_module_real_general_messagebox_64",
				"semar": true,
				"xtype": "exmlwidgetmessageboxpanel",
				"ismessagebox": true,
				"MessageBOX_Multiline": false,
				"MessageBOX_Prompt": false,
				"MessageBOX_Buttons": "OK",
				"MessageBOX_Icon": "INFO",
				"html": "<div style=\"padding:10px;\"><center><h3>MessagexBOX Body<\/h3><\/center><\/div>",
				"name": "sosttransclaim_module_real_general_messagebox_64",
				"layout": {
					"type": "fit"
				},
				"hidden": true
			}]
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_sosttransclaim, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_sosttransclaim, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_blk_i_form_text_24', 'change', {
				key: 'getValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_text_24').getValue() != Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44').getValue()) {
						Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_text_24').setValue(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44').getValue());
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_14_blk_i_form_text_24_change = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_text_24')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_text_24');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('change', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_14_blk_i_form_text_24', 'change');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_14_blk_i_form_text_24_change);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57', 'change', {
				key: 'getValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57').getValue() != Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51').getValue()) {
						Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57').setValue(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51').getValue());
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57_change = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('change', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57', 'change');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57_change);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58', 'change', {
				key: 'getValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58').getValue() != Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29').getValue()) {
						Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58').setValue(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29').getValue());
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58_change = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('change', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58', 'change');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58_change);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59', 'change', {
				key: 'getValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59').getValue() != Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35').getValue()) {
						Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59').setValue(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35').getValue());
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59_change = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('change', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59', 'change');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59_change);
					};
				};
			}, 100);
			/*pushRegisterEvent('location_name_id', 'click', {
				key: 'browserefShow',
				fn: function(callbackIndex) {
					doTestVisualBrowseref('location_name_browseref_32', JSON.parse(true), callbackIndex);
				}
			});
			var intervale_location_name_id_click = setInterval(function() {
				if (Ext.getCmp('location_name_id')) {
					var tempEl = Ext.getCmp('location_name');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('location_name_id', 'click');
								};
							});
							clearInterval(intervale_location_name_id_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('location_name_id', 'click');
							});
							clearInterval(intervale_location_name_id_click);
						};
					};
				};
			}, 100);*/
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_btn_simpan', 'click', {
				key: 'invokeCall',
				fn: function(callbackIndex) {
					var invokeComponent = doGetWindowWorkspace(workspace_sosttransclaim, 'sosttransclaim_module_real_general_invoke_63');
					if (invokeComponent != undefined && Ext.isObject(invokeComponent)) {
						var tempParams = Ext.clone(invokeComponent.invokeParams);
						var procParams = {};
						if (tempParams) {
							if (tempParams.length > 0) {
								while (!Ext.isArray(tempParams)) {
									tempParams = Ext.decode(tempParams);
								};
								if (Ext.isArray(tempParams)) {
									tempParams = tempParams;
									for (var pr in tempParams) {
										if (Ext.isNumeric(pr)) {
											if (tempParams[pr]) {
												if (tempParams[pr]['set'] == 'get') {
													if (Ext.getCmp(tempParams[pr]['value'])) {
														tempParams[pr]['value'] = Ext.getCmp(tempParams[pr]['value']).getValue();
													};
												} else if (tempParams[pr]['set'] == 'grid') {
													var tempSetValFromGrid = tempParams[pr]['value'].split('=');
													if (tempSetValFromGrid.length == 2) {
														tempParams[pr]['value'] = doGetGridSelectionSingle(tempSetValFromGrid[0], tempSetValFromGrid[1], ',', 'string');
													};
												}
											}
										}
									};
									procParams = {
										params: Ext.encode(tempParams)
									};
								};
							};
						};
						var procName = invokeComponent.invokeCall;
						var procReturn = invokeComponent.invokeReturn;
						if (new String(procName).length > 0) {
							Ext.getBody().mask('Please wait...');
							var joinUri = 'select/function';
							if (procReturn == false) {
								joinUri = 'call/procedure';
							};
							Ext.Ajax.request({
								url: BASE_URL + 'sosttransclaim/call_method/' + joinUri + '/' + procName,
								method: 'POST',
								params: procParams,
								success: function(response) {
									if (new String(response.responseText).length > 0) {
										var obj = Ext.decode(response.responseText);
										if (Ext.isObject(obj)) {
											if (obj.data != null && obj.data != undefined) {
												//if function and butuh di set, dimari.
												if ('sosttransclaim_module_real_general_messagebox_64' != 'true' && new String('sosttransclaim_module_real_general_messagebox_64').length > 0 && Ext.isString('sosttransclaim_module_real_general_messagebox_64')) {
													//spesial untuk messagebox
													if (Ext.getCmp('sosttransclaim_module_real_general_messagebox_64').ismessagebox == true) {
														if (String(obj.data[procName]).length > 0) {
															SemarBOXPanelDialog(Ext.getCmp('sosttransclaim_module_real_general_messagebox_64'), {
																msg: obj.data[procName]
															});
														};
													} else {
														if (Ext.getCmp('sosttransclaim_module_real_general_messagebox_64')) {
															Ext.getCmp('sosttransclaim_module_real_general_messagebox_64').setValue(obj.data[procName]);
														}
													};
												}
											}
										};
									};

									if (Ext.isFunction(callbackIndex)) {
										callbackIndex()
									};
								},
								failure: function(response) {
									Ext.MessageBox.show({
										title: 'Warning !',
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.ERROR
									});
								},
								callback: function() {
									Ext.getBody().unmask();
								}
							});
						} else {
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
						};
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_btn_simpan', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_tambah').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_14_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_14_btn_simpan', 'click');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_14_btn_simpan_click);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sosttransclaim_module_real_general_form_14'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_14_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_ubah')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_14_btn_ubah', 'click');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_14_btn_ubah_click);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_14_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_batal')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_14_btn_batal', 'click');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_14_btn_batal_click);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_14_btn_cetak', 'click', {
				key: 'exportDocument',
				fn: function(callbackIndex) {
					var documentComponent = doGetWindowWorkspace(workspace_sosttransclaim, 'sosttransclaim_module_real_general_document_61');
					if (documentComponent != undefined && Ext.isObject(documentComponent)) {
						var tempParams = documentComponent.documentParams;
						var procParams = {};
						if (tempParams) {
							if (tempParams.length > 0) {
								while (!Ext.isArray(tempParams)) {
									tempParams = Ext.decode(tempParams);
								};
								if (Ext.isArray(tempParams)) {
									tempParams = tempParams;
								};
							};
						};

						if (!Ext.isArray(tempParams)) {
							tempParams = [];
						};

						doSemarDoc(documentComponent.Type_Doc, {
							title: documentComponent.title,
							Template_ID: documentComponent.Template_ID,
							Data_ID: documentComponent.Data_ID,
							Grid_ID: documentComponent.Grid_ID
						}, tempParams);
						if (Ext.isFunction(callbackIndex)) {
							callbackIndex()
						};
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_14_btn_cetak_click = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_cetak')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_cetak');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_14_btn_cetak', 'click');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_14_btn_cetak_click);
					};
				};
			}, 100);
			
			// ----------------- TAMBAHAN --------------
			pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('sosttransclaim_module_real_general_grid_4');
					if (new String('memberId=memberId').match('=')) {
						var hasAfterCheckComp = new String('memberId=memberId').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						Ext.getCmp('sosttransclaim_module_real_general_grid_21').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex();
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_grid_21').getStore().load();
					//Ext.getCmp('sosttransclaim_module_real_general_grid_21').setActiveTab(JSON.parse(0));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			// -------------- END ----------------
			
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_ubah').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sosttransclaim_module_real_general_form_14'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14').getForm().loadRecord((Ext.getCmp('sosttransclaim_module_real_general_grid_21').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('sosttransclaim_module_real_general_grid_21').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_grid_21_itemdblclick = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_grid_21')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_grid_21');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('itemdblclick', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_grid_21', 'itemdblclick');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_grid_21_itemdblclick);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sosttransclaim_module_real_general_form_14'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_ubah').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_grid_21_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_tambah')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_tambah', 'click');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_grid_21_btn_tambah_click);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('sosttransclaim_module_real_general_grid_21').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('claimId'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'sosttransclaim/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'claimId',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('sosttransclaim_module_real_general_grid_21').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_grid_21_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_hapus')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_hapus', 'click');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_grid_21_btn_hapus_click);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('sosttransclaim', //nama module
								Ext.getCmp('sosttransclaim_module_real_general_grid_21').title, //title
								'sosttransclaim_module_real_general_grid_21', //id grid
								'claimId', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_grid_21_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_excel')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_grid_21_btn_excel', 'click');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_grid_21_btn_excel_click);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sosttransclaim_module_real_general_form_14'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			/*pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_grid_21_btn_tambah').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_ubah').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			/*pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sosttransclaim_module_real_general_form_28'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_tabpanel_3').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_28').getForm().loadRecord((Ext.getCmp('sosttransclaim_module_real_general_grid_4').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('sosttransclaim_module_real_general_grid_4').getSelectionModel().getSelection()[0] : []));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_grid_4_itemdblclick = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_grid_4')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_grid_4');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('itemdblclick', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_grid_4', 'itemdblclick');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_grid_4_itemdblclick);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29', 'change', {
				key: 'reverseValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58').ismessagebox == true) {
						if (String(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29').getValue()).length > 0) {
							SemarBOXPanelDialog(Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58'), {
								msg: Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29').getValue()
							});
						};
					} else {
						if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58').getValue() != Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29').getValue()) {
							Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_58').setValue(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29').getValue());
						};
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29_change = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('change', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29', 'change');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_29_change);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35', 'change', {
				key: 'reverseValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59').ismessagebox == true) {
						if (String(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35').getValue()).length > 0) {
							SemarBOXPanelDialog(Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59'), {
								msg: Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35').getValue()
							});
						};
					} else {
						if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59').getValue() != Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35').getValue()) {
							Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_59').setValue(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35').getValue());
						};
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35_change = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('change', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35', 'change');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_35_change);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44', 'change', {
				key: 'reverseValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_text_24').ismessagebox == true) {
						if (String(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44').getValue()).length > 0) {
							SemarBOXPanelDialog(Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_text_24'), {
								msg: Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44').getValue()
							});
						};
					} else {
						if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_text_24').getValue() != Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44').getValue()) {
							Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_text_24').setValue(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44').getValue());
						};
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44_change = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('change', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44', 'change');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_text_44_change);
					};
				};
			}, 100);
			pushRegisterEvent('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51', 'change', {
				key: 'reverseValue',
				fn: function(callbackIndex) {

					if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57').ismessagebox == true) {
						if (String(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51').getValue()).length > 0) {
							SemarBOXPanelDialog(Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57'), {
								msg: Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51').getValue()
							});
						};
					} else {
						if (Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57').getValue() != Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51').getValue()) {
							Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_hidden_57').setValue(Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51').getValue());
						};
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51_change = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51');
					if ('change' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('change' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('change', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51', 'change');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_28_ar_0_general_form_28_blk_i_form_hidden_51_change);
					};
				};
			}, 100);
			
			pushRegisterEvent('sosttransclaim_module_real_general_form_28_btn_proses', 'click', {
				key: 'invokeCall',
				fn: function(callbackIndex) {
					var invokeComponent = doGetWindowWorkspace(workspace_sosttransclaim, 'sosttransclaim_module_real_general_invoke_proses');
					if (invokeComponent != undefined && Ext.isObject(invokeComponent)) {
						var tempParams = Ext.clone(invokeComponent.invokeParams);
						var procParams = {};
						if (tempParams) {
							if (tempParams.length > 0) {
								while (!Ext.isArray(tempParams)) {
									tempParams = Ext.decode(tempParams);
								};
								if (Ext.isArray(tempParams)) {
									tempParams = tempParams;
									for (var pr in tempParams) {
										if (Ext.isNumeric(pr)) {
											if (tempParams[pr]) {
												if (tempParams[pr]['set'] == 'get') {
													if (Ext.getCmp(tempParams[pr]['value'])) {
														tempParams[pr]['value'] = Ext.getCmp(tempParams[pr]['value']).getValue();
													};
												} else if (tempParams[pr]['set'] == 'grid') {
													var tempSetValFromGrid = tempParams[pr]['value'].split('=');
													if (tempSetValFromGrid.length == 2) {
														tempParams[pr]['value'] = doGetGridSelectionSingle(tempSetValFromGrid[0], tempSetValFromGrid[1], ',', 'string');
													};
												}
											}
										}
									};
									procParams = {
										params: Ext.encode(tempParams)
									};
								};
							};
						};
						var procName = invokeComponent.invokeCall;
						var procReturn = invokeComponent.invokeReturn;
						if (new String(procName).length > 0) {
							Ext.getBody().mask('Please wait...');
							var joinUri = 'select/function';
							if (procReturn == false) {
								joinUri = 'call/procedure';
							};
							Ext.Ajax.request({
								url: BASE_URL + 'sosttransclaim/call_method/' + joinUri + '/' + procName,
								method: 'POST',
								params: procParams,
								success: function(response) {
									if (new String(response.responseText).length > 0) {
										var obj = Ext.decode(response.responseText);
										if (Ext.isObject(obj)) {
											if (obj.data != null && obj.data != undefined) {
												//if function and butuh di set, dimari.
												if ('sosttransclaim_module_real_general_messagebox_62' != 'true' && new String('sosttransclaim_module_real_general_messagebox_62').length > 0 && Ext.isString('sosttransclaim_module_real_general_messagebox_62')) {
													//spesial untuk messagebox
													if (Ext.getCmp('sosttransclaim_module_real_general_messagebox_62').ismessagebox == true) {
														if (String(obj.data[procName]).length > 0) {
															if (obj.data[procName] == 'Decline') {
																semarDeactiveAllX(Ext.getCmp('sosttransclaim_module_real_general_form_14'), JSON.parse(false));
																Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan').setDisabled(JSON.parse(false));
																Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_ubah').setDisabled(JSON.parse(true));
																Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_combo_26').setValue("Fail");
																
																
															} else {
																semarDeactiveAllX(Ext.getCmp('sosttransclaim_module_real_general_form_14'), JSON.parse(false));
																Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan').setDisabled(JSON.parse(false));
																Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_ubah').setDisabled(JSON.parse(true));
																Ext.getCmp('sosttransclaim_module_real_general_form_14_blk_i_form_combo_26').setValue("Success");
																
															};
															SemarBOXPanelDialog(Ext.getCmp('sosttransclaim_module_real_general_messagebox_62'), {
																msg: obj.data[procName]
															});
														};
													} else {
														if (Ext.getCmp('sosttransclaim_module_real_general_messagebox_62')) {
															Ext.getCmp('sosttransclaim_module_real_general_messagebox_62').setValue(obj.data[procName]);
														}
													};
												}
											}
										};
									};

									if (Ext.isFunction(callbackIndex)) {
										callbackIndex()
									};
								},
								failure: function(response) {
									Ext.MessageBox.show({
										title: 'Warning !',
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.ERROR
									});
								},
								callback: function() {
									Ext.getBody().unmask();
								}
							});
						} else {
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
						};
					};
				}
			});
			
			/*pushRegisterEvent('sosttransclaim_module_real_general_form_28_btn_proses', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('sosttransclaim_module_real_general_form_14'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_form_28_btn_proses', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('sosttransclaim_module_real_general_form_28_btn_proses', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('sosttransclaim_module_real_general_form_14_btn_ubah').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});*/
			
			var intervale_sosttransclaim_module_real_general_form_28_btn_proses_click = setInterval(function() {
				if (Ext.getCmp('sosttransclaim_module_real_general_form_28_btn_proses')) {
					var tempEl = Ext.getCmp('sosttransclaim_module_real_general_form_28_btn_proses');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						tempEl.on('click', function(e, t) {
							if (e.xtype == 'exmlwidgetmessageboxpanel') {
								ii = t;
							};
							runRegisterEvent('sosttransclaim_module_real_general_form_28_btn_proses', 'click');
						});
						clearInterval(intervale_sosttransclaim_module_real_general_form_28_btn_proses_click);
					};
				};
			}, 100);
		}
	}
}

<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>