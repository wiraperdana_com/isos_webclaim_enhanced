<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Model digenerate oleh eXML Builder.
 * Model untuk module sosttransnews.
 *
 */
class model_sosttransnews extends MY_Model
{

	/**
	 * __construct Model untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * _load_model Model untuk module $modulename.
	 */
	public function _load_model($model_name)
	{
		$CI =& get_instance();
		$CI->load->model($model_name);
		return $CI->$model_name;
	}

    function get_QUERY(){
	$sesi_type = $this->session->userdata("opd_zs_simpeg");
    $this->db->where(array('news_date !=' => ''));
	$this->db->select('*');
    $this->db->from('sos_ttrans_news'); 
 
 		if ($this->input->get_post('query')){
			$qfilter_1 = str_replace(" ","",trim($this->input->post('query')));
			$qfilter_2 = str_replace(".","",trim($this->input->post('query'))); 
		    $this->db->like('news_judul', trim($this->input->post('query')));
			$this->db->or_like('news_content', trim($this->input->post('query')));
			$this->db->or_like('news_sumber', trim($this->input->post('query')));
 		} 
		
		$Filters_Model = $this->_load_model('filters_model');
		$Filters_Model->get_SORT();
		$Filters_Model->get_SORT_Get();
		$Filters_Model->get_FILTER();
	}

	function get_AllData(){
		$data = array();
		
		$this->get_QUERY();
		
		$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
		$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 20;
		
		$this->db->limit($limit, $start);
		$Q = $this->db->get();
		foreach ($Q->result() as $obj)
		{
			$data[] = $obj;
		}    		
		return $data;
	}
	
	function get_CountData(){
		$this->get_QUERY();
		return $this->db->count_all_results();
	}
	
	/**
	 * _do_insertupdate_data Model untuk module $modulename.
	 */
	function Insert_Data(){
		list($hari,$bulan,$tahun) = explode("/",$this->input->post('news_date'));
		$format_tgl = "$tahun-$bulan-$hari";
		$tgl_news = date("Y-m-d", strtotime($format_tgl));
		
		$data_s_insert = array('createdBy' => $this->session->userdata("user_zs_exmldashboard"), 'createdDate' => date("Y-m-d H:i:s"));
		$data_s_update = array('updatedBy' => $this->session->userdata("user_zs_exmldashboard"), 'updatedDate' => date("Y-m-d H:i:s"));
		
		$data = array(
			'news_judul' => $this->input->post('news_judul'),
			'news_content'=> $this->input->post('news_content'),
			'news_sumber' => $this->input->post('news_sumber'), 
			'news_date' => $tgl_news, 
		);
		
		$array_update = array_merge($data, $data_s_update);
		if($this->input->post('newsId')){
			$data_ID = array('newsId' => $this->input->post('newsId'));
			$this->db->trans_start(); 
			$this->db->update('sos_ttrans_news',$array_update, $data_ID);
			$this->db->trans_complete();
			return "Updated"; exit;
		}
		
		$options = array('newsId' => $this->input->post('newsId'));
		$Q = $this->db->get_where('sos_ttrans_news', $options,1);
		if($Q->num_rows() > 0){
		  return "Exist";
		}else{
			$array_insert = array_merge($data, $data_s_insert);
			$this->db->trans_start();
			$this->db->insert('sos_ttrans_news', $array_insert);
			$id = $this->db->insert_id(); 
			$this->db->trans_complete();
		  return $id;
		}
		$Q->free_result();
		$this->db->close();
		
	}
	
	function Delete_Data(){
		$sesi_type = $this->session->userdata("user_zs_exmldashboard");
		$records = explode('-', $this->input->post('postdata'), -1);
		$data = $this->getBy_ArrayID($records);
		if(count($data)){
    	$this->db->trans_start();
    	foreach($data as $key => $list){	
    		$W_NIP = array('newsId' => $list['newsId']); 
    	    $this->db->delete('sos_ttrans_news', array('newsId' => $list['newsId']));
    	}
  		$this->db->trans_complete();
		}
	}
	
	function getBy_ArrayID($ID = array()){
		if(count($ID)){
			$data = array();
			$this->db->select('*');
			$this->db->from("sos_ttrans_news");
			$this->db->where_in('newsId',$ID);
			$Q = $this->db->get('');
			if($Q->num_rows() > 0){
				foreach ($Q->result_array() as $row){
					$data[] = $row;
			    }
		     }
		$Q->free_result();
		return $data;
		}
	}
	 
}
