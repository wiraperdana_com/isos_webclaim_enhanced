<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller digenerate oleh eXML Builder.
 * Controller untuk module sosttransnews.
 *
 */
class sosttransnews extends CI_Controller
{

	/**
	 * __construct Controller untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_sosttransnews'));
	}


	/**
	 * index Controller untuk module $modulename.
	 */
	public function index()
	{
		if($this->input->post('id_open')){
			$data['jsscript'] = TRUE;
			$this->load->view('index',$data);
		}else{
			$this->load->view('index');
		};
	}

   /*
    call_method Controller untuk module $modulename.
	 */
	 function ext_insert(){  	
		$Status = $this->model_sosttransnews->Insert_Data();	 
		if($Status == "Exist"){
			echo "{success:false, info: { reason: 'News sudah ada !' }}";
		}elseif($Status == "Updated"){
			echo "{success:true, info: { reason: 'Sukses merubah data !' }}";
		}elseif(is_numeric($Status)){
			echo "{success:true, info: { reason: '".$Status."' }}";
		}else{
			echo "{success:false, info: { reason: 'Gagal menambah Data !' }}";
		}
   }
	
	function ext_delete(){
		$this->model_sosttransnews->Delete_Data();	
     }
	/**
	 * call_method Controller untuk module $modulename.
	 */
	function ext_get_all(){
		if($this->input->post("id_open")){
			$data = $this->model_sosttransnews->get_AllData();	  			 	
			$total = $this->model_sosttransnews->get_CountData();	  
   		    echo '({total:'. $total . ',results:'.json_encode($data).'})';
  	    }
	}
	
	function insert_arsip($type){
 		$config['upload_path'] = './assets/filemanager/news';
 	   
		if($type == 'imgs'){
			$config['allowed_types'] = 'jpg|jpeg|png|bmp';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$upload = $this->upload->do_upload('news_image');
		}
		 
		if($type == 'pdf'){
			$config['allowed_types'] = 'pdf|doc';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$upload = $this->upload->do_upload('news_file');
		}
		//$config['max_size'] = '5024'; 
 		 
		$attachment1='';
		if($upload==FALSE):
		   //die();
		else:
		   $imgs = $this->upload->data();
		   $attachment1 = $imgs['file_name'];
  		endif;
		
		$options = array('newsId' => $this->input->post('newsId'));
		$Q = $this->db->get_where('sos_ttrans_news', $options,1);	
		
		if($type == 'imgs'){			 
 			@unlink($config['upload_path'].'/'.base64_decode($Q->row()->news_image));
 		    $data['news_image'] = base64_encode($attachment1);
		}
		if($type == 'pdf'){ 
 			@unlink($config['upload_path'].'/'.base64_decode($Q->row()->news_file));
			$data['news_file'] = base64_encode($attachment1);
		}
 		
 		if($Q->num_rows() > 0){
 			$data_ID = array('newsId' =>  $this->input->post('newsId'));  
			$obj = $this->db->update('sos_ttrans_news',$data, $data_ID);
			$id = $this->input->post('newsId');
 		}else{  
			$obj = $this->db->insert('sos_ttrans_news' , $data);
			$id = $this->db->insert_id();
		}
		
		
		
  	    if($obj){
		    echo "{success:true, errors: { reason: 'SUKSES', newsId: ".$id." }}";
		}else{
			echo "{success:false, errors: { reason: 'Gagal Upload !' }}";	
		}
	}
	
	
	
	function ext_download(){
		$t=$this->input;
	    $this->load->helper('download');
		$newsId = $this->uri->segment(3);
		$types = $this->uri->segment(4);
		if($newsId !=''){
			$files = $this->db->select('news_image, news_file')->from('sos_ttrans_news')->where('newsId', $newsId)->get()->row();
			if($types == 'imgs') { $image_name = "./assets/filemanager/news/".base64_decode($files->news_image); }
			if($types == 'files') { $image_name = "./assets/filemanager/news/".base64_decode($files->news_file); }
			$data = file_get_contents($image_name); // Read the file's contents
 			force_download($image_name, $data);	
 		}
	}
	
	 
}
