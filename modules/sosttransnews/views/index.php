<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

// TABEL NEWS  ------------------------------------------------- START
var Params_T_News = null;
Ext.define('MT_News', {extend: 'Ext.data.Model',
  fields: ['newsId', 'news_date', 'news_judul', 'news_content', 'news_sumber', 'news_image', 'news_file']
});

var Reader_T_News = new Ext.create('Ext.data.JsonReader', {
  id: 'Reader_T_News', root: 'results', totalProperty: 'total', idProperty: 'newsId'  	
});

var Proxy_T_News = new Ext.create('Ext.data.AjaxProxy', {
  id: 'Proxy_T_News', url: BASE_URL + 'sosttransnews/ext_get_all', actionMethods: {read:'POST'}, extraParams: {id_open:1},
  reader: Reader_T_News,
  afterRequest: function(request, success) {
   	Params_T_News = request.operation.params;
  }
});

var Data_T_News = new Ext.create('Ext.data.Store', {
	id: 'Data_T_News', model: 'MT_News', pageSize: 20, noCache: false, autoLoad: true,
  proxy: Proxy_T_News
});

var Search_T_News = new Ext.create('Ext.ux.form.SearchField', {
  id: 'Search_T_News', store: Data_T_News, emptyText: 'Pencarian ...', width: 350    
});

var tbT_News = new Ext.create('Ext.toolbar.Toolbar', { 
	items:[
        {
            text: 'Tambah', id: 'Tambah_News', iconCls: 'icon-add',
            handler: function() {
                News_Tambah();
            }
        }, '-', {
            text: 'Hapus', id: 'Hapus_News', iconCls: 'icon-delete',
            handler: function() {
                News_Hapus();
            }
        }, '-',
 		{xtype: 'button', text: 'Export Xls',  id: 'Export_xls', iconCls: 'icon-xls', margin: '0 5px 0 0', tooltip: {text: 'Export ke Excel'}, handler: function(){ CetakXls(); }},'->',
 		Search_T_News
	]
});

var Filters_T_News = new Ext.create('Ext.ux.grid.filter.Filter', {
  ftype: 'filters', autoReload: true, local: false, store: Data_T_News,
  filters: [
     {type: 'date', dataIndex: 'news_date'}, 
     {type: 'string', dataIndex: 'news_judul'}, 
     {type: 'string', dataIndex: 'news_content'},     
     {type: 'string', dataIndex: 'news_sumber'}
  ]
});
var cbGrid_T_News = new Ext.create('Ext.selection.CheckboxModel');
var Grid_T_News = new Ext.create('Ext.grid.Panel', {
	id: 'Grid_T_News', store: Data_T_News, frame: false, border: true, loadMask: true, noCache: false,
    style: 'margin:0 auto;', autoHeight: true, height:'100%', columnLines: true, selModel: cbGrid_T_News,
	columns: [
  	{header:'No', xtype: 'rownumberer', width: 35, resizable: true, style: 'padding-top: .5px;'}, 
  	{header: "Date", dataIndex: 'news_date', width: 75 },
    {header: "Judul", dataIndex: 'news_judul', width: 180}, 
	{header: "Content", dataIndex: 'news_sumber', width: 180},
    {header: "Sumber", dataIndex: 'news_sumber', width: 180}
  ],
  features: [Filters_T_News],
  tbar: tbT_News,
  dockedItems: [{xtype: 'pagingtoolbar', store: Data_T_News, dock: 'bottom', displayInfo: true}],
  listeners: {
  	selectionchange: function(model, records) {
     	if (records[0]) {
      			Form_News.getForm().loadRecord(records[0]);
       			Form_News_PDF.getForm().loadRecord(records[0]);
                Ext.getCmp('news_date').setValue(new Date(records[0].get('news_date'))); 
                Ext.getCmp('newsId_pdf').setValue(records[0].get('newsId'));
                Ext.getCmp('newsId_img').setValue(records[0].get('newsId'));
                Ext.getCmp('newsId').setValue(records[0].get('newsId'));
                Ext.getCmp('Ubah_News').enable(true);
         }
    },
  	itemdblclick: function(dataview, record, item, index, e) {
  		Ext.getCmp('MainTab_T_News').setActiveTab('Tab2_T_News');
  	}    
  }
});


var tbT_Forms = new Ext.create('Ext.toolbar.Toolbar', { 
	items:[
        {
            text: 'Simpan', id: 'Simpan_News', iconCls: 'icon-save',
            handler: function() {
                News_Simpan();
            }
        }, '-', {
            text: 'Ubah', id: 'Ubah_News', iconCls: 'icon-edit',
            handler: function() {
                Ubah_News();
            }
        }, '-', {
            text: 'Batal', id: 'Batal_News', iconCls: 'icon-undo',
            handler: function() {
                Deactive_Form_News();
            }
        },  
	]
});


//FORM ARSIP Images
var Form_News_Images = new Ext.create('Ext.form.Panel', {
  id: 'Form_News_Images', url: BASE_URL + 'sosttransnews/insert_arsip/imgs', fileUpload: true,  border:false,
  frame: false, width: '100%', height: 80, margins: '0 5 0 0', defaults: {anchor: '100%', allowBlank: true, msgTarget: 'side',labelWidth: 50},
  items:[ 
      {
       xtype:'fieldset', title :'Images Files',width: 625, 
       items: [{xtype: 'fieldcontainer', layout: 'hbox', defaults: {hideLabel: true}, combineErrors: false,
                items:[{xtype : 'hidden', name :'newsId', id :'newsId_img'},
                        {xtype: 'filefield', name: 'news_image', id:'fileimgs', emptyText: 'Upload Files', 
                        buttonText: '', buttonConfig: {iconCls: 'icon-image_add'}, margins: '0 5 0 0', width: 425,
                        listeners: {
                          'change': function(){
                            if(Form_News_Images.getForm().isValid()){                                
                              Form_News_Images.getForm().submit({
                                waitMsg: 'Sedang meng-upload ...',
                                success: function(form, action) {
                                  obj = Ext.decode(action.response.responseText);
                                 
                                  if(obj.errors.reason == 'SUKSES'){
                                      Ext.getCmp('newsId_img').setValue(obj.errors.newsId);
                                      Ext.getCmp('newsId_pdf').setValue(obj.errors.newsId);
                                      Ext.getCmp('newsId').setValue(obj.errors.newsId);
                                      Ext.MessageBox.show({title:'Informasi !', msg: 'Sukses upload Data !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});
                                  } 
                                },
                                failure: function(form, action){
                                  obj = Ext.decode(action.response.responseText);
                                  Ext.MessageBox.show({title:'Gagal Upload !', msg: obj.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                                }, 
                                scope: this
                              });
                            }
                          }
                        }
                      },
                      {xtype: 'button', text: 'Hapus', id:'Btn_Hapus_imgs', tooltip: {text: 'Hapus Arsip Digital'}, handler: function() {Reset_News();}, margins: '0 5 0 0'},
                      {xtype: 'button', text: 'Download', id:'Btn_Download_imgs', target: '_blank', tooltip: {text: 'Download Arsip Digital'}, handler: function() { Download_News('imgs');}
                      }]//fieldcontainer 
      }]//Fieldset
   }]
});

//FORM ARSIP PDF
var Form_News_PDF = new Ext.create('Ext.form.Panel', {
  id: 'Form_News_PDF', url: BASE_URL + 'sosttransnews/insert_arsip/pdf', fileUpload: true,  border:false,
  frame: false, width: '100%', height: 100, margins: '0 5 0 0', defaults: {anchor: '100%', allowBlank: true, msgTarget: 'side',labelWidth: 50},
  items:[ 
      {
       xtype:'fieldset', title :'PDF Files',width: 625, 
       items: [{xtype: 'fieldcontainer', layout: 'hbox', defaults: {hideLabel: true}, combineErrors: false,
                items:[{xtype : 'hidden', name :'newsId', id :'newsId_pdf'},
                        {xtype: 'filefield', name: 'news_file', id:'fileNews', emptyText: 'Upload Files', 
                        buttonText: '', buttonConfig: {iconCls: 'icon-image_add'}, margins: '0 5 0 0', width: 425,
                        listeners: {
                          'change': function(){
                            if(Form_News_PDF.getForm().isValid()){                                
                              Form_News_PDF.getForm().submit({
                                waitMsg: 'Sedang meng-upload ...',
                                success: function(form, action) {
                                  obj = Ext.decode(action.response.responseText);
                                 
                                  if(obj.errors.reason == 'SUKSES'){
                                      Ext.getCmp('newsId_pdf').setValue(obj.errors.newsId);
                                      Ext.getCmp('newsId_img').setValue(obj.errors.newsId);
                                      Ext.getCmp('newsId').setValue(obj.errors.newsId);
                                      Ext.MessageBox.show({title:'Informasi !', msg: 'Sukses upload Data !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});
                                  } 
                                },
                                failure: function(form, action){
                                  obj = Ext.decode(action.response.responseText);
                                  Ext.MessageBox.show({title:'Gagal Upload !', msg: obj.errors.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                                }, 
                                scope: this
                              });
                            }
                          }
                        }
                      },
                      {xtype: 'button', text: 'Hapus', id:'Btn_Hapus_News', tooltip: {text: 'Hapus Arsip Digital'}, handler: function() {Reset_News();}, margins: '0 5 0 0'},
                      {xtype: 'button', text: 'Download', id:'Btn_Download_News', target: '_blank', tooltip: {text: 'Download Arsip Digital'}, handler: function() { Download_News('files');}
                      }]//fieldcontainer 
      }]//Fieldset
   }]
});
 
function Download_News(types){
    var newsId = Ext.getCmp('newsId').getValue();
    window.open(BASE_URL + "sosttransnews/ext_download/" + newsId + '/' + types,'_blank'); 
 }
 

var Form_News = new Ext.create('Ext.form.Panel', {
    id: 'Form_News',
    url: BASE_URL + 'sosttransnews/ext_insert',
    frame: false,
    bodyStyle: 'padding: 5px 5px 0 0;',
    fieldDefaults: {
        labelAlign: 'left',
        msgTarget: 'side'
    }, 
    defaults: {
       
    },
    buttonAlign: 'left',
    autoScroll: true,
     items: [
       {
        xtype: 'fieldset', 
        defaults: {
            anchor: '100%',
             allowBlank: false
        },
        margins: '0 5px 0 0',
        style: 'padding: 10 10 10 10; border-width: 0px;',
        flex: 1,
        items: [ {xtype : 'hidden', name :'newsId', id :'newsId'}, 
                  {
                      xtype: 'datefield',
                      fieldLabel: 'Date',
                      name: 'news_date',
                      id: 'news_date', 
                      anchor: '100%', 
                      format: 'd/m/Y', emptyText: 'dd/mm/yyyy', maskRe: /[\d\/\;]/, altFormats: 'dmY|Y-m-d',
             		  value : new Date(),
                  },{
                      xtype: 'textfield',
                      fieldLabel: 'Judul',
                      name: 'news_judul', 
                      id: 'news_judul', 
                      anchor: '100%',
                      value : '-' 
                  },{
                      xtype: 'htmleditor',
                      fieldLabel: 'Date',
                      name: 'news_content',
                      id: 'news_content',
                      height : 210, 
                      anchor: '100%' ,
                      value : '-'
                  },{
                      xtype: 'textfield',
                      fieldLabel: 'Sumber',
                      name: 'news_sumber', 
                      id: 'news_sumber',
                      anchor: '100%', 
                      value : '-'
                  }]  
                  
    },
    {
    xtype: 'fieldset', margins: '5 5 5 5',  style: 'padding: 0; border-width: 0px; text-align: left;',
    items: [Form_News_Images]
    },
    {
    xtype: 'fieldset', margins: '5 5 5 5',  style: 'padding: 0; border-width: 0px; text-align: left;',
    items: [Form_News_PDF]
    }]
 });

 
//TAMBAH////////////////
function News_Tambah() { 
  Form_News.getForm().reset();
  Active_Form_News(); 
}
/////////////////////ACTION//////////////////////////////////////////    

var Box_T_News = new Ext.create('Ext.panel.Panel', {
   id: 'Box_T_News', layout: 'border', width: '100%', height: '100%', bodyStyle: 'padding: 0px;', border: false,
   items: [
   	{id: 'West_T_News', title:'News List', region: 'west', width: '100%', minWidth: 187, split: true, collapsible: false,
   	 items: [Grid_T_News]
	  },
	  {id: 'Center_T_News', title:'News Detail', region: 'center', layout: 'card', collapsible: false, margins: '0 0 0 0' , 
       border: true, items: [Form_News],tbar :tbT_Forms,
	  }
   ]
});

var boxborder_T_News = new Ext.create('Ext.panel.Panel', {
   id: 'boxborder_T_News', layout: 'border',
   width: '100%', height: '100%', bodyStyle: 'padding: 0px;',
   items: [
		{region: 'center', layout: 'card', collapsible: false, margins: '0 0 0 0', width: '100%', border: false,
	     items: [Box_T_News]
		}
   ]
});

var new_tabpanel = {
	id: 'trans_diklat_sk', title: 'News', iconCls: 'icon-menu_pim',  border: false, closable: true,  
	layout: 'fit', items: [boxborder_T_News]
}


function News_Simpan(){
	Ext.getCmp('Form_News').on({
  	beforeaction: function() {Ext.getCmp('Form_News').body.mask();}
  });

  Form_News.getForm().submit({            			
  	success: function(form, action){
  		Ext.getCmp('Form_News').body.unmask(); 
  		obj = Ext.decode(action.response.responseText);
  		if(IsNumeric(obj.info.reason)){ 
  			Ext.MessageBox.show({title:'Informasi !', msg: 'Sukses menambah data !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});  			
  		}else{
  			Ext.MessageBox.show({title:'Informasi !', msg: obj.info.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});
  		}
        Data_T_News.load();
  		Deactive_Form_News();
  	},
    failure: function(form, action){
    	Ext.getCmp('Form_News').body.unmask();
      if (action.failureType == 'server') {
      	obj = Ext.decode(action.response.responseText);
        Ext.MessageBox.show({title:'Peringatan !', msg: obj.info.reason, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
      }else{
      	if (typeof(action.response) == 'undefined') {
        	Ext.MessageBox.show({title:'Peringatan !', msg: 'Silahkan isi dengan benar !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
        }else{
        	Ext.MessageBox.show({title:'Peringatan !', msg: 'Server tidak dapat dihubungi !', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
        }
      }
    }
  });
}

function News_Hapus() {
        var sm = Grid_T_News.getSelectionModel(),
            sel = sm.getSelection();
        if (sel.length > 0) {
            Ext.Msg.show({
                title: 'Konfirmasi',
                msg: 'Apakah Anda yakin untuk menghapus ?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.Msg.QUESTION,
                fn: function(btn) {
                    if (btn == 'yes') {
                        var data = '';
                        for (i = 0; i < sel.length; i++) {
                            data = data + sel[i].get('newsId') + '-';
                        }
                        Ext.Ajax.request({
                            url: BASE_URL + 'sosttransnews/ext_delete',
                            method: 'POST',
                            params: {
                                postdata: data
                            },
                            success: function(response) {
                                Data_T_News.load();
                            },
                            failure: function(response) {
                                Ext.MessageBox.show({
                                    title: 'Peringatan !',
                                    msg: obj.errors.reason,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                        });
                        Form_News.getForm().reset();
                    }
                }
            });
        }
    }
 
 
function  Ubah_News() {
      var newsId = Form_News.getForm().findField('newsId').getValue();
      if (newsId) { 
           Active_Form_News();
       }
  }
 
function CetakXls(){  
 	doManualSemarDocByGrid('sosttransnews', //nama module
                  Ext.getCmp('Grid_T_News').title, //title
                  'Grid_T_News', //id grid
                  'newsId', //kolom grid
                  'sos_ttrans_news' //table or view
                  ); 
} 
                
function Active_Form_News() {
  Ext.getCmp('Tambah_News').setDisabled(true);
  Ext.getCmp('Ubah_News').setDisabled(true);
  Ext.getCmp('Hapus_News').setDisabled(true);
  Ext.getCmp('Simpan_News').setDisabled(false);
  Ext.getCmp('Batal_News').setDisabled(false); 
  
  Ext.getCmp('news_date').setReadOnly(false);
  Ext.getCmp('news_judul').setReadOnly(false);
  Ext.getCmp('news_content').setReadOnly(false);
  Ext.getCmp('news_sumber').setReadOnly(false);
  Ext.getCmp('fileNews').enable(true); 
  Ext.getCmp('fileimgs').enable(true); 
  Ext.getCmp('Btn_Hapus_News').enable(true);
  Ext.getCmp('Btn_Download_News').enable(true);
  Ext.getCmp('Btn_Hapus_imgs').enable(true); 
  Ext.getCmp('Btn_Download_imgs').enable(true);    
}

function Deactive_Form_News() { 
   Ext.getCmp('Tambah_News').setDisabled(false);
   Ext.getCmp('Ubah_News').setDisabled(true);
   Ext.getCmp('Hapus_News').setDisabled(false);
   Ext.getCmp('Simpan_News').setDisabled(true);
   Ext.getCmp('Batal_News').setDisabled(true);
   
   Ext.getCmp('news_date').setReadOnly(true);
   Ext.getCmp('news_judul').setReadOnly(true);
   Ext.getCmp('news_content').setReadOnly(true);
   Ext.getCmp('news_sumber').setReadOnly(true);
   Ext.getCmp('fileNews').disable(true); 
   Ext.getCmp('fileimgs').disable(true);
   Ext.getCmp('Btn_Hapus_News').disable(true);
   Ext.getCmp('Btn_Download_News').disable(true); 
   Ext.getCmp('Btn_Hapus_imgs').disable(true); 
   Ext.getCmp('Btn_Download_imgs').disable(true);   
}
 Deactive_Form_News();   
<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>