
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php header("Content-Type: application/x-javascript"); ?>

<?php if(isset($jsscript) && $jsscript == TRUE){ ?>

//model define - start
Ext.define('semar_baka_model', {
	"fields": [{
		"name": "key"
	}, {
		"name": "val"
	}],
	"id": "semar_baka_model",
	"name": "semar_baka_model",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_1', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "1",
		"name": "status_data",
		"sortDir": "ASC",
		"persist": false,
		"type": "smallint",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "updatedBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0000-00-00 00:00:00",
		"name": "createdDate",
		"sortDir": "ASC",
		"persist": false,
		"type": "timestamp",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "createdBy",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "description",
		"sortDir": "ASC",
		"persist": false,
		"type": "text",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_status",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_email",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_telp",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_address",
		"sortDir": "ASC",
		"persist": false,
		"type": "text",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_codeId",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": false,
		"type": "int",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0",
		"name": "providerId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_1",
	"name": "data_model_1",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_11', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_type",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "",
		"name": "providertypeId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_11",
	"name": "data_model_11",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_23', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "client_name",
		"sortDir": "ASC",
		"persist": false,
		"type": "string",
		"useNull": false
	}, {
		"dateFormat": null,
		"defaultValue": "0",
		"name": "clientId",
		"sortDir": "ASC",
		"persist": true,
		"type": "int",
		"useNull": false
	}],
	"id": "data_model_23",
	"name": "data_model_23",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_26', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "location_name",
		"sortDir": "ASC",
		"persist": true,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_26",
	"name": "data_model_26",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_30', {
	"fields": [{
		"dateFormat": null,
		"defaultValue": "",
		"name": "provider_status",
		"sortDir": "ASC",
		"persist": true,
		"type": "string",
		"useNull": false
	}],
	"id": "data_model_30",
	"name": "data_model_30",
	"extend": "Ext.data.Model"
});
Ext.define('data_model_31', {
	"fields": [{
		name:'providerId'
	},{
		"name": "client_name"
	},{
		name:'description'
	}],
	"id": "data_model_31",
	"name": "data_model_31",
	"extend": "Ext.data.Model"
});
//model define - end
//store define - start
Ext.create('Ext.data.Store', {
	"id": "semar_baka_store",
	"storeId": "semar_baka_store",
	"model": "semar_baka_model",
	"proxy": {
		"type": "memory",
		"reader": "array"
	},
	"name": "semar_baka_store"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_1",
	"autoLoad": false,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 100,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvsostmasprovider\/call_method\/select\/table\/tvsostmasprovider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "providerId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_2",
	"storeId": "data_store_2",
	"name": "data_store_2"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_11",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvsostmasprovider\/call_method\/select\/table\/sos_tref_provider_type",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "providertypeId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_12",
	"storeId": "data_store_12",
	"name": "data_store_12",
	listeners:{
		load:function(){
			this.insert(0,{provider_type:'All'});
		}
	}
});
Ext.create('Ext.data.Store', {
	"fields": ["key", "val"],
	"data": [{
		"key": "Active",
		"val": "Active"
	}, {
		"key": "Inactive",
		"val": "Inactive"
	}],
	"id": "data_store_18",
	"storeId": "data_store_18",
	"name": "data_store_18"
});
Ext.create('Ext.data.Store', {
	"model": "data_model_23",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvsostmasprovider\/call_method\/select\/table\/tvbrowserefclient",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "clientId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_24",
	"storeId": "data_store_24",
	"name": "data_store_24",
	listeners:{
		load:function(){
			this.insert(0,{client_name:'All'});
		}
	}
});
Ext.create('Ext.data.Store', {
	"model": "data_model_26",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvsostmasprovider\/call_method\/select\/table\/tvbrowsereflocation",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "location_name",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_27",
	"storeId": "data_store_27",
	"name": "data_store_27",
	listeners:{
		load:function(){
			this.insert(0,{location_name:'All'});
		}
	}
});
Ext.create('Ext.data.Store', {
	"model": "data_model_30",
	"autoLoad": true,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvsostmasprovider\/call_method\/select\/table\/tvproviderstatus",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			"idProperty": "provider_status",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_31",
	"storeId": "data_store_31",
	"name": "data_store_31",
	listeners:{
		load:function(){
			this.insert(0,{provider_status:'All'});
		}
	}
});

Ext.create('Ext.data.Store', {
	"model": "data_model_31",
	"autoLoad": false,
	"autoSync": false,
	"buffered": false,
	"clearOnPageLoad": true,
	"pageSize": 25,
	"purgePageCount": 5,
	"remoteFilter": false,
	"remoteGroup": false,
	"remoteSort": false,
	"sortOnFilter": true,
	"proxy": {
		"type": "ajax",
		"url": "<?php echo base_url();?>tvsostmasprovider\/call_method\/select\/table\/tvsostmasclientprovider",
		"extraParams": {
			"id_open": "1",
			"semar": "true"
		},
		"filterParam": "filter",
		"groupParam": "group",
		"limitParam": "limit",
		"pageParam": "page",
		"sortParam": "sort",
		"startParam": "start",
		"timeout": 30000,
		"simpleSortMode": false,
		"noCache": true,
		"reader": {
			"type": "json",
			"root": "data",
			//"idProperty": "providerId",
			"implicitIncludes": true,
			"messagesProperty": "",
			"successProperty": "success",
			"totalProperty": "total"
		},
		"actionMethods": {
			"read": "POST"
		}
	},
	"id": "data_store_32",
	"storeId": "data_store_32",
	"name": "data_store_32"
});
//store define - end
var workspace_tvsostmasprovider = {
	"w2_space": {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "center",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"modulecode": "tvbrowsereflocation",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "tvsostmasprovider_module_real_general_form_5",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"semar": true,
				"width": 640,
				"height": 492,
				"modal": true,
				"bodyStyle": "padding:5px;",
				"layout": {
					"type": "fit"
				},
				"closeAction": "destroy",
				"id": "tvsostmasprovider_module_real_general_browseref_7",
				"name": "tvsostmasprovider_module_real_general_browseref_7",
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"modulecode": "tvbrowserefclient",
				"browserefHeight": 492,
				"browserefWidth": 640,
				"browserefClick": 1,
				"formTarget": "tvsostmasprovider_module_real_general_form_5",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"semar": true,
				"width": 640,
				"height": 492,
				"modal": true,
				"bodyStyle": "padding:5px;",
				"layout": {
					"type": "fit"
				},
				"closeAction": "destroy",
				"id": "tvsostmasprovider_module_real_general_browseref_6",
				"name": "tvsostmasprovider_module_real_general_browseref_6",
				"items": []
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "north",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "south",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "east",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": []
		}]
	}
}
var new_tabpanel = {
	id: 'newtab_tvsostmasprovider_module',
	title: 'Master Provider',
	iconCls: 'icon-gears',
	border: false,
	closable: true,
	layout: 'border',
	items: {
		"defaults": {
			"layout": {
				"type": "vbox",
				"align": "stretch",
				"pack": "start"
			},
			"margins": 4,
			"flex": 1,
			"items": [],
			"border": false,
			"autoScroll": true
		},
		"region": "center",
		"flex": 1,
		"border": 1,
		"layout": "border",
		"autoScroll": true,
		"items": [{
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 6,
			"title": "",
			"width": 991,
			"region": "center",
			"height": 795,
			"autoScroll": true,
			"margins": {
				"top": 4,
				"right": 4,
				"bottom": 4,
				"left": 4,
				"height": 8,
				"width": 8
			},
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "Master Provider",
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvsostmasprovider_module_real_general_tabpanel_3",
				"semar": true,
				"xtype": "tabpanel",
				"name": "tvsostmasprovider_module_real_general_tabpanel_3",
				"defaults": {
					"layout": {
						"type": "vbox",
						"align": "stretch",
						"pack": "start"
					},
					"flex": 1
				},
				"layout": {
					"type": "card"
				},
				"items": [{
					"title": "Provider List",
					"id": "tvsostmasprovider_module_real_general_tabpanel_3_tab_1",
					"name": "tvsostmasprovider_module_real_general_tabpanel_3_tab_1",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"loadMask": true,
						"noCache": false,
						"autoHeight": true,
						"columnLines": false,
						"invalidateScrollerOnRefresh": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "tvsostmasprovider_module_real_general_grid_4",
						"semar": true,
						"xtype": "grid",
						"tbar": [
							[{
								"id": "tvsostmasprovider_module_real_general_grid_4_btn_tambah",
								"name": "tvsostmasprovider_module_real_general_grid_4_btn_tambah",
								"itemId": "tvsostmasprovider_module_real_general_grid_4_btn_tambah",
								"text": "Tambah",
								"iconCls": "semar-add",
								"configs": {
									"toolbar": "top",
									"text": "Tambah",
									"iconCls": "semar-add",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvsostmasprovider_module_real_general_grid_4_btn_hapus",
								"name": "tvsostmasprovider_module_real_general_grid_4_btn_hapus",
								"itemId": "tvsostmasprovider_module_real_general_grid_4_btn_hapus",
								"text": "Hapus",
								"iconCls": "semar-delete",
								"configs": {
									"toolbar": "top",
									"text": "Hapus",
									"iconCls": "semar-delete",
									"type": "delete",
									"value": "sos_tmas_provider",
									"split": true,
									"active": true
								}
							}, "-"],
							[{
								"id": "tvsostmasprovider_module_real_general_grid_4_btn_excel",
								"name": "tvsostmasprovider_module_real_general_grid_4_btn_excel",
								"itemId": "tvsostmasprovider_module_real_general_grid_4_btn_excel",
								"text": "Excel",
								"iconCls": "semar-excel",
								"configs": {
									"toolbar": "top",
									"text": "Excel",
									"iconCls": "semar-excel",
									"type": "excel",
									"value": "tvsostmasprovider",
									"split": true,
									"active": true
								}
							}, "-"],
							[{
								"id": "tvsostmasprovider_module_real_general_grid_4_btn_resetfilter",
								"name": "tvsostmasprovider_module_real_general_grid_4_btn_resetfilter",
								"itemId": "tvsostmasprovider_module_real_general_grid_4_btn_resetfilter",
								"text": "ResetFilter",
								"iconCls": "semar-undo",
								"configs": {
									"toolbar": "top",
									"text": "ResetFilter",
									"iconCls": "semar-undo",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}]
						],
						"store": "data_store_2",
						"dockedItems": [{
							"id": "tvsostmasprovider_module_real_general_grid_4_pagingtoolbar",
							"hidden": false,
							"xtype": "pagingtoolbar",
							"store": "data_store_2",
							"dock": "bottom",
							"displayInfo": true
						}, {
							"id": "tvsostmasprovider_module_real_general_grid_4_searchfield",
							"dock": "top",
							"hidden": false,
							"xtype": "semarwidgetsearchtoolbar",
							"grid_id": "tvsostmasprovider_module_real_general_grid_4",
							"store": "data_store_2",
							"text": "Search"
						}],
						"columns": [{
							"text": "providerId",
							"dataIndex": "providerId",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "clientId",
							"dataIndex": "clientId",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Provider Name",
							"dataIndex": "provider_name",
							"width": 247,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Client",
							"dataIndex": "client_name",
							"width": 191,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Provider Code",
							"dataIndex": "provider_codeId",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Provider Type",
							"dataIndex": "provider_type",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "provider_address",
							"dataIndex": "provider_address",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "provider_telp",
							"dataIndex": "provider_telp",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "provider_email",
							"dataIndex": "provider_email",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Location",
							"dataIndex": "location_name",
							"width": 181,
							"height": null,
							"flex": null,
							"filter": {
								"type": "string"
							},
							"search": true,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "Status",
							"dataIndex": "provider_status",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "description",
							"dataIndex": "description",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "createdBy",
							"dataIndex": "createdBy",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "createdDate",
							"dataIndex": "createdDate",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "updatedBy",
							"dataIndex": "updatedBy",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "updatedDate",
							"dataIndex": "updatedDate",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}, {
							"text": "status_data",
							"dataIndex": "status_data",
							"width": 100,
							"height": null,
							"flex": 1,
							"filter": {
								"type": "string"
							},
							"hidden": true,
							"search": null,
							"xtype": "gridcolumn",
							"format": null
						}],
						"selType": "checkboxmodel",
						"selModel": {
							"mode": "MULTI",
							"selType": "checkboxmodel"
						},
						"name": "tvsostmasprovider_module_real_general_grid_4",
						"layout": {
							"type": "fit"
						},
						"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
					}]
				}, {
					"title": "Provider Detail",
					"id": "tvsostmasprovider_module_real_general_tabpanel_3_tab_2",
					"name": "tvsostmasprovider_module_real_general_tabpanel_3_tab_2",
					"items": [{
						"frame": false,
						"border": true,
						"disabled": false,
						"flex": 1,
						"title": "",
						"url": "tvsostmasprovider\/call_method\/insertupdate\/table\/sos_tmas_provider",
						"method": "POST",
						"autoScroll": true,
						"margins": {
							"top": 0,
							"right": 0,
							"bottom": 0,
							"left": 0,
							"height": 0,
							"width": 0
						},
						"id": "tvsostmasprovider_module_real_general_form_5",
						"semar": true,
						"xtype": "form",
						"tbar": [
							[{
								"id": "tvsostmasprovider_module_real_general_form_5_btn_simpan",
								"name": "tvsostmasprovider_module_real_general_form_5_btn_simpan",
								"itemId": "tvsostmasprovider_module_real_general_form_5_btn_simpan",
								"text": "Simpan",
								"iconCls": "semar-save",
								"configs": {
									"toolbar": "top",
									"text": "Simpan",
									"iconCls": "semar-save",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvsostmasprovider_module_real_general_form_5_btn_ubah",
								"name": "tvsostmasprovider_module_real_general_form_5_btn_ubah",
								"itemId": "tvsostmasprovider_module_real_general_form_5_btn_ubah",
								"text": "Ubah",
								"iconCls": "semar-edit",
								"configs": {
									"toolbar": "top",
									"text": "Ubah",
									"iconCls": "semar-edit",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}],
							[{
								"id": "tvsostmasprovider_module_real_general_form_5_btn_batal",
								"name": "tvsostmasprovider_module_real_general_form_5_btn_batal",
								"itemId": "tvsostmasprovider_module_real_general_form_5_btn_batal",
								"text": "Batal",
								"iconCls": "semar-undo",
								"configs": {
									"toolbar": "top",
									"text": "Batal",
									"iconCls": "semar-undo",
									"type": "normal",
									"value": "true",
									"active": true
								}
							}]
						],
						"name": "tvsostmasprovider_module_real_general_form_5",
						"defaults": {
							"xtype": "panel",
							"layout": "anchor",
							"anchor": "100%",
							"flex": 1,
							"margin": 5,
							"defaults": {
								"anchor": "100%",
								"flex": 1
							}
						},
						"layout": {
							"type": "anchor"
						},
						"items": [{
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_text_8",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_text_8",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Nama Client",
								"name": "client_name",
								"allowBlank": false,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_text_9",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_text_9",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Kode Provider",
								"name": "provider_codeId",
								"allowBlank": false,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_text_9",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_text_10",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_text_10",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Nama Provider",
								"name": "provider_name",
								"allowBlank": false,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_text_10",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_combo_13",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_combo_13",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Provider Tipe",
								"name": "provider_type",
								"allowBlank": false,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_combo_13",
								"semar": true,
								"xtype": "combobox",
								"store": "data_store_12",
								"displayField": "provider_type",
								"valueField": "provider_type",
								"queryMode": "local",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_textarea_14",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_textarea_14",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Alamat",
								"name": "provider_address",
								"allowBlank": true,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_textarea_14",
								"semar": true,
								"xtype": "textareafield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_text_15",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_text_15",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Telp",
								"name": "provider_telp",
								"allowBlank": true,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_text_15",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_text_16",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_text_16",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Email",
								"name": "provider_email",
								"allowBlank": true,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_text_16",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_text_17",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_text_17",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Lokasi",
								"name": "location_name",
								"allowBlank": false,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17",
								"semar": true,
								"xtype": "textfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_combo_19",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_combo_19",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Status",
								"name": "provider_status",
								"allowBlank": false,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_combo_19",
								"semar": true,
								"xtype": "combobox",
								"store": "data_store_18",
								"displayField": "val",
								"valueField": "key",
								"queryMode": "local",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_textarea_20",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_textarea_20",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"emptyText": "-",
								"fieldLabel": "Deskripsi",
								"name": "description",
								"allowBlank": true,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_textarea_20",
								"semar": true,
								"xtype": "textareafield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_hidden_21",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_hidden_21",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"name": "providerId",
								"allowBlank": true,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_hidden_21",
								"semar": true,
								"xtype": "hiddenfield",
								"items": []
							}]
						}, {
							"frame": false,
							"border": false,
							"disabled": false,
							"flex": 1,
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_form_hidden_22",
							"semar": true,
							"xtype": "panel",
							"name": "tvsostmasprovider_module_real_general_form_5_blk_form_hidden_22",
							"defaults": {
								"anchor": "100%",
								"flex": 1
							},
							"layout": {
								"type": "anchor"
							},
							"items": [{
								"frame": false,
								"border": false,
								"disabled": false,
								"flex": 1,
								"title": "",
								"margin": "10 5 3 10",
								"padding": "0 0 0 0",
								"readOnly": false,
								"labelWidth": 100,
								"labelAlign": "left",
								"name": "clientId",
								"allowBlank": true,
								"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_hidden_22",
								"semar": true,
								"xtype": "hiddenfield",
								"items": []
							}]
						},{
							"border": true,
							"disabled": false,
							"margin": "3 5 3 120",
							"padding": "0 0 0 0",
							"isformitems": true,
							"id": "tvsostmasprovider_module_real_general_form_5_blk_i_form_grid_03",
							"semar": true,
							"xtype": "gridpanel",
							minHeight:100,
							width:500,
							columnLines:true,
							store:"data_store_32",
							columns: [
								{
									text:'No.',
									xtype:'rownumberer'
								},
								{
									flex:1,
									text:'Client',
									dataIndex:'client_name'
								},
								{
									flex:1,
									text:'Keterangan',
									dataIndex:'description'
								}
							]
						}]
					}]
				}, {
					"title": "Advanced Search",
					"id": "tvsostmasprovider_module_real_general_tabpanel_3_tab_3",
					"name": "tvsostmasprovider_module_real_general_tabpanel_3_tab_3",
					layout:'fit',
					bodyPadding:10,
					"items": [
						{	
							xtype:'form',
							border:1,
							id:'form_Advance_Search_02',
							fieldDefaults: {
								msgTarget: 'side'
							},
							layout:'vbox',
							semar:true,
							bodyPadding:10,
							defaults:{
								xtype:'textfield',
								labelSeparator:'',
								labelWidth:150,
								width:450
							},
							items:[
								{
									fieldLabel:'Provider Name',
									name:'provider_name_'
								},
								{
									xtype:'combo',	
									fieldLabel:'Provider Type',
									editable:false,
									name:'provider_type',
									store: {
										fields:['provider_type'],
										autoLoad:true,
										"proxy": {
											"type": "ajax",
											"url": "<?php echo base_url();?>tvsostmasprovider\/call_method\/select\/table\/sos_tref_provider_type",
											"extraParams": {
												"id_open": "1",
												"semar": "true"
											},
											reader:{
												root:'data'
											}
										}
									},
									queryMode: 'local',
									displayField: 'provider_type',
									valueField: 'provider_type',
								},
								{
									fieldLabel:'Location',
									name:'location_name'
								},
								{
									xtype:'combo',	
									fieldLabel:'Provider Status',
									editable:false,
									name:'provider_status',
									store: ['Active','Inactive'],
									queryMode: 'local',
									displayField: 'provider_status',
									valueField: 'provider_status',
								}
							],
							buttons:[
								{
									width:50,
									text:'Search',
									handler:function(){
										var myParams = Ext.getCmp('form_Advance_Search_02').getForm().getValues();
										for(var key in myParams) {
											var val = myParams[key];
											if(!val){
												delete myParams[key];
											}else{
												Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().getProxy().extraParams[key] = myParams[key];
											}
										}
										Ext.getCmp('tvsostmasprovider_module_real_general_tabpanel_3').setActiveTab(0);
										Ext.getStore('data_store_2').load();
									}
								},
								{
									xtype:'button',
									text:'Reset',
									handler:function(){
										Ext.getCmp('form_Advance_Search_02').getForm().reset();
										Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().getProxy().extraParams = {id_open:1,semar:true};
										Ext.getStore('data_store_2').load();
									}
								}
							]
						}
					]
				}]
			}]
		}, {
			"frame": false,
			"border": true,
			"disabled": false,
			"flex": 1,
			"title": "",
			"region": "west",
			"autoScroll": true,
			"semar": true,
			"xtype": "panel",
			"layout": {
				"type": "vbox",
				"align": "stretch"
			},
			"items": [{
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvsostmasprovider_module_real_general_grid_25",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_24",
				"dockedItems": [{
					"id": "tvsostmasprovider_module_real_general_grid_25_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_24",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "tvsostmasprovider_module_real_general_grid_25_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "tvsostmasprovider_module_real_general_grid_25",
					"store": "data_store_24",
					"text": "Search"
				}],
				"columns": [{
					"text": "clientId",
					"dataIndex": "clientId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Client Name",
					"dataIndex": "client_name",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvsostmasprovider_module_real_general_grid_25",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvsostmasprovider_module_real_general_grid_28",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_27",
				"dockedItems": [{
					"id": "tvsostmasprovider_module_real_general_grid_28_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_27",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "tvsostmasprovider_module_real_general_grid_28_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "tvsostmasprovider_module_real_general_grid_28",
					"store": "data_store_27",
					"text": "Search"
				}],
				"columns": [{
					"text": "Location",
					"dataIndex": "location_name",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvsostmasprovider_module_real_general_grid_28",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvsostmasprovider_module_real_general_grid_29",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_12",
				"dockedItems": [{
					"id": "tvsostmasprovider_module_real_general_grid_29_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_12",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "tvsostmasprovider_module_real_general_grid_29_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "tvsostmasprovider_module_real_general_grid_29",
					"store": "data_store_12",
					"text": "Search"
				}],
				"columns": [{
					"text": "providertypeId",
					"dataIndex": "providertypeId",
					"width": 100,
					"height": null,
					"flex": 1,
					"filter": {
						"type": "string"
					},
					"hidden": true,
					"search": null,
					"xtype": "gridcolumn",
					"format": null
				}, {
					"text": "Type",
					"dataIndex": "provider_type",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvsostmasprovider_module_real_general_grid_29",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}, {
				"frame": false,
				"border": true,
				"disabled": false,
				"flex": 1,
				"title": "",
				"loadMask": true,
				"noCache": false,
				"autoHeight": true,
				"columnLines": false,
				"invalidateScrollerOnRefresh": true,
				"margins": {
					"top": 0,
					"right": 0,
					"bottom": 0,
					"left": 0,
					"height": 0,
					"width": 0
				},
				"id": "tvsostmasprovider_module_real_general_grid_33",
				"semar": true,
				"xtype": "grid",
				"store": "data_store_31",
				"dockedItems": [{
					"id": "tvsostmasprovider_module_real_general_grid_33_pagingtoolbar",
					"hidden": false,
					"xtype": "pagingtoolbar",
					"store": "data_store_31",
					"dock": "bottom",
					"displayInfo": true
				}, {
					"id": "tvsostmasprovider_module_real_general_grid_33_searchfield",
					"dock": "top",
					"hidden": false,
					"xtype": "semarwidgetsearchtoolbar",
					"grid_id": "tvsostmasprovider_module_real_general_grid_33",
					"store": "data_store_31",
					"text": "Search"
				}],
				"columns": [{
					"text": "Status",
					"dataIndex": "provider_status",
					"width": 100,
					"height": null,
					"flex": "1",
					"filter": {
						"type": "string"
					},
					"search": true,
					"xtype": "gridcolumn",
					"format": null
				}],
				"selType": "rowmodel",
				"selModel": {
					"mode": "SINGLE",
					"selType": "rowmodel"
				},
				"name": "tvsostmasprovider_module_real_general_grid_33",
				"layout": {
					"type": "fit"
				},
				"features": new Ext.create('Ext.ux.grid.filter.Filter', { ftype: 'filters', autoReload: true, local: false })
			}]
		}, {
			"margins": "0 0 0 0",
			"padding": "0 0 0 0",
			"paddings": "0 0 0 0",
			"komentar": "ini buat inisialisasi komponen yang dibutuhkan untuk event lainnya, diambil dari workspace ketika kompilasi.",
			"border": false,
			"hidden": true,
			"items": []
		}]
	},
	listeners: {
		afterrender: function() {
			var eventRegister = [];
			var pushRegisterEvent = function(itemX, itemO, newObjEvent) {
					eventRegister.push({
						id: itemX,
						on: itemO,
						ev: newObjEvent
					});
				};
			var callRegisterEvent = function(tempArry, index) {
					if (Ext.isObject(tempArry[index])) {
						tempArry[index]['ev']['fn'](function() {
							callRegisterEvent(tempArry, index + 1);
						});
					};
				};
			var runRegisterEvent = function(itemX, itemO) {
					var arry = [];
					var temp = eventRegister;
					for (var i in temp) {
						if (Ext.isNumeric(i)) {
							if (Ext.isObject(temp[i])) {
								if (temp[i]['id'] == itemX && temp[i]['on'] == itemO) {
									arry.push(temp[i]);
								}
							};
						};
					};
					callRegisterEvent(arry, 0);
				};
			var doGetWindowWkStacksX = {};
			var doGetWindowWorkspace = function(workspaceItems, idWindow) {
					var result = undefined;
					if (Ext.isObject(doGetWindowWkStacksX[idWindow])) {
						result = doGetWindowWkStacksX[idWindow];
					};
					if (result == undefined) {
						if (Ext.isObject(workspaceItems)) {
							for (var i in workspaceItems) {
								if ((Ext.isObject(workspaceItems[i]) || Ext.isArray(workspaceItems[i])) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								} else {
									if (i == 'id' && result == undefined) {
										if (workspaceItems[i] == idWindow && result == undefined) {
											result = workspaceItems;
										};
									};
								};
							}
						} else if (Ext.isArray(workspaceItems) && result == undefined) {
							for (var i in workspaceItems) {
								if (Ext.isNumeric(i) && Ext.isObject(workspaceItems[i]) && result == undefined) {
									result = doGetWindowWorkspace(workspaceItems[i], idWindow);
								};
							}
						};
					};
					return result;
				};
			var doTestVisualBrowseref = function(compontId, isShow, callbackIndexX) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						var checkBrowseref = doGetWindowWorkspace(workspace_tvsostmasprovider, compontId);
						if (checkBrowseref != undefined && Ext.isObject(checkBrowseref)) {
							doGetWindowWkStacksX[compontId] = checkBrowseref;
							delete checkBrowseref.xtype;
							delete checkBrowseref.id;
							checkBrowseref['id'] = dynamcID;
							checkBrowseref['width'] = checkBrowseref['browserefWidth'];
							checkBrowseref['height'] = checkBrowseref['browserefHeight'];
							checkBrowseref['modal'] = true;
							checkBrowseref['bodyStyle'] = 'padding:5px;';
							checkBrowseref['layout'] = 'fit';
							checkBrowseref['closeAction'] = 'destroy';

							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {} else {
								checkBrowseref['html'] = '<div style=\'padding:10px;\'><center><h3>Browseref Unset</h3></center></div>';
							};

							Ext.create('Ext.window.Window', checkBrowseref).show();
							Ext.getCmp(dynamcID).doLayout();
							if (checkBrowseref.modulecode != undefined && new String(checkBrowseref.modulecode).length > 0) {
								Load_BrowseRef(dynamcID, checkBrowseref.modulecode, function() {
									var tempEventClick = undefined;
									if (checkBrowseref.browserefClick == 1) {
										tempEventClick = 'itemclick';
									} else if (checkBrowseref.browserefClick >= 2) {
										tempEventClick = 'itemdblclick';
									};
									if (tempEventClick != undefined && new String(checkBrowseref.formTarget).length > 0 && checkBrowseref.formTarget != undefined) {
										Ext.getCmp(dynamcID).down('[xtype=grid]').on(tempEventClick, function() {
											var smTemp = Ext.getCmp(dynamcID).down('[xtype=grid]').getSelectionModel(),
												selTemp = smTemp.getSelection();
											if (selTemp.length > 0) {
												Ext.getCmp(checkBrowseref.formTarget).getForm().loadRecord(selTemp[0]);
												Ext.getCmp(dynamcID).hide();
											};
										});
									};
									if (Ext.isFunction(callbackIndexX)) {
										callbackIndexX();
									};
								});
							} else {
								if (Ext.isFunction(callbackIndexX)) {
									callbackIndexX();
								};
							}
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).destroy();
						};
						if (Ext.isFunction(callbackIndexX)) {
							callbackIndexX();
						};
					};
				};
			var doTestVisualWindow = function(compontId, isShow) {
					var dynamcID = compontId;
					if (isShow) {
						if (Ext.getCmp(compontId)) {
							Ext.getCmp(compontId).show();
						} else {
							var checkWindow = doGetWindowWorkspace(workspace_tvsostmasprovider, compontId);
							if (checkWindow != undefined && Ext.isObject(checkWindow)) {
								doGetWindowWkStacksX[compontId] = checkWindow;
								delete checkWindow.xtype;
								checkWindow['id'] = dynamcID;
								checkWindow['width'] = checkWindow['windowWidth'];
								checkWindow['height'] = checkWindow['windowHeight'];
								checkWindow['modal'] = true;
								checkWindow['bodyStyle'] = 'padding:5px;';
								checkWindow['layout'] = 'fit';
								checkWindow['closeAction'] = 'hide';
								Ext.create('Ext.window.Window', checkWindow).show();
								Ext.getCmp(dynamcID).doLayout();
							};
						};
					} else {
						if (Ext.getCmp(dynamcID)) {
							Ext.getCmp(dynamcID).hide();
						};
					};
				};
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4', 'itemdblclick', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4', 'itemdblclick', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvsostmasprovider_module_real_general_form_5'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4', 'itemdblclick', {
				key: 'loadRecord',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5').getForm().loadRecord((Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getSelectionModel().getSelection().length > 0 ? Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getSelectionModel().getSelection()[0] : []));
					var record = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getSelectionModel().getSelection()[0];
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5_blk_i_form_grid_03').getStore().getProxy().extraParams = {	"id_open": "1","semar": "true","providerId":record.data.providerId};
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5_blk_i_form_grid_03').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4', 'itemdblclick', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_tabpanel_3').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_4_itemdblclick = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_4')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4');
					if ('itemdblclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemdblclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemdblclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_4', 'itemdblclick');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_itemdblclick);
						} else {
							tempEl.on('itemdblclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_4', 'itemdblclick');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_itemdblclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvsostmasprovider_module_real_general_form_5'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_tambah', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_tabpanel_3').setActiveTab(JSON.parse(1));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_4_btn_tambah_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_tambah')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_tambah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_tambah', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_btn_tambah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_tambah', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_btn_tambah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_hapus', 'click', {
				key: 'dataGrid',
				fn: function(callbackIndex) {
					var doProcDataTemp = function() {
							var dataTemp = [];
							var smTemp = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									dataTemp.push(selTemp[i].get('providerId'));
								};
								dataTemp = dataTemp.join('-');
							};
							return dataTemp;
						};
					var tempData = doProcDataTemp();
					if (tempData.length > 0) {
						var configsComponent1 = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_hapus').configs;
						if (configsComponent1) {

							var doProcReqsTemp = function(dataTemp, typexx, addos) {
									Ext.getBody().mask('Please wait...');
									Ext.Ajax.request({
										url: BASE_URL + 'tvsostmasprovider/call_method/' + typexx + '/table/' + addos,
										method: 'POST',
										params: {
											where: 'providerId',
											postdata: dataTemp
										},
										success: function(response) {
											try {
												Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().load();
											} catch (e) {}
											if (Ext.isFunction(callbackIndex)) {
												callbackIndex()
											};
										},
										failure: function(response) {
											Ext.MessageBox.show({
												title: 'Warning !',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.ERROR
											});
										},
										callback: function() {
											Ext.getBody().unmask();
										}
									});
								};
							var doConfirmTemp = function(done) {
									Ext.Msg.show({
										title: 'Confirm',
										msg: 'Are you sure ?',
										buttons: Ext.Msg.YESNO,
										icon: Ext.Msg.QUESTION,
										fn: function(btn) {
											if (btn == 'yes') {
												done();
											}
										}
									});
								};

							if (configsComponent1['type'] == 'delete' || configsComponent1['type'] == 'process') {
								if (new String(configsComponent1['value']).length > 0) {
									doConfirmTemp(function() {
										doProcReqsTemp(tempData, configsComponent1['type'], configsComponent1['value']);
									});
								};
							};
						};
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_4_btn_hapus_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_hapus')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_hapus');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_hapus', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_btn_hapus_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_hapus', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_btn_hapus_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_excel', 'click', {
				key: 'excelGrid',
				fn: function(callbackIndex) {
					var configsComponent1 = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_excel').configs;
					if (configsComponent1) {
						if (configsComponent1['type'] == 'excel') {
							if (new String(configsComponent1['value']).length > 0) {
								doManualSemarDocByGrid('tvsostmasprovider', //nama module
								Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').title, //title
								'tvsostmasprovider_module_real_general_grid_4', //id grid
								'providerId', //kolom grid
								configsComponent1['value'] //table or view
								);
								if (Ext.isFunction(callbackIndex)) {
									callbackIndex()
								};
							};
						};
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_4_btn_excel_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_excel')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_excel');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_excel', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_btn_excel_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_excel', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_btn_excel_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_resetfilter', 'click', {
				key: 'removeParam',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams = {
						id_open: '1',
						semar: 'true'
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_resetfilter', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_4_btn_resetfilter_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_resetfilter')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_4_btn_resetfilter');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_resetfilter', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_btn_resetfilter_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_4_btn_resetfilter', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_4_btn_resetfilter_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click', {
				key: 'submit',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5').on({
						beforeaction: function() {
							Ext.getCmp('tvsostmasprovider_module_real_general_form_5').body.mask('Loading...');
						}
					});
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5').getForm().submit({
						url: Ext.getCmp('tvsostmasprovider_module_real_general_form_5').url,
						method: Ext.getCmp('tvsostmasprovider_module_real_general_form_5').method,
						clientValidation: true,
						success: function(form, action) {
							Ext.getCmp('tvsostmasprovider_module_real_general_form_5').body.unmask();
							Ext.Msg.alert('Success', action.result.msg);
							Ext.getCmp('tvsostmasprovider_module_real_general_form_5').getForm().setValues(action.result.data);

							//more events - start
							if (Ext.isFunction(callbackIndex)) {
								callbackIndex()
							};
							//more events - end
						},
						failure: function(form, action) {
							Ext.getCmp('tvsostmasprovider_module_real_general_form_5').body.unmask();
							switch (action.failureType) {
							case Ext.form.action.Action.CLIENT_INVALID:
								Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
								break;
							case Ext.form.action.Action.CONNECT_FAILURE:
								Ext.Msg.alert('Failure', 'Ajax communication failed');
								break;
							case Ext.form.action.Action.SERVER_INVALID:
								Ext.Msg.alert('Failure', action.result.msg);
							}
						}
					});
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_tabpanel_3').setActiveTab(JSON.parse(0));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_25').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_28').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_29').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_33').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_form_5_btn_simpan_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_simpan')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_simpan');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_btn_simpan_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_simpan', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_btn_simpan_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_ubah', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_simpan').setDisabled(JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_ubah', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvsostmasprovider_module_real_general_form_5'), JSON.parse(false));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_form_5_btn_ubah_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_ubah')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_ubah');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_ubah', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_btn_ubah_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_ubah', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_btn_ubah_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_batal', 'click', {
				key: 'activeTab',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_tabpanel_3').setActiveTab(JSON.parse(0));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_batal', 'click', {
				key: 'disable',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_simpan').setDisabled(JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_batal', 'click', {
				key: 'disableForm',
				fn: function(callbackIndex) {
					semarDeactiveAllX(Ext.getCmp('tvsostmasprovider_module_real_general_form_5'), JSON.parse(true));
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_batal', 'click', {
				key: 'reset',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_form_5').getForm().reset();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_form_5_btn_batal_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_batal')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_form_5_btn_batal');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_batal', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_btn_batal_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_form_5_btn_batal', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_btn_batal_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8', 'click', {
				key: 'browserefShow',
				fn: function(callbackIndex) {
					doTestVisualBrowseref('tvsostmasprovider_module_real_general_browseref_6', JSON.parse(true), callbackIndex);
				}
			});
			var intervale_tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_blk_i_form_text_8_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17', 'click', {
				key: 'browserefShow',
				fn: function(callbackIndex) {
					doTestVisualBrowseref('tvsostmasprovider_module_real_general_browseref_7', JSON.parse(true), callbackIndex);
				}
			});
			var intervale_tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17_click = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17');
					if ('click' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('click' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('click' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17', 'click');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17_click);
						} else {
							tempEl.on('click', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17', 'click');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_form_5_blk_i_form_text_17_click);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_25', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('tvsostmasprovider_module_real_general_grid_25');
					if (new String('client_name=client_name').match('=')) {
						var hasAfterCheckComp = new String('client_name=client_name').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						}; 
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
						
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_25', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_25_itemclick = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_25')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_25');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_25', 'itemclick');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_25_itemclick);
						} else {
							tempEl.on('itemclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_25', 'itemclick');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_25_itemclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_28', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('tvsostmasprovider_module_real_general_grid_28');
					if (new String('location_name=location_name').match('=')) {
						var hasAfterCheckComp = new String('location_name=location_name').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_28', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_28_itemclick = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_28')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_28');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_28', 'itemclick');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_28_itemclick);
						} else {
							tempEl.on('itemclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_28', 'itemclick');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_28_itemclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_29', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('tvsostmasprovider_module_real_general_grid_29');
					if (new String('provider_type=provider_type').match('=')) {
						var hasAfterCheckComp = new String('provider_type=provider_type').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_29', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_29_itemclick = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_29')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_29');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_29', 'itemclick');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_29_itemclick);
						} else {
							tempEl.on('itemclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_29', 'itemclick');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_29_itemclick);
						};
					};
				};
			}, 100);
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_33', 'itemclick', {
				key: 'changeParam',
				fn: function(callbackIndex) {
					var checkingComponent = Ext.getCmp('tvsostmasprovider_module_real_general_grid_33');
					if (new String('provider_status=provider_status').match('=')) {
						var hasAfterCheckComp = new String('provider_status=provider_status').split('=');
						var relAfterCheckComp = hasAfterCheckComp[1];
						if (checkingComponent.xtype == 'grid') {
							var smTemp = checkingComponent.getSelectionModel(),
								selTemp = smTemp.getSelection();
							if (selTemp.length > 0) {
								for (i = 0; i < selTemp.length; i++) {
									relAfterCheckComp = selTemp[i].get(hasAfterCheckComp[1]);
								};
							};
						} else {
							if (typeof checkingComponent.getValue == 'function') {
								relAfterCheckComp = checkingComponent.getValue();
							};
						};
						if(relAfterCheckComp === 'All'){
							Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams = {id_open:1,semar:true};
						}else{
							Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().proxy.extraParams[hasAfterCheckComp[0]] = relAfterCheckComp;
						}
					};
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			pushRegisterEvent('tvsostmasprovider_module_real_general_grid_33', 'itemclick', {
				key: 'load',
				fn: function(callbackIndex) {
					Ext.getCmp('tvsostmasprovider_module_real_general_grid_4').getStore().load();
					if (Ext.isFunction(callbackIndex)) {
						callbackIndex()
					};
				}
			});
			var intervale_tvsostmasprovider_module_real_general_grid_33_itemclick = setInterval(function() {
				if (Ext.getCmp('tvsostmasprovider_module_real_general_grid_33')) {
					var tempEl = Ext.getCmp('tvsostmasprovider_module_real_general_grid_33');
					if ('itemclick' == 'click' && tempEl.ownerCt.isformitems == true && tempEl.xtype != 'button') {
						tempEl = tempEl.inputEl;
					} else if ('itemclick' == 'click') {
						tempEl = tempEl.el;
					};
					if (tempEl) {
						if ('itemclick' == 'keyenter') {
							tempEl.on('specialkey', function(e, t) {
								if (t.getKey() == t.ENTER) {
									runRegisterEvent('tvsostmasprovider_module_real_general_grid_33', 'itemclick');
								};
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_33_itemclick);
						} else {
							tempEl.on('itemclick', function(e, t) {
								if (e.xtype == 'exmlwidgetmessageboxpanel') {
									ii = t;
								};
								runRegisterEvent('tvsostmasprovider_module_real_general_grid_33', 'itemclick');
							});
							clearInterval(intervale_tvsostmasprovider_module_real_general_grid_33_itemclick);
						};
					};
				};
			}, 100);
		}
	}
}

<?php }else{ echo "var new_tabpanel = 'GAGAL';"; } ?>