<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller digenerate oleh eXML Builder.
 * Controller untuk module tvsostmasprovider.
 *
 */
class tvsostmasprovider extends CI_Controller
{

	/**
	 * __construct Controller untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_tvsostmasprovider'));
	}


	/**
	 * index Controller untuk module $modulename.
	 */
	public function index()
	{
		if($this->input->post('id_open')){
			$data['jsscript'] = TRUE;
			$this->load->view('index',$data);
		}else{
			$this->load->view('index');
		};
	}


	/**
	 * call_method Controller untuk module $modulename.
	 */
	public function call_method($querytype, $typex, $valuex)
	{
		echo json_encode( $this->model_tvsostmasprovider->call_method( $typex, $querytype, $valuex ) );
	}

}
