<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller digenerate oleh eXML Builder.
 * Controller untuk module sostrefservicetype.
 *
 */
class sostrefservicetype extends CI_Controller
{

	/**
	 * __construct Controller untuk module $modulename.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('model_sostrefservicetype'));
	}


	/**
	 * widget_referensi Controller untuk module $modulename.
	 */
	public function widget_referensi()
	{
		if($this->input->post('id_open')){
			$data['jsscript'] = TRUE;
			$this->load->view('referensi',$data);
		}else{
			$this->load->view('referensi');
		};
	}


	/**
	 * call_method Controller untuk module $modulename.
	 */
	public function call_method($querytype, $typex, $valuex)
	{
		echo json_encode( $this->model_sostrefservicetype->call_method( $typex, $querytype, $valuex ) );
	}

}
