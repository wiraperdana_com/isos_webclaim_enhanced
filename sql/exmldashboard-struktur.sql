CREATE DATABASE  IF NOT EXISTS `exmldashboard` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `exmldashboard`;
-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: exmldashboard
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `filter_data`
--

DROP TABLE IF EXISTS `filter_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter_data` (
  `filter_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_data_code` varchar(255) DEFAULT NULL,
  `filter_data_status` int(11) DEFAULT '0',
  `filter_data_column` text,
  `filter_data_value` text,
  PRIMARY KEY (`filter_data_id`),
  UNIQUE KEY `filter_data_code_UNIQUE` (`filter_data_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filter_data_detail`
--

DROP TABLE IF EXISTS `filter_data_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter_data_detail` (
  `filter_data_detail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filter_data_detail_code` text,
  `filter_data_detail_group_id` bigint(20) DEFAULT NULL,
  `filter_data_detail_column` text,
  `filter_data_detail_value` text,
  `filter_data_detail_spesial` int(11) DEFAULT '0',
  `filter_data_detail_status` int(11) DEFAULT '0',
  PRIMARY KEY (`filter_data_detail_id`),
  UNIQUE KEY `filter_data_detail_group_id_UNIQUE` (`filter_data_detail_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `semarconfig`
--

DROP TABLE IF EXISTS `semarconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semarconfig` (
  `semarconfig_id` int(11) NOT NULL AUTO_INCREMENT,
  `semarconfig_key` varchar(500) NOT NULL,
  `semarconfig_val` longtext NOT NULL,
  `semarconfig_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`semarconfig_id`),
  UNIQUE KEY `semarconfig_key` (`semarconfig_key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tdash`
--

DROP TABLE IF EXISTS `tdash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tdash` (
  `tdash_id` int(11) NOT NULL AUTO_INCREMENT,
  `tdash_group` int(11) DEFAULT '0',
  `tdash_module` varchar(255) NOT NULL,
  `tdash_active` int(11) DEFAULT '1',
  PRIMARY KEY (`tdash_id`),
  UNIQUE KEY `tdash_group_tdash_module` (`tdash_group`,`tdash_module`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tfm`
--

DROP TABLE IF EXISTS `tfm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tfm` (
  `tfmid` int(11) NOT NULL AUTO_INCREMENT,
  `tfm_refprefix` text NOT NULL,
  `tfm_refid` text NOT NULL,
  `tfm_date` text NOT NULL,
  `tfm_type` text NOT NULL,
  PRIMARY KEY (`tfmid`),
  UNIQUE KEY `PK_tfm` (`tfmid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tlog`
--

DROP TABLE IF EXISTS `tlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tlog` (
  `ID_Log` int(11) NOT NULL AUTO_INCREMENT,
  `logIP` varchar(15) DEFAULT NULL,
  `logDateTime` date DEFAULT NULL,
  `logUser` varchar(30) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_Log`),
  UNIQUE KEY `PK_tlog_ID_Log` (`ID_Log`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmenu`
--

DROP TABLE IF EXISTS `tmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmenu` (
  `ID_Menu` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) DEFAULT NULL,
  `parent_id` smallint(5) DEFAULT '1',
  `pages` varchar(50) DEFAULT '-',
  `url_query` varchar(120) DEFAULT '-',
  `iconCls` varchar(30) DEFAULT '-',
  `expanded` varchar(5) NOT NULL,
  `leaf` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_Menu`),
  UNIQUE KEY `PK_tmenu_ID_Menu` (`ID_Menu`),
  KEY `menu` (`menu`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tnf`
--

DROP TABLE IF EXISTS `tnf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnf` (
  `tnf_id` int(11) NOT NULL AUTO_INCREMENT,
  `tnf_title` varchar(50) NOT NULL,
  `tnf_description` text NOT NULL,
  `tnf_send_date` varchar(50) NOT NULL,
  `tnf_send_NIP` varchar(50) NOT NULL,
  `tnf_ref_id` varchar(50) NOT NULL,
  `tnf_ref_table` varchar(50) NOT NULL,
  `tnf_type` int(10) DEFAULT '0',
  PRIMARY KEY (`tnf_id`),
  UNIQUE KEY `PK_tnotifikasi` (`tnf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tnf_approval`
--

DROP TABLE IF EXISTS `tnf_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnf_approval` (
  `tnf_approval_id` int(11) NOT NULL AUTO_INCREMENT,
  `tnf_approval_value` int(10) DEFAULT '0',
  `tnf_approval_value_approve` int(10) NOT NULL,
  `tnf_approval_value_reject` int(10) NOT NULL,
  `tnf_approval_date` varchar(50) NOT NULL,
  `tnf_approval_tnf_id` bigint(19) NOT NULL,
  `tnf_approval_by_NIP` varchar(50) NOT NULL,
  `tnf_approval_note_1` text,
  `tnf_approval_note_2` text,
  PRIMARY KEY (`tnf_approval_id`),
  UNIQUE KEY `PK_tnotifikasi_approval` (`tnf_approval_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tnf_receiver`
--

DROP TABLE IF EXISTS `tnf_receiver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnf_receiver` (
  `tnf_receiver_id` int(11) NOT NULL AUTO_INCREMENT,
  `tnf_receiver_value` varchar(50) NOT NULL,
  `tnf_receiver_date` varchar(50) NOT NULL,
  `tnf_receiver_is_read` int(10) DEFAULT '0',
  `tnf_receiver_tnf_id` bigint(19) NOT NULL,
  `tnf_receiver_as_approval` int(10) DEFAULT '0',
  `tnf_receiver_type` int(10) DEFAULT '0',
  PRIMARY KEY (`tnf_receiver_id`),
  UNIQUE KEY `PK_tnotifikasi_receiver` (`tnf_receiver_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tnfst`
--

DROP TABLE IF EXISTS `tnfst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnfst` (
  `tnfst_id` int(11) NOT NULL AUTO_INCREMENT,
  `tnfst_title` varchar(500) NOT NULL,
  `tnfst_modulecode` varchar(500) NOT NULL,
  PRIMARY KEY (`tnfst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tnfstd`
--

DROP TABLE IF EXISTS `tnfstd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnfstd` (
  `tnfstd_id` int(11) NOT NULL AUTO_INCREMENT,
  `tnfstd_nml` varchar(1000) NOT NULL,
  `tnfstd_NIP` varchar(1000) NOT NULL,
  `tnfstd_unor` varchar(1000) DEFAULT NULL,
  `tnfstd_unker` varchar(1000) DEFAULT NULL,
  `tnfstd_asapprv` varchar(1000) NOT NULL,
  `tnfstd_tnfst_id` int(11) NOT NULL,
  `tnfstd_state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tnfstd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tnfstdlevel`
--

DROP TABLE IF EXISTS `tnfstdlevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnfstdlevel` (
  `tnfstdlevel_id` int(11) NOT NULL AUTO_INCREMENT,
  `tnfstdlevel_level` int(11) NOT NULL DEFAULT '1',
  `tnfstdlevel_asapprv` varchar(1000) NOT NULL,
  `tnfstdlevel_tnfst_id` int(11) NOT NULL,
  `tnfstdlevel_state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tnfstdlevel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tnfstdstate`
--

DROP TABLE IF EXISTS `tnfstdstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnfstdstate` (
  `tnfstdstate_id` int(11) NOT NULL AUTO_INCREMENT,
  `tnfstdstate_type` varchar(50) NOT NULL,
  `tnfstdstate_ref_id_asal` int(11) NOT NULL,
  `tnfstdstate_ref_id_tujuan_modul` int(11) NOT NULL,
  `tnfstdstate_ref_id_tujuan_status` int(11) NOT NULL,
  PRIMARY KEY (`tnfstdstate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tpegawai`
--

DROP TABLE IF EXISTS `tpegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tpegawai` (
  `ID_Pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `NIP` varchar(21) NOT NULL,
  `NIP_Lama` varchar(12) DEFAULT NULL,
  `nama_lengkap` varchar(120) DEFAULT NULL,
  `gelar_d` varchar(15) DEFAULT NULL,
  `gelar_b` varchar(15) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT '0000-00-00',
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `suku` varchar(30) DEFAULT '-',
  `agama` enum('Islam','Katolik','Protestan','Hindu','Budha') DEFAULT 'Islam',
  `warga_negara` enum('WNI','WNA') DEFAULT 'WNI',
  `status_kawin` enum('Belum Kawin','Kawin','Janda','Duda') DEFAULT 'Belum Kawin',
  `gol_darah` enum('-','A','B','AB','O','A+','A-','B+','B-','AB+','AB-','O+','O-') DEFAULT '-',
  `tinggi` tinyint(3) unsigned DEFAULT NULL,
  `berat` tinyint(3) unsigned DEFAULT NULL,
  `rambut` varchar(12) DEFAULT NULL,
  `bentuk_muka` varchar(12) DEFAULT NULL,
  `warna_kulit` varchar(12) DEFAULT NULL,
  `ciri2_khas` varchar(30) DEFAULT NULL,
  `cacat_tubuh` varchar(30) DEFAULT NULL,
  `hobby` varchar(30) DEFAULT NULL,
  `no_KTP` varchar(16) DEFAULT '-',
  `no_NPWP` varchar(20) DEFAULT '-',
  `no_KARPEG` varchar(30) DEFAULT '-',
  `no_ASKES` varchar(30) DEFAULT '-',
  `no_KARIS` varchar(30) DEFAULT '-',
  `no_TASPEN` varchar(30) DEFAULT '-',
  `no_SPMT` varchar(30) DEFAULT NULL,
  `tgl_SPMT` date DEFAULT NULL,
  `no_KPKN` varchar(30) DEFAULT NULL,
  `no_KTUA` varchar(30) DEFAULT NULL,
  `kode_jpeg` tinyint(4) DEFAULT '1',
  `kode_dupeg` int(4) DEFAULT '1',
  `kode_unor` int(11) DEFAULT NULL,
  `urut_unor` int(11) DEFAULT NULL,
  `kode_jab` int(11) DEFAULT NULL,
  `kode_grade` int(11) DEFAULT NULL,
  `kode_golru` tinyint(4) DEFAULT NULL,
  `TMT_kpkt` date DEFAULT '0000-00-00',
  `kode_eselon` tinyint(4) DEFAULT NULL,
  `asal_instansi` varchar(150) DEFAULT NULL,
  `lokasi_kerja` varchar(150) DEFAULT NULL,
  `IDP_Pddk` int(11) DEFAULT NULL,
  `IDP_Kpkt` int(11) DEFAULT NULL,
  `IDP_Jab` int(11) DEFAULT NULL,
  `IDP_Diklat` int(11) DEFAULT NULL,
  `IDP_Dik_PIM` int(11) DEFAULT NULL,
  `IDP_Dik_PraJab` int(11) DEFAULT NULL,
  `IDP_Dik_Teknis` int(11) DEFAULT NULL,
  `IDP_Dik_Fung` int(11) DEFAULT NULL,
  `IDP_DP3` int(11) DEFAULT NULL,
  `IDP_SKP` int(11) DEFAULT NULL,
  `IDP_KGB` int(11) DEFAULT NULL,
  `IDP_AK` int(11) DEFAULT NULL,
  `IDP_Reward` int(11) DEFAULT NULL,
  `IDP_HukDis` int(11) DEFAULT NULL,
  `IDP_Seminar` int(11) DEFAULT NULL,
  `IDP_KT` int(11) DEFAULT NULL,
  `IDP_Org` int(11) DEFAULT NULL,
  `IDP_PK` int(11) DEFAULT NULL,
  `nilai_toefl` float DEFAULT NULL,
  `tgl_toefl` datetime DEFAULT NULL,
  `createdBy` varchar(15) DEFAULT NULL,
  `createdDate` datetime DEFAULT '0000-00-00 00:00:00',
  `updatedBy` varchar(15) DEFAULT NULL,
  `updatedDate` datetime DEFAULT '0000-00-00 00:00:00',
  `status_data` tinyint(1) DEFAULT '1',
  `plafon_kesehatan` double DEFAULT NULL,
  `ID_pinger` int(11) DEFAULT NULL,
  `nomor_kpj` varchar(50) DEFAULT NULL,
  `nominal_potongan` double DEFAULT NULL,
  PRIMARY KEY (`NIP`),
  UNIQUE KEY `ID_Pegawai` (`ID_Pegawai`),
  KEY `NIP_Lama` (`NIP_Lama`),
  KEY `nama_lengkap` (`nama_lengkap`),
  KEY `kode_dupeg` (`kode_dupeg`),
  KEY `kode_golru` (`kode_golru`),
  KEY `TMT_kpkt` (`TMT_kpkt`),
  KEY `kode_eselon` (`kode_eselon`),
  KEY `kode_unor` (`kode_unor`),
  KEY `IDP_Pddk` (`IDP_Pddk`),
  KEY `IDP_Kpkt` (`IDP_Kpkt`),
  KEY `IDP_Jab` (`IDP_Jab`),
  KEY `IDP_Diklat` (`IDP_Diklat`),
  KEY `IDP_Dik_PIM` (`IDP_Dik_PIM`),
  KEY `IDP_Dik_PraJab` (`IDP_Dik_PraJab`),
  KEY `IDP_DP3` (`IDP_DP3`),
  KEY `IDP_KGB` (`IDP_KGB`),
  KEY `IDP_AK` (`IDP_AK`),
  KEY `IDP_Reward` (`IDP_Reward`),
  KEY `IDP_HukDis` (`IDP_HukDis`),
  KEY `IDP_Seminar` (`IDP_Seminar`),
  KEY `IDP_KT` (`IDP_KT`),
  KEY `IDP_Org` (`IDP_Org`),
  KEY `IDP_PK` (`IDP_PK`),
  KEY `idx_nip_tpegawai` (`NIP`(10))
) ENGINE=MyISAM AUTO_INCREMENT=165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tpest`
--

DROP TABLE IF EXISTS `tpest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tpest` (
  `tpest_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpest_title` varchar(500) NOT NULL,
  `tpest_modulecode` varchar(500) NOT NULL,
  PRIMARY KEY (`tpest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tpestd`
--

DROP TABLE IF EXISTS `tpestd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tpestd` (
  `tpestd_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpestd_text` varchar(200) NOT NULL,
  `tpestd_tpest_id` int(11) NOT NULL,
  `tpestd_state` int(11) NOT NULL,
  `tpestd_true` int(11) NOT NULL,
  `tpestd_false` int(11) NOT NULL,
  `tpestd_next` int(11) NOT NULL,
  `tpestd_proses` int(11) NOT NULL,
  `tpestd_status` int(11) NOT NULL,
  PRIMARY KEY (`tpestd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tuser`
--

DROP TABLE IF EXISTS `tuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuser` (
  `ID_User` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(11) NOT NULL DEFAULT '',
  `pass` varchar(32) NOT NULL DEFAULT '',
  `fullname` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `NIP` varchar(21) NOT NULL DEFAULT '0',
  `kode_unker` int(10) NOT NULL,
  `registerDate` varchar(19) NOT NULL DEFAULT 'etdate(',
  `lastvisitDate` varchar(19) NOT NULL DEFAULT 'etdate(',
  `type` varchar(16) NOT NULL,
  `status` varchar(8) NOT NULL,
  `group` bigint(19) NOT NULL,
  PRIMARY KEY (`ID_User`),
  UNIQUE KEY `PK_tuser_ID_User` (`ID_User`),
  UNIQUE KEY `tuser$user` (`user`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tuser_menu`
--

DROP TABLE IF EXISTS `tuser_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuser_menu` (
  `ID_UM` int(11) NOT NULL AUTO_INCREMENT,
  `ID_User` int(10) DEFAULT '0',
  `ID_Menu` int(10) DEFAULT '0',
  `u_access` smallint(5) DEFAULT '1',
  `u_insert` smallint(5) DEFAULT '1',
  `u_update` smallint(5) DEFAULT '1',
  `u_delete` smallint(5) DEFAULT '1',
  `u_proses` smallint(5) DEFAULT '1',
  `u_print` smallint(5) DEFAULT '1',
  `u_print_sk` smallint(5) DEFAULT '1',
  PRIMARY KEY (`ID_UM`),
  UNIQUE KEY `PK_tuser_menu_ID_UM` (`ID_UM`),
  KEY `ID_Menu` (`ID_Menu`),
  KEY `ID_User` (`ID_User`)
) ENGINE=InnoDB AUTO_INCREMENT=495 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tuser_semar_menu`
--

DROP TABLE IF EXISTS `tuser_semar_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuser_semar_menu` (
  `idmenu` int(11) NOT NULL AUTO_INCREMENT,
  `parent_idmenu` bigint(19) NOT NULL DEFAULT '0',
  `general_text` varchar(50) NOT NULL,
  `general_iconclass` varchar(50) NOT NULL,
  `general_idbutton` varchar(50) NOT NULL,
  `general_idnewtab_popup` varchar(50) NOT NULL,
  `general_url` varchar(50) NOT NULL,
  `general_type` varchar(50) NOT NULL,
  `general_bottombreak` int(10) NOT NULL DEFAULT '0',
  `general_status` varchar(50) NOT NULL,
  `general_variableprefix` varchar(50) NOT NULL,
  `general_ismenu` int(10) NOT NULL DEFAULT '0',
  `process_view` int(10) NOT NULL DEFAULT '0',
  `process_tambah` int(10) NOT NULL DEFAULT '0',
  `process_ubah` int(10) NOT NULL DEFAULT '0',
  `process_hapus` int(10) NOT NULL DEFAULT '0',
  `process_proses` int(10) NOT NULL DEFAULT '0',
  `process_cetak` int(10) NOT NULL DEFAULT '0',
  `process_cetaksk` int(10) NOT NULL DEFAULT '0',
  `general_identify` varchar(200) DEFAULT NULL,
  `sortby` int(2) DEFAULT NULL,
  `ordermenu` int(11) DEFAULT NULL,
  `modulename` varchar(500) DEFAULT NULL,
  `widget_notifikasi` varchar(500) DEFAULT NULL,
  `widget_ess` varchar(500) DEFAULT NULL,
  `widget_profilPNS` varchar(500) DEFAULT NULL,
  `widget_referensi` varchar(500) DEFAULT NULL,
  `widget_dashboard` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idmenu`),
  UNIQUE KEY `PK_tuser_semar_menu_idmenu` (`idmenu`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tuser_semar_menu_group_collections`
--

DROP TABLE IF EXISTS `tuser_semar_menu_group_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuser_semar_menu_group_collections` (
  `id_groupcollections` int(11) NOT NULL AUTO_INCREMENT,
  `general_group_name` varchar(50) NOT NULL,
  `general_group_active` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_groupcollections`),
  UNIQUE KEY `PK_tuser_semar_menu_group_collections_id_groupcollections` (`id_groupcollections`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tuser_semar_menu_group_menu`
--

DROP TABLE IF EXISTS `tuser_semar_menu_group_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuser_semar_menu_group_menu` (
  `ID_Group_Menu` int(11) NOT NULL AUTO_INCREMENT,
  `u_access` smallint(5) DEFAULT '1',
  `u_insert` smallint(5) DEFAULT '1',
  `u_update` smallint(5) DEFAULT '1',
  `u_delete` smallint(5) DEFAULT '1',
  `u_proses` smallint(5) DEFAULT '1',
  `u_print` smallint(5) DEFAULT '1',
  `u_print_sk` smallint(5) DEFAULT '1',
  `ID_Group_Menu_ID_Menu` bigint(19) NOT NULL,
  `ID_Group_Menu_ID_Group` bigint(19) NOT NULL,
  PRIMARY KEY (`ID_Group_Menu`),
  UNIQUE KEY `PK_tuser_semar_menu_group_menu_ID_Group_Menu` (`ID_Group_Menu`)
) ENGINE=InnoDB AUTO_INCREMENT=17652 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tuser_semar_menu_group_user`
--

DROP TABLE IF EXISTS `tuser_semar_menu_group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuser_semar_menu_group_user` (
  `ID_Group_User` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Group_User_ID_User` bigint(19) NOT NULL,
  `ID_Group_User_ID_Group` bigint(19) NOT NULL,
  PRIMARY KEY (`ID_Group_User`),
  UNIQUE KEY `PK_tuser_semar_menu_group_user_ID_Group_User` (`ID_Group_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `tview_menu`
--

DROP TABLE IF EXISTS `tview_menu`;
/*!50001 DROP VIEW IF EXISTS `tview_menu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `tview_menu` AS SELECT 
 1 AS `ID_User`,
 1 AS `user`,
 1 AS `ID_Menu`,
 1 AS `menu`,
 1 AS `parent_id`,
 1 AS `pages`,
 1 AS `url_query`,
 1 AS `u_access`,
 1 AS `u_insert`,
 1 AS `u_update`,
 1 AS `u_delete`,
 1 AS `u_proses`,
 1 AS `u_print`,
 1 AS `u_print_sk`,
 1 AS `expanded`,
 1 AS `leaf`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `tview_user`
--

DROP TABLE IF EXISTS `tview_user`;
/*!50001 DROP VIEW IF EXISTS `tview_user`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `tview_user` AS SELECT 
 1 AS `ID_User`,
 1 AS `group`,
 1 AS `user`,
 1 AS `pass`,
 1 AS `fullname`,
 1 AS `email`,
 1 AS `NIP`,
 1 AS `kode_unker`,
 1 AS `registerDate`,
 1 AS `lastvisitDate`,
 1 AS `type`,
 1 AS `status`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'exmldashboard'
--

--
-- Dumping routines for database 'exmldashboard'
--
/*!50003 DROP PROCEDURE IF EXISTS `_test_proc1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `_test_proc1`(IN `_logIP` TEXT, IN `_logUser` TEXT, IN `_logDateTime` TEXT, IN `_Description` TEXT)
BEGIN



	insert into tlog ( logIP, logUser, logDateTime, Description ) values ( _logIP, _logUser, _logDateTime, _Description );



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `_test_proc2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `_test_proc2`()
BEGIN



	insert into tlog ( logIP, logUser, logDateTime, Description ) values ( 'via _test_proc2', 'via _test_proc2', now(), 'via _test_proc2' );



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `_test_proc_chart` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `_test_proc_chart`( IN limitlabelrow int, IN limitdataperrow int )
    MODIFIES SQL DATA
    DETERMINISTIC
BEGIN

	DECLARE i INT;

	DECLARE i2 INT;

	DECLARE temp_val longtext;

	SET i=0;

	loop1: LOOP

		SET i=i+1;

		IF i > limitlabelrow THEN 

			LEAVE loop1;

		END IF;

		

		SET i2=0;

		SET temp_val="";

		loop2: LOOP

			SET i2=i2+1;

			IF i2 > limitdataperrow THEN 

				LEAVE loop2;

			END IF;

			

			if i2 <= 1 then

				SET temp_val= concat( FLOOR((RAND() * (limitdataperrow-i2+1))+1) );

			else

				set temp_val = concat( temp_val, ',', FLOOR((RAND() * (limitdataperrow-i2+1))+1) );

			end if;

			

		END LOOP loop2;

		

		CREATE TEMPORARY TABLE IF NOT EXISTS _temp_table__test_proc_chart (

			label varchar(100),

			fillColor varchar(100),

			strokeColor varchar(100),

			pointColor varchar(100),

			pointStrokeColor varchar(100),

			pointHighlightFill varchar(100),

			pointHighlightStroke varchar(100),

			value longtext

		);

		INSERT INTO _temp_table__test_proc_chart (

			label,

			fillColor,

			strokeColor,

			pointColor,

			pointStrokeColor,

			pointHighlightFill,

			pointHighlightStroke,

			value

		) values (

			CONCAT("nama label ",i),

			concat('#',SUBSTRING((lpad(hex(round(rand() * 10000000)),6,0)),-6)),

			concat('#',SUBSTRING((lpad(hex(round(rand() * 10000000)),6,0)),-6)),

			concat('#',SUBSTRING((lpad(hex(round(rand() * 10000000)),6,0)),-6)),

			concat('#',SUBSTRING((lpad(hex(round(rand() * 10000000)),6,0)),-6)),

			concat('#',SUBSTRING((lpad(hex(round(rand() * 10000000)),6,0)),-6)),

			concat('#',SUBSTRING((lpad(hex(round(rand() * 10000000)),6,0)),-6)),

			temp_val

		);



	END LOOP loop1;

	SELECT * FROM _temp_table__test_proc_chart;

		drop temporary table if exists _temp_table__test_proc_chart;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `tview_menu`
--

/*!50001 DROP VIEW IF EXISTS `tview_menu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `tview_menu` AS select `x`.`ID_User` AS `ID_User`,`x`.`user` AS `user`,`b`.`ID_Menu` AS `ID_Menu`,`b`.`menu` AS `menu`,`b`.`parent_id` AS `parent_id`,`b`.`pages` AS `pages`,`b`.`url_query` AS `url_query`,`a`.`u_access` AS `u_access`,`a`.`u_insert` AS `u_insert`,`a`.`u_update` AS `u_update`,`a`.`u_delete` AS `u_delete`,`a`.`u_proses` AS `u_proses`,`a`.`u_print` AS `u_print`,`a`.`u_print_sk` AS `u_print_sk`,`b`.`expanded` AS `expanded`,`b`.`leaf` AS `leaf` from ((`tuser` `x` join `tuser_menu` `a` on((`a`.`ID_User` = `x`.`ID_User`))) join `tmenu` `b` on((`b`.`ID_Menu` = `a`.`ID_Menu`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tview_user`
--

/*!50001 DROP VIEW IF EXISTS `tview_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `tview_user` AS select `tuser`.`ID_User` AS `ID_User`,`tuser`.`group` AS `group`,`tuser`.`user` AS `user`,`tuser`.`pass` AS `pass`,`tuser`.`fullname` AS `fullname`,`tuser`.`email` AS `email`,`tuser`.`NIP` AS `NIP`,`tuser`.`kode_unker` AS `kode_unker`,`tuser`.`registerDate` AS `registerDate`,`tuser`.`lastvisitDate` AS `lastvisitDate`,`tuser`.`type` AS `type`,`tuser`.`status` AS `status` from `tuser` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-18 11:40:21
